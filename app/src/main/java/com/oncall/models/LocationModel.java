package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 24-Nov-16.
 */

public class LocationModel implements Parcelable {

    public String id, name, lat, lng, location_type;

    public LocationModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        lat = in.readString();
        lng = in.readString();
        location_type = in.readString();
    }

    public static final Creator<LocationModel> CREATOR = new Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };

    @Override
    public String toString() {
        return "LocationModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", location_type='" + location_type + '\'' +
                '}';
    }

    public LocationModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(location_type);
    }
}
