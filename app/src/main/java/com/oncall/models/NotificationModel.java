package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 11-Apr-17.
 */

public class NotificationModel implements Parcelable {

    public String id, message, title, subtitle, tickerText, user_type, order_id;

    public NotificationModel(Parcel in) {
        id = in.readString();
        message = in.readString();
        title = in.readString();
        subtitle = in.readString();
        tickerText = in.readString();
        user_type = in.readString();
        order_id = in.readString();
    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };

    public NotificationModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(message);
        parcel.writeString(title);
        parcel.writeString(subtitle);
        parcel.writeString(tickerText);
        parcel.writeString(user_type);
        parcel.writeString(order_id);
    }
}
