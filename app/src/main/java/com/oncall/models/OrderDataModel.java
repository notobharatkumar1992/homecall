package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 22-Mar-17.
 */

public class OrderDataModel implements Parcelable {

    public String id, user_id, landmark, name, street, email, phone, instruction, address, time, date, currency_id, price, category_id, service_id, lat, lng, subcategory_id, booking_date, booking_time, reference_id, status, created, modified;

    protected OrderDataModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        landmark = in.readString();
        name = in.readString();
        street = in.readString();
        email = in.readString();
        phone = in.readString();
        instruction = in.readString();
        address = in.readString();
        time = in.readString();
        date = in.readString();
        currency_id = in.readString();
        price = in.readString();
        category_id = in.readString();
        service_id = in.readString();
        lat = in.readString();
        lng = in.readString();
        subcategory_id = in.readString();
        booking_date = in.readString();
        booking_time = in.readString();
        reference_id = in.readString();
        status = in.readString();
        created = in.readString();
        modified = in.readString();
    }

    public static final Creator<OrderDataModel> CREATOR = new Creator<OrderDataModel>() {
        @Override
        public OrderDataModel createFromParcel(Parcel in) {
            return new OrderDataModel(in);
        }

        @Override
        public OrderDataModel[] newArray(int size) {
            return new OrderDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(user_id);
        parcel.writeString(landmark);
        parcel.writeString(name);
        parcel.writeString(street);
        parcel.writeString(email);
        parcel.writeString(phone);
        parcel.writeString(instruction);
        parcel.writeString(address);
        parcel.writeString(time);
        parcel.writeString(date);
        parcel.writeString(currency_id);
        parcel.writeString(price);
        parcel.writeString(category_id);
        parcel.writeString(service_id);
        parcel.writeString(lat);
        parcel.writeString(lng);
        parcel.writeString(subcategory_id);
        parcel.writeString(booking_date);
        parcel.writeString(booking_time);
        parcel.writeString(reference_id);
        parcel.writeString(status);
        parcel.writeString(created);
        parcel.writeString(modified);
    }
}
