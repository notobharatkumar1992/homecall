package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 12-Oct-16.
 */
public class VendorModel implements Parcelable {
    public int status, checked, orderCheck;
    public String id, vendor_id, email, name, last_name, profile_pic, contact_no, address1, address2, city, state, country, zip, currency_id, member_since, membership_id, business_name, business_subcategory_id, business_category_id, latitude, longitude, payment_status, paid_amt, category, image, icon, full_name, name_email;

    public VendorModel() {
    }

    @Override
    public String toString() {
        return "VendorModel{" +
                "status=" + status +
                ", checked=" + checked +
                ", orderCheck=" + orderCheck +
                ", id='" + id + '\'' +
                ", vendor_id='" + vendor_id + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", profile_pic='" + profile_pic + '\'' +
                ", contact_no='" + contact_no + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", zip='" + zip + '\'' +
                ", currency_id='" + currency_id + '\'' +
                ", member_since='" + member_since + '\'' +
                ", membership_id='" + membership_id + '\'' +
                ", business_name='" + business_name + '\'' +
                ", business_subcategory_id='" + business_subcategory_id + '\'' +
                ", business_category_id='" + business_category_id + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", payment_status='" + payment_status + '\'' +
                ", paid_amt='" + paid_amt + '\'' +
                ", category='" + category + '\'' +
                ", image='" + image + '\'' +
                ", icon='" + icon + '\'' +
                ", full_name='" + full_name + '\'' +
                ", name_email='" + name_email + '\'' +
                '}';
    }

    protected VendorModel(Parcel in) {
        status = in.readInt();
        checked = in.readInt();
        orderCheck = in.readInt();
        id = in.readString();
        vendor_id = in.readString();
        email = in.readString();
        name = in.readString();
        last_name = in.readString();
        profile_pic = in.readString();
        contact_no = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        zip = in.readString();
        currency_id = in.readString();
        member_since = in.readString();
        membership_id = in.readString();
        business_name = in.readString();
        business_subcategory_id = in.readString();
        business_category_id = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        payment_status = in.readString();
        paid_amt = in.readString();
        category = in.readString();
        image = in.readString();
        icon = in.readString();
        full_name = in.readString();
        name_email = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(checked);
        dest.writeInt(orderCheck);
        dest.writeString(id);
        dest.writeString(vendor_id);
        dest.writeString(email);
        dest.writeString(name);
        dest.writeString(last_name);
        dest.writeString(profile_pic);
        dest.writeString(contact_no);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(zip);
        dest.writeString(currency_id);
        dest.writeString(member_since);
        dest.writeString(membership_id);
        dest.writeString(business_name);
        dest.writeString(business_subcategory_id);
        dest.writeString(business_category_id);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(payment_status);
        dest.writeString(paid_amt);
        dest.writeString(category);
        dest.writeString(image);
        dest.writeString(icon);
        dest.writeString(full_name);
        dest.writeString(name_email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VendorModel> CREATOR = new Creator<VendorModel>() {
        @Override
        public VendorModel createFromParcel(Parcel in) {
            return new VendorModel(in);
        }

        @Override
        public VendorModel[] newArray(int size) {
            return new VendorModel[size];
        }
    };
}
