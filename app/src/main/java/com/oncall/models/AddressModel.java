package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Noto-11 on 3/18/2017.
 */

public class AddressModel implements Parcelable {
    public String name, contact_no, address, address_2, city, city2, state, zip_code, landmark, street, latitude, longitude;

    public AddressModel() {
    }

    @Override
    public String toString() {
        return "AddressModel{" +
                "name='" + name + '\'' +
                ", contact_no='" + contact_no + '\'' +
                ", address='" + address + '\'' +
                ", address_2='" + address_2 + '\'' +
                ", city='" + city + '\'' +
                ", city2='" + city2 + '\'' +
                ", state='" + state + '\'' +
                ", zip_code='" + zip_code + '\'' +
                ", landmark='" + landmark + '\'' +
                ", street='" + street + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    protected AddressModel(Parcel in) {
        name = in.readString();
        contact_no = in.readString();
        address = in.readString();
        address_2 = in.readString();
        city = in.readString();
        city2 = in.readString();
        state = in.readString();
        zip_code = in.readString();
        landmark = in.readString();
        street = in.readString();
        latitude = in.readString();
        longitude = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(contact_no);
        dest.writeString(address);
        dest.writeString(address_2);
        dest.writeString(city);
        dest.writeString(city2);
        dest.writeString(state);
        dest.writeString(zip_code);
        dest.writeString(landmark);
        dest.writeString(street);
        dest.writeString(latitude);
        dest.writeString(longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AddressModel> CREATOR = new Creator<AddressModel>() {
        @Override
        public AddressModel createFromParcel(Parcel in) {
            return new AddressModel(in);
        }

        @Override
        public AddressModel[] newArray(int size) {
            return new AddressModel[size];
        }
    };
}
