package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

import static com.oncall.AppDelegate.CUSTOMER;


public class UserDataModel implements Parcelable {
    public int id = -1, business_category_id, postal_code, category_id, sub_category_id, membership_id, login_type, device_type, is_verified, is_login, is_view, role_id, brewery_id;
    public String business_subcategory_id, vendor_id, member_since, name_email, name, first_name, last_name, email, phone, address, latitude, longitude, country_id, state_id, city_id, contact_no, service, image, session, category, verification_code, device_id, created, password, file_path, business_name, gender, birthday, social_id;
    public String state, city, address1, address2, city2, country, zip, token, sessionid, full_name, currency_id, currency, currency_symbol, currency_slug;
    int user_type = CUSTOMER;

    public UserDataModel() {
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "id=" + id +
                ", business_category_id=" + business_category_id +
                ", postal_code=" + postal_code +
                ", category_id=" + category_id +
                ", sub_category_id=" + sub_category_id +
                ", membership_id=" + membership_id +
                ", login_type=" + login_type +
                ", device_type=" + device_type +
                ", is_verified=" + is_verified +
                ", is_login=" + is_login +
                ", is_view=" + is_view +
                ", role_id=" + role_id +
                ", brewery_id=" + brewery_id +
                ", business_subcategory_id='" + business_subcategory_id + '\'' +
                ", vendor_id='" + vendor_id + '\'' +
                ", member_since='" + member_since + '\'' +
                ", name_email='" + name_email + '\'' +
                ", name='" + name + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", city_id='" + city_id + '\'' +
                ", contact_no='" + contact_no + '\'' +
                ", service='" + service + '\'' +
                ", image='" + image + '\'' +
                ", session='" + session + '\'' +
                ", category='" + category + '\'' +
                ", verification_code='" + verification_code + '\'' +
                ", device_id='" + device_id + '\'' +
                ", created='" + created + '\'' +
                ", password='" + password + '\'' +
                ", file_path='" + file_path + '\'' +
                ", business_name='" + business_name + '\'' +
                ", gender='" + gender + '\'' +
                ", birthday='" + birthday + '\'' +
                ", social_id='" + social_id + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city2='" + city2 + '\'' +
                ", country='" + country + '\'' +
                ", zip='" + zip + '\'' +
                ", token='" + token + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", full_name='" + full_name + '\'' +
                ", currency_id='" + currency_id + '\'' +
                ", currency='" + currency + '\'' +
                ", currency_symbol='" + currency_symbol + '\'' +
                ", currency_slug='" + currency_slug + '\'' +
                ", user_type=" + user_type +
                '}';
    }

    protected UserDataModel(Parcel in) {
        id = in.readInt();
        business_category_id = in.readInt();
        postal_code = in.readInt();
        category_id = in.readInt();
        sub_category_id = in.readInt();
        membership_id = in.readInt();
        login_type = in.readInt();
        device_type = in.readInt();
        is_verified = in.readInt();
        is_login = in.readInt();
        is_view = in.readInt();
        role_id = in.readInt();
        brewery_id = in.readInt();
        business_subcategory_id = in.readString();
        vendor_id = in.readString();
        member_since = in.readString();
        name_email = in.readString();
        name = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        phone = in.readString();
        address = in.readString();
        latitude = in.readString();
        longitude = in.readString();

        country_id = in.readString();
        state_id = in.readString();
        city_id = in.readString();
        contact_no = in.readString();
        service = in.readString();
        image = in.readString();
        session = in.readString();
        category = in.readString();
        verification_code = in.readString();
        device_id = in.readString();
        created = in.readString();
        password = in.readString();
        file_path = in.readString();
        business_name = in.readString();
        gender = in.readString();
        birthday = in.readString();
        social_id = in.readString();
        state = in.readString();
        city = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        city2 = in.readString();
        country = in.readString();
        zip = in.readString();
        token = in.readString();
        sessionid = in.readString();
        full_name = in.readString();
        currency_id = in.readString();
        currency = in.readString();
        currency_symbol = in.readString();
        currency_slug = in.readString();
        user_type = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(business_category_id);
        dest.writeInt(postal_code);
        dest.writeInt(category_id);
        dest.writeInt(sub_category_id);
        dest.writeInt(membership_id);
        dest.writeInt(login_type);
        dest.writeInt(device_type);
        dest.writeInt(is_verified);
        dest.writeInt(is_login);
        dest.writeInt(is_view);
        dest.writeInt(role_id);
        dest.writeInt(brewery_id);
        dest.writeString(business_subcategory_id);
        dest.writeString(vendor_id);
        dest.writeString(member_since);
        dest.writeString(name_email);
        dest.writeString(name);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(country_id);
        dest.writeString(state_id);
        dest.writeString(city_id);
        dest.writeString(contact_no);
        dest.writeString(service);
        dest.writeString(image);
        dest.writeString(session);
        dest.writeString(category);
        dest.writeString(verification_code);
        dest.writeString(device_id);
        dest.writeString(created);
        dest.writeString(password);
        dest.writeString(file_path);
        dest.writeString(business_name);
        dest.writeString(gender);
        dest.writeString(birthday);
        dest.writeString(social_id);
        dest.writeString(state);
        dest.writeString(city);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(city2);
        dest.writeString(country);
        dest.writeString(zip);
        dest.writeString(token);
        dest.writeString(sessionid);
        dest.writeString(full_name);
        dest.writeString(currency_id);
        dest.writeString(currency);
        dest.writeString(currency_symbol);
        dest.writeString(currency_slug);
        dest.writeInt(user_type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel in) {
            return new UserDataModel(in);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };
}
