package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 07-Apr-17.
 */

public class ReviewModel implements Parcelable {

    public String id, rating, review, order_id, created, subcategory_id, subcategories, full_name, user_id;

    public ReviewModel(Parcel in) {
        id = in.readString();
        rating = in.readString();
        review = in.readString();
        order_id = in.readString();
        created = in.readString();
        subcategory_id = in.readString();
        subcategories = in.readString();
        full_name = in.readString();
        user_id = in.readString();
    }

    public static final Creator<ReviewModel> CREATOR = new Creator<ReviewModel>() {
        @Override
        public ReviewModel createFromParcel(Parcel in) {
            return new ReviewModel(in);
        }

        @Override
        public ReviewModel[] newArray(int size) {
            return new ReviewModel[size];
        }
    };

    public ReviewModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(rating);
        parcel.writeString(review);
        parcel.writeString(order_id);
        parcel.writeString(created);
        parcel.writeString(subcategory_id);
        parcel.writeString(subcategories);
        parcel.writeString(full_name);
        parcel.writeString(user_id);
    }
}
