package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Heena on 14-Feb-17.
 */
public class BookOrderModel implements Parcelable {

    public ArrayList<ServiceModel> arrayServices = new ArrayList<>();
    public AddressModel addressModel;
    public String id, str_currency_id, str_description, str_service_id, str_service_currency, str_date, str_time, minimum_booking_time = "00:00";
    public int booking_from;
    public VendorModel vendorModel;

    public BookOrderModel() {
    }

    protected BookOrderModel(Parcel in) {
        arrayServices = in.createTypedArrayList(ServiceModel.CREATOR);
        addressModel = in.readParcelable(AddressModel.class.getClassLoader());
        id = in.readString();
        str_currency_id = in.readString();
        str_description = in.readString();
        str_service_id = in.readString();
        str_service_currency = in.readString();
        str_date = in.readString();
        str_time = in.readString();
        minimum_booking_time = in.readString();
        booking_from = in.readInt();
        vendorModel = in.readParcelable(VendorModel.class.getClassLoader());
    }

    public static final Creator<BookOrderModel> CREATOR = new Creator<BookOrderModel>() {
        @Override
        public BookOrderModel createFromParcel(Parcel in) {
            return new BookOrderModel(in);
        }

        @Override
        public BookOrderModel[] newArray(int size) {
            return new BookOrderModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(arrayServices);
        parcel.writeParcelable(addressModel, i);
        parcel.writeString(id);
        parcel.writeString(str_currency_id);
        parcel.writeString(str_description);
        parcel.writeString(str_service_id);
        parcel.writeString(str_service_currency);
        parcel.writeString(str_date);
        parcel.writeString(str_time);
        parcel.writeString(minimum_booking_time);
        parcel.writeInt(booking_from);
        parcel.writeParcelable(vendorModel, i);
    }
}
