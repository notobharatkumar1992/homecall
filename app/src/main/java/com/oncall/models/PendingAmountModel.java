package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 11-May-17.
 */

public class PendingAmountModel implements Parcelable {

    public String id, user_id, order_id, comment, price, total_amount;

    protected PendingAmountModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        order_id = in.readString();
        comment = in.readString();
        price = in.readString();
        total_amount = in.readString();
    }

    public static final Creator<PendingAmountModel> CREATOR = new Creator<PendingAmountModel>() {
        @Override
        public PendingAmountModel createFromParcel(Parcel in) {
            return new PendingAmountModel(in);
        }

        @Override
        public PendingAmountModel[] newArray(int size) {
            return new PendingAmountModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(user_id);
        parcel.writeString(order_id);
        parcel.writeString(comment);
        parcel.writeString(price);
        parcel.writeString(total_amount);
    }
}
