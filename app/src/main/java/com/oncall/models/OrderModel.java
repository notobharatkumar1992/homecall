package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 27-Mar-17.
 */
public class OrderModel implements Parcelable {

    public String id, category_id, category_name, subcategory_id, price, currency_id, user_id, vendor_id, reference_id, booking_date, booking_time, comment, name, phone, email, landmark, street, address, status, created;

    protected OrderModel(Parcel in) {
        id = in.readString();
        category_id = in.readString();
        category_name = in.readString();
        subcategory_id = in.readString();
        price = in.readString();
        currency_id = in.readString();
        user_id = in.readString();
        vendor_id = in.readString();
        reference_id = in.readString();
        booking_date = in.readString();
        booking_time = in.readString();
        comment = in.readString();
        name = in.readString();
        phone = in.readString();
        email = in.readString();
        landmark = in.readString();
        street = in.readString();
        address = in.readString();
        status = in.readString();
        created = in.readString();
    }

    public static final Creator<OrderModel> CREATOR = new Creator<OrderModel>() {
        @Override
        public OrderModel createFromParcel(Parcel in) {
            return new OrderModel(in);
        }

        @Override
        public OrderModel[] newArray(int size) {
            return new OrderModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(category_id);
        parcel.writeString(category_name);
        parcel.writeString(subcategory_id);
        parcel.writeString(price);
        parcel.writeString(currency_id);
        parcel.writeString(user_id);
        parcel.writeString(vendor_id);
        parcel.writeString(reference_id);
        parcel.writeString(booking_date);
        parcel.writeString(booking_time);
        parcel.writeString(comment);
        parcel.writeString(name);
        parcel.writeString(phone);
        parcel.writeString(email);
        parcel.writeString(landmark);
        parcel.writeString(street);
        parcel.writeString(address);
        parcel.writeString(status);
        parcel.writeString(created);
    }
}
