package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Bharat on 28-Mar-17.
 */

public class RequestModel implements Parcelable {

    public String id, category_id, subcategory_id, price, currency_id, user_id, vendor_id, reference_id, booking_date, booking_time, comment, name, phone, email, city, city_2, state, address, address2, latitude, longitude, zip_code, status, created, minimum_booking_time = "00:00";

    public CategoryModel categoryModel;
    public CustomerModel customerModel;
    public ReviewModel reviewModel;
    public ArrayList<ServiceModel> arrayServices = new ArrayList<>();

    public RequestModel(Parcel in) {
        id = in.readString();
        category_id = in.readString();
        subcategory_id = in.readString();
        price = in.readString();
        currency_id = in.readString();
        user_id = in.readString();
        vendor_id = in.readString();
        reference_id = in.readString();
        booking_date = in.readString();
        booking_time = in.readString();
        comment = in.readString();
        name = in.readString();
        phone = in.readString();
        email = in.readString();
        city = in.readString();
        state = in.readString();
        address = in.readString();
        address2 = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        zip_code = in.readString();
        status = in.readString();
        created = in.readString();
        minimum_booking_time = in.readString();
        categoryModel = in.readParcelable(CategoryModel.class.getClassLoader());
        customerModel = in.readParcelable(CustomerModel.class.getClassLoader());
        reviewModel = in.readParcelable(ReviewModel.class.getClassLoader());
        arrayServices = in.createTypedArrayList(ServiceModel.CREATOR);

    }

    public static final Creator<RequestModel> CREATOR = new Creator<RequestModel>() {
        @Override
        public RequestModel createFromParcel(Parcel in) {
            return new RequestModel(in);
        }

        @Override
        public RequestModel[] newArray(int size) {
            return new RequestModel[size];
        }
    };

    public RequestModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(category_id);
        parcel.writeString(subcategory_id);
        parcel.writeString(price);
        parcel.writeString(currency_id);
        parcel.writeString(user_id);
        parcel.writeString(vendor_id);
        parcel.writeString(reference_id);
        parcel.writeString(booking_date);
        parcel.writeString(booking_time);
        parcel.writeString(comment);
        parcel.writeString(name);
        parcel.writeString(phone);
        parcel.writeString(email);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(address);
        parcel.writeString(address2);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(zip_code);
        parcel.writeString(status);
        parcel.writeString(created);
        parcel.writeString(minimum_booking_time);
        parcel.writeParcelable(categoryModel, i);
        parcel.writeParcelable(customerModel, i);
        parcel.writeParcelable(reviewModel, i);
        parcel.writeTypedList(arrayServices);
    }
}

