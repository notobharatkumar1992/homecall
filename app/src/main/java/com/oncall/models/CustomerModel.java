package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 28-Mar-17.
 */

public class CustomerModel implements Parcelable {

    public String id, role_id, email, name, last_name, profile_pic, contact_no, address1, address2, city, state, country, currency_id, latitude, longitude, full_name, business_name = "";

    public CustomerModel(Parcel in) {
        id = in.readString();
        role_id = in.readString();
        email = in.readString();
        name = in.readString();
        last_name = in.readString();
        profile_pic = in.readString();
        contact_no = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        currency_id = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        full_name = in.readString();
        business_name = in.readString();
    }

    public static final Creator<CustomerModel> CREATOR = new Creator<CustomerModel>() {
        @Override
        public CustomerModel createFromParcel(Parcel in) {
            return new CustomerModel(in);
        }

        @Override
        public CustomerModel[] newArray(int size) {
            return new CustomerModel[size];
        }
    };

    public CustomerModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(role_id);
        parcel.writeString(email);
        parcel.writeString(name);
        parcel.writeString(last_name);
        parcel.writeString(profile_pic);
        parcel.writeString(contact_no);
        parcel.writeString(address1);
        parcel.writeString(address2);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(country);
        parcel.writeString(currency_id);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(full_name);
        parcel.writeString(business_name);
    }
}
