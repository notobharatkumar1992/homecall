package com.oncall.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 12-Oct-16.
 */
public class ServiceModel implements Parcelable {
    public String sub_category_id, sub_category, category_id, price, description, minimum_time, image, how_works, terms, cancellation_policy;
    public int id, status, checked;

    public ServiceModel() {
    }

    @Override
    public String toString() {
        return "ServiceModel{" +
                "sub_category_id='" + sub_category_id + '\'' +
                ", sub_category='" + sub_category + '\'' +
                ", category_id='" + category_id + '\'' +
                ", price='" + price + '\'' +
                ", description='" + description + '\'' +
                ", minimum_time='" + minimum_time + '\'' +
                ", image='" + image + '\'' +
                ", how_works='" + how_works + '\'' +
                ", terms='" + terms + '\'' +
                ", cancellation_policy='" + cancellation_policy + '\'' +
                ", id=" + id +
                ", status=" + status +
                ", checked=" + checked +
                '}';
    }

    protected ServiceModel(Parcel in) {
        sub_category_id = in.readString();
        sub_category = in.readString();
        category_id = in.readString();
        price = in.readString();
        description = in.readString();
        minimum_time = in.readString();
        image = in.readString();
        how_works = in.readString();
        terms = in.readString();
        cancellation_policy = in.readString();
        id = in.readInt();
        status = in.readInt();
        checked = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sub_category_id);
        dest.writeString(sub_category);
        dest.writeString(category_id);
        dest.writeString(price);
        dest.writeString(description);
        dest.writeString(minimum_time);
        dest.writeString(image);
        dest.writeString(how_works);
        dest.writeString(terms);
        dest.writeString(cancellation_policy);
        dest.writeInt(id);
        dest.writeInt(status);
        dest.writeInt(checked);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceModel> CREATOR = new Creator<ServiceModel>() {
        @Override
        public ServiceModel createFromParcel(Parcel in) {
            return new ServiceModel(in);
        }

        @Override
        public ServiceModel[] newArray(int size) {
            return new ServiceModel[size];
        }
    };
}
