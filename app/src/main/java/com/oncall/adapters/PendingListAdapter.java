package com.oncall.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.R;
import com.oncall.activities.MainActivity;
import com.oncall.activities.MyOrderDetailActivity;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.PendingAmountModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class PendingListAdapter extends RecyclerView.Adapter<PendingListAdapter.ViewHolder> {

    private ArrayList<PendingAmountModel> arrayList = new ArrayList<>();
    private OnListItemClickListener onListItemClickListener;
    private View v;
    private FragmentActivity context;
    public Prefs prefs;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PendingAmountModel serviceModel = arrayList.get(position);
        holder.txt_c_service.setText(serviceModel.comment);
        holder.txt_c_currency.setText(" AED " + serviceModel.price);
        holder.img_c_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrderDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.order_id, serviceModel.order_id);
                bundle.putInt(Tags.from, MainActivity.FROM_ORDER_LIST);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    public PendingListAdapter(FragmentActivity context, ArrayList<PendingAmountModel> heroDealsModels, OnListItemClickListener onListItemClickListener) {
        this.context = context;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(heroDealsModels);
        this.onListItemClickListener = onListItemClickListener;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_c_info;
        TextView txt_c_service, txt_c_currency;
        RelativeLayout rl_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            img_c_info = (ImageView) itemView.findViewById(R.id.img_c_info);
            txt_c_currency = (TextView) itemView.findViewById(R.id.txt_c_currency);
            txt_c_service = (TextView) itemView.findViewById(R.id.txt_c_service);
            rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        }
    }
}