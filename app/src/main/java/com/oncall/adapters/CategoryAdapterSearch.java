package com.oncall.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.models.CategoryModel;

import java.util.ArrayList;

import carbon.widget.TextView;


public class CategoryAdapterSearch extends BaseAdapter {

    public final Activity context;
    public ArrayList<CategoryModel> arrayList, mainArrayList;

    public CategoryAdapterSearch(Activity context, ArrayList<CategoryModel> arrayList) {
        this.context = context;
        if (this.arrayList == null)
            this.arrayList = new ArrayList<>();
        if (this.mainArrayList == null)
            this.mainArrayList = new ArrayList<>();

        this.mainArrayList.addAll(arrayList);
        this.arrayList.addAll(mainArrayList);
    }

    @Override
    public CategoryModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.dialog_listitem, null, true);
        TextView txtarrayList = (TextView) rowView.findViewById(R.id.txt_c_listvalue);
        txtarrayList.setText(arrayList.get(position).title);
        return rowView;
    }

    public void filter(String string) {
        arrayList.clear();
        if (string.length() != 0) {
            for (CategoryModel cityItems : mainArrayList) {
                AppDelegate.LogT("name => " + cityItems.title + " = " + string);
                if (cityItems.title.toLowerCase().contains(string.toLowerCase())) {
                    arrayList.add(cityItems);
                }
            }
        } else {
            arrayList.addAll(mainArrayList);
        }
        notifyDataSetChanged();
    }

}
