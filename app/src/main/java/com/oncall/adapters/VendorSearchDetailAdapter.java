package com.oncall.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListCategoryItemClick;
import com.oncall.models.VendorModel;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class VendorSearchDetailAdapter extends RecyclerView.Adapter<VendorSearchDetailAdapter.ViewHolder> {

    private final ArrayList<VendorModel> categoryModelArrayList;
    public ArrayList<VendorModel> allcategoryModelArrayList;
    View v;
    FragmentActivity context;
    public OnListCategoryItemClick onListCategoryItemClick;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_vendor_name_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final VendorModel cityModel = categoryModelArrayList.get(position);
        AppDelegate.LogT("cityModels city=" + cityModel.full_name + "");
        holder.txt_c_title.setText(cityModel.full_name + "");
        holder.txt_c_business_name.setText(cityModel.business_name + "");
        holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListCategoryItemClick != null) {
                    onListCategoryItemClick.setOnListItemClickListener(Tags.city_name, position, cityModel, holder.txt_c_title);
                }
            }
        });
    }

    public VendorSearchDetailAdapter(FragmentActivity context, ArrayList<VendorModel> cityModels, OnListCategoryItemClick onListCityItemClick) {
        this.context = context;
        this.allcategoryModelArrayList = new ArrayList<>();
        this.allcategoryModelArrayList.addAll(cityModels);
        this.categoryModelArrayList = new ArrayList<>();
        this.categoryModelArrayList.addAll(allcategoryModelArrayList);
        this.onListCategoryItemClick = onListCityItemClick;
        AppDelegate.LogT("cityModels length=" + cityModels.size() + "");
    }

    public void filterSting(String value) {
        categoryModelArrayList.clear();
        if (AppDelegate.isValidString(value)) {
            for (VendorModel cityModel : allcategoryModelArrayList) {
                AppDelegate.LogT("match => " + cityModel.full_name + " == " + value);
                if (cityModel.full_name.toString().toLowerCase().contains(value.toLowerCase())
                        || cityModel.business_name.toString().toLowerCase().contains(value.toLowerCase())
                        || cityModel.city.toString().toLowerCase().contains(value.toLowerCase())) {
                    categoryModelArrayList.add(cityModel);
                }
            }
        } else {
            categoryModelArrayList.addAll(allcategoryModelArrayList);
        }
        AppDelegate.LogT("cityModels size => " + categoryModelArrayList.size());
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return categoryModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_c_title, txt_c_business_name;
        public RelativeLayout rl_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            txt_c_business_name = (TextView) itemView.findViewById(R.id.txt_c_business_name);
            rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        }
    }
}