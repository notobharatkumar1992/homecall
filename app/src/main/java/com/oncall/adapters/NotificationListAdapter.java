package com.oncall.adapters;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.activities.MainActivity;
import com.oncall.activities.MyOrderDetailActivity;
import com.oncall.activities.NotificationDetailActivity;
import com.oncall.constants.Tags;
import com.oncall.firebase.MyFirebaseMessagingService;
import com.oncall.models.NotificationModel;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {

    private final ArrayList<NotificationModel> categoryModelArrayList;
    View v;
    FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_vendor_name_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotificationModel notificationModel = categoryModelArrayList.get(position);
        holder.txt_c_title.setText(notificationModel.title + "");
        holder.txt_c_business_name.setText(notificationModel.message + "");
        holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (notificationModel.user_type.equalsIgnoreCase(MyFirebaseMessagingService.USER_VENDOR + "")) {
                    if (notificationModel.title.equalsIgnoreCase(MyFirebaseMessagingService.NEW_ORDER)
                            || notificationModel.title.equalsIgnoreCase(MyFirebaseMessagingService.ORDER_RESCHEDULE)) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Tags.order_id, notificationModel.order_id);
                        bundle.putString(Tags.notification_id, notificationModel.id);
                        bundle.putString(Tags.type, notificationModel.title);
                        bundle.putInt(Tags.from, MainActivity.FROM_CLASS_NOTIFICATION_LIST);
                        Intent intent = new Intent(context, PendingRequestDetailActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putString(Tags.order_id, notificationModel.order_id);
                        bundle.putString(Tags.notification_id, notificationModel.id);
                        bundle.putString(Tags.type, notificationModel.title);
                        bundle.putInt(Tags.from, MainActivity.FROM_CLASS_NOTIFICATION_LIST);
                        Intent intent = new Intent(context, ManageJobDetailActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                } else */
                if (notificationModel.user_type.equalsIgnoreCase(MyFirebaseMessagingService.USER_CUSTOMER + "")) {
                    if (AppDelegate.isValidString(notificationModel.order_id)) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Tags.order_id, notificationModel.order_id);
                        bundle.putString(Tags.notification_id, notificationModel.id);
                        bundle.putString(Tags.type, notificationModel.title);
                        bundle.putInt(Tags.from, MainActivity.FROM_CLASS_NOTIFICATION_LIST);
                        Intent intent = new Intent(context, MyOrderDetailActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.notification, notificationModel);
                        Intent intent = new Intent(context, NotificationDetailActivity.class);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    public NotificationListAdapter(FragmentActivity context, ArrayList<NotificationModel> cityModels) {
        this.context = context;
        this.categoryModelArrayList = cityModels;
    }

    @Override
    public int getItemCount() {
        return categoryModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_c_title, txt_c_business_name;
        public RelativeLayout rl_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            txt_c_business_name = (TextView) itemView.findViewById(R.id.txt_c_business_name);
            rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        }
    }
}