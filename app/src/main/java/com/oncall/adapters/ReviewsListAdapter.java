package com.oncall.adapters;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.models.ReviewModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.ViewHolder> {

    private ArrayList<ReviewModel> mainArrayList;
    private View v;
    private FragmentActivity context;
    public Prefs prefs;
    public int action;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ReviewModel orderModel = mainArrayList.get(position);
        String str_placed_on = orderModel.created.substring(0, orderModel.created.indexOf("T"));
        holder.txt_c_name.setText(orderModel.full_name);
        holder.txt_c_feedback.setText(orderModel.review);
        holder.txt_c_rated_on.setText(str_placed_on);
        holder.txt_c_services.setText(orderModel.subcategories);
        try {
            holder.rb_rating.setRating(Float.parseFloat(orderModel.rating + ""));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
//        holder.main_containt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AppDelegate.LogT("item clicked => " + position);
//                Bundle bundle = new Bundle();
//                bundle.putString(Tags.order_id, orderModel.id);
//                bundle.putParcelable(Tags.request_model, orderModel);
//                Intent intent = new Intent(context, PendingRequestDetailActivity.class);
//                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false,
//                        new Pair<>(holder.ll_order_detail, context.getString(R.string.manage_requests)));
//                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
//                intent.putExtras(bundle);
//                context.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
//            }
//        });

    }

    public ReviewsListAdapter(FragmentActivity context, ArrayList<ReviewModel> mainArrayList) {
        this.context = context;
        this.mainArrayList = mainArrayList;
    }

    @Override
    public int getItemCount() {
        return mainArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_name, txt_c_rated_on, txt_c_feedback, txt_c_services;
        RatingBar rb_rating;
        RelativeLayout main_containt;
        LinearLayout ll_order_detail;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_name = (TextView) itemView.findViewById(R.id.txt_c_name);
            txt_c_rated_on = (TextView) itemView.findViewById(R.id.txt_c_rated_on);
            txt_c_feedback = (TextView) itemView.findViewById(R.id.txt_c_feedback);
            txt_c_services = (TextView) itemView.findViewById(R.id.txt_c_services);
            rb_rating = (RatingBar) itemView.findViewById(R.id.rb_rating);
            main_containt = (RelativeLayout) itemView.findViewById(R.id.main_containt);
            ll_order_detail = (LinearLayout) itemView.findViewById(R.id.ll_order_detail);
        }
    }
}