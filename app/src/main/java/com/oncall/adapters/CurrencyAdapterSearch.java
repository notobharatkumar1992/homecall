package com.oncall.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.CategoryModel;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class CurrencyAdapterSearch extends RecyclerView.Adapter<CurrencyAdapterSearch.ViewHolder> {

    private ArrayList<CategoryModel> heroDealsModels;
    private OnListItemClickListener onListItemClickListener;
    private View v;
    private FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CategoryModel heroDeals = heroDealsModels.get(position);
        holder.txt_c_title.setText(heroDeals.title + "");
        if (heroDeals.checked == 0) {
            holder.img_check.setSelected(false);
            holder.txt_c_title.setTextColor(context.getResources().getColor(R.color.et_txt));
        } else {
            holder.img_check.setSelected(true);
            holder.txt_c_title.setTextColor(context.getResources().getColor(R.color.green));
        }
        holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null) {
                    onListItemClickListener.setOnListItemClickListener(Tags.name, position, true);
                }
            }
        });
    }

    public CurrencyAdapterSearch(FragmentActivity context, ArrayList<CategoryModel> heroDealsModels, OnListItemClickListener onListItemClickListener) {
        this.context = context;
        this.heroDealsModels = heroDealsModels;
        this.onListItemClickListener = onListItemClickListener;
    }

    @Override
    public int getItemCount() {
        return heroDealsModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_check;
        TextView txt_c_title;
        LinearLayout ll_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            img_check = (ImageView) itemView.findViewById(R.id.img_check);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        }
    }
}