package com.oncall.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.activities.ServiceDetailActivity;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.ServiceModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class CategoryServicePaymentListAdapter extends RecyclerView.Adapter<CategoryServicePaymentListAdapter.ViewHolder> {

    private ArrayList<ServiceModel> mainArrayList, arrayList;
    private OnListItemClickListener onListItemClickListener;
    private View v;
    private FragmentActivity context;
    public Prefs prefs;
    public boolean fromOrder;
    public String image_url;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_servicelist_payment_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServiceModel serviceModel = arrayList.get(position);
        holder.txt_c_service.setText(serviceModel.sub_category);
        holder.txt_c_currency.setText(" AED " + serviceModel.price);
        holder.img_c_check.setVisibility(fromOrder ? View.INVISIBLE : View.VISIBLE);
        if (serviceModel.checked == 0) {
            holder.img_c_check.setSelected(false);
        } else {
            holder.img_c_check.setSelected(true);
        }
        holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.LogT("item clicked => " + position);
                int mainArrayItemPosition = position;
                for (int i = 0; i < mainArrayList.size(); i++) {
                    if (mainArrayList.get(i).sub_category_id.toString().toLowerCase().equalsIgnoreCase(serviceModel.sub_category_id)) {
                        mainArrayItemPosition = i;
                        break;
                    }
                }
                if (onListItemClickListener != null) {
                    onListItemClickListener.setOnListItemClickListener(Tags.name, mainArrayItemPosition, serviceModel.checked != 0);
                }
            }
        });
        holder.txt_c_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ServiceDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image, image_url);
                bundle.putParcelable(Tags.service, serviceModel);
                intent.putExtras(bundle);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false,
                        new Pair<>(holder.txt_c_service, "title"),
                        new Pair<>(holder.txt_c_currency, "currency"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                context.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        });
        holder.img_c_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.LogT("item clicked => " + position);
                int mainArrayItemPosition = position;
                for (int i = 0; i < mainArrayList.size(); i++) {
                    if (mainArrayList.get(i).sub_category_id.toString().toLowerCase().equalsIgnoreCase(serviceModel.sub_category_id)) {
                        mainArrayItemPosition = i;
                        break;
                    }
                }
                if (onListItemClickListener != null) {
                    onListItemClickListener.setOnListItemClickListener(Tags.name, mainArrayItemPosition, serviceModel.checked != 0);
                }
            }
        });
        holder.img_c_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ServiceDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image, image_url);
                bundle.putParcelable(Tags.service, serviceModel);
                intent.putExtras(bundle);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false,
                        new Pair<>(holder.txt_c_service, "title"),
                        new Pair<>(holder.txt_c_currency, "currency"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                context.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        });
    }


    public CategoryServicePaymentListAdapter(FragmentActivity context, ArrayList<ServiceModel> heroDealsModels, OnListItemClickListener onListItemClickListener, boolean fromOrder, String image_url) {
        this.context = context;
        this.mainArrayList = new ArrayList<>();
        this.mainArrayList.addAll(heroDealsModels);
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(heroDealsModels);
        this.onListItemClickListener = onListItemClickListener;
        this.fromOrder = fromOrder;
        this.image_url = image_url;
    }

    public void filter(String string) {
        arrayList.clear();
        if (string.length() != 0) {
            for (ServiceModel cityItems : mainArrayList) {
                try {
                    AppDelegate.LogT("name => " + cityItems.sub_category + " = " + string);
                    if (cityItems.sub_category.toLowerCase().contains(string.toLowerCase())) {
                        arrayList.add(cityItems);
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        } else {
            arrayList.addAll(mainArrayList);
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_c_check, img_c_info;
        TextView txt_c_service, txt_c_currency;
        RelativeLayout rl_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            img_c_check = (ImageView) itemView.findViewById(R.id.img_c_check);
            img_c_info = (ImageView) itemView.findViewById(R.id.img_c_info);
            txt_c_currency = (TextView) itemView.findViewById(R.id.txt_c_currency);
            txt_c_service = (TextView) itemView.findViewById(R.id.txt_c_service);
            rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        }
    }
}