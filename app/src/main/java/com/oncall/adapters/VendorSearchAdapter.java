package com.oncall.adapters;


import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.activities.VendorSearchListActivity;
import com.oncall.constants.Tags;
import com.oncall.models.CategoryModel;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class VendorSearchAdapter extends RecyclerView.Adapter<VendorSearchAdapter.ViewHolder> {

    private ArrayList<CategoryModel> serviceModelsArrayList, mainArrayList;
    private View v;
    private FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_vendor_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CategoryModel serviceModel = serviceModelsArrayList.get(position);
        holder.txt_c_title.setText(serviceModel.title);
        holder.img_c_detail.setVisibility(View.GONE);
        holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VendorSearchListActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false,
                        new Pair<>(holder.txt_c_title, "square_blue_name"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                intent.putExtra(Tags.category, serviceModel);
                context.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        });
    }

    public VendorSearchAdapter(FragmentActivity context, ArrayList<CategoryModel> serviceModelsArrayList) {
        this.context = context;
        this.serviceModelsArrayList = serviceModelsArrayList;
    }

    @Override
    public int getItemCount() {
        return serviceModelsArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_title;
        ImageView img_c_detail;
        RelativeLayout rl_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
            img_c_detail = (ImageView) itemView.findViewById(R.id.img_c_detail);
        }
    }
}