package com.oncall.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.activities.MainActivity;
import com.oncall.activities.MyOrderActivity;
import com.oncall.activities.MyOrderDetailActivity;
import com.oncall.activities.PaymentActivity;
import com.oncall.activities.ScheduleJobActivity;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.RequestModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;

import carbon.widget.EditText;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class MyOrderListAdapter extends RecyclerView.Adapter<MyOrderListAdapter.ViewHolder> {

    private ArrayList<RequestModel> mainArrayList, arrayList;
    private OnListItemClickListener onListItemClickListener;
    private View v;
    private FragmentActivity context;
    public Prefs prefs;
    public int action;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_order_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RequestModel serviceModel = arrayList.get(position);
        try {
            holder.txt_c_placed_on.setText(serviceModel.booking_date + "       " + serviceModel.booking_time);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        holder.txt_c_reference_no.setText(serviceModel.reference_id);
        holder.txt_c_service.setText(serviceModel.categoryModel.title);

        holder.txt_c_cancel.setVisibility(View.GONE);
        holder.txt_c_reschedule.setVisibility(View.GONE);
        holder.txt_c_rate_us.setVisibility(View.GONE);
        holder.txt_c_complete.setVisibility(View.GONE);
        if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "")) {
            holder.txt_c_status.setText("Pending");
            holder.txt_c_cancel.setVisibility(View.VISIBLE);
            holder.txt_c_reschedule.setVisibility(View.VISIBLE);
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_ACCEPTED + "")) {
            holder.txt_c_status.setText("Accepted");
            holder.txt_c_cancel.setVisibility(View.VISIBLE);
            holder.txt_c_reschedule.setVisibility(View.VISIBLE);
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_REJECTED + "")) {
            holder.txt_c_status.setText("Rejected");
            holder.txt_c_reschedule.setVisibility(View.VISIBLE);
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_STARTED + "")) {
            holder.txt_c_status.setText("Started");
            holder.txt_c_complete.setVisibility(View.VISIBLE);
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_COMPLETED + "")) {
            holder.txt_c_status.setText("Completed");
            holder.txt_c_rate_us.setVisibility(View.VISIBLE);
            if (serviceModel.reviewModel == null) {
                holder.txt_c_rate_us.setVisibility(View.VISIBLE);
            } else {
                holder.txt_c_rate_us.setVisibility(View.GONE);
            }
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_CANCELLED + "")) {
            holder.txt_c_status.setText("Cancelled");
        }

        holder.main_containt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrderDetailActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false,
                        new Pair<>(holder.ll_order_detail, "order_detail"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.request_model, serviceModel);
                bundle.putString(Tags.order_id, serviceModel.id);
                bundle.putInt(Tags.from, MainActivity.FROM_ORDER_LIST);
                intent.putExtras(bundle);
                context.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        });

        holder.txt_c_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null)
                    onListItemClickListener.setOnListItemClickListener(Tags.cancel, position, Integer.parseInt(serviceModel.id));
            }
        });

        holder.txt_c_rate_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null)
                    onListItemClickListener.setOnListItemClickListener(Tags.rating, position, Integer.parseInt(serviceModel.id));
            }
        });

        holder.txt_c_reschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.showAlert(context, "Reschedule Order", "Are you sure you want to reschedule this job?", "Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rescheduleJob(context, serviceModel);
                    }
                }, "No", null);
            }
        });

        holder.txt_c_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PaymentActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.request_model, serviceModel);
                bundle.putString(Tags.order_id, serviceModel.id);
                bundle.putInt(Tags.from, MainActivity.FROM_ORDER_LIST);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    private void rescheduleJob(Context context, RequestModel requestModel) {
        if (context != null && requestModel != null && requestModel.arrayServices != null && requestModel.arrayServices.size() > 0) {
            StringBuilder stringBuilder = null;
            for (int i = 0; i < requestModel.arrayServices.size(); i++) {
                if (stringBuilder == null) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("" + requestModel.arrayServices.get(i).sub_category_id);
                } else {
                    stringBuilder.append("," + requestModel.arrayServices.get(i).sub_category_id);
                }
            }
            Intent intent = new Intent(context, ScheduleJobActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Tags.from, MainActivity.BOOKING_FROM_RESCHEDULE);
            bundle.putParcelable(Tags.customer, requestModel.customerModel);
            bundle.putString(Tags.order_id, requestModel.id);
            bundle.putString(Tags.service_id, stringBuilder.toString());
            bundle.putString(Tags.currency_id, requestModel.currency_id);
            bundle.putString(Tags.description, requestModel.comment);
            bundle.putParcelableArrayList(Tags.ARRAY_SERVICES, requestModel.arrayServices);
            bundle.putParcelable(Tags.request_model, requestModel);
            intent.putExtras(bundle);
            context.startActivity(intent);
        } else {
            AppDelegate.LogE("Value is null at adapter.");
        }
    }

    public MyOrderListAdapter(FragmentActivity context, ArrayList<RequestModel> mainArrayList, OnListItemClickListener onListItemClickListener, int action) {
        this.context = context;
        this.onListItemClickListener = onListItemClickListener;

        if (this.arrayList == null)
            this.arrayList = new ArrayList<>();
        if (this.mainArrayList == null)
            this.mainArrayList = new ArrayList<>();

        this.mainArrayList.addAll(mainArrayList);
        this.arrayList.addAll(mainArrayList);
    }


    public void filter(int action) {
        arrayList.clear();
        AppDelegate.LogT("action => " + action + " == " + MyOrderActivity.ORDER_ALL);
        if (action == MyOrderActivity.ORDER_ALL) {
            arrayList.addAll(mainArrayList);
        } else if (action == MyOrderActivity.ORDER_PENDING) {
            for (RequestModel orderModel : mainArrayList) {
                if (orderModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "")) {
                    arrayList.add(orderModel);
                }
            }
        } else if (action == MyOrderActivity.ORDER_COMPLETED) {
            for (RequestModel orderModel : mainArrayList) {
                if (orderModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_COMPLETED + "")) {
                    arrayList.add(orderModel);
                }
            }
        } else if (action == MyOrderActivity.ORDER_CANCELLED) {
            for (RequestModel orderModel : mainArrayList) {
                if (orderModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_CANCELLED + "")) {
                    arrayList.add(orderModel);
                }
            }
        }
        AppDelegate.LogT("arrayList => " + arrayList.size());
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_placed_on, txt_c_reference_no, txt_c_service, txt_c_status;
        TextView txt_c_rate_us, txt_c_complete, txt_c_reschedule, txt_c_cancel;
        RelativeLayout main_containt;
        LinearLayout ll_order_detail;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_placed_on = (TextView) itemView.findViewById(R.id.txt_c_placed_on);
            txt_c_reference_no = (TextView) itemView.findViewById(R.id.txt_c_reference_no);
            txt_c_service = (TextView) itemView.findViewById(R.id.txt_c_service);
            txt_c_status = (TextView) itemView.findViewById(R.id.txt_c_status);
            txt_c_rate_us = (TextView) itemView.findViewById(R.id.txt_c_rate_us);
            txt_c_complete = (TextView) itemView.findViewById(R.id.txt_c_complete);
            txt_c_reschedule = (TextView) itemView.findViewById(R.id.txt_c_reschedule);
            txt_c_cancel = (TextView) itemView.findViewById(R.id.txt_c_cancel);
            main_containt = (RelativeLayout) itemView.findViewById(R.id.main_containt);
            ll_order_detail = (LinearLayout) itemView.findViewById(R.id.ll_order_detail);
        }
    }

    public AlertDialog alertDialog;
    public String str_cancelDialogComment = "";

    public void showCancelOrderDialog(final int pos) {
        /* Alert Dialog Code Start*/
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Cancel Order"); //Set Alert dialog title here
        alert.setCancelable(false);

        // Set an EditText view to get user input
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        input.setLayoutParams(params);
        input.setPadding(AppDelegate.dpToPix(context, 25), AppDelegate.dpToPix(context, 15), AppDelegate.dpToPix(context, 25), 0);
        input.setHint("Enter your comment");
        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        input.setFocusable(true);
        input.setSelection(input.length());
        input.setFocusableInTouchMode(true);
        input.setEnabled(true);
        alert.setView(input);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                str_cancelDialogComment = input.getText().toString();
                if (onListItemClickListener != null)
                    onListItemClickListener.setOnListItemClickListener(Tags.cancel, pos);
                dialog.cancel();

            } // End of onClick(DialogInterface dialog, int whichButton)
        }); //End of alert.setPositiveButton
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                dialog.cancel();
            }
        }); //End of alert.setNegativeButton
        alertDialog = alert.create();
        alertDialog.show();
    }


}