package com.oncall.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.models.ServiceModel;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.ViewHolder> {

    private ArrayList<ServiceModel> serviceModelsArrayList;
    private View v;
    private FragmentActivity context;
    String currency, currency_symbol;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.servicelist_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServiceModel serviceModel = serviceModelsArrayList.get(position);

        holder.txt_c_service.setText(serviceModel.sub_category);
        try {
            currency = new Prefs(context).getUserdata().currency_slug;
            currency_symbol = new Prefs(context).getUserdata().currency_symbol;

            JSONObject priceObject = new JSONObject(serviceModel.price);
            AppDelegate.LogT("price==" + priceObject);
            if (priceObject.has(currency)) {
                AppDelegate.LogT("priceObject.has(currency)=true");
                holder.txt_c_currency.setText(priceObject.getString(currency) + " " + currency_symbol);
            } else {

            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public ServiceListAdapter(FragmentActivity context, ArrayList<ServiceModel> serviceModelsArrayList) {
        this.context = context;
        this.serviceModelsArrayList = serviceModelsArrayList;
    }

    @Override
    public int getItemCount() {
        return serviceModelsArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_service, txt_c_currency;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_service = (TextView) itemView.findViewById(R.id.txt_c_service);
            txt_c_currency = (TextView) itemView.findViewById(R.id.txt_c_currency);
        }
    }
}