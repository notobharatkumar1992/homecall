package com.oncall.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.activities.MainActivity;
import com.oncall.activities.ManageJobDetailActivity;
import com.oncall.activities.MyOrderActivity;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.RequestModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class ManageJobsListAdapter extends RecyclerView.Adapter<ManageJobsListAdapter.ViewHolder> {

    private ArrayList<RequestModel> mainArrayList, arrayList;
    private OnListItemClickListener onListItemClickListener;
    private View v;
    private FragmentActivity context;
    public Prefs prefs;
    public int action;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_order_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RequestModel serviceModel = arrayList.get(position);
        String str_placed_on = serviceModel.booking_date.substring(0, serviceModel.booking_date.indexOf("T"));
        holder.txt_c_placed_on.setText(str_placed_on + "   |   " + serviceModel.booking_time);
        holder.txt_c_reference_no.setText(serviceModel.reference_id);
        holder.txt_c_service.setText(serviceModel.categoryModel.title);
        if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "")) {
            holder.txt_c_status.setText("PENDING");
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_ACCEPTED + "")) {
            holder.txt_c_status.setText("ACCEPTED");
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_STARTED + "")) {
            holder.txt_c_status.setText("STARTED");
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_COMPLETED + "")) {
            holder.txt_c_status.setText("COMPLETED");
        } else if (serviceModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_CANCELLED + "")) {
            holder.txt_c_status.setText("CANCELLED");
        }
        holder.main_containt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ManageJobDetailActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false,
                        new Pair<>(holder.ll_order_detail, context.getString(R.string.pending_requests)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.order_id, serviceModel.id);
                bundle.putParcelable(Tags.request_model, serviceModel);
                bundle.putInt(Tags.from, MainActivity.FROM_ORDER_LIST);
                intent.putExtras(bundle);
                context.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        });
    }

    public ManageJobsListAdapter(FragmentActivity context, ArrayList<RequestModel> mainArrayList, OnListItemClickListener onListItemClickListener) {
        this.context = context;
        this.onListItemClickListener = onListItemClickListener;

        if (this.arrayList == null)
            this.arrayList = new ArrayList<>();
        if (this.mainArrayList == null)
            this.mainArrayList = new ArrayList<>();

        this.mainArrayList.addAll(mainArrayList);
        this.arrayList.addAll(mainArrayList);
    }


    public void filter(int action) {
        arrayList.clear();
        AppDelegate.LogT("action => " + action + " == " + MyOrderActivity.ORDER_ALL);
        if (action == MyOrderActivity.ORDER_ALL) {
            arrayList.addAll(mainArrayList);
        } else if (action == MyOrderActivity.ORDER_PENDING) {
            for (RequestModel orderModel : mainArrayList) {
                if (orderModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "")) {
                    arrayList.add(orderModel);
                }
            }
        } else if (action == MyOrderActivity.ORDER_ACCEPTED) {
            for (RequestModel orderModel : mainArrayList) {
                if (orderModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_ACCEPTED + "")) {
                    arrayList.add(orderModel);
                }
            }
        } else if (action == MyOrderActivity.ORDER_STARTED) {
            for (RequestModel orderModel : mainArrayList) {
                if (orderModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_STARTED + "")) {
                    arrayList.add(orderModel);
                }
            }
        } else if (action == MyOrderActivity.ORDER_COMPLETED) {
            for (RequestModel orderModel : mainArrayList) {
                if (orderModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_COMPLETED + "")) {
                    arrayList.add(orderModel);
                }
            }
        }
        AppDelegate.LogT("arrayList => " + arrayList.size());
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_placed_on, txt_c_reference_no, txt_c_service, txt_c_status;
        RelativeLayout main_containt;
        LinearLayout ll_order_detail;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_placed_on = (TextView) itemView.findViewById(R.id.txt_c_placed_on);
            txt_c_reference_no = (TextView) itemView.findViewById(R.id.txt_c_reference_no);
            txt_c_service = (TextView) itemView.findViewById(R.id.txt_c_service);
            txt_c_status = (TextView) itemView.findViewById(R.id.txt_c_status);
            main_containt = (RelativeLayout) itemView.findViewById(R.id.main_containt);
            ll_order_detail = (LinearLayout) itemView.findViewById(R.id.ll_order_detail);
        }
    }
}