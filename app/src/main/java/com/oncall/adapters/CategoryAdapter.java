package com.oncall.adapters;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.models.CategoryModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

/**
 * Created by Heena on 07-Oct-16.
 */
public class CategoryAdapter extends BaseAdapter {

    private final ImageLoader imageLoader;
    private final DisplayImageOptions options;
    private Context context;
    ArrayList<CategoryModel> categoryModelArrayList;
    private ImageView img_c_image, img_c_category;
    private RelativeLayout rl_c_relative;
    private TextView txt_c_name;
    android.widget.ImageView img_loading1;

    public CategoryAdapter(Context context, ArrayList<CategoryModel> categoryModelArrayList) {
        this.context = context;
        this.categoryModelArrayList = categoryModelArrayList;
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration imgconfig = ImageLoaderConfiguration.createDefault(context);
        imageLoader.init(imgconfig);
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)/*.showImageOnFail(context.getResources().getDrawable(R.drawable.user_noimage))*/.
                        build();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;
        gridView = inflater.inflate(R.layout.category_item, null, false);
        img_c_image = (ImageView) gridView.findViewById(R.id.img_c_image);
        img_c_category = (ImageView) gridView.findViewById(R.id.img_c_category);
        img_loading1 = (android.widget.ImageView) gridView.findViewById(R.id.img_loading1);
        rl_c_relative = (RelativeLayout) gridView.findViewById(R.id.rl_c_relative);
        txt_c_name = (TextView) gridView.findViewById(R.id.txt_c_name);
        txt_c_name.setText(categoryModelArrayList.get(position).title);
//        img_loading1.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading1.getDrawable();
        frameAnimation.setCallback(img_loading1);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        imageLoader.displayImage(categoryModelArrayList.get(position).icon, img_c_category, options);
//        imageLoader.displayImage(categoryModelArrayList.get(position).image, img_c_image, options);
        return gridView;
    }

    @Override
    public int getCount() {
        AppDelegate.LogT("categoryModelArrayList.size()==" + categoryModelArrayList.size());
        return categoryModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}