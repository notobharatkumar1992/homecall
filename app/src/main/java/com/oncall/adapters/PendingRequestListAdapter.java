package com.oncall.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.activities.MainActivity;
import com.oncall.activities.PendingRequestDetailActivity;
import com.oncall.constants.Tags;
import com.oncall.models.RequestModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class PendingRequestListAdapter extends RecyclerView.Adapter<PendingRequestListAdapter.ViewHolder> {

    private ArrayList<RequestModel> mainArrayList;
    private View v;
    private FragmentActivity context;
    public Prefs prefs;
    public int action;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_request_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RequestModel orderModel = mainArrayList.get(position);
        String str_placed_on = orderModel.booking_date.substring(0, orderModel.booking_date.indexOf("T"));
        holder.txt_c_customer_name.setText(orderModel.name);
        holder.txt_c_placed_on.setText(str_placed_on + "   |   " + orderModel.booking_time);
        holder.txt_c_reference_no.setText(orderModel.reference_id);
        if (orderModel.categoryModel != null)
            holder.txt_c_service.setText(orderModel.categoryModel.title);
        holder.main_containt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.LogT("item clicked => " + position);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.order_id, orderModel.id);
                bundle.putParcelable(Tags.request_model, orderModel);
                bundle.putInt(Tags.from, MainActivity.FROM_ORDER_LIST);
                Intent intent = new Intent(context, PendingRequestDetailActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false,
                        new Pair<>(holder.ll_order_detail, context.getString(R.string.pending_requests)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                intent.putExtras(bundle);
                context.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        });
    }

    public PendingRequestListAdapter(FragmentActivity context, ArrayList<RequestModel> mainArrayList) {
        this.context = context;
        this.mainArrayList = mainArrayList;
    }

    @Override
    public int getItemCount() {
        return mainArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_customer_name, txt_c_placed_on, txt_c_reference_no, txt_c_service;
        RelativeLayout main_containt;
        LinearLayout ll_order_detail;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_customer_name = (TextView) itemView.findViewById(R.id.txt_c_customer_name);
            txt_c_placed_on = (TextView) itemView.findViewById(R.id.txt_c_placed_on);
            txt_c_reference_no = (TextView) itemView.findViewById(R.id.txt_c_reference_no);
            txt_c_service = (TextView) itemView.findViewById(R.id.txt_c_service);
            main_containt = (RelativeLayout) itemView.findViewById(R.id.main_containt);
            ll_order_detail = (LinearLayout) itemView.findViewById(R.id.ll_order_detail);
        }
    }
}