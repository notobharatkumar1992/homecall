package com.oncall.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oncall.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by Bharat on 10/03/2016.
 */
public class MyPagerLoopAdapter extends android.support.v4.view.PagerAdapter {
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    public static int LOOPS_COUNT = 4000;
    int count;

    public MyPagerLoopAdapter(int count) {
        this.count = count;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        imageLoader.init(ImageLoaderConfiguration.createDefault(view.getContext()));
        position = position % count; // use modulo for infinite cycling
        ImageView imageView = new ImageView(view.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        switch (position) {
            case 0:
                imageLoader.displayImage("drawable://" + R.drawable.tutorial_1, imageView);
//                Picasso.with(view.getContext()).load(R.drawable.tutorial_01).into(imageView);
                break;
            case 1:
                imageLoader.displayImage("drawable://" + R.drawable.tutorial_2, imageView);
//                Picasso.with(view.getContext()).load(R.drawable.tutorial_02).into(imageView);
                break;
            case 2:
                imageLoader.displayImage("drawable://" + R.drawable.tutorial_3, imageView);
//                Picasso.with(view.getContext()).load(R.drawable.tutorial_03).into(imageView);
                break;
            case 3:
                imageLoader.displayImage("drawable://" + R.drawable.tutorial_4, imageView);
//                Picasso.with(view.getContext()).load(R.drawable.tutorial_04).into(imageView);
                break;
        }
        view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }


    @Override
    public int getCount() {
        return count * LOOPS_COUNT; // simulate infinite by big number of products
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}