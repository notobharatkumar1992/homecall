package com.oncall.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.activities.MainActivity;
import com.oncall.activities.ManageJobsActivity;
import com.oncall.activities.MyProfileActivity;
import com.oncall.activities.NotificationUserActivity;
import com.oncall.adapters.PagerAdapter;
import com.oncall.constants.Constants;
import com.oncall.models.CategoryModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;
import java.util.List;

import carbon.widget.TextView;


/**
 * Created by Heena on 26-07-2016.
 */
public class VendorDashboardFragment extends Fragment implements View.OnClickListener {

    public static Handler mHandler;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();
    android.widget.LinearLayout pager_indicator;
    ViewPager view_pager;
    private ArrayList<CategoryModel> categoryModelArrayList;
    GridView grid_category;
    TextView txt_c_address, txt_c_manage_requests;
    ArrayList<String> imageArrayList;
    Prefs prefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Constants.isMapScreen = true;
        return inflater.inflate(R.layout.dashboard_driver_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_HOME);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    private void initView(View view) {
//        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
//        if (new Prefs(getActivity()).getLocationdata() != null)
//            txt_c_address.setText(new Prefs(getActivity()).getLocationdata().name);
//        txt_c_address.setOnClickListener(this);

        txt_c_manage_requests = (TextView) view.findViewById(R.id.txt_c_manage_requests);

        view.findViewById(R.id.ll_c_manage_requsets).setOnClickListener(this);
        view.findViewById(R.id.img_c_manage_requests).setOnClickListener(this);
        view.findViewById(R.id.ll_c_manage_jobs).setOnClickListener(this);
        view.findViewById(R.id.img_c_manage_jobs).setOnClickListener(this);
        view.findViewById(R.id.ll_c_edit_profile).setOnClickListener(this);
        view.findViewById(R.id.img_c_edit_profile).setOnClickListener(this);
        view.findViewById(R.id.img_c_notification).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_address:
                break;

            case R.id.ll_c_manage_requsets:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new PendingRequestFragment(), R.id.main_containt);
                break;
            case R.id.img_c_manage_requests:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new PendingRequestFragment(), R.id.main_containt);
                break;

            case R.id.ll_c_manage_jobs:
                startActivity(new Intent(getActivity(), ManageJobsActivity.class));
                break;

            case R.id.img_c_manage_jobs:
                startActivity(new Intent(getActivity(), ManageJobsActivity.class));
                break;

            case R.id.ll_c_edit_profile:
                startActivity(new Intent(getActivity(), MyProfileActivity.class));
                break;
            case R.id.img_c_edit_profile:
                startActivity(new Intent(getActivity(), MyProfileActivity.class));
                break;

            case R.id.img_c_notification:
                startActivity(new Intent(getActivity(), NotificationUserActivity.class));
                break;

        }
    }
}
