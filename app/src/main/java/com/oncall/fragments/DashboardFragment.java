package com.oncall.fragments;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ScrollView;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.activities.CategoryServiceListActivity;
import com.oncall.activities.MainActivity;
import com.oncall.activities.NotificationUserActivity;
import com.oncall.activities.VendorSearchDashboardActivity;
import com.oncall.adapters.CategoryAdapter;
import com.oncall.adapters.PagerAdapter;
import com.oncall.constants.Constants;
import com.oncall.constants.Tags;
import com.oncall.models.CategoryModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 26-07-2016.
 */
public class DashboardFragment extends Fragment implements View.OnClickListener {

    public static Handler mHandler;
    private ScrollView sc_main;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();
    android.widget.LinearLayout pager_indicator;
    ViewPager view_pager;
    private ArrayList<CategoryModel> categoryModelArrayList;
    GridView grid_category;
    ArrayList<String> imageArrayList;
    Prefs prefs;
    public EditText et_search;
    public View view;
    android.widget.LinearLayout pager_indicator_hero;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Constants.isMapScreen = true;
        return inflater.inflate(R.layout.dashboard_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
        executedashBoardAsync();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_HOME);
        if (view != null)
            if (new Prefs(getActivity()).getUserdata() != null && AppDelegate.isValidString(new Prefs(getActivity()).getUserdata().id + "")) {
                view.findViewById(R.id.img_c_notification).setOnClickListener(this);
            } else {
                view.findViewById(R.id.img_c_notification).setVisibility(View.GONE);
            }
    }

    private void executedashBoardAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getDashboard(new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    if (!isAdded())
                        return;
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray array = obj_json.getJSONArray(Tags.banner);
                            imageArrayList = new ArrayList<String>();
                            for (int i = 0; i < array.length(); i++) {
                                imageArrayList.add(array.getJSONObject(i).getString(Tags.image));
                            }
                            setBanner(imageArrayList);
                            JSONArray categJsonArray = obj_json.getJSONArray(Tags.category);
                            categoryModelArrayList = new ArrayList<>();
                            for (int i = 0; i < categJsonArray.length(); i++) {
                                CategoryModel serviceModel = new CategoryModel();
                                serviceModel.id = categJsonArray.getJSONObject(i).getInt(Tags.id);
                                serviceModel.title = categJsonArray.getJSONObject(i).getString(Tags.category);
                                serviceModel.image = categJsonArray.getJSONObject(i).getString(Tags.image);
                                serviceModel.icon = categJsonArray.getJSONObject(i).getString(Tags.icon);
//                                serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                categoryModelArrayList.add(serviceModel);
                            }
                            prefs.setCategoryArrayList(categoryModelArrayList);
                            setGrid_category(categoryModelArrayList);

                            sc_main.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    sc_main.fullScroll(ScrollView.FOCUS_UP);
                                }
                            }, 50);
                        } else {
                            AppDelegate.showToast(getActivity(), obj_json.getString(Tags.message));
                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    if (!isAdded())
                        return;
                    try {
                        mHandler.sendEmptyMessage(11);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(getActivity(), obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(getActivity(), "Please try again.");
                    }
                }
            });
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
        view = null;
    }

    private void initView(View view) {
        et_search = (EditText) view.findViewById(R.id.et_search);
        et_search.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_Regular)));
        if (new Prefs(getActivity()).getUserdata() != null && AppDelegate.isValidString(new Prefs(getActivity()).getUserdata().id + "")) {
            view.findViewById(R.id.img_c_notification).setOnClickListener(this);
        } else {
            view.findViewById(R.id.img_c_notification).setVisibility(View.GONE);
        }

        view.findViewById(R.id.view_vendor_search).setOnClickListener(this);
        view.findViewById(R.id.img_c_search).setOnClickListener(this);
        sc_main = (ScrollView) view.findViewById(R.id.sc_main);
        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
        pager_indicator = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator);
        pager_indicator_hero = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator_hero);
        pager_indicator.setOnClickListener(this);
        grid_category = (GridView) view.findViewById(R.id.grid_category);
        grid_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), CategoryServiceListActivity.class);
                intent.putExtra(Tags.name, categoryModelArrayList.get(position).title);
                intent.putExtra(Tags.image_url, categoryModelArrayList.get(position).image);
                intent.putExtra(Tags.cat_id, categoryModelArrayList.get(position).id);
                for (int i = 0; i < categoryModelArrayList.size(); i++) {
                    categoryModelArrayList.get(i).checked = 0;
                }
                categoryModelArrayList.get(position).checked = 1;
                intent.putParcelableArrayListExtra(Tags.category, categoryModelArrayList);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                        new Pair<>(view.findViewById(R.id.txt_c_name), "square_blue_name"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        });

        view.findViewById(R.id.rl_c_previous).setOnClickListener(this);
        view.findViewById(R.id.rl_c_next).setOnClickListener(this);
    }

    private android.widget.ImageView dots_Hero[];
    private int dotsCount, dotsCountHero, item_position = 0, selected_tab = 0, click_type = 0;

    private void switchBannerPage_Hero(int position) {
        if (dotsCountHero > 0) {
            for (int i = 0; i < dotsCountHero; i++) {
                dots_Hero[i].setImageResource(R.drawable.white_radius_square);
            }
            dots_Hero[position].setImageResource(R.drawable.yellow_radius_square);
        }
    }

    public void setBanner(ArrayList<String> imageArray) {
        for (int i = 0; i < imageArray.size(); i++) {
            Fragment fragment = new BannerFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Tags.DATA, imageArray.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }

        pager_indicator_hero.removeAllViews();
        dotsCountHero = bannerFragment.size();
        if (dotsCountHero > 0) {
            dots_Hero = new android.widget.ImageView[dotsCountHero];
            for (int i = 0; i < dotsCountHero; i++) {
                dots_Hero[i] = new android.widget.ImageView(getActivity());
                dots_Hero[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams(AppDelegate.getInstance(getActivity()).dpToPix(getActivity(), 7), AppDelegate.getInstance(getActivity()).dpToPix(getActivity(), 7));
                params.setMargins(AppDelegate.getInstance(getActivity()).dpToPix(getActivity(), 3), 0, AppDelegate.getInstance(getActivity()).dpToPix(getActivity(), 3), 0);
                pager_indicator_hero.addView(dots_Hero[i], params);
            }
            dots_Hero[0].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
        }

        AppDelegate.LogT("bannerFragment.size ==> " + bannerFragment.size() + "");
        bannerPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), bannerFragment);
        view_pager.setAdapter(bannerPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage_Hero(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage_Hero(0);
        timeOutLoop();
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        int value = view_pager.getCurrentItem();
                        if (value == bannerPagerAdapter.getCount() - 1) {
                            value = 0;
                        } else {
                            value++;
                        }
                        if (value < bannerPagerAdapter.getCount())
                            view_pager.setCurrentItem(value, true);
                        if (isAdded()) {
                            mHandler.postDelayed(this, 3000);
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    public void setGrid_category(ArrayList<CategoryModel> categoryModelArrayList) {
        int grid_height = AppDelegate.dpToPix(getActivity(), 130);
        AppDelegate.LogT("grid_height = " + grid_height + "");
        int no_of_items = categoryModelArrayList.size();
        AppDelegate.LogT("no_of_items => " + no_of_items + ", no_of_items % 3 == 0 => " + (no_of_items % 3 == 0));
        if (no_of_items % 3 == 0) {
            no_of_items = no_of_items / 3;
            AppDelegate.LogT("true no_of_items => " + no_of_items);
        } else {
            int testValue1 = no_of_items + 1;
            int testValue2 = no_of_items + 2;
            if (testValue1 % 3 == 0) {
                no_of_items = (no_of_items + 1) / 3;
            } else if (testValue2 % 3 == 0) {
                no_of_items = (no_of_items + 2) / 3;
            }
            AppDelegate.LogT("false no_of_items => " + no_of_items);
        }
//      no_of_items = no_of_items / 2;
        AppDelegate.LogT("Number of rows= " + no_of_items + "");
        ViewGroup.LayoutParams params = grid_category.getLayoutParams();
        params.height = no_of_items * grid_height;
        AppDelegate.LogT("height of grid view =" + "in pixel =" + no_of_items * grid_height + "In dp=" + AppDelegate.pixToDP(getActivity(), no_of_items * grid_height));
        grid_category.setLayoutParams(params);
        grid_category.requestLayout();
        CategoryAdapter dealsCategoryAdapter = new CategoryAdapter(getActivity(), categoryModelArrayList);
        grid_category.setAdapter(dealsCategoryAdapter);
        grid_category.invalidate();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_c_previous:
                if (view_pager.getAdapter().getCount() > 0)
                    if (view_pager.getCurrentItem() == 0) {
                        view_pager.setCurrentItem(view_pager.getAdapter().getCount() - 1);
                    } else {
                        view_pager.setCurrentItem(view_pager.getCurrentItem() - 1);
                    }
                break;
            case R.id.rl_c_next:
                if (view_pager.getAdapter().getCount() > 0)
                    if (view_pager.getCurrentItem() == view_pager.getAdapter().getCount() - 1) {
                        view_pager.setCurrentItem(0);
                    } else {
                        view_pager.setCurrentItem(view_pager.getCurrentItem() + 1);
                    }
                break;

            case R.id.pager_indicator: {
                Intent intent = new Intent(getActivity(), VendorSearchDashboardActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                        new Pair<>(pager_indicator, "search")
                        /*,new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name))*/);
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
            }

            case R.id.view_vendor_search: {
                Intent intent = new Intent(getActivity(), VendorSearchDashboardActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                        new Pair<>(pager_indicator, "search")
                        /*,new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name))*/);
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
            }

            case R.id.img_c_notification: {
                startActivity(new Intent(getActivity(), NotificationUserActivity.class));
                break;
            }
        }
    }
}
