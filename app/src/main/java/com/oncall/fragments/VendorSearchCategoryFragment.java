package com.oncall.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.activities.MainActivity;
import com.oncall.adapters.VendorSearchAdapter;
import com.oncall.constants.Tags;
import com.oncall.models.CategoryModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


/**
 * Created by Heena on 26-07-2016.
 */
public class VendorSearchCategoryFragment extends Fragment implements View.OnClickListener {
    public static Handler mHandler;
    private RecyclerView rv_category;
    private ArrayList<CategoryModel> categoryModelArrayList;
    TextView txt_c_titlename, txt_c_address;
    Prefs prefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.vendor_search_fragment, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
        executedashBoardAsync();
        new Prefs(getActivity()).setAddressData(null);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void executedashBoardAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getDashboard(new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray array = obj_json.getJSONArray(Tags.banner);
                            JSONArray categJsonArray = obj_json.getJSONArray(Tags.category);
                            categoryModelArrayList = new ArrayList<>();
                            for (int i = 0; i < categJsonArray.length(); i++) {
                                CategoryModel serviceModel = new CategoryModel();
                                serviceModel.id = categJsonArray.getJSONObject(i).getInt(Tags.id);
                                serviceModel.title = categJsonArray.getJSONObject(i).getString(Tags.category);
                                serviceModel.image = categJsonArray.getJSONObject(i).getString(Tags.image);
                                categoryModelArrayList.add(serviceModel);
                            }
                            setCategory(categoryModelArrayList);
                        } else {
                            AppDelegate.showToast(getActivity(), obj_json.getString(Tags.message));
                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(getActivity(), obj_json);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void setCategory(ArrayList<CategoryModel> categoryModelArrayList) {
        VendorSearchAdapter vendorSearchAdapter = new VendorSearchAdapter(getActivity(), categoryModelArrayList);
        rv_category.setAdapter(vendorSearchAdapter);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    private void initView(View view) {
        txt_c_titlename = (TextView) view.findViewById(R.id.txt_c_titlename);
        txt_c_titlename.setText("Vendor Search");
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        if (new Prefs(getActivity()).getLocationdata() != null)
            txt_c_address.setText(new Prefs(getActivity()).getLocationdata().name);
        txt_c_address.setOnClickListener(this);

        rv_category = (RecyclerView) view.findViewById(R.id.rv_category);
        rv_category.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_category.setHasFixedSize(true);
        rv_category.setItemAnimator(new DefaultItemAnimator());

        view.findViewById(R.id.img_c_menu).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_address:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new MapFragment(), R.id.main_containt);
                break;
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }
}
