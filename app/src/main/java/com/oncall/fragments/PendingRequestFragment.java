package com.oncall.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.activities.MainActivity;
import com.oncall.adapters.PendingRequestListAdapter;
import com.oncall.constants.Constants;
import com.oncall.constants.Tags;
import com.oncall.interfaces.PageReload;
import com.oncall.models.CategoryModel;
import com.oncall.models.CustomerModel;
import com.oncall.models.RequestModel;
import com.oncall.models.ServiceModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 28-Mar-17.
 */
public class PendingRequestFragment extends Fragment implements View.OnClickListener, PageReload {

    public static PageReload pageReload;
    public static boolean needToUpdate = false;
    public Prefs prefs;
    public Handler mHandler;
    public ImageView img_c_menu;
    public TextView txt_c_titlename;

    public ArrayList<RequestModel> arrayRequest = new ArrayList<>();

    RecyclerView rv_category;
    PendingRequestListAdapter manageRequestAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Constants.isMapScreen = true;
        pageReload = this;
        return inflater.inflate(R.layout.vendor_search_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
    }

    @Override
    public void onResume() {
        super.onResume();
        execute_manageRequest();
    }

    private void initView(View view) {
        txt_c_titlename = (TextView) view.findViewById(R.id.txt_c_titlename);
        txt_c_titlename.setText("Pending Requests");

        img_c_menu = (ImageView) view.findViewById(R.id.img_c_menu);
        img_c_menu.setOnClickListener(this);

        rv_category = (RecyclerView) view.findViewById(R.id.rv_category);
        rv_category.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_category.setHasFixedSize(true);
        rv_category.setItemAnimator(new DefaultItemAnimator());

    }

    private void setAdapter() {
        manageRequestAdapter = new PendingRequestListAdapter(getActivity(), arrayRequest);
        rv_category.setAdapter(manageRequestAdapter);
    }


    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                }
            }
        };
    }

    private void execute_manageRequest() {
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().manageRequest(prefs.getUserdata().id + "", new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        arrayRequest.clear();
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray jsonArray = obj_json.getJSONArray(Tags.OrderData);
                            for (int k = 0; k < jsonArray.length(); k++) {
                                JSONObject object = jsonArray.getJSONObject(k);
                                RequestModel requestModel = new RequestModel();
                                requestModel.id = object.getString(Tags.id);
                                requestModel.category_id = object.getString(Tags.category_id);
                                requestModel.subcategory_id = object.getString(Tags.subcategory_id);
                                requestModel.price = object.getString(Tags.price);
                                requestModel.currency_id = object.getString(Tags.currency_id);
                                requestModel.user_id = object.getString(Tags.user_id);
                                requestModel.vendor_id = object.getString(Tags.vendor_id);
//                                requestModel.orderCheck = JSONParser.getInt(object, Tags.orderCheck);

                                requestModel.reference_id = object.getString(Tags.reference_id);
                                requestModel.booking_date = object.getString(Tags.booking_date);
                                requestModel.booking_time = object.getString(Tags.booking_time);
                                requestModel.comment = object.getString(Tags.comment);
                                requestModel.name = object.getString(Tags.name);
                                requestModel.phone = object.getString(Tags.phone);
                                requestModel.email = object.getString(Tags.email);
                                requestModel.city = object.getString(Tags.city);
                                requestModel.state = object.getString(Tags.state);
                                requestModel.address = object.getString(Tags.address);
                                requestModel.address2 = object.getString(Tags.address_2);
                                requestModel.status = object.getString(Tags.status);
                                requestModel.created = object.getString(Tags.created);

                                if (AppDelegate.isValidString(object.optString(Tags.Categories))) {
                                    JSONObject categoryJSONObject = object.getJSONObject(Tags.Categories);
                                    requestModel.categoryModel = new CategoryModel();
                                    requestModel.categoryModel.id = categoryJSONObject.getInt(Tags.id);
                                    requestModel.categoryModel.title = categoryJSONObject.getString(Tags.category);
                                    requestModel.categoryModel.image = categoryJSONObject.getString(Tags.image);
                                    requestModel.categoryModel.icon = categoryJSONObject.getString(Tags.icon);
                                }

                                if (AppDelegate.isValidString(object.optString(Tags.Customer))) {
                                    JSONObject customerJSONObject = object.getJSONObject(Tags.Customer);
                                    requestModel.customerModel = new CustomerModel();
                                    requestModel.customerModel.id = customerJSONObject.getString(Tags.id);
                                    requestModel.customerModel.role_id = customerJSONObject.getString(Tags.role_id);
                                    requestModel.customerModel.name = customerJSONObject.getString(Tags.name);
                                    requestModel.customerModel.email = customerJSONObject.getString(Tags.email);
                                    requestModel.customerModel.last_name = customerJSONObject.getString(Tags.last_name);
                                    requestModel.customerModel.profile_pic = customerJSONObject.getString(Tags.profile_pic);
                                    requestModel.customerModel.contact_no = customerJSONObject.getString(Tags.contact_no);
                                    requestModel.customerModel.address1 = customerJSONObject.getString(Tags.address1);
                                    requestModel.customerModel.currency_id = customerJSONObject.getString(Tags.currency_id);
                                    requestModel.customerModel.latitude = customerJSONObject.getString(Tags.latitude);
                                    requestModel.customerModel.longitude = customerJSONObject.getString(Tags.longitude);
                                    requestModel.customerModel.full_name = customerJSONObject.getString(Tags.full_name);
                                }

                                if (AppDelegate.isValidString(obj_json.optString(Tags.service))) {
                                    JSONArray array = obj_json.getJSONArray(Tags.service);
                                    ArrayList<ServiceModel> serviceArray = new ArrayList<ServiceModel>();
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < array.length(); i++) {
                                        stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                        ServiceModel serviceModel = new ServiceModel();
                                        serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
                                        serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
                                        serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
                                        serviceModel.description = array.getJSONObject(i).optString(Tags.description);
                                        serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                        serviceArray.add(serviceModel);
                                    }
                                    requestModel.arrayServices = serviceArray;
                                }
                                arrayRequest.add(requestModel);
                            }
                        } else {
                            AppDelegate.showToast(getActivity(), obj_json.getString(Tags.message));
                        }
                        setAdapter();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        if (error != null) {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                            AppDelegate.checkJsonMessage(getActivity(), obj_json);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        pageReload = null;
    }

    @Override
    public void needToReloadPage(String name, String status) {
        if (name.equalsIgnoreCase(Tags.RELOAD)) {
            execute_manageRequest();
        }
    }
}
