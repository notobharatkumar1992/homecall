package com.oncall.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.activities.ChangePasswordActivity;
import com.oncall.activities.MainActivity;
import com.oncall.models.UserDataModel;
import com.oncall.utils.Prefs;
import com.oncall.utils.TransitionHelper;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class SettingFragment extends Fragment implements View.OnClickListener {

    public Handler mHandler;
    public TextView txt_c_range;
    public UserDataModel userDataModel;
    private RelativeLayout rl_c_change_password, rl_c_upgrade_membership;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            Drawable background = this.getResources().getDrawable(R.color.colorPrimary);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        return inflater.inflate(R.layout.setting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_SETTING);
        userDataModel = new Prefs(getActivity()).getUserdata();
        initView(view);
        setValues();
        setHandler();
    }
    public void initView(View view) {
        txt_c_range = (TextView) view.findViewById(R.id.txt_c_range);
        rl_c_change_password = (RelativeLayout) view.findViewById(R.id.rl_c_change_password);
        rl_c_upgrade_membership = (RelativeLayout) view.findViewById(R.id.rl_c_upgrade_membership);
        txt_c_range.setOnClickListener(this);
        view.findViewById(R.id.img_c_menu).setOnClickListener(this);
        view.findViewById(R.id.txt_c_change_password).setOnClickListener(this);
//        view.findViewById(R.id.txt_c_upgrade).setOnClickListener(this);
    }

    public void setValues() {
//        if (AppDelegate.isValidString(new Prefs(SettingActivity.this).getStringValuefromTemp(Tags.social_id, ""))) {
//            rl_c_change_password.setVisibility(View.GONE);
//        } else {
//            rl_c_change_password.setVisibility(View.VISIBLE);
//        }
//        if (AppDelegate.isValidString(new Prefs(getActivity()).getUserdata().social_id)) {
//            rl_c_change_password.setVisibility(View.GONE);
//        } else {
//            rl_c_change_password.setVisibility(View.VISIBLE);
//        }
       /* if (new Prefs(this).getUserdata() != null) {
            if (new Prefs(this).getUserdata().membership_id == 2) {
                rl_c_upgrade_membership.setVisibility(View.GONE);
            } else {
                rl_c_upgrade_membership.setVisibility(View.VISIBLE);
            }
        } else {
            rl_c_upgrade_membership.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_SETTING);
        setValues();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                }
            }
        };
    }

    public Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
            case R.id.txt_c_range:
//                showRangeDialog();
                break;
            case R.id.txt_c_change_password: {
                intent = new Intent(getActivity(), ChangePasswordActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false, new Pair<>(getView().findViewById(R.id.txt_c_change_password_ph), "change_password"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
            }
//            case R.id.txt_c_upgrade: {
//                intent = new Intent(SettingActivity.this, UpgradeMembershipActivity.class);
//                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false, new Pair<>(getView().findViewById(R.id.txt_c_upgrade_ph), "upgrade_membership"));
//                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
//                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
//                break;
            }
        }
    }

//    private void showRangeDialog() {
//        final Dialog rangeDialog = new Dialog(this, android.R.style.Theme_Light);
//        rangeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        rangeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
//        rangeDialog.setContentView(R.layout.dialog_range);
//
//        final TextView txt_c_range = (TextView) rangeDialog.findViewById(R.id.txt_c_range);
//        txt_c_range.setText(int_seek_range + "");
//        SeekBar sb_range = (SeekBar) rangeDialog.findViewById(R.id.sb_range);
//        try {
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//
//                Drawable wrapDrawable = DrawableCompat.wrap(sb_range.getIndeterminateDrawable());
//                DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(this, R.color.ripple_color));
//                sb_range.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
//            } else {
//                sb_range.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.ripple_color), PorterDuff.Mode.SRC_IN);
//            }
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
//        final int MIN_VALUE = 1;
//        sb_range.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                AppDelegate.LogT("setOnSeekBarChangeListener progress => " + progress);
//                if (progress < MIN_VALUE) {
//                /* if seek bar value is lesser than min value then set min value to seek bar */
//                    seekBar.setProgress(MIN_VALUE);
//                    int_seek_range = 1;
//                    txt_c_range.setText(int_seek_range + "");
//                    AppDelegate.LogT("setOnSeekBarChangeListener progress after  => " + progress);
//                } else {
//                    int_seek_range = progress;
//                    txt_c_range.setText(int_seek_range + "");
//                }
//            }
//
//            public void onStartTrackingTouch(SeekBar arg0) {
//                //do something
//                txt_c_range.setText(int_seek_range + "");
//            }
//
//            public void onStopTrackingTouch(SeekBar seekBar) {
//                //do something
//                txt_c_range.setText(int_seek_range + "");
//            }
//
//        });
//        sb_range.setProgress(int_seek_range);
//
//        rangeDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                execute_updateRangeApi(int_seek_range + "");
//                rangeDialog.dismiss();
//            }
//        });
//
//        rangeDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rangeDialog.dismiss();
//            }
//        });
//        rangeDialog.show();
//    }


