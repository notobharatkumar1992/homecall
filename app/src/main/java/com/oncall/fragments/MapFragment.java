package com.oncall.fragments;

import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.oncall.AppDelegate;
import com.oncall.Async.LocationAddress;
import com.oncall.Async.PlacesService;
import com.oncall.R;
import com.oncall.activities.MainActivity;
import com.oncall.adapters.LocationMapAdapter;
import com.oncall.constants.Tags;
import com.oncall.expandablelayout.ExpandableRelativeLayout;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.LocationModel;
import com.oncall.models.Place;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;
import com.oncall.utils.WorkaroundMapFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


/**
 * Created by HEENA on 07/29/2016.
 */
public class MapFragment extends Fragment implements OnListItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, WorkaroundMapFragment.OnTouchListener, View.OnClickListener {

    private GoogleMap map_business;
    private String name = "", country = "", state = "", city = "", image = "", company_name = "";
    private Handler mHandler;
    private TextView txt_c_title, txt_c_save;
    ImageView img_c_loading1;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    WorkaroundMapFragment map;
    TextView txt_c_address;
    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;

    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    private EditText et_search;
    public static final int COLLAPSE = 0, EXPAND = 1;
    Prefs prefs;
    LinearLayout ll_c_select_location;
    LocationModel locationModel;
    boolean fetchFromMapLocation = false;
    private boolean canSearch = true;
    public boolean fromCurrentLocation = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            Drawable background = this.getResources().getDrawable(R.color.colorPrimary);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        return inflater.inflate(R.layout.map_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (new Prefs(getActivity()).getLocationdata() != null) {
            locationModel = new Prefs(getActivity()).getLocationdata();
            currentLatitude = Double.parseDouble(locationModel.lat);
            currentLongitude = Double.parseDouble(locationModel.lng);
            canSearch = false;
            fromCurrentLocation = false;
        } else {
            locationModel = new LocationModel();
            fromCurrentLocation = true;
        }
        initGPS();
        showGPSalert();
        setHandler();
        initView(view);
    }

    boolean multipleTimesFocus = false;

    public void createMarker() {
        if (map_business != null)
            map_business.clear();
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        AppDelegate.LogT("create Marker==>" + currentLatitude + "," + currentLongitude + ", status = " + status + ", multipleTimesFocus => " + multipleTimesFocus);
        if (status == ConnectionResult.SUCCESS) {
            try {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatitude, currentLongitude))
                        .zoom(15.0f).build();
                if (!multipleTimesFocus) {
                    map_business.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    multipleTimesFocus = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), status);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AppDelegate.LogT("After 1 sec => fetchFromMapLocation = true ");
                fetchFromMapLocation = true;
            }
        }, 1500);
    }

    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    getActivity(), 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapFragment.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView(View view) {
        txt_c_title = (TextView) view.findViewById(R.id.txt_c_title);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);

        map = (WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map);
        map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map_business = googleMap;
                showMap();
            }
        });
        et_search = (EditText) view.findViewById(R.id.et_search);
        et_search.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_Regular)));
        canSearch = false;
        if (new Prefs(getActivity()).getLocationdata() != null) {
            txt_c_address.setText(new Prefs(getActivity()).getLocationdata().name);
            et_search.setText(new Prefs(getActivity()).getLocationdata().name);
        }
        addtextWatcher();
        canSearch = true;
        ll_c_select_location = (LinearLayout) view.findViewById(R.id.ll_c_select_location);
        ll_c_select_location.setOnClickListener(this);
//      ll_c_select_location.setVisibility(View.GONE);
        no_location_text_pickup = (TextView) view.findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) view.findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(getActivity(), mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) view.findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) view.findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);
        view.findViewById(R.id.ll_c_location).setOnClickListener(this);
//        AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.service_error_title), getActivity().getString(R.string.service_error_message), "OK");

    }

    private void addtextWatcher() {
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                AppDelegate.LogT("et_search afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private String place_name = "", place_address = "";

    public void setResultFromGeoCoderApi(Bundle bundle) {
        mHandler.sendEmptyMessage(11);
        if (bundle.getString(Tags.PLACE_NAME) != null) {
            place_name = bundle.getString(Tags.PLACE_NAME);
            place_address = bundle.getString(Tags.PLACE_ADD);
            showMap();
        } else {
            AppDelegate.showToast(getActivity(), getResources().getString(R.string.no_location_available));
        }
    }

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
        LocationAddress.getAddressFromLocation(arg0.latitude, arg0.longitude, getActivity(), mHandler);
    }

    private void showMap() {
        if (map_business == null) {
            return;
        }
        map.setListener(this);
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(true);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        try {
            View locationButton = ((View) map.getView().findViewById(0x1).getParent()).findViewById(0x2);
//            locationButton.setVisibility(View.GONE);
            android.widget.RelativeLayout.LayoutParams rlp = (android.widget.RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            rlp.addRule(android.widget.RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.setMargins(30, 30, 20, 20);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, android.widget.RelativeLayout.TRUE);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        createMarker();
        map_business.clear();
        multipleTimesFocus = true;
//        mHandler.sendEmptyMessage(10);
//        new Handler(getActivity().getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//                AppDelegate.setStictModePermission();
//                setloc(currentLatitude, currentLongitude);
//            } // This is your code
//        });
        map_business.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (fetchFromMapLocation) {
                    canSearch = false;
                    AppDelegate.LogT("centerLat => " + cameraPosition.target.latitude + ", " + cameraPosition.target.longitude + "");
                    currentLatitude = cameraPosition.target.latitude;
                    currentLongitude = cameraPosition.target.longitude;
                    setloc(currentLatitude, currentLongitude);
                }
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchFromMapLocation = true;
//                canSearch = true;
            }
        }, 1000);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 2:
//                        getLatLngFromGEOcoder(msg.getData());
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        mHandler.sendEmptyMessage(10);
                        AppDelegate.LogT("mHandler.sendEmptyMessage(3) called choose location Activity;");
                        LocationAddress.getLocationFromReverseGeocoder(et_search.getText().toString(), getActivity(), mHandler);
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null) {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0) {
                            if (et_search.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        if (canSearch) {
                            if (et_search.length() != 0) {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            if (et_search.length() != 0) {
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 9:
                        AppDelegate.LogT("Handler 9 called for fetching lat lng from address");
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_search.length() != 0) {
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;
                }
            }
        };
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude = " + currentLatitude + ", longitude = " + currentLongitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            try {
                currentLatitude = data.getDouble(Tags.latitude);
                currentLongitude = data.getDouble(Tags.longitude);
                locationModel.lat = currentLatitude + "";
                locationModel.lng = currentLongitude + "";
                locationModel.name = et_search.getText().toString() + "";
                locationModel.location_type = Tags.other;
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            fetchFromMapLocation = false;
            multipleTimesFocus = false;
            createMarker();
        } else {
        }
    }

    private void setAddressFromGEOcoder(Bundle data) {
        AppDelegate.LogT("setAddressFromGEOcoder called => " + data.toString());
        mHandler.sendEmptyMessage(11);
        if (AppDelegate.isValidString(data.getString(Tags.ADDRESS))) {
            AppDelegate.LogT("Address is valid => " + data.getString(Tags.ADDRESS));
            currentLatitude = data.getDouble(Tags.LAT);
            currentLongitude = data.getDouble(Tags.LNG);
            canSearch = false;
            et_search.setText(data.getString(Tags.ADDRESS));
            locationModel.lat = currentLatitude + "";
            locationModel.lng = currentLongitude + "";
            locationModel.name = data.getString(Tags.ADDRESS) + "";
            locationModel.location_type = Tags.other + "";
            multipleTimesFocus = true;
            fetchFromMapLocation = false;
            createMarker();
        } else {
            AppDelegate.LogT("Address is not valid => " + data.getString(Tags.ADDRESS));
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AppDelegate.LogT("After 1 sec => fetchFromMapLocation = true ");
                canSearch = true;
            }
        }, 1500);
    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_search.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(et_search.getText().toString()));
                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, MapFragment.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            new Prefs(getActivity()).putStringValue(Tags.LAT, String.valueOf(location.getLatitude()));
            new Prefs(getActivity()).putStringValue(Tags.LNG, String.valueOf(location.getLongitude()));
            if (fromCurrentLocation) {
                canSearch = false;
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                setloc(currentLatitude, currentLongitude);
                AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            }
        }
    }

    private void setloc(double latitude, double longitude) {
        mHandler.sendEmptyMessage(10);
        LocationAddress.getAddressFromLocation(latitude, longitude, getActivity(), mHandler);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);
        new Prefs(getActivity()).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
        new Prefs(getActivity()).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapFragment.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onTouch() {
        AppDelegate.LogT("on touch called");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_c_location:
                getMyLoc();
                break;
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
            case R.id.ll_c_select_location:
                executeSearchServiceAsync();
                break;
        }
    }

    private void executeSearchServiceAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().checkLocation(new Prefs(getActivity()).getUserdata().id + "", locationModel.lat, locationModel.lng, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    if (!isAdded())
                        return;
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(getActivity(), obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(getActivity(), getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    if (!isAdded())
                        return;
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            new Prefs(getActivity()).setLocationData(locationModel);
                            getActivity().getSupportFragmentManager().popBackStack();
                        } else {
                            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.service_error_title), getActivity().getString(R.string.service_error_message), "OK");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void getMyLoc() {
        if (map_business.getMyLocation() != null) { // Check to ensure coordinates aren't null, probably a better way of doing this...
//            map_business.setCenterCoordinate(new LatLngZoom(map_business.getMyLocation().getLatitude(), map_business.getMyLocation().getLongitude(), 20), true);
            LatLng latLng = new LatLng(map_business.getMyLocation().getLatitude(), map_business.getMyLocation().getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
            map_business.moveCamera(cameraUpdate);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(getActivity());
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(getActivity())) {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position) {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_search.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                } else if (AppDelegate.isValidString(placeDetail_pickup.name)) {
                    et_search.setText(placeDetail_pickup.name);
                } else if (AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_search.setText(placeDetail_pickup.vicinity);
                }
                et_search.setSelection(et_search.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(10);
                stopTimer();
                onlyStopThread(mThreadOnSearch_pickup);
                LocationAddress.getLocationFromReverseGeocoder(et_search.getText().toString(), getActivity(), mHandler);
            }
            canSearch = true;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {
    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }
}
