package com.oncall.net;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

public interface APIInterface {

     /*              Common web services             */

    @FormUrlEncoded
    @POST("/login")
    void userLogin(@Field("email") String email,
                   @Field("password") String password,
                   @Field("device_type") String device_type,
                   @Field("device_id") String device_id,
                   @Field("device_unique_id") String device_unique_id,
                   Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/checkSocialLogin")
    void checkSocialLogin(@Field("email") String email,
                          @Field("social_id") String social_id,
                          @Field("type") String type,
                          @Field("device_type") String device_type,
                          @Field("device_id") String device_id,
                          @Field("device_unique_id") String device_unique_id,
                          Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/accountVerify")
    void accountVerify(@Field("email") String email,
                       @Field("phone_otp") String password,
                       @Field("email_activation_code") String device_id,
                       Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/resendActivation")
    void resendActivation(@Field("email") String email,
                          @Field("phone") String password,
                          Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/signUp")
    void userRegistration(
            @Field("name") String name,
            @Field("last_name") String last_name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("address") String address,
            @Field("phone") String phone,
            @Field("device_type") int device_type,
            @Field("device_id") String device_id,
            @Field("business_name") String business_name,
            @Field("category") String category,
            @Field("service") String service,
            @Field("latitude") String lat,
            @Field("longitude") String lng,
            @Field("city") String city,
            @Field("state") String state,
            @Field("country") String country,
            @Field("zip") String zip,
            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/signUp")
    void signUp(
            @Field("first_name") String name,
            @Field("last_name") String last_name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("address") String address,
            @Field("phone") String phone,
            @Field("device_type") int device_type,
            @Field("device_id") String device_id,
            @Field("latitude") String lat,
            @Field("longitude") String lng,
            @Field("city") String city,
            @Field("state") String state,
            @Field("country") String country,
            @Field("zip") String zip,
            @Field("device_unique_id") String device_unique_id,
            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/signUp")
    void signUp(
            @Field("first_name") String name,
            @Field("last_name") String last_name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("address") String address,
            @Field("phone") String phone,
            @Field("device_type") int device_type,
            @Field("device_id") String device_id,
            @Field("latitude") String lat,
            @Field("longitude") String lng,
            @Field("city") String city,
            @Field("state") String state,
            @Field("country") String country,
            @Field("zip") String zip,
            @Field("social_id") String social_id,
            @Field("profile_pic") String profile_pic,
            @Field("type") String type,
            @Field("device_unique_id") String device_unique_id,
            Callback<Response> responseCallback);


    @Multipart
    @POST("/editProfile")
    void editProfile(@Part("user_id") String user_id,
                     @Part("sessionid") String sessionid,
                     @Part("image") TypedFile image,
                     @Part("first_name") String first_name,
                     @Part("last_name") String last_name,
                     @Part("address") String address,
                     @Part("address2") String address2,
                     @Part("phone") String phone,
                     @Part("device_type") int device_type,
                     @Part("device_id") String device_id,
                     @Part("city") String city,
                     @Part("city2") String city2,
                     @Part("state") String state,
                     Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/editProfile")
    void editProfile(@Field("user_id") String user_id,
                     @Field("sessionid") String sessionid,
                     @Field("first_name") String first_name,
                     @Field("last_name") String last_name,
                     @Field("address") String address,
                     @Field("address2") String address2,
                     @Field("phone") String phone,
                     @Field("device_type") int device_type,
                     @Field("device_id") String device_id,
                     @Field("city") String city,
                     @Field("city_2") String city2,
                     @Field("state") String state,
                     Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/forgotPassword")
    void forgotPassword(@Field("email") String email,
                        Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/myProfile")
    void myProfile(@Field("user_id") String user_id,
                   @Field("sessionid") String sessionid,
                   Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/changePassword")
    void changePassword(@Field("user_id") String user_id,
                        @Field("sessionid") String sessionid,
                        @Field("new_password") String new_password,
                        @Field("old_password") String old_password,
                        Callback<Response> responseCallback);


    /*              Customer web services           */


    @FormUrlEncoded
    @POST("/checkActivation")
    void checkActivation(@Field("email") String email,
                         Callback<Response> responseCallback);

    @POST("/getCategory")
    void getCategory(Callback<Response> responseCallback);


//    @POST("/getCurrency")
//    void getCurrency(Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/getService")
    void getServices(@Field("cat_id") int category_id,
                     Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/checkLocation")
    void checkLocation(@Field("user_id") String user_id,
                       @Field("latitude") String latitude,
                       @Field("longitude") String longitude,
                       Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/contactUs")
    void contactUs(@Field("email") String email,
                   @Field("subject") String subject,
                   @Field("message") String message,
                   Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/checkLocation")
    void checkLocation(@Field("user_id") String user_id,
                       @Field("latitude") String latitude,
                       @Field("longitude") String longitude,
                       @Field("cat_id") String cat_id,
                       Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/getVendors")
    void getVendors(@Field("cat_id") String cat_id,
                    @Field("latitude") String lat,
                    @Field("longitude") String lng,
                    Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/searchServices")
    void searchServices(@Field("keyword") String keyword,
                        Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/getCategoryDetail")
    void getCategoryDetail(@Field("cat_id") String cat_id,
                           Callback<Response> responseCallback);

    @POST("/getDashboard")
    void getDashboard(Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/changeCurrency")
    void changeCurrency(@Field("user_id") String user_id,
                        @Field("currency_id") String currency_id,
                        Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/getVendorServices")
    void getVendorServices(@Field("user_id") String user_id,
                           @Field("vendor_id") String vendor_id,
                           Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/bookOrder")
    void bookOrder(
            @Field("user_id") String user_id,
            @Field("sessionid") String sessionid,
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("instruction") String instruction,
            @Field("address") String address,
            @Field("time") String time,
            @Field("date") String date,
            @Field("price") String price,
            @Field("category_id") String category_id,
            @Field("service_id") String service_id,
            @Field("address_2") String address_2,
            @Field("city") String city,
            @Field("city_2") String city_2,
            @Field("state") String state,
            @Field("zip") String zip,
            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/bookOrder")
    void bookOrderToVendor(
            @Field("user_id") String user_id,
            @Field("vendor_id") String vendor_id,
            @Field("landmark") String landmark,
            @Field("name") String name,
            @Field("street") String street,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("instruction") String instruction,
            @Field("address") String address,
            @Field("time") String time,
            @Field("date") String date,
            @Field("currency_id") String currency_id,
            @Field("price") String price,
            @Field("category_id") String category_id,
            @Field("service_id") String service_id,
            @Field("lat") String lat,
            @Field("lng") String lng,
            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/myOrder")
    void myOrder(@Field("user_id") String user_id,
                 @Field("sessionid") String sessionid,
                 @Field("cat_id") String category_id,
                 Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/orderDetail")
    void orderDetail(@Field("user_id") String user_id,
                     @Field("sessionid") String sessionid,
                     @Field("order_id") String order_id,
                     Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/cancelOrder")
    void cancelOrder(@Field("user_id") String user_id,
                     @Field("sessionid") String sessionid,
                     @Field("order_id") String order_id,
                     Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/vendorKeywordSearch")
    void vendorKeywordSearch(@Field("user_id") String user_id,
                             @Field("keyword") String keyword,
                             @Field("latitude") String lat,
                             @Field("longitude") String lng,
                             Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/cancelOrder")
    void cancelJob(@Field("user_id") String user_id,
                   @Field("sessionid") String sessionid,
                   @Field("order_id") String keyword,
                   @Field("comment") String lat,
                   Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/resheduleOrder")
    void orderReschedule(@Field("user_id") String user_id,
                         @Field("sessionid") String sessionid,
                         @Field("order_id") String order_id,
                         @Field("date") String date,
                         @Field("time") String time,
                         @Field("instruction") String comment,
                         Callback<Response> responseCallback);


    /*              Vendor web services             */

    @FormUrlEncoded
    @POST("/manageRequest")
    void manageRequest(@Field("vendor_id") String user_id,
                       Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/manageJobs")
    void manageJobs(@Field("vendor_id") String user_id,
                    Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/actionRequest")
    void actionRequest(@Field("order_id") String order_id,
                       @Field("action") String action,
                       Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/addRating")
    void addRating(@Field("user_id") String user_id,
                   @Field("sessionid") String sessionid,
                   @Field("order_id") String order_id,
                   @Field("rating") String rating,
                   @Field("comment") String comment,
                   Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/viewRating")
    void viewRating(@Field("user_id") String user_id,
                    @Field("vendor_id") String vendor_id,
                    Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/notificationList")
    void notificationList(@Field("user_id") String user_id,
                          @Field("sessionid") String sessionid,
                          Callback<Response> responseCallback);

//    @FormUrlEncoded
//    @POST("/markView")
//    void marknotifyFromNotificationList(@Field("notification_id") String notification_id,
//                                        @Field("sessionid") String sessionid,
//                                        @Field("action") String action,
//                                        Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/markView")
    void markView(@Field("user_id") String user_id,
                  @Field("sessionid") String sessionid,
                  @Field("notification_id") String notification_id,
                  Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/getTImeZone")
    void getTImeZone(@Field("service_id") String service_id,
                     Callback<Response> responseCallback);

    @POST("/aboutUs")
    void aboutUs(Callback<Response> responseCallback);

    @POST("/privacyPolicy")
    void privacyPolicy(Callback<Response> responseCallback);


    @POST("/termsCondition")
    void termsCondition(Callback<Response> responseCallback);

    @POST("/help")
    void help(Callback<Response> responseCallback);

    @POST("/contactDetail")
    void contactDetail(Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/otpForPhoneNumber")
    void otpForPhoneNumber(@Field("user_id") String user_id,
                           @Field("sessionid") String sessionid,
                           @Field("new_phone_number") String new_phone_number,
                           Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/changePhoneNumber")
    void changePhoneNumber(@Field("user_id") String user_id,
                           @Field("sessionid") String sessionid,
                           @Field("new_phone_number") String new_phone_number,
                           @Field("otp") String otp,
                           Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/checkDueAmount")
    void checkDueAmount(@Field("user_id") String user_id,
                        @Field("sessionid") String sessionid,
                        Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/completeOrder")
    void completeOrder(@Field("user_id") String user_id,
                       @Field("sessionid") String sessionid,
                       @Field("transaction_id") String transaction_id,
                       @Field("order_id") String order_id,
                       @Field("amount") String amount,
                       Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/logout")
    void logout(@Field("user_id") String user_id,
                Callback<Response> responseCallback);


}



