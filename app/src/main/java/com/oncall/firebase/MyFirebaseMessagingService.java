package com.oncall.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.activities.MainActivity;
import com.oncall.activities.MyOrderActivity;
import com.oncall.activities.MyOrderDetailActivity;
import com.oncall.constants.Tags;
import com.oncall.models.NotificationModel;
import com.oncall.utils.Prefs;

import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFireBaseMsgService";
    public static final int USER_CUSTOMER = 0, USER_VENDOR = 1;
    public static final String NEW_ORDER = "New Order", CANCEL_ORDER = "job Cancellation", ORDER_STATUS = "Order Status", ORDER_RESCHEDULE = "Order Reschedule", UPCOMING_OFFER = "Upcoming Offer", NEW_SERVICE_INTRODUCED = "New Service Introduced", VENDOR_ARRIVAL = "Vendor Arrival";

    public Prefs prefs;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        prefs = new Prefs(this);
        Log.d(TAG, "remoteMessage: " + remoteMessage.getData());
        try {
            if (remoteMessage.getData().get(Tags.TYPE).equalsIgnoreCase(UPCOMING_OFFER + "") || remoteMessage.getData().get(Tags.TYPE).equalsIgnoreCase(NEW_SERVICE_INTRODUCED + ""))
                showActionNotificationUser(this, remoteMessage.getData());
            else if (prefs.getUserdata() != null && prefs.getUserdata().id > 0)
                showActionNotificationUser(this, remoteMessage.getData());
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public static void showActionNotificationUser(Context mContext, Map<String, String> messageModel) {
        try {
            String message = messageModel.get(Tags.message);

            Notification.Builder notificationBuilder = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle(mContext.getString(R.string.app_name))
                        .setStyle(new Notification.BigTextStyle().bigText(message))
                        .setContentText(message);
            } else {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle(message)
                        .setContentText(message);
            }

            Bundle bundle = new Bundle();
            AppDelegate.LogT("messageModel.get(Tags.TYPE) => " + messageModel.get(Tags.TYPE) + ", 1 => " + ORDER_STATUS + ", " + NEW_ORDER);
            if (messageModel.get(Tags.TYPE).equalsIgnoreCase(ORDER_STATUS + "")
                    || messageModel.get(Tags.TYPE).equalsIgnoreCase(NEW_ORDER + "")
                    || messageModel.get(Tags.TYPE).equalsIgnoreCase(ORDER_RESCHEDULE + "")
                    || messageModel.get(Tags.TYPE).equalsIgnoreCase(VENDOR_ARRIVAL)) {
                if (MyOrderDetailActivity.pageReload != null)
                    MyOrderDetailActivity.pageReload.needToReloadPage(Tags.RELOAD, messageModel.get(Tags.order_id));
                else if (MyOrderActivity.pageReload != null)
                    MyOrderActivity.pageReload.needToReloadPage(Tags.RELOAD, messageModel.get(Tags.order_id));

                bundle.putString(Tags.FROM, ORDER_STATUS + "");
                bundle.putString(Tags.user_type, messageModel.get(Tags.user_type) + "");
                bundle.putString(Tags.message, messageModel.get(Tags.message) + "");
                bundle.putString(Tags.order_id, messageModel.get(Tags.order_id) + "");
                bundle.putString(Tags.notification_id, messageModel.get(Tags.notification_id) + "");
                bundle.putString(Tags.TYPE, messageModel.get(Tags.TYPE) + "");
                bundle.putInt(Tags.from, MainActivity.FROM_PUSH_NOTIFICATION);
            } else if (messageModel.get(Tags.TYPE).equalsIgnoreCase(UPCOMING_OFFER + "")
                    || messageModel.get(Tags.TYPE).equalsIgnoreCase(NEW_SERVICE_INTRODUCED + "")) {
                bundle.putString(Tags.FROM, messageModel.get(Tags.TYPE) + "");
                bundle.putString(Tags.user_type, messageModel.get(Tags.user_type) + "");
                bundle.putString(Tags.message, messageModel.get(Tags.message) + "");
                bundle.putString(Tags.order_id, messageModel.get(Tags.order_id) + "");
                bundle.putString(Tags.notification_id, messageModel.get(Tags.notification_id) + "");
                bundle.putString(Tags.TYPE, messageModel.get(Tags.TYPE) + "");
                bundle.putInt(Tags.from, MainActivity.FROM_PUSH_NOTIFICATION);

                NotificationModel notificationModel = new NotificationModel();
                notificationModel.id = messageModel.get(Tags.notification_id);
                notificationModel.message = messageModel.get(Tags.message);
                notificationModel.title = messageModel.get(Tags.title);
                notificationModel.subtitle = messageModel.get(Tags.subtitle);
                notificationModel.tickerText = messageModel.get(Tags.tickerText);
                notificationModel.user_type = messageModel.get(Tags.user_type);
                notificationModel.order_id = messageModel.get(Tags.order_id);
                bundle.putParcelable(Tags.notification, notificationModel);
            }

            Intent push = new Intent(mContext, NotificationHandler.class);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            push.putExtras(bundle);
            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(mContext, 0, push, PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder.setContentIntent(fullScreenPendingIntent);

            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setContentIntent(fullScreenPendingIntent)/*.setFullScreenIntent(fullScreenPendingIntent, false)*/;
                notification = notificationBuilder.build();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                notification = new Notification(R.drawable.app_icon, message, System.currentTimeMillis());
            } else {
                notification = notificationBuilder.build();
            }
            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            ((NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public static int getRandomNumer() {
        Random r = new Random();
        int value = r.nextInt(80 - 65) + 65;
        return value;
    }
}