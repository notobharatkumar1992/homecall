package com.oncall.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.oncall.AppDelegate;
import com.oncall.activities.MainActivity;
import com.oncall.activities.MyOrderDetailActivity;
import com.oncall.activities.NotificationDetailActivity;
import com.oncall.constants.Tags;
import com.oncall.utils.Prefs;

/**
 * Created by Bharat on 06/13/2016.
 */
public class NotificationHandler extends FragmentActivity {

    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getIntent().getExtras();
        AppDelegate.LogT("NotificationHandler => " + bundle);
        if (new Prefs(this).getUserdata() != null && AppDelegate.isValidString(new Prefs(this).getUserdata().id + "")) {

        } else {
            AppDelegate.LogE("Notification handler user data is null.");
            finish();
            return;
        }
        if (bundle != null && AppDelegate.isValidString(bundle.getString(Tags.user_type))) {
            if (bundle.getString(Tags.user_type).equalsIgnoreCase(MyFirebaseMessagingService.USER_CUSTOMER + "")) {
                if (bundle.getString(Tags.type).equalsIgnoreCase(MyFirebaseMessagingService.ORDER_STATUS + "")
                        || bundle.getString(Tags.type).equalsIgnoreCase(MyFirebaseMessagingService.NEW_ORDER + "")
                        || bundle.getString(Tags.type).equalsIgnoreCase(MyFirebaseMessagingService.ORDER_RESCHEDULE + "")
                        || bundle.getString(Tags.TYPE).equalsIgnoreCase(MyFirebaseMessagingService.VENDOR_ARRIVAL)) {
                    if (MainActivity.mActivity == null)
                        startActivity(new Intent(this, MainActivity.class));
                    Intent intent = new Intent(NotificationHandler.this, MyOrderDetailActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else if (bundle.getString(Tags.type).equalsIgnoreCase(MyFirebaseMessagingService.UPCOMING_OFFER + "")
                        || bundle.getString(Tags.type).equalsIgnoreCase(MyFirebaseMessagingService.NEW_SERVICE_INTRODUCED + "")) {
                    if (MainActivity.mActivity == null)
                        startActivity(new Intent(this, MainActivity.class));
                    Intent intent = new Intent(NotificationHandler.this, NotificationDetailActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            } else {
                Intent intent = new Intent(NotificationHandler.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            AppDelegate.LogT("NotificationHandler startActivity called");
        } else {
            AppDelegate.LogE("NotificationHandler, From value or bundle is null.");
            Intent intent = new Intent(NotificationHandler.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        finish();
    }
}
