package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ScrollView;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryServiceListAdapter;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListCategoryItemClick;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.ServiceModel;
import com.oncall.models.UserDataModel;
import com.oncall.models.VendorModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ProgressBar;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class VendorSearchDashboardActivity extends AppCompatActivity implements View.OnClickListener, OnListCategoryItemClick {

    public static Activity mActivity;
    public Handler mHandler;
    public TextView txt_c_range, txt_c_no_vendor, txt_c_continue;
    public ProgressBar pb_c_progressbar;
    private ArrayList<ServiceModel> serviceArray = new ArrayList<>();
    EditText et_search;
    CategoryServiceListAdapter serviceListAdapter;
    RecyclerView rv_services;
    public String str_keyword = "";
    EditText et_instructions;
    public UserDataModel userDataModel;
    public ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.green);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.vendor_search_list_activity);
        userDataModel = new Prefs(this).getUserdata();
        str_keyword = getIntent().getStringExtra(Tags.keyword);
        mActivity = this;
        initView();
        setHandler();
    }

    public void initView() {
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        pb_c_progressbar = (ProgressBar) findViewById(R.id.pb_c_progressbar);
        txt_c_no_vendor = (TextView) findViewById(R.id.txt_c_no_vendor);
        txt_c_continue = (TextView) findViewById(R.id.txt_c_continue);
        txt_c_continue.setOnClickListener(this);

        et_instructions = (EditText) findViewById(R.id.et_instructions);
        et_instructions.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        rv_services = (RecyclerView) findViewById(R.id.rv_services);
        rv_services.setLayoutManager(new LinearLayoutManager(this));
        rv_services.setHasFixedSize(true);
        rv_services.setItemAnimator(new DefaultItemAnimator());
        findViewById(R.id.img_c_back).setOnClickListener(this);
        et_search = (EditText) findViewById(R.id.et_search);
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mHandler.sendEmptyMessage(5);
            }
        });
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
    }

    private void executeVendorsAsync() {
        str_keyword = et_search.getText().toString();
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(12);
            SingletonRestClient.get().searchServices(str_keyword, new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(13);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray array = obj_json.getJSONArray(Tags.response);
                            serviceArray = new ArrayList<>();
                            StringBuilder stringBuilder = new StringBuilder();
                            for (int i = 0; i < array.length(); i++) {
                                stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                ServiceModel serviceModel = new ServiceModel();
                                serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
                                serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
                                serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
                                serviceModel.description = array.getJSONObject(i).optString(Tags.description);
                                serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                serviceModel.minimum_time = obj_json.optString(Tags.minimum_time);
                                serviceModel.image = obj_json.optString(Tags.image);
                                serviceModel.how_works = obj_json.optString(Tags.how_works);
                                serviceModel.terms = obj_json.optString(Tags.terms);
                                serviceModel.cancellation_policy = obj_json.optString(Tags.cancellation_policy);
//                                if (i == 0) {
//                                    serviceModel.checked = 1;
//                                }
                                serviceArray.add(serviceModel);
                            }
                            mHandler.sendEmptyMessage(1);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    serviceListAdapter.onListItemClickListener.setOnListItemClickListener("", 0, true);
                                }
                            }, 300);
                        } else {
                            AppDelegate.showToast(VendorSearchDashboardActivity.this, obj_json.getString(Tags.message));
                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(13);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(VendorSearchDashboardActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userDataModel = new Prefs(this).getUserdata();
        setValues();
    }

    private void setValues() {
        if (userDataModel != null && userDataModel.id != -1) {
            txt_c_continue.setText("Continue");
        } else {
            txt_c_continue.setText("LOGIN");
        }
    }

    private boolean[] is_checked;
    public String service_id = "", service = "", title_name = "";

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(VendorSearchDashboardActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(VendorSearchDashboardActivity.this);
                        break;
                    case 12:
                        pb_c_progressbar.setVisibility(View.VISIBLE);
                        break;
                    case 13:
                        pb_c_progressbar.setVisibility(View.GONE);
                        break;

                    case 1:
                        if (serviceArray != null && serviceArray.size() > 0) {
                            AppDelegate.LogT("cityModels=");
                            if (is_checked == null || is_checked.length != serviceArray.size()) {
                                is_checked = new boolean[serviceArray.size()];
                                for (int i = 0; i < serviceArray.size(); i++) {
                                    if (service_id.equalsIgnoreCase(serviceArray.get(i).id + "")) {
                                        is_checked[i] = true;
                                    } else
                                        is_checked[i] = false;
                                }
                            } else {
                                for (int i = 0; i < is_checked.length; i++) {
                                    serviceArray.get(i).checked = is_checked[i] ? 1 : 0;
                                }
                            }
                            serviceListAdapter = new CategoryServiceListAdapter(VendorSearchDashboardActivity.this, serviceArray, new OnListItemClickListener() {
                                @Override
                                public void setOnListItemClickListener(String name, int position) {
                                }

                                @Override
                                public void setOnListItemClickListener(String name, int position, int PICK_UP) {

                                }

                                @Override
                                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                                    AppDelegate.LogT("interface value => " + like_dislike);
                                    for (int j = 0; j < is_checked.length; j++) {
                                        is_checked[j] = false;
                                    }
                                    for (int j = 0; j < serviceArray.size(); j++) {
                                        serviceArray.get(j).checked = 0;
                                    }
                                    serviceArray.get(position).checked = 1;
                                    serviceListAdapter.notifyDataSetChanged();
                                    rv_services.invalidate();
                                    StringBuilder stringBuilder = null;
                                    StringBuilder service_name = null;
                                    for (int i = 0; i < serviceArray.size(); i++) {
                                        service_id = "";
                                        service = "";
                                        if (serviceArray.get(i).checked == 1) {
                                            is_checked[i] = true;
                                            if (stringBuilder == null) {
                                                stringBuilder = new StringBuilder();
                                                stringBuilder.append("" + serviceArray.get(i).sub_category_id);
                                            } else {
                                                stringBuilder.append("," + serviceArray.get(i).sub_category_id);
                                            }
                                            if (service_name == null) {
                                                service_name = new StringBuilder();
                                                service_name.append("" + serviceArray.get(i).sub_category);
                                            } else {
                                                service_name.append(", " + serviceArray.get(i).sub_category);
                                            }
                                        } else {
                                            is_checked[i] = false;
                                        }
                                    }
                                    if (stringBuilder != null) {
                                        service_id = stringBuilder.toString();
                                        service = service_name.toString();
                                        serviceListAdapter.notifyDataSetChanged();
                                    } else {
                                    }
                                }
                            }, false, "");
                            rv_services.setAdapter(serviceListAdapter);
                            serviceListAdapter.notifyDataSetChanged();
                            rv_services.invalidate();
                            rv_services.setVisibility(View.VISIBLE);
                        } else {
                            rv_services.setVisibility(View.GONE);
                        }
                        break;

                    case 5:
                        if (et_search.length() != 0) {
                            pb_c_progressbar.setVisibility(View.VISIBLE);
                            stopStartThread(mThreadOnSearch_pickup);
                            txt_c_no_vendor.setVisibility(View.GONE);
                        } else {
                            serviceArray.clear();
                            mHandler.sendEmptyMessage(1);
                        }
                        break;
                    case 21:
                        txt_c_no_vendor.setText(getResources().getString(R.string.not_connected));
                        pb_c_progressbar.setVisibility(View.INVISIBLE);
                        txt_c_no_vendor.setVisibility(View.VISIBLE);
                        rv_services.setVisibility(View.GONE);
                        break;
                }
            }
        };
    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(theThread);
        if (!AppDelegate.haveNetworkConnection(VendorSearchDashboardActivity.this, false)) {
            txt_c_no_vendor.setText(getResources().getString(R.string.not_connected));
            pb_c_progressbar.setVisibility(View.INVISIBLE);
            txt_c_no_vendor.setVisibility(View.VISIBLE);
            rv_services.setVisibility(View.GONE);
        } else {
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    public Thread mThreadOnSearch_pickup;
    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(VendorSearchDashboardActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_search.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                executeVendorsAsync();
            }
        }
    };


    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    public Intent intent;
    public boolean needComment = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_continue:
                if (!AppDelegate.isValidString(service_id)) {
                    AppDelegate.showToast(VendorSearchDashboardActivity.this, getString(R.string.validation_select_services));
                } else if (userDataModel == null || userDataModel.id == -1) {
                    Intent intent = new Intent(VendorSearchDashboardActivity.this, SignInActivity.class);
                    intent.putExtra(Tags.FROM, "Service");
                    startActivity(intent);
                } else if (needComment || et_instructions.length() > 0) {
                    ArrayList<ServiceModel> listArray = new ArrayList<>();
                    for (int i = 0; i < serviceArray.size(); i++) {
                        if (serviceArray.get(i).checked == 1)
                            listArray.add(serviceArray.get(i));
                    }
                    Intent intent = new Intent(VendorSearchDashboardActivity.this, ScheduleJobActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Tags.from, MainActivity.BOOKING_FROM_CATEGORIES);
                    bundle.putString(Tags.service, service);
                    bundle.putString(Tags.service_id, service_id);
                    bundle.putString(Tags.description, et_instructions.getText().toString());
                    bundle.putParcelableArrayList(Tags.ARRAY_SERVICES, listArray);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
//                    scrollView.fullScroll(View.FOCUS_DOWN);
//                    et_instructions.setFocusable(true);
//                    et_instructions.setFocusableInTouchMode(true);
//                    AppDelegate.showKeyboard(VendorSearchDashboardActivity.this, et_instructions);
//                    needComment = true;
                    AppDelegate.showAlert(VendorSearchDashboardActivity.this, "Additional Notes", "Would you like to add additional notes for your job?", "YES", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                            et_instructions.setFocusable(true);
                            et_instructions.setFocusableInTouchMode(true);
                            AppDelegate.showKeyboard(VendorSearchDashboardActivity.this, et_instructions);
                            needComment = true;
                        }
                    }, "No", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ArrayList<ServiceModel> listArray = new ArrayList<>();
                            for (int i = 0; i < serviceArray.size(); i++) {
                                if (serviceArray.get(i).checked == 1)
                                    listArray.add(serviceArray.get(i));
                            }
                            Intent intent = new Intent(VendorSearchDashboardActivity.this, ScheduleJobActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt(Tags.from, MainActivity.BOOKING_FROM_CATEGORIES);
                            bundle.putString(Tags.service, service);
                            bundle.putString(Tags.service_id, service_id);
                            bundle.putString(Tags.description, et_instructions.getText().toString());
                            bundle.putParcelableArrayList(Tags.ARRAY_SERVICES, listArray);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            needComment = true;
                        }
                    });
                }
                break;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, VendorModel categoryModel, View view) {
        if (name.equalsIgnoreCase(Tags.city_name)) {
            AppDelegate.LogT("cityModel==" + categoryModel);
            Intent intent = new Intent(this, VendorSearchProfileActivity.class);
            intent.putExtra(Tags.vendor, categoryModel);
            final Pair<View, String>[] pairs = com.oncall.TransitionHelper.createSafeTransitionParticipants(this, false, new Pair<>(view, "square_blue_name"));
            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
            startActivity(intent/*, transitionActivityOptions.toBundle()*/);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }
}



