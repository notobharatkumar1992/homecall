package com.oncall.activities;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryDialogAdapter;
import com.oncall.adapters.ManageJobsListAdapter;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.interfaces.PageReload;
import com.oncall.models.CategoryModel;
import com.oncall.models.CustomerModel;
import com.oncall.models.RequestModel;
import com.oncall.models.ServiceModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 27-Mar-17.
 */
public class ManageJobsActivity extends AppCompatActivity implements OnListItemClickListener, View.OnClickListener, PageReload {

    public static PageReload pageReload;
    public Prefs prefs;
    public Handler mHandler;

    public ArrayList<RequestModel> mainArrayRequestModel = new ArrayList<>();
    public ArrayList<RequestModel> arrayRequestModel = new ArrayList<>();
    public RecyclerView recycler_view;
    public ManageJobsListAdapter manageJobsListAdapter;
    public LinearLayout ll_c_all, ll_c_completed, ll_c_work_on_progress, ll_c_accepted;
    //    0=>Pendinng,1=>Accepted,2=>Rejected,3=>Canceled,4=>Started,5=>Completed
    public static final int ORDER_ALL = -1, ORDER_PENDING = 0, ORDER_ACCEPTED = 1, ORDER_REJECTED = 2, ORDER_CANCELLED = 3, ORDER_STARTED = 4, ORDER_COMPLETED = 5;
    public int selected_page = ORDER_ALL;
    public boolean showDialogClicked = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_jobs_activity);
        pageReload = this;
        prefs = new Prefs(ManageJobsActivity.this);
        setHandler();
        categoryArray = prefs.getCategoryArrayList();
        if (categoryArray == null || categoryArray.size() == 0) {
            showDialogClicked = false;
            executedashBoardAsync();
        }
        initView();
        switchPage(ORDER_ALL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        executeManageJobsAsync();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ManageJobsActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ManageJobsActivity.this);
                        break;

                    case 1:
                        break;
                }
            }
        };
    }

    private void executeManageJobsAsync() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().manageJobs(new Prefs(ManageJobsActivity.this).getUserdata().id + "",
                    new Callback<Response>() {

                        @Override
                        public void success(Response response, Response response2) {
                            mHandler.sendEmptyMessage(11);

                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                if (obj_json.getInt(Tags.status) == 1) {
                                    arrayRequestModel.clear();
                                    mainArrayRequestModel.clear();
                                    JSONArray jsonArray = obj_json.getJSONArray(Tags.order);
                                    for (int j = 0; j < jsonArray.length(); j++) {
                                        JSONObject object = jsonArray.getJSONObject(j);
                                        RequestModel requestModel = new RequestModel();
                                        requestModel.id = object.getString(Tags.id);
                                        requestModel.category_id = object.getString(Tags.category_id);
                                        requestModel.subcategory_id = object.getString(Tags.subcategory_id);
                                        requestModel.price = object.getString(Tags.price);
                                        requestModel.currency_id = object.getString(Tags.currency_id);
                                        requestModel.user_id = object.getString(Tags.user_id);
                                        requestModel.vendor_id = object.getString(Tags.vendor_id);

                                        requestModel.reference_id = object.getString(Tags.reference_id);
                                        requestModel.booking_date = object.getString(Tags.booking_date);
                                        requestModel.booking_time = object.getString(Tags.booking_time);
                                        requestModel.comment = object.getString(Tags.comment);
                                        requestModel.name = object.getString(Tags.name);
                                        requestModel.phone = object.getString(Tags.phone);
                                        requestModel.email = object.getString(Tags.email);
                                        requestModel.city = object.getString(Tags.city);
                                        requestModel.state = object.getString(Tags.state);
                                        requestModel.address = object.getString(Tags.address);
                                        requestModel.address2 = object.getString(Tags.address_2);
//                                        requestModel.latitude = object.getString(Tags.latitude);
//                                        requestModel.longitude = object.getString(Tags.longitude);
                                        requestModel.status = object.getString(Tags.status);
                                        requestModel.created = object.getString(Tags.created);
//                                        requestModel.orderCheck = 1;

                                        if (AppDelegate.isValidString(object.optString(Tags.Categories))) {
                                            JSONObject categoryJSONObject = object.getJSONObject(Tags.Categories);
                                            requestModel.categoryModel = new CategoryModel();
                                            requestModel.categoryModel.id = categoryJSONObject.getInt(Tags.id);
                                            requestModel.categoryModel.title = categoryJSONObject.getString(Tags.category);
                                            requestModel.categoryModel.image = categoryJSONObject.getString(Tags.image);
                                            requestModel.categoryModel.icon = categoryJSONObject.getString(Tags.icon);
                                        }

                                        if (AppDelegate.isValidString(object.optString(Tags.Customer))) {
                                            JSONObject customerJSONObject = object.getJSONObject(Tags.Customer);
                                            requestModel.customerModel = new CustomerModel();
                                            requestModel.customerModel.id = customerJSONObject.getString(Tags.id);
                                            requestModel.customerModel.role_id = customerJSONObject.getString(Tags.role_id);
                                            requestModel.customerModel.name = customerJSONObject.getString(Tags.name);
                                            requestModel.customerModel.email = customerJSONObject.getString(Tags.email);
                                            requestModel.customerModel.last_name = customerJSONObject.getString(Tags.last_name);
                                            requestModel.customerModel.profile_pic = customerJSONObject.getString(Tags.profile_pic);
                                            requestModel.customerModel.contact_no = customerJSONObject.getString(Tags.contact_no);
                                            requestModel.customerModel.address1 = customerJSONObject.getString(Tags.address1);
                                            requestModel.customerModel.currency_id = customerJSONObject.getString(Tags.currency_id);
                                            requestModel.customerModel.latitude = customerJSONObject.getString(Tags.latitude);
                                            requestModel.customerModel.longitude = customerJSONObject.getString(Tags.longitude);
                                            requestModel.customerModel.full_name = customerJSONObject.getString(Tags.full_name);
                                            if (AppDelegate.isValidString(customerJSONObject.optString(Tags.business_name)))
                                                requestModel.customerModel.business_name = customerJSONObject.optString(Tags.business_name);
                                            else requestModel.customerModel.business_name = "";
                                        }

                                        JSONArray array = object.getJSONArray(Tags.service);
                                        ArrayList<ServiceModel> serviceArray = new ArrayList<ServiceModel>();
                                        StringBuilder stringBuilder = new StringBuilder();
                                        for (int i = 0; i < array.length(); i++) {
                                            stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                            ServiceModel serviceModel = new ServiceModel();
                                            serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
                                            serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
                                            serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
                                            serviceModel.description = array.getJSONObject(i).optString(Tags.description);
                                            serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                            AppDelegate.LogT("Filling time price => " + serviceModel.price);
                                            serviceArray.add(serviceModel);
                                        }
                                        requestModel.arrayServices = serviceArray;
                                        arrayRequestModel.add(requestModel);
                                        mainArrayRequestModel.add(requestModel);

                                    }

                                    manageJobsListAdapter = new ManageJobsListAdapter(ManageJobsActivity.this, arrayRequestModel, ManageJobsActivity.this);
                                    recycler_view.setAdapter(manageJobsListAdapter);
                                    manageJobsListAdapter.filter(selected_page);

                                } else {
                                    AppDelegate.showToast(ManageJobsActivity.this, obj_json.getString(Tags.message));
                                }
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }

                        @Override
                        public void failure(RestError restError) {
                            mHandler.sendEmptyMessage(11);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(ManageJobsActivity.this, obj_json);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
        }
    }

    public ArrayList<RequestModel> testArrayRequestModel = new ArrayList<>();
    public String[] arrayCategory;

    private void refreshArrayAccordingCategory() {
        AppDelegate.LogT("refreshArrayAccordingCategory called, category_id => " + category_id);
        testArrayRequestModel = new ArrayList<>();
        testArrayRequestModel.addAll(mainArrayRequestModel);
        if (AppDelegate.isValidString(category_id)) {
            arrayRequestModel.clear();
            arrayCategory = category_id.split(",");
            AppDelegate.LogT("refreshArrayAccordingCategory called, arrayCategory => " + arrayCategory.length);
            for (int i = 0; i < arrayCategory.length; i++) {
                for (int j = 0; j < testArrayRequestModel.size(); j++) {
                    AppDelegate.LogT("arrayCategory[i] => " + arrayCategory[i] + " == " + testArrayRequestModel.get(j).category_id);
                    if (arrayCategory[i].equalsIgnoreCase(testArrayRequestModel.get(j).category_id)) {
                        if (arrayRequestModel.size() == 0) {
                            arrayRequestModel.add(testArrayRequestModel.get(j));
                        } else {
                            boolean shouldNotAdd = false;
                            for (int k = 0; k < arrayRequestModel.size(); k++) {
                                if (arrayRequestModel.get(k).id.equalsIgnoreCase(testArrayRequestModel.get(j).id)) {
                                    shouldNotAdd = true;
                                }
                            }
                            if (!shouldNotAdd) {
                                arrayRequestModel.add(testArrayRequestModel.get(j));
                            }
                        }
                    }
                }
            }
            AppDelegate.LogT("arrayRequestModel => " + arrayRequestModel.size());
            manageJobsListAdapter = new ManageJobsListAdapter(ManageJobsActivity.this, arrayRequestModel, ManageJobsActivity.this);
            recycler_view.setAdapter(manageJobsListAdapter);
            manageJobsListAdapter.filter(selected_page);
        } else {
            manageJobsListAdapter = new ManageJobsListAdapter(ManageJobsActivity.this, arrayRequestModel, ManageJobsActivity.this);
            recycler_view.setAdapter(manageJobsListAdapter);
            manageJobsListAdapter.filter(selected_page);
        }
    }

    private void initView() {
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        recycler_view.setNestedScrollingEnabled(false);
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        manageJobsListAdapter = new ManageJobsListAdapter(this, arrayRequestModel, this);
        recycler_view.setAdapter(manageJobsListAdapter);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.img_c_filter).setOnClickListener(this);

        ll_c_all = (LinearLayout) findViewById(R.id.ll_c_all);
        ll_c_all.setOnClickListener(this);
        ll_c_completed = (LinearLayout) findViewById(R.id.ll_c_completed);
        ll_c_completed.setOnClickListener(this);
        ll_c_work_on_progress = (LinearLayout) findViewById(R.id.ll_c_work_on_progress);
        ll_c_work_on_progress.setOnClickListener(this);
        ll_c_accepted = (LinearLayout) findViewById(R.id.ll_c_accepted);
        ll_c_accepted.setOnClickListener(this);

    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.name)) {
            arrayRequestModel.get(position);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;

            case R.id.img_c_filter:
                showDialogClicked = true;
                if (categoryArray != null && categoryArray.size() > 0) {
                    categoryDialog();
                } else {
                    executedashBoardAsync();
                }
                break;

            case R.id.ll_c_all:
                switchPage(ORDER_ALL);
                break;

            case R.id.ll_c_completed:
                switchPage(ORDER_COMPLETED);
                break;

            case R.id.ll_c_work_on_progress:
                switchPage(ORDER_STARTED);
                break;

            case R.id.ll_c_accepted:
                switchPage(ORDER_ACCEPTED);
                break;

        }
    }

    private RecyclerView recycler_deals;
    private boolean[] is_checked;
    public String category_id;
    private String currency_str = "", currencysymbbol_str = "", currencyslug_str = "";
    public ArrayList<CategoryModel> categoryArray;
    private CategoryDialogAdapter mAdapter;

    public void categoryDialog() {
        if (categoryArray != null) {
            final Dialog categoryDialog = new Dialog(this, android.R.style.Theme_Light);
            categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            categoryDialog.setContentView(R.layout.dialog_services);
            recycler_deals = (RecyclerView) categoryDialog.findViewById(R.id.recycler_deals);
            recycler_deals.setNestedScrollingEnabled(false);
            recycler_deals.setHasFixedSize(true);
            recycler_deals.setLayoutManager(new LinearLayoutManager(this));
            recycler_deals.setItemAnimator(new DefaultItemAnimator());
            TextView txt2 = (TextView) categoryDialog.findViewById(R.id.txt2);
            txt2.setText("Categories");

            if (is_checked == null || is_checked.length != categoryArray.size()) {
                is_checked = new boolean[categoryArray.size()];
                for (int i = 0; i < categoryArray.size(); i++) {
                    is_checked[i] = true;
                    categoryArray.get(i).checked = 1;
                }
            } else {
                for (int i = 0; i < is_checked.length; i++) {
                    categoryArray.get(i).checked = is_checked[i] ? 1 : 0;
                }
            }

            mAdapter = new CategoryDialogAdapter(this, categoryArray, new OnListItemClickListener() {
                @Override
                public void setOnListItemClickListener(String name, int position) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, int PICK_UP) {

                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                    categoryArray.get(position).checked = like_dislike ? 1 : 0;
                    mAdapter.notifyDataSetChanged();
                    recycler_deals.invalidate();
                }
            });
            recycler_deals.setAdapter(mAdapter);

            categoryDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuilder stringBuilder = null;
                    for (int i = 0; i < categoryArray.size(); i++) {
                        if (categoryArray.get(i).checked == 1) {
                            is_checked[i] = true;
                            if (stringBuilder == null) {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("" + categoryArray.get(i).id);
                            } else {
                                stringBuilder.append("," + categoryArray.get(i).id);
                            }
                        } else {
                            is_checked[i] = false;
                        }
                    }
                    if (stringBuilder != null) {
                        category_id = stringBuilder.toString();
                        refreshArrayAccordingCategory();
                        categoryDialog.dismiss();
                    } else {
                        AppDelegate.showToast(ManageJobsActivity.this, "Please select category.");
                    }
                }
            });
            categoryDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDialog.dismiss();
                }
            });
            categoryDialog.show();
        }
    }

    private void executedashBoardAsync() {
        if (AppDelegate.haveNetworkConnection(ManageJobsActivity.this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getDashboard(new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray categJsonArray = obj_json.getJSONArray(Tags.category);
                            categoryArray = new ArrayList<>();
                            for (int i = 0; i < categJsonArray.length(); i++) {
                                CategoryModel serviceModel = new CategoryModel();
                                serviceModel.id = categJsonArray.getJSONObject(i).getInt(Tags.id);
                                serviceModel.title = categJsonArray.getJSONObject(i).getString(Tags.category);
                                serviceModel.image = categJsonArray.getJSONObject(i).getString(Tags.image);
                                serviceModel.icon = categJsonArray.getJSONObject(i).getString(Tags.icon);
//                                serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                categoryArray.add(serviceModel);
                            }
                            if (showDialogClicked) {
                                categoryDialog();
                                showDialogClicked = false;
                            }
                        } else {
                            AppDelegate.showToast(ManageJobsActivity.this, obj_json.getString(Tags.message));
                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    try {
                        mHandler.sendEmptyMessage(11);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ManageJobsActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }


    private void switchPage(int value) {
        selected_page = value;
        ll_c_all.setSelected(false);
        ll_c_completed.setSelected(false);
        ll_c_work_on_progress.setSelected(false);
        ll_c_accepted.setSelected(false);
        switch (value) {
            case ORDER_ALL:
                ll_c_all.setSelected(true);
                manageJobsListAdapter.filter(ORDER_ALL);
                break;
            case ORDER_COMPLETED:
                ll_c_completed.setSelected(true);
                manageJobsListAdapter.filter(ORDER_COMPLETED);
                break;
            case ORDER_STARTED:
                ll_c_work_on_progress.setSelected(true);
                manageJobsListAdapter.filter(ORDER_STARTED);
                break;
            case ORDER_ACCEPTED:
                ll_c_accepted.setSelected(true);
                manageJobsListAdapter.filter(ORDER_ACCEPTED);
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        pageReload = null;
    }

    @Override
    public void needToReloadPage(String name, String status) {
        if (name.equalsIgnoreCase(Tags.RELOAD)) {
            executeManageJobsAsync();
        }
    }
}
