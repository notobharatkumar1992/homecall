package com.oncall.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.ReviewsListAdapter;
import com.oncall.constants.Tags;
import com.oncall.interfaces.PageReload;
import com.oncall.models.ReviewModel;
import com.oncall.models.VendorModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 07-Apr-17.
 */
public class VendorReviewsActivity extends AppCompatActivity implements View.OnClickListener, PageReload {

    public static PageReload pageReload;
    public Handler mHandler;
    public Prefs prefs;

    public TextView txt_c_titlename;
    public RecyclerView rv_category;
    public ReviewsListAdapter manageRequestAdapter;

    public VendorModel vendorModel;
    public String str_vendor_id = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        prefs = new Prefs(this);
        vendorModel = getIntent().getExtras().getParcelable(Tags.vendor);
        str_vendor_id = getIntent().getStringExtra(Tags.id);
        pageReload = this;
        initView();
        setHandler();
        execute_viewRating();
    }

    private void initView() {
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        txt_c_titlename.setText("Reviews & Ratings");

        findViewById(R.id.img_c_menu).setOnClickListener(this);

        rv_category = (RecyclerView) findViewById(R.id.rv_category);
        rv_category.setLayoutManager(new LinearLayoutManager(VendorReviewsActivity.this));
        rv_category.setHasFixedSize(true);
        rv_category.setItemAnimator(new DefaultItemAnimator());

    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(VendorReviewsActivity.this);
                        break;

                    case 11:
                        AppDelegate.hideProgressDialog(VendorReviewsActivity.this);
                        break;
                }
            }
        };
    }

    private void execute_viewRating() {
        if (AppDelegate.haveNetworkConnection(VendorReviewsActivity.this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().viewRating(prefs.getUserdata().id + "", str_vendor_id, new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray jsonArray = obj_json.getJSONArray(Tags.data);
                            ArrayList<ReviewModel> arrayReviews = new ArrayList<ReviewModel>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ReviewModel reviewModel = new ReviewModel();
                                reviewModel.id = jsonObject.getString(Tags.id);
                                reviewModel.rating = jsonObject.getString(Tags.rating);
                                reviewModel.review = jsonObject.getString(Tags.review);
                                reviewModel.order_id = jsonObject.getString(Tags.order_id);
                                reviewModel.created = jsonObject.getString(Tags.created);
                                try {
                                    reviewModel.subcategories = jsonObject.getJSONObject(Tags.order).getJSONArray(Tags.subcategories).getJSONObject(0).getString(Tags.subcategories);
                                    reviewModel.full_name = jsonObject.getJSONObject(Tags.customer).getString(Tags.full_name);
                                    arrayReviews.add(reviewModel);
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                            updateAdapter(arrayReviews);
                        } else {
                            AppDelegate.showToast(VendorReviewsActivity.this, obj_json.getString(Tags.message));
                        }
//                        setAdapter();
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        if (error != null) {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                            AppDelegate.checkJsonMessage(VendorReviewsActivity.this, obj_json);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void updateAdapter(ArrayList<ReviewModel> arrayReviews) {
        manageRequestAdapter = new ReviewsListAdapter(VendorReviewsActivity.this, arrayReviews);
        rv_category.setAdapter(manageRequestAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_menu:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pageReload = null;
    }

    @Override
    public void needToReloadPage(String name, String status) {
        if (name.equalsIgnoreCase(Tags.RELOAD)) {
            execute_viewRating();
        }
    }
}
