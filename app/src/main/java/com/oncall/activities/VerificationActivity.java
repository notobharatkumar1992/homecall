package com.oncall.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.models.UserDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.parser.JSONParser;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 24-Apr-17.
 */
public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {

    public Handler mHandler;

    public EditText et_phone_otp, et_email_otp;
    public String str_emailAddress = "", str_phone = "", mobileotp = "", emailotp = "", login_from = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_activity);
        login_from = getIntent().getStringExtra(Tags.FROM);
        str_emailAddress = getIntent().getStringExtra(Tags.email);
        str_phone = getIntent().getStringExtra(Tags.phone);
        mobileotp = getIntent().getStringExtra(Tags.mobileotp);
        emailotp = getIntent().getStringExtra(Tags.emailotp);
        initView();
        sethHandler();
        et_email_otp.setText(emailotp + "");
        et_phone_otp.setText(mobileotp + "");
    }

    private void sethHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(VerificationActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(VerificationActivity.this);
                        break;
                }
            }
        };
    }

    private void initView() {
        et_phone_otp = (EditText) findViewById(R.id.et_phone_otp);
        et_phone_otp.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_email_otp = (EditText) findViewById(R.id.et_email_otp);
        et_email_otp.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_resend_otp).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_resend_otp:
                executeResendActivation();
                break;

            case R.id.txt_c_submit:
                executeAccontVerify();
                break;
        }
    }

    private void executeAccontVerify() {
        if (et_phone_otp.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_phone_otp));
        } else if (et_email_otp.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_email_otp));
        } else if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().accountVerify(str_emailAddress, et_phone_otp.getText().toString(), et_email_otp.getText().toString(), new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            AppDelegate.sendEventTracker(VerificationActivity.this, "User verified");

                            AppDelegate.showToast(VerificationActivity.this, obj_json.getString(Tags.message));
                            JSONObject object = obj_json.getJSONObject(Tags.response);
                            UserDataModel userDataModel = new UserDataModel();
                            userDataModel.id = JSONParser.getInt(object, Tags.id);
                            userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                            userDataModel.email = JSONParser.getString(object, Tags.email);
                            userDataModel.first_name = JSONParser.getString(object, Tags.name);
                            userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                            userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                            userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                            userDataModel.address = JSONParser.getString(object, Tags.address1);
                            userDataModel.latitude = JSONParser.getString(object, Tags.latitude);
                            userDataModel.longitude = JSONParser.getString(object, Tags.longitude);
                            userDataModel.address2 = JSONParser.getString(object, Tags.address_2);
                            userDataModel.city = JSONParser.getString(object, Tags.city);
                            userDataModel.state = JSONParser.getString(object, Tags.state);
                            userDataModel.country = JSONParser.getString(object, Tags.country);
                            userDataModel.zip = JSONParser.getString(object, Tags.zip);
                            userDataModel.token = JSONParser.getString(object, Tags.token);
                            userDataModel.sessionid = JSONParser.getString(object, Tags.sessionid);


                            if (LoginTutorialActivity.mActivity != null)
                                LoginTutorialActivity.mActivity.finish();
                            if (SignInActivity.mActivity != null)
                                SignInActivity.mActivity.finish();
                            if (SignUpVendorActivity.mActivity != null)
                                SignUpVendorActivity.mActivity.finish();
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();

                            new Prefs(VerificationActivity.this).setUserData(userDataModel);
                            if (!AppDelegate.isValidString(login_from)) {
                                Intent intent = new Intent(VerificationActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                            SignInActivity.finishAllInitialActivities();
                            finish();

                        } else {
                            AppDelegate.showToast(VerificationActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.LogT("restError = " + new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())));
                        AppDelegate.checkJsonMessage(VerificationActivity.this, obj_json);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executeResendActivation() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().resendActivation(str_emailAddress, str_phone, new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        AppDelegate.showToast(VerificationActivity.this, obj_json.getString(Tags.message));
                        if (AppDelegate.isValidString(obj_json.optString(Tags.mobileotp)))
                            et_phone_otp.setText(obj_json.optString(Tags.mobileotp) + "");
                        if (AppDelegate.isValidString(obj_json.optString(Tags.emailotp)))
                            et_email_otp.setText(obj_json.optString(Tags.emailotp) + "");
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(VerificationActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }
}
