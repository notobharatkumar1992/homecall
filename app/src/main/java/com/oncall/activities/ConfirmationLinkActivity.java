package com.oncall.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.models.UserDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.parser.JSONParser;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 06-Dec-16.
 */

public class ConfirmationLinkActivity extends AppCompatActivity implements View.OnClickListener {

    public Handler mHandler;
    RelativeLayout rl_username;
    LinearLayout ll_or;
    TextView txt_c_msg, txt_c_thanks_msg, txt_c_description;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.green);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//          window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.confirmation_link_activity);
        initView();
        setHandler();
    }

    private void initView() {
        findViewById(R.id.txt_c_resend_link).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_c_logout).setOnClickListener(this);
        txt_c_msg = (TextView) findViewById(R.id.txt_c_msg);
        txt_c_thanks_msg = (TextView) findViewById(R.id.txt_c_thanks_msg);
        txt_c_description = (TextView) findViewById(R.id.txt_c_description);
        txt_c_description.setOnClickListener(this);
        rl_username = (RelativeLayout) findViewById(R.id.rl_username);
        ll_or = (LinearLayout) findViewById(R.id.ll_or);
        if (new Prefs(this) != null)
            if ((new Prefs(this).getTempUserdata()) != null)
                ((TextView) findViewById(R.id.txt_c_username)).setText(new Prefs(this).getTempUserdata().email);
        txt_c_msg.setVisibility(View.VISIBLE);
        txt_c_msg.setText(getString(R.string.thanks_detail_signup));
        rl_username.setVisibility(View.VISIBLE);
        txt_c_description.setVisibility(View.VISIBLE);
        txt_c_description.setText(getString(R.string.varified));
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ConfirmationLinkActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ConfirmationLinkActivity.this);
                        break;
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                executeVerifyAsync();
                break;
            case R.id.txt_c_description:
                executeVerifyAsync();
                break;
            case R.id.img_c_logout:
//                new Prefs(ConfirmationLinkActivity.this).clearTempPrefs();
                new Prefs(ConfirmationLinkActivity.this).setTempUserData(null);
                new Prefs(ConfirmationLinkActivity.this).clearSharedPreference();
                startActivity(new Intent(ConfirmationLinkActivity.this, LoginTutorialActivity.class));
                finish();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_resend_link:
//                check_validation();
                executeResendAsync();
                break;
        }
    }

    private void executeVerifyAsync() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            if ((new Prefs(this).getTempUserdata()) != null) {
                SingletonRestClient.get().checkActivation(new Prefs(this).getTempUserdata().email, new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        super.failure(error);
                        mHandler.sendEmptyMessage(11);
                        try {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                            AppDelegate.checkJsonMessage(ConfirmationLinkActivity.this, obj_json);
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(ConfirmationLinkActivity.this, "");
                        }
                    }

                    @Override
                    public void success(Response response, Response response2) {
                        mHandler.sendEmptyMessage(11);
                        try {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                            if (obj_json.getString(Tags.status).contains("1")) {
                                new Prefs(ConfirmationLinkActivity.this).setTempUserData(null);
//                                    AppDelegate.showToast(ConfirmationLinkActivity.this, obj_json.getString(Tags.message));
                                JSONObject object = obj_json.getJSONObject(Tags.response);
                                UserDataModel userDataModel = new UserDataModel();
                                userDataModel.id = JSONParser.getInt(object, Tags.id);
                                userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                                userDataModel.email = JSONParser.getString(object, Tags.email);
                                userDataModel.name = JSONParser.getString(object, Tags.name);
                                userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                                userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                                userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                                userDataModel.address = JSONParser.getString(object, Tags.address1);
                                userDataModel.latitude = JSONParser.getString(object, Tags.latitude);
                                userDataModel.longitude = JSONParser.getString(object, Tags.longitude);
                                userDataModel.address2 = JSONParser.getString(object, Tags.address_2);
                                userDataModel.city = JSONParser.getString(object, Tags.city);
                                userDataModel.state = JSONParser.getString(object, Tags.state);
                                userDataModel.country = JSONParser.getString(object, Tags.country);
                                userDataModel.zip = JSONParser.getString(object, Tags.zip);
                                userDataModel.token = JSONParser.getString(object, Tags.token);
                                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                                userDataModel.name_email = JSONParser.getString(object, Tags.name_email);
                                if (object.has(Tags.service)) {
                                    if (AppDelegate.isValidString(object.optString(Tags.service))) {
                                        JSONArray array = object.getJSONArray(Tags.service);
                                        StringBuilder stringBuilder = new StringBuilder();

                                        for (int i = 0; i < array.length(); i++) {
                                            stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                        }
                                        String service = stringBuilder.toString();
                                        service = service.substring(0, service.length() - 2);
                                        userDataModel.service = service;
                                    }
                                }
                                if (object.has(Tags.category)) {
                                    userDataModel.category = object.optString(Tags.category);
                                }
                                new Prefs(ConfirmationLinkActivity.this).setUserData(userDataModel);
                                startActivity(new Intent(ConfirmationLinkActivity.this, MainActivity.class));
                                SignInActivity.finishAllInitialActivities();
                                finish();
                            } else {
                                AppDelegate.showAlert(ConfirmationLinkActivity.this, getString(R.string.not_varified));
                                    /*if (getIntent().getIntExtra(Tags.from, AppDelegate.FROM_SIGNUP) == AppDelegate.FROM_SIGNUP) {
                                        AppDelegate.showAlert(ConfirmationLinkActivity.this, obj_json.getString(Tags.message));
                                        txt_c_description.setBackgroundColor(getResources().getColor(R.color.txt_color));
                                        txt_c_description.setTextColor(getResources().getColor(R.color.white));
                                    } else {
                                        txt_c_description.setText(getString(R.string.not_varified));
                                        txt_c_description.setBackgroundColor(getResources().getColor(R.color.black));
                                        txt_c_description.setTextColor(getResources().getColor(R.color.darkred));
                                    }*/


                            } /*else {
                                AppDelegate.showToast(ConfirmationLinkActivity.this, getString(R.string.something_wrong_with_server_response));
                            }*/
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(ConfirmationLinkActivity.this, getString(R.string.something_wrong_with_server_response));
                        }
                    }
                });
            }
        }
    }

    private void executeResendAsync() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            if ((new Prefs(this).getTempUserdata()) != null) {
                SingletonRestClient.get().resendActivation(new Prefs(this).getTempUserdata().email, new Prefs(this).getTempUserdata().password, new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        super.failure(error);
                        mHandler.sendEmptyMessage(11);
                        try {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                            AppDelegate.checkJsonMessage(ConfirmationLinkActivity.this, obj_json);
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(ConfirmationLinkActivity.this, "");
                        }
                    }

                    @Override
                    public void success(Response response, Response response2) {
                        mHandler.sendEmptyMessage(11);
                        try {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                            if (obj_json.getString(Tags.status).contains("1")) {
                                AppDelegate.showAlert(ConfirmationLinkActivity.this, obj_json.getString(Tags.message));
                            } else {
                                AppDelegate.showToast(ConfirmationLinkActivity.this, obj_json.getString(Tags.message));
                            }
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    }
                });
            }
        }
    }
//    private void parseChangePassword(String result) {
//        try {
//            JSONObject jsonObject = new JSONObject(result);
//            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//                new Prefs(ChangePasswordActivity.this).putStringValueinTemp(Tags.PASSWORD, et_new_password.getText().toString());
//                finish();
//            } else {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            }
//        } catch (Exception e) {
//           AppDelegate.LogE(e);
//        }
//    }
}

