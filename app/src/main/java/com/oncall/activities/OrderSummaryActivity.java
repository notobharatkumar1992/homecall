package com.oncall.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.google.gson.Gson;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryServiceSummaryListAdapter;
import com.oncall.constants.Tags;
import com.oncall.models.BookOrderModel;
import com.oncall.models.OrderDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 22-Mar-17.
 */
public class OrderSummaryActivity extends AppCompatActivity implements View.OnClickListener {

    public Prefs prefs;
    public Handler mHandler;
    public TextView txt_c_instruction_ph, txt_c_special_instruction, txt_c_total;
    public TextView txt_c_date, txt_c_time, txt_c_name, txt_c_phone, txt_c_email, txt_c_address, txt_c_address_2;
    public BookOrderModel bookOrderModel;

    public RecyclerView rv_services;
    public CategoryServiceSummaryListAdapter serviceListAdapter;
    public LinearLayout ll_additional_address;

    int price = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_summery);
        prefs = new Prefs(this);
        bookOrderModel = getIntent().getExtras().getParcelable(Tags.book_order);
        AppDelegate.LogT("at OrderSummaryActivity bookOrderModel.str_time => " + bookOrderModel.str_time);
        initView();
        setValues();
        setHandler();
    }

    public boolean checkValidTime() {
        String str_minimumTime = bookOrderModel.minimum_booking_time;
        AppDelegate.LogT("str_minimumTime => " + str_minimumTime);
        Calendar minimumTimeCalender = Calendar.getInstance();
        try {
            minimumTimeCalender.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(new Prefs(OrderSummaryActivity.this).getServerTime()));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        Date minimumTimeDate = minimumTimeCalender.getTime();
        Date selectedDate = Calendar.getInstance().getTime();
        try {
            selectedDate = new SimpleDateFormat(ScheduleJobActivity.DATE_FORMAT + " " + ScheduleJobActivity.TIME_FORMAT_12_HOUR).parse(bookOrderModel.str_date + " " + bookOrderModel.str_time);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        AppDelegate.LogT("minimumTimeCalender => " + minimumTimeDate + ", selectedDate => " + selectedDate + ", BEFORE => " + minimumTimeDate.before(selectedDate) + ", Equal => " + minimumTimeDate.equals(selectedDate));
        if (minimumTimeDate.before(selectedDate)) {
            return true;
        } else {
            AppDelegate.showToast(OrderSummaryActivity.this, "Your order time has been expire booking time should be greater than " + str_minimumTime + " from current time.");
            return false;
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(OrderSummaryActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(OrderSummaryActivity.this);
                        break;
                    case 5:
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    private void setValues() {
        if (AppDelegate.isValidString(bookOrderModel.str_description)) {
            txt_c_instruction_ph.setVisibility(View.VISIBLE);
            txt_c_special_instruction.setText(Html.fromHtml(bookOrderModel.str_description));
        } else {
            txt_c_instruction_ph.setVisibility(View.GONE);
            txt_c_special_instruction.setVisibility(View.INVISIBLE);
        }
        for (int i = 0; i < bookOrderModel.arrayServices.size(); i++) {
            try {
                price += Integer.parseInt(bookOrderModel.arrayServices.get(i).price);
                AppDelegate.LogT("price = " + price);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        try {
            txt_c_total.setText("TOTAL: AED " + price);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        txt_c_date.setText(bookOrderModel.str_date);
        txt_c_time.setText(bookOrderModel.str_time);
        txt_c_name.setText(bookOrderModel.addressModel.name);
        txt_c_phone.setText(bookOrderModel.addressModel.contact_no);
        try {
            String s = txt_c_phone.getText().toString();
            txt_c_phone.setText(String.format("%s %s %s", s.subSequence(0, 3), s.subSequence(3, 6), s.subSequence(6, s.length())));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        txt_c_email.setText(new Prefs(OrderSummaryActivity.this).getUserdata().email);
        txt_c_address.setText(bookOrderModel.addressModel.address + (AppDelegate.isValidString(bookOrderModel.addressModel.city) ? ", " + bookOrderModel.addressModel.city : ""));
        if (AppDelegate.isValidString(bookOrderModel.addressModel.address_2) && AppDelegate.isValidString(bookOrderModel.addressModel.city2))
            txt_c_address_2.setText(bookOrderModel.addressModel.address_2 + ", " + bookOrderModel.addressModel.city2);
        else ll_additional_address.setVisibility(View.GONE);

        serviceListAdapter = new CategoryServiceSummaryListAdapter(this, bookOrderModel.arrayServices, null, true, "");
        rv_services.setAdapter(serviceListAdapter);
        setListViewHeightBasedOnChildren(rv_services, bookOrderModel.arrayServices.size());
        ((ScrollView) findViewById(R.id.scrollView)).post(new Runnable() {
            @Override
            public void run() {
                ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_UP);
            }
        });
    }

    public void setListViewHeightBasedOnChildren(RecyclerView listView, int gridAdapter) {
        try {
            if (gridAdapter == 0) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < gridAdapter; i++) {
                totalHeight += AppDelegate.dpToPix(OrderSummaryActivity.this, 100);
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView() {
        txt_c_special_instruction = (TextView) findViewById(R.id.txt_c_special_instruction);
        txt_c_instruction_ph = (TextView) findViewById(R.id.txt_c_instruction_ph);
        txt_c_total = (TextView) findViewById(R.id.txt_c_total);
        txt_c_email = (TextView) findViewById(R.id.txt_c_email);

        txt_c_date = (TextView) findViewById(R.id.txt_c_date);
        txt_c_time = (TextView) findViewById(R.id.txt_c_time);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_phone = (TextView) findViewById(R.id.txt_c_phone);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        txt_c_address_2 = (TextView) findViewById(R.id.txt_c_address_2);

        ll_additional_address = (LinearLayout) findViewById(R.id.ll_additional_address);

        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_confirm_order).setOnClickListener(this);

        rv_services = (RecyclerView) findViewById(R.id.rv_services);
        rv_services.setLayoutManager(new LinearLayoutManager(this));
        rv_services.setHasFixedSize(true);
        rv_services.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_c_confirm_order:
                if (checkValidTime()) {
                    AppDelegate.sendEventTracker(this, "Order booked");
                    executeBookOrder();
                }
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void executeBookOrder() {
        if (bookOrderModel.booking_from == MainActivity.BOOKING_FROM_CATEGORIES) {
            if (AppDelegate.haveNetworkConnection(this)) {
                mHandler.sendEmptyMessage(10);
                AppDelegate.LogT("Total price => " + price);
                SingletonRestClient.get().bookOrder(
                        new Prefs(this).getUserdata().id + "",
                        new Prefs(this).getUserdata().sessionid + "",
                        bookOrderModel.addressModel.name,
                        new Prefs(this).getUserdata().email,
                        bookOrderModel.addressModel.contact_no,
                        bookOrderModel.str_description,
                        bookOrderModel.addressModel.address,
                        bookOrderModel.str_time,
                        bookOrderModel.str_date,
                        price + "",
                        bookOrderModel.arrayServices.get(0).category_id,
                        bookOrderModel.str_service_id,
                        bookOrderModel.addressModel.address_2 + "",
                        bookOrderModel.addressModel.city + "",
                        bookOrderModel.addressModel.city2 + "",
                        bookOrderModel.addressModel.state + "",
                        bookOrderModel.addressModel.zip_code + "",
                        new Callback<Response>() {
                            @Override
                            public void failure(RestError restError) {
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                super.failure(error);
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                    AppDelegate.checkJsonMessage(OrderSummaryActivity.this, obj_json);

                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                    AppDelegate.showToast(OrderSummaryActivity.this, "");
                                }
                            }

                            @Override
                            public void success(Response response, Response response2) {
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                    if (obj_json.getString(Tags.status).contains("1")) {

                                        JSONObject jsonObject = obj_json.getJSONObject(Tags.OrderData);
                                        OrderDataModel orderDataModel = new Gson().fromJson(jsonObject.toString(), OrderDataModel.class);
                                        Intent intent = new Intent(OrderSummaryActivity.this, OrderConfirmationActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelable(Tags.OrderData, orderDataModel);
                                        intent.putExtras(bundle);
                                        startActivity(intent);

                                        if (VendorSearchProfileActivity.mActivity != null)
                                            VendorSearchProfileActivity.mActivity.finish();
                                        if (ScheduleJobActivity.mActivity != null)
                                            ScheduleJobActivity.mActivity.finish();
                                        if (AddAddressActivity.mActivity != null)
                                            AddAddressActivity.mActivity.finish();
                                        if (CategoryServiceListActivity.mActivity != null)
                                            CategoryServiceListActivity.mActivity.finish();
                                        if (VendorSearchListActivity.mActivity != null)
                                            VendorSearchListActivity.mActivity.finish();
                                        if (VendorSearchDashboardActivity.mActivity != null)
                                            VendorSearchDashboardActivity.mActivity.finish();
                                        finish();
                                    } else {
                                        AppDelegate.showToast(OrderSummaryActivity.this, obj_json.getString(Tags.message));
                                    }
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                        });
            }
        } else if (bookOrderModel.booking_from == MainActivity.BOOKING_FROM_RESCHEDULE) {
            if (AppDelegate.haveNetworkConnection(this)) {
                mHandler.sendEmptyMessage(10);
                AppDelegate.LogT("Total price => " + price);
                SingletonRestClient.get().orderReschedule(
                        new Prefs(this).getUserdata().id + "",
                        new Prefs(OrderSummaryActivity.this).getUserdata().sessionid + "",
                        bookOrderModel.id,
                        bookOrderModel.str_date,
                        bookOrderModel.str_time,
                        bookOrderModel.str_description + "",
                        new Callback<Response>() {
                            @Override
                            public void failure(RestError restError) {
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                super.failure(error);
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                    AppDelegate.checkJsonMessage(OrderSummaryActivity.this, obj_json);

                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                    AppDelegate.showToast(OrderSummaryActivity.this, "");
                                }
                            }

                            @Override
                            public void success(Response response, Response response2) {
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                    if (obj_json.getInt(Tags.session_status) == 0) {
                                        AppDelegate.showToast(OrderSummaryActivity.this, obj_json.getString(Tags.message));
                                        new Prefs(OrderSummaryActivity.this).setUserData(null);
                                        if (MainActivity.mActivity != null)
                                            MainActivity.mActivity.finish();
                                        startActivity(new Intent(OrderSummaryActivity.this, SignInActivity.class));
                                        finish();
                                    } else if (obj_json.getString(Tags.status).contains("1")) {
                                        JSONObject jsonObject = obj_json.getJSONObject(Tags.OrderData);
                                        OrderDataModel orderDataModel = new Gson().fromJson(jsonObject.toString(), OrderDataModel.class);
                                        Intent intent = new Intent(OrderSummaryActivity.this, OrderConfirmationActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelable(Tags.OrderData, orderDataModel);
                                        intent.putExtras(bundle);
                                        startActivity(intent);

                                        if (VendorSearchProfileActivity.mActivity != null)
                                            VendorSearchProfileActivity.mActivity.finish();
                                        if (ScheduleJobActivity.mActivity != null)
                                            ScheduleJobActivity.mActivity.finish();
                                        if (AddAddressActivity.mActivity != null)
                                            AddAddressActivity.mActivity.finish();
                                        if (CategoryServiceListActivity.mActivity != null)
                                            CategoryServiceListActivity.mActivity.finish();
                                        if (VendorSearchListActivity.mActivity != null)
                                            VendorSearchListActivity.mActivity.finish();
                                        if (VendorSearchDashboardActivity.mActivity != null)
                                            VendorSearchDashboardActivity.mActivity.finish();
                                        finish();
                                    } else {
                                        AppDelegate.showToast(OrderSummaryActivity.this, obj_json.getString(Tags.message));
                                    }
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                        });
            }
        } else {
            if (AppDelegate.haveNetworkConnection(this)) {
                mHandler.sendEmptyMessage(10);
                AppDelegate.LogT("Total price => " + price);
                SingletonRestClient.get().bookOrderToVendor(
                        new Prefs(this).getUserdata().id + "",
                        bookOrderModel.vendorModel.id,
                        bookOrderModel.addressModel.landmark,
                        bookOrderModel.addressModel.name,
                        bookOrderModel.addressModel.street,
                        new Prefs(this).getUserdata().email,
                        bookOrderModel.addressModel.contact_no,
                        bookOrderModel.str_description,
                        bookOrderModel.addressModel.address,
                        bookOrderModel.str_time,
                        bookOrderModel.str_date,
                        bookOrderModel.str_currency_id,
                        price + "",
                        bookOrderModel.arrayServices.get(0).category_id,
                        bookOrderModel.str_service_id,
                        bookOrderModel.addressModel.latitude + "",
                        bookOrderModel.addressModel.longitude + "",
                        new Callback<Response>() {
                            @Override
                            public void failure(RestError restError) {
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                super.failure(error);
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                    AppDelegate.checkJsonMessage(OrderSummaryActivity.this, obj_json);

                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                    AppDelegate.showToast(OrderSummaryActivity.this, "");
                                }
                            }

                            @Override
                            public void success(Response response, Response response2) {
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                    if (obj_json.getString(Tags.status).contains("1")) {
                                        JSONObject jsonObject = obj_json.getJSONObject(Tags.OrderData);
                                        OrderDataModel orderDataModel = new Gson().fromJson(jsonObject.toString(), OrderDataModel.class);
                                        Intent intent = new Intent(OrderSummaryActivity.this, OrderConfirmationActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelable(Tags.OrderData, orderDataModel);
                                        intent.putExtras(bundle);
                                        startActivity(intent);

                                        if (VendorSearchProfileActivity.mActivity != null)
                                            VendorSearchProfileActivity.mActivity.finish();
                                        if (ScheduleJobActivity.mActivity != null)
                                            ScheduleJobActivity.mActivity.finish();
                                        if (VendorSearchListActivity.mActivity != null)
                                            VendorSearchListActivity.mActivity.finish();
                                        if (VendorSearchDashboardActivity.mActivity != null)
                                            VendorSearchDashboardActivity.mActivity.finish();

                                        finish();
                                    } else {
                                        AppDelegate.showToast(OrderSummaryActivity.this, obj_json.getString(Tags.message));
                                    }
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                        });
            }
        }
    }
}
