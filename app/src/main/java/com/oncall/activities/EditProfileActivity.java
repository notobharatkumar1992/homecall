package com.oncall.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.oncall.AppDelegate;
import com.oncall.Async.LocationAddress;
import com.oncall.Async.PlacesService;
import com.oncall.R;
import com.oncall.adapters.LocationMapAdapter;
import com.oncall.constants.Tags;
import com.oncall.expandablelayout.ExpandableRelativeLayout;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.Place;
import com.oncall.models.UserDataModel;
import com.oncall.utils.CircleImageView;
import com.oncall.utils.Prefs;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ImageView;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;
import retrofit.mime.TypedFile;

/**
 * Created by Heena on 12-Jan-17.
 */
public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    private CircleImageView cimg_user;
    private EditText et_contact, et_password, et_confirm_password, et_name, et_address;
    private Handler mHandler;
    private UserDataModel userDataModel;
    TextView txt_c_title, txt_c_username;
    ImageView img_c_loading1;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;

    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    public static final int COLLAPSE = 0, EXPAND = 1;
    Prefs prefs;
    private double currentLatitude = 0, currentLongitude = 0;
    private boolean canSearch = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.edit_profile);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        initView();
        prefs = new Prefs(this);
        setHandler();
        setData();
    }

    private void setData() {
        userDataModel = prefs.getUserdata();
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.image)) {
            img_c_loading1.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
            frameAnimation.setCallback(img_c_loading1);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            imageLoader.loadImage(new Prefs(this).getUserdata().image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                    img_c_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    cimg_user.setImageBitmap(bitmap);
                    img_c_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    img_c_loading1.setVisibility(View.GONE);
                }
            });
        }
        if (userDataModel != null) {
            txt_c_username.setText(userDataModel.email);
            et_contact.setText(userDataModel.contact_no);
            et_address.setText(AppDelegate.isValidString(userDataModel.address) ? userDataModel.address : "");
            et_name.setText(AppDelegate.isValidString(userDataModel.name) ? userDataModel.name : "");
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(EditProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(EditProfileActivity.this);
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
//                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        mHandler.sendEmptyMessage(10);
                        AppDelegate.LogT("mHandler.sendEmptyMessage(3) called choose location Activity;");
                        LocationAddress.getLocationFromReverseGeocoder(et_address.getText().toString(), EditProfileActivity.this, mHandler);
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null) {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0) {
                            if (et_address.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        if (canSearch) {
                            if (et_address.length() != 0) {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            if (et_address.length() != 0) {
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 9:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_address.length() != 0) {
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;
                }
            }
        };
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude = " + currentLatitude + ", longitude = " + currentLongitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            try {
                currentLatitude = data.getDouble(Tags.latitude);
                currentLongitude = data.getDouble(Tags.longitude);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
        }

    }

    private void addtextWatcher() {
        et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                AppDelegate.LogT("et_search afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private void initView() {
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        et_contact = (EditText) findViewById(R.id.et_contact);
        txt_c_username = (TextView) findViewById(R.id.txt_c_username);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        et_contact.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_confirm_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        et_confirm_password.setVisibility(View.GONE);
        et_password.setVisibility(View.GONE);
        et_contact.setImeOptions(EditorInfo.IME_ACTION_DONE);
        et_name = (EditText) findViewById(R.id.et_name);
        et_address = (EditText) findViewById(R.id.et_address);
        et_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_address.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        addtextWatcher();
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(this, mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);
    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(EditProfileActivity.this, false)) {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(EditProfileActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_address.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(et_address.getText().toString()));
                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };

    private void setloc(double latitude, double longitude) {
        mHandler.sendEmptyMessage(10);
        LocationAddress.getAddressFromLocation(latitude, longitude, EditProfileActivity.this, mHandler);
    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(EditProfileActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(EditProfileActivity.this)) {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position) {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                } else if (AppDelegate.isValidString(placeDetail_pickup.name)) {
                    et_address.setText(placeDetail_pickup.name);
                } else if (AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.vicinity);
                }
                et_address.setSelection(et_address.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(3);
            }
            canSearch = true;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                executeEditProfileAsync();
                break;
            case R.id.cimg_user:
                showImageSelectorList();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    /* image selection*/
    public static File capturedFile;
    public static Uri imageURI = null;

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(EditProfileActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        ListView modeList = new ListView(EditProfileActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(EditProfileActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private Bitmap OriginalPhoto;

    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                OriginalPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 512, 512, true);
                cimg_user.setImageBitmap(OriginalPhoto);
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile(EditProfileActivity.this);
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(EditProfileActivity.this, getString(R.string.file_not_created));
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public static String getNewFile(Context mContext) {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getString(R.string.app_name));
        } else {
            directoryFile = mContext.getDir(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppDelegate.SELECT_PICTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(EditProfileActivity.this, data.getData());
                } else {
                    Toast.makeText(EditProfileActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (imageURI != null) {
                    startCropActivity(EditProfileActivity.this, imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        capturedFile = new File(mActivity.getCacheDir(), destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(capturedFile));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

//    public static boolean imageUriIsValid(FragmentActivity mActivity, Uri contentUri) {
//        ContentResolver cr = mActivity.getContentResolver();
//        String[] projection = {MediaStore.MediaColumns.DATA};
//        Cursor cur = cr.query(contentUri, projection, null, null, null);
//        if (cur != null) {
//            if (cur.moveToFirst()) {
//                String filePath = cur.getString(0);
//                if (new File(filePath).exists()) {
//                    // do something if it exists
//                    cur.close();
//                    return true;
//                } else {
//                    // File was not found
//                    cur.close();
//                    AppDelegate.showToast(mActivity, "File not found please try again later.");
//                    return false;
//                }
//            } else {
//                // Uri was ok but no entry found.
//                AppDelegate.showToast(mActivity, "No image found please try again later.");
//                return false;
//            }
//        } else {
//            // content Uri was invalid or some other error occurred
//            AppDelegate.showToast(mActivity, "Image path was invalid some exception occured at your device.");
//            return false;
//        }
//    }\

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(100);

//        if (SellItemFragment.onPictureResult != null) {
//            options.setHideBottomControls(false);
//            options.setFreeStyleCropEnabled(false);
//        } else {
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
//        }


        return uCrop.withOptions(options);
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void writeImageFile(Bitmap bitmap, String filePath) {
        if (AppDelegate.isValidString(filePath) && bitmap != null) {
            capturedFile = new File(filePath);
        } else {
            return;
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    private void executeEditProfileAsync() {
       /* if (capturedFile == null) {
            AppDelegate.showToast(this, "Please select an image.");
        } else*/
        if (OriginalPhoto != null)
            writeImageFile(OriginalPhoto, getNewFile(EditProfileActivity.this));
        if (et_contact.length() == 0) {
            AppDelegate.showToast(this, "Please enter Phone Number.");
        } else if (et_name.length() == 0) {
            AppDelegate.showToast(this, "Please enter Name.");
        } else if (et_address.length() == 0) {
            AppDelegate.showToast(this, "Please enter Address.");
        } /*else if (!et_contact.getText().toString().contains("+")) {
            AppDelegate.showToast(this, "Invalid phone_icon number ! Please send a phone_icon number in international format. [+][country code][phone_icon number including area code]");
        } *//* else if (et_password.length() == 0) {
            AppDelegate.showToast(this, "Please enter Password.");
        } else if (et_confirm_password.length() == 0) {
            AppDelegate.showToast(this, "Please enter Confirm Password.");
        } else if (!et_password.getText().toString().equals(et_confirm_password.getText().toString())) {
            AppDelegate.showToast(this, "Please enter valid Confirm Password, must be same as Password.");
        }*/ else if (AppDelegate.haveNetworkConnection(this)) {

            String device_token = new Prefs(EditProfileActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }

            mHandler.sendEmptyMessage(10);
            userDataModel = prefs.getUserdata();
            TypedFile typedFileCurrentPhotoPath = null;
            if (capturedFile != null) {
                typedFileCurrentPhotoPath = new TypedFile("multipart/form-data",
                        new File(String.valueOf(capturedFile)));
//
//                SingletonRestClient.get().editProfile(userDataModel.id + "", userDataModel.role_id + "", typedFileCurrentPhotoPath, et_name.getText().toString(), ""/*, txt_c_username.getText().toString()*/, "", et_address.getText().toString(), et_contact.getText().toString(), AppDelegate.DEVICE_TYPE, device_token, "", "", "", currentLatitude + "", currentLongitude + "", new Callback<Response>() {
//                    @Override
//                    public void failure(RestError restError) {
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        super.failure(error);
//                        mHandler.sendEmptyMessage(11);
//                        try {
//                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                            AppDelegate.checkJsonMessage(EditProfileActivity.this, obj_json);
//                        } catch (Exception e) {
//                            AppDelegate.LogE(e);
//                            AppDelegate.showToast(EditProfileActivity.this, "");
//                        }
//                    }
//
//                    @Override
//                    public void success(Response response, Response response2) {
//                        mHandler.sendEmptyMessage(11);
//                        try {
//                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                            if (obj_json.getInt(Tags.status) == 1) {
//                                AppDelegate.showToast(EditProfileActivity.this, obj_json.getString(Tags.message));
//                                JSONObject object = obj_json.getJSONObject(Tags.response);
//                                UserDataModel userDataModel = new Prefs(EditProfileActivity.this).getUserdata();
//                                userDataModel.id = JSONParser.getInt(object, Tags.id);
//                                userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
//                                userDataModel.email = JSONParser.getString(object, Tags.email);
//                                userDataModel.name = JSONParser.getString(object, Tags.name);
//                                userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
//                                userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
//                                userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
//                                userDataModel.address = JSONParser.getString(object, Tags.address1);
//                                userDataModel.latitude = JSONParser.getString(object, Tags.lat);
//                                userDataModel.longitude = JSONParser.getString(object, "long");
//                                userDataModel.address2 = JSONParser.getString(object, Tags.address2);
//                                userDataModel.city = JSONParser.getString(object, Tags.city);
//                                userDataModel.state = JSONParser.getString(object, Tags.state);
//                                userDataModel.country = JSONParser.getString(object, Tags.country);
//                                userDataModel.zip = JSONParser.getString(object, Tags.zip);
//                                userDataModel.token = JSONParser.getString(object, Tags.token);
//                                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
//                                userDataModel.name_email = JSONParser.getString(object, Tags.name_email);
//                                if (object.has(Tags.service)) {
//                                    if (AppDelegate.isValidString(object.optString(Tags.service))) {
//                                        JSONArray array = object.getJSONArray(Tags.service);
//                                        StringBuilder stringBuilder = new StringBuilder();
//
//                                        for (int i = 0; i < array.length(); i++) {
//                                            stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
//                                        }
//                                        String service = stringBuilder.toString();
//                                        service = service.substring(0, service.length() - 2);
//                                        userDataModel.service = service;
//                                    }
//                                }
//                                if (object.has(Tags.category)) {
//                                    userDataModel.category = object.optString(Tags.category);
//                                }
//                                new Prefs(EditProfileActivity.this).setUserData(userDataModel);
//                                finish();
//                            } else {
//                                AppDelegate.showAlert(EditProfileActivity.this, obj_json.getString(Tags.message));
//                            }
//                        } catch (Exception e) {
//                            AppDelegate.LogE(e);
//                        }
//                    }
//                });
            } else {
                AppDelegate.LogT("else called");
//                SingletonRestClient.get().editProfile(userDataModel.id + "", userDataModel.role_id + "", et_name.getText().toString(), ""/*, txt_c_username.getText().toString()*/, "", et_address.getText().toString(), et_contact.getText().toString(), AppDelegate.DEVICE_TYPE, device_token, "", "", "", currentLatitude + "", currentLongitude + "", new Callback<Response>() {
//                    @Override
//                    public void failure(RestError restError) {
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        super.failure(error);
//                        mHandler.sendEmptyMessage(11);
//                        try {
//                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                            AppDelegate.checkJsonMessage(EditProfileActivity.this, obj_json);
//                        } catch (Exception e) {
//                            AppDelegate.LogE(e);
//                            AppDelegate.showToast(EditProfileActivity.this, "");
//                        }
//                    }
//
//                    @Override
//                    public void success(Response response, Response response2) {
//                        mHandler.sendEmptyMessage(11);
//                        try {
//                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                            if (obj_json.getInt(Tags.status) == 1) {
//                                AppDelegate.showToast(EditProfileActivity.this, obj_json.getString(Tags.message));
//                                JSONObject object = obj_json.getJSONObject(Tags.response);
//                                UserDataModel userDataModel = new Prefs(EditProfileActivity.this).getUserdata();
//                                userDataModel.id = JSONParser.getInt(object, Tags.id);
//                                userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
//                                userDataModel.email = JSONParser.getString(object, Tags.email);
//                                userDataModel.name = JSONParser.getString(object, Tags.name);
//                                userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
//                                userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
//                                userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
//                                userDataModel.address = JSONParser.getString(object, Tags.address1);
//                                userDataModel.latitude = JSONParser.getString(object, Tags.lat);
//                                userDataModel.longitude = JSONParser.getString(object, "long");
//                                userDataModel.address2 = JSONParser.getString(object, Tags.address2);
//                                userDataModel.city = JSONParser.getString(object, Tags.city);
//                                userDataModel.state = JSONParser.getString(object, Tags.state);
//                                userDataModel.country = JSONParser.getString(object, Tags.country);
//                                userDataModel.zip = JSONParser.getString(object, Tags.zip);
//                                userDataModel.token = JSONParser.getString(object, Tags.token);
//                                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
//                                userDataModel.name_email = JSONParser.getString(object, Tags.name_email);
//                                if (object.has(Tags.service)) {
//                                    if (AppDelegate.isValidString(object.optString(Tags.service))) {
//                                        JSONArray array = object.getJSONArray(Tags.service);
//                                        StringBuilder stringBuilder = new StringBuilder();
//
//                                        for (int i = 0; i < array.length(); i++) {
//                                            stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
//                                        }
//                                        String service = stringBuilder.toString();
//                                        service = service.substring(0, service.length() - 2);
//                                        userDataModel.service = service;
//                                    }
//                                }
//                                if (object.has(Tags.category)) {
//                                    userDataModel.category = object.optString(Tags.category);
//                                }
//                                new Prefs(EditProfileActivity.this).setUserData(userDataModel);
//                                finish();
//                            } else {
//                                AppDelegate.showAlert(EditProfileActivity.this, obj_json.getString(Tags.message));
//                            }
//                        } catch (Exception e) {
//                            AppDelegate.LogE(e);
//                        }
//                    }
//                });
            }
        }
    }
}
