package com.oncall.activities;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TimePicker;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.constants.Tags;
import com.oncall.models.AddressModel;
import com.oncall.models.BookOrderModel;
import com.oncall.models.RequestModel;
import com.oncall.models.ServiceModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.DateUtils;
import com.oncall.utils.Prefs;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 18-Mar-17.
 */
public class ScheduleJobActivity extends AppCompatActivity implements View.OnClickListener, OnDateSelectedListener, CalendarView.OnDateChangeListener {

    public static Activity mActivity;
    public Handler mHandler;
    public CalendarView calendarViewObject;

    public Prefs prefs;
    private int currentTileSize;
    private int currentTileWidth;
    private int currentTileHeight;

    public TextView select_date, txt_c_time;

    public EditText et_instructions;
    public RelativeLayout rl_inst;
    public static final String TIME_FORMAT = "HH:mm";
    public Bundle bundle;
    public Calendar minimumTimeCalender = Calendar.getInstance();
    public String str_minimumTime = "00:00";
    public ArrayList<ServiceModel> serviceArray;
    public String service, service_id, description;
    public int booking_from = MainActivity.BOOKING_FROM_CATEGORIES;
    public RequestModel requestModel;
    public ScrollView scrollView;
    public boolean needComment = false;
    private String str_global_dateTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_job_activity);
        getData();
        initView();
//        getValues();
        setHandler();
        executeGetTimeZone();
    }

    private void getData() {
        selectedCalendar = Calendar.getInstance();
        minimumTimeCalender = Calendar.getInstance();
        prefs = new Prefs(this);
        mActivity = this;
        bundle = getIntent().getExtras();
        serviceArray = bundle.getParcelableArrayList(Tags.ARRAY_SERVICES);
        service = bundle.getString(Tags.service);
        service_id = bundle.getString(Tags.service_id);
        description = bundle.getString(Tags.description);
        booking_from = bundle.getInt(Tags.from);
        requestModel = bundle.getParcelable(Tags.request_model);
    }

    private void getValues() {
        if (serviceArray != null && serviceArray.size() > 0 && AppDelegate.isValidString(str_minimumTime)) {
            Calendar minimumBookingTimeCalendar = Calendar.getInstance();
            AppDelegate.LogT("str_minimumTime => " + str_minimumTime + ", minimumBookingTimeCalendar before=> " + minimumBookingTimeCalendar.getTime());
            try {
                minimumBookingTimeCalendar.setTime(new SimpleDateFormat(TIME_FORMAT).parse(str_minimumTime));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.LogT("minimumBookingTimeCalendar after => " + minimumBookingTimeCalendar.getTime() + ", " + minimumBookingTimeCalendar.get(Calendar.HOUR_OF_DAY));
            try {
                minimumTimeCalender.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str_global_dateTime));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }

            AppDelegate.LogT("minimumTimeCalender Before => " + minimumTimeCalender.getTime());
            minimumTimeCalender.add(Calendar.HOUR_OF_DAY, minimumBookingTimeCalendar.get(Calendar.HOUR_OF_DAY));
            minimumTimeCalender.add(Calendar.MINUTE, minimumBookingTimeCalendar.get(Calendar.MINUTE));
//          minimumTimeCalender.add(Calendar.MINUTE, 1);

            calendarViewObject.setMinDate(minimumTimeCalender.getTimeInMillis());
            minimumTimeCalender.add(Calendar.SECOND, 10);
            selectedCalendar.setTimeInMillis(calendarViewObject.getDate() + 10000);
            AppDelegate.LogT("minimumTimeCalender => " + minimumTimeCalender.getTime());
            calendarViewObject.invalidate();
        }

        select_date.setText(Html.fromHtml("When would you like to Schedule?\nYour booking time should be greater then <b>" + str_minimumTime + "</b> from current time."));

        if (booking_from == MainActivity.BOOKING_FROM_RESCHEDULE) {
            rl_inst.setVisibility(View.VISIBLE);
        } else {
            rl_inst.setVisibility(View.GONE);
        }
        timeHandler.removeCallbacks(timeRunnable);
        timeHandler.postDelayed(timeRunnable, 1000);
    }

    private void initView() {
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_continue).setOnClickListener(this);
        select_date = (TextView) findViewById(R.id.select_date);
        txt_c_time = (TextView) findViewById(R.id.txt_c_time);

        findViewById(R.id.ll_c_time).setOnClickListener(this);

        rl_inst = (RelativeLayout) findViewById(R.id.rl_inst);
        rl_inst.setVisibility(View.GONE);
        et_instructions = (EditText) findViewById(R.id.et_instructions);
        et_instructions.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        calendarViewObject = (CalendarView) findViewById(R.id.calendarViewObject);
        calendarViewObject.setOnDateChangeListener(this);
//        calendarView = (MaterialCalendarView) findViewById(R.id.calendarView);
////        calendarView.state().edit().setMinimumDate(CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))).commit();
////        calendarView.setSelectedDate(CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
//        calendarView.setTopbarVisible(true);
//        calendarView.setArrowColor(R.color.side_bar_color);
//        calendarView.setSelectionColor(R.color.side_bar_color);
//        calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
//        calendarView.setArrowColor(R.color.shadow);
//        calendarView.setSelectionColor(R.color.shadow);
//        calendarView.setDrawingCacheBackgroundColor(getResources().getColor(R.color.shadow));
//        calendarView.setShowOtherDates(MaterialCalendarView.SHOW_DECORATED_DISABLED);
//        calendarView.setTileHeight(120);
//        calendarView.setTileWidth(120);
//
//        calendarView.setArrowColor(getResources().getColor(R.color.side_bar_color));
////        calendarView.setLeftArrowMask(getResources().getDrawable(R.drawable.ic_navigation_arrow_back));
////        calendarView.setRightArrowMask(getResources().getDrawable(R.drawable.ic_navigation_arrow_forward));
//        calendarView.setSelectionColor(getResources().getColor(R.color.side_bar_color));
//        calendarView.setHeaderTextAppearance(R.style.TextAppearance_AppCompat_Medium);
//        calendarView.setWeekDayTextAppearance(R.style.TextAppearance_AppCompat_Medium);
//        calendarView.setOnDateChangedListener(this);

        currentTileSize = MaterialCalendarView.DEFAULT_TILE_SIZE_DP;
        currentTileWidth = MaterialCalendarView.DEFAULT_TILE_SIZE_DP;
        currentTileHeight = MaterialCalendarView.DEFAULT_TILE_SIZE_DP;

        selectedDate = new SimpleDateFormat(DATE_FORMAT).format(Calendar.getInstance().getTime());
    }


    public static final String inputFormat = "hh:mm aa";

    public SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);

    private boolean compareDates(String compareStringOne, String compareStringTwo) {
        Date date = Calendar.getInstance().getTime();
        Date dateCompareOne;
        Date dateCompareTwo;
        boolean isBetween = false;
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, 1);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        try {
            date = new SimpleDateFormat("HH:mm").parse(hour + ":" + minute);
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }
        dateCompareOne = parseDate(compareStringOne);
        dateCompareTwo = parseDate(compareStringTwo);
//        AppDelegate.LogT("compareDates called => " + compareStringOne + ", " + compareStringTwo + ", " + hour + ":" + minute);
        if (date.before(dateCompareTwo)) {
//            AppDelegate.LogT("true, dateCompareOne==>" + dateCompareOne + ", dateCompareTwo==>" + dateCompareTwo + ", date==>" + date);
            isBetween = true;
        } else {
//            AppDelegate.LogT("false,dateCompareOne==>" + dateCompareOne + ", dateCompareTwo==>" + dateCompareTwo + ", date==>" + date);
        }
        return isBetween;
    }

    private Date parseDate(String date) {
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            AppDelegate.LogE(e);
            return new Date(0);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ScheduleJobActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ScheduleJobActivity.this);
                        break;
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_continue:
                continueBooking();
                break;
//            case R.id.ll_c_add_address:
//                Intent intent = new Intent(this, AddAddressActivity.class);
//                intent.putExtra(Tags.FROM, booking_from);
//                intent.putExtra(Tags.category_id, serviceArray.get(0).category_id);
//                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(ScheduleJobActivity.this, false,
//                        new Pair<>(findViewById(R.id.textView), "title"));
//                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(ScheduleJobActivity.this, pairs);
//                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            case R.id.img_c_back:
                finish();
                break;
            case R.id.ll_c_time:
                showTimeDialog();
                break;
        }
    }

    public Calendar selectedCalendar = Calendar.getInstance();

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        //If you change a decorate, you need to invalidate decorators
        AppDelegate.LogT("widget.getSelectedDate().getDate() => " + widget.getSelectedDate().getDate());
        selectedCalendar.setTime(widget.getSelectedDate().getDate());

//        selectedCalendar.add(Calendar.HOUR_OF_DAY, minimumTimeCalender.get(Calendar.HOUR_OF_DAY));
//        selectedCalendar.add(Calendar.MINUTE, minimumTimeCalender.get(Calendar.MINUTE));
//        AppDelegate.LogT("selectedCalendar.getDate() => " + selectedCalendar.getTime());

        selectedDate = new SimpleDateFormat(DATE_FORMAT).format(selectedCalendar.getTime());

        if (mHour == -1 || mMinute == -1) {
            mHour = selectedCalendar.get(Calendar.HOUR_OF_DAY);
            mMinute = selectedCalendar.get(Calendar.MINUTE);
        } else if (DateUtils.isToday(selectedCalendar)) {
            mHour = selectedCalendar.get(Calendar.HOUR_OF_DAY);
            mMinute = selectedCalendar.get(Calendar.MINUTE);
            try {
                txt_c_time.setText(new SimpleDateFormat(TIME_FORMAT_12_HOUR).format(new SimpleDateFormat(TIME_FORMAT_24_HOUR).parse(mHour + ":" + mMinute)));
                selectedTime = txt_c_time.getText().toString();
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        widget.invalidateDecorators();
        showTimeDialog();
    }

    @Override
    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
        //If you change a decorate, you need to invalidate decorators
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        calendar.set(Calendar.HOUR_OF_DAY, minimumTimeCalender.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, minimumTimeCalender.get(Calendar.MINUTE));

        AppDelegate.LogT("widget.getSelectedDate().getDate() => " + calendar.getTime());
        selectedCalendar.setTimeInMillis(calendar.getTimeInMillis());

//        selectedCalendar.add(Calendar.HOUR_OF_DAY, minimumTimeCalender.get(Calendar.HOUR_OF_DAY));
//        selectedCalendar.add(Calendar.MINUTE, minimumTimeCalender.get(Calendar.MINUTE));
//        AppDelegate.LogT("selectedCalendar.getDate() => " + selectedCalendar.getTime());

        selectedDate = new SimpleDateFormat(DATE_FORMAT).format(selectedCalendar.getTime());

        if (mHour == -1 || mMinute == -1) {
            mHour = selectedCalendar.get(Calendar.HOUR_OF_DAY);
            mMinute = selectedCalendar.get(Calendar.MINUTE);
        } else if (DateUtils.isToday(selectedCalendar)) {
            mHour = selectedCalendar.get(Calendar.HOUR_OF_DAY);
            mMinute = selectedCalendar.get(Calendar.MINUTE);
            try {
                txt_c_time.setText(new SimpleDateFormat(TIME_FORMAT_12_HOUR).format(new SimpleDateFormat(TIME_FORMAT_24_HOUR).parse(mHour + ":" + mMinute)));
                selectedTime = txt_c_time.getText().toString();
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }

//        widget.invalidateDecorators();
        showTimeDialog();
    }


    public String selectedDate, selectedTime;

    private int mHour = -1, mMinute = -1;

    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String TIME_FORMAT_12_HOUR = "hh:mm aa";
    public static final String TIME_FORMAT_24_HOUR = "HH:mm";

    public static Date getDateObject(String date, String date_format) {
        try {
            return new SimpleDateFormat(date_format).parse(date);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return Calendar.getInstance().getTime();
    }

    public void showTimeDialog() {
        if (mHour < 0 || mMinute < 0) {
            mHour = minimumTimeCalender.get(Calendar.HOUR_OF_DAY);
            mMinute = minimumTimeCalender.get(Calendar.MINUTE);
        }

        // Launch Time Picker Dialog
        final TimePickerDialog tpd = new TimePickerDialog(ScheduleJobActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        try {
                            String sTime = new SimpleDateFormat(TIME_FORMAT_12_HOUR).format(new SimpleDateFormat(TIME_FORMAT_24_HOUR).parse(hourOfDay + ":" + minute));
                            AppDelegate.LogT("selected_time => " + sTime + ", " + selectedCalendar.getTime());
//                            if (DateUtils.isToday(selectedCalendar.getTime())) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(selectedCalendar.getTime());

                            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            calendar.set(Calendar.MINUTE, minute);
                            AppDelegate.LogT("minimumTimeCalender.getTime() => " + minimumTimeCalender.getTime());
                            AppDelegate.LogT("selectedCalendar.getTime() => " + calendar.getTime());
                            if (minimumTimeCalender.getTime().before(calendar.getTime()) || minimumTimeCalender.getTimeInMillis() == calendar.getTimeInMillis() + 60000) {
                                txt_c_time.setText(sTime);
                                mHour = hourOfDay;
                                mMinute = minute;
                                selectedTime = sTime;
                            } else {
                                AppDelegate.showToast(ScheduleJobActivity.this, "Please select future time for booking.");
                            }
//                            } else {
//                                txt_c_time.setText(sTime);
//                                mHour = hourOfDay;
//                                mMinute = minute;
//                                selectedTime = sTime;
//                            }
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    }
                }
                , mHour, mMinute, false);
        tpd.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public Handler timeHandler = new Handler();
    public Runnable timeRunnable = new Runnable() {
        @Override
        public void run() {
            minimumTimeCalender.add(Calendar.SECOND, 1);
            try {
                prefs.setServerTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(minimumTimeCalender.getTime()));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            timeHandler.postDelayed(this, 1000);
        }
    };

    public boolean checkBookingTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(selectedCalendar.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, mHour);
        calendar.set(Calendar.MINUTE, mMinute);
        AppDelegate.LogT("checkBookingTime => " + minimumTimeCalender.getTime() + " == " + calendar.getTime());
        if (minimumTimeCalender.getTime().before(calendar.getTime()) || minimumTimeCalender.getTimeInMillis() <= calendar.getTimeInMillis() + 60000) {
            return true;
        } else {
            AppDelegate.showToast(ScheduleJobActivity.this, "Please select future time for booking.");
            return false;
        }
    }

    private void continueBooking() {
        BookOrderModel bookOrderModel = new BookOrderModel();
        if (booking_from == MainActivity.BOOKING_FROM_RESCHEDULE) {
            bookOrderModel.id = requestModel.id;
        }
        if (AppDelegate.isValidString(selectedDate) && AppDelegate.isValidString(selectedTime)) {
            bookOrderModel.minimum_booking_time = str_minimumTime;
            if (!checkBookingTime()) {
            } else if (booking_from == MainActivity.BOOKING_FROM_RESCHEDULE) {
                if (needComment || et_instructions.length() > 0) {
                    bookOrderModel.str_date = selectedDate;
                    bookOrderModel.str_time = selectedTime;
                    AppDelegate.LogT("bookOrderModel.str_time => " + bookOrderModel.str_time);
                    bookOrderModel.arrayServices = serviceArray;
                    bookOrderModel.str_service_id = service_id;
                    bookOrderModel.str_description = et_instructions.getText().toString();
                    bookOrderModel.booking_from = booking_from;

                    AddressModel addressModel = new AddressModel();
                    addressModel.name = requestModel.name;
                    addressModel.contact_no = requestModel.phone;
                    addressModel.address = requestModel.address;
                    addressModel.address_2 = requestModel.address2;
                    addressModel.city = requestModel.city;
                    addressModel.state = requestModel.state;
                    addressModel.zip_code = requestModel.zip_code;

                    bookOrderModel.addressModel = addressModel;

                    Intent intent = new Intent(this, OrderSummaryActivity.class);
                    intent.putExtra(Tags.FROM, booking_from);
                    intent.putExtra(Tags.category_id, serviceArray.get(0).category_id);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(ScheduleJobActivity.this, false,
                            new Pair<>(findViewById(R.id.txt_c_titlename), "title"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(ScheduleJobActivity.this, pairs);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Tags.book_order, bookOrderModel);
                    bundle.putInt(Tags.from, booking_from);
                    intent.putExtras(bundle);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                } else {
                    AppDelegate.showToast(ScheduleJobActivity.this, "Please enter Additional Notes");
//                    scrollView.fullScroll(View.FOCUS_DOWN);
//                    et_instructions.setFocusable(true);
//                    et_instructions.setFocusableInTouchMode(true);
//                    AppDelegate.showKeyboard(ScheduleJobActivity.this, et_instructions);
//                    needComment = true;
                }
            } else {
                bookOrderModel.str_date = selectedDate;
                bookOrderModel.str_time = selectedTime;
                AppDelegate.LogT("bookOrderModel.str_time => " + bookOrderModel.str_time);
                bookOrderModel.arrayServices = serviceArray;
                bookOrderModel.str_service_id = service_id;
                bookOrderModel.str_description = description;
                bookOrderModel.booking_from = booking_from;

                Intent intent = new Intent(this, AddAddressActivity.class);
                intent.putExtra(Tags.FROM, booking_from);
                intent.putExtra(Tags.category_id, serviceArray.get(0).category_id);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(ScheduleJobActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_titlename), "title"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(ScheduleJobActivity.this, pairs);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.book_order, bookOrderModel);
                bundle.putInt(Tags.from, booking_from);
                intent.putExtras(bundle);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        } else {
            AppDelegate.showToast(ScheduleJobActivity.this, "Please select valid booking date & time.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
        if (timeHandler != null && timeRunnable != null) {
            timeHandler.removeCallbacks(timeRunnable);
        }
    }

    public void retryAlert() {
        AppDelegate.showAlert(ScheduleJobActivity.this, "Retry", "Something went wrong, please try again later.", "RETRY", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeGetTimeZone();
            }
        }, "CANCEL", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void executeGetTimeZone() {
        if (AppDelegate.haveNetworkConnection(this)) {
            AppDelegate.showProgressDialog(ScheduleJobActivity.this);
            SingletonRestClient.get().getTImeZone(service_id, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                    AppDelegate.hideProgressDialog(ScheduleJobActivity.this);
                    retryAlert();
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    AppDelegate.hideProgressDialog(ScheduleJobActivity.this);
                    retryAlert();
                    try {
                        if (error == null) {
                            return;
                        }
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ScheduleJobActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(ScheduleJobActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        AppDelegate.hideProgressDialog(ScheduleJobActivity.this);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                        {"status":1,"dataFlow":1,"date":"2017-05-15 18:59:58","minimum_time":"6:0","message":"Date and time."}
                        if (obj_json.getString(Tags.status).contains("1")) {
                            try {
                                str_minimumTime = obj_json.getString(Tags.minimum_time);
                                str_global_dateTime = obj_json.getString(Tags.date);

                                getValues();
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }

                        } else {
                            AppDelegate.showAlert(ScheduleJobActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }
}
