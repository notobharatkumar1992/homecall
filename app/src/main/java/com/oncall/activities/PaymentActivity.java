package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryServicePaymentListAdapter;
import com.oncall.adapters.PendingListAdapter;
import com.oncall.constants.Tags;
import com.oncall.models.PendingAmountModel;
import com.oncall.models.RequestModel;
import com.oncall.models.ServiceModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 11-May-17.
 */
public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    public Handler mHandler;
    public Prefs prefs;
    public int grand_total_price = 0;

    public TextView txt_c_pending_amount, txt_c_service_amount, txt_c_grand_total;

    public RecyclerView rv_services;
    public ArrayList<ServiceModel> serviceArray = new ArrayList<>();
    public CategoryServicePaymentListAdapter serviceListAdapter;
    public RequestModel requestModel;
    public String str_order_id = "", notification_id = "", type = "";
    public int from = MainActivity.FROM_ORDER_LIST;

    public RecyclerView rv_pending_amount;
    public ArrayList<PendingAmountModel> arrayPendingAmount = new ArrayList<>();
    public PendingListAdapter pendingListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_acitivity);
        prefs = new Prefs(this);
        requestModel = getIntent().getExtras().getParcelable(Tags.request_model);
        str_order_id = getIntent().getExtras().getString(Tags.order_id);
        from = getIntent().getExtras().getInt(Tags.from);
        notification_id = getIntent().getExtras().getString(Tags.notification_id);
        type = getIntent().getExtras().getString(Tags.type);
        initView();
        setValues();
        setHandler();
        startPayPalService();
        checkDueAmount();
    }

    private void setValues() {
        serviceArray = requestModel.arrayServices;
        grand_total_price = AppDelegate.getValidInt(requestModel.price);
        if (serviceArray != null && serviceArray.size() > 0) {
            serviceListAdapter = new CategoryServicePaymentListAdapter(this, serviceArray, null, true, requestModel.categoryModel.image);
            rv_services.setAdapter(serviceListAdapter);
        }

        if (arrayPendingAmount != null && arrayPendingAmount.size() > 0) {
            findViewById(R.id.rl_pending_amount).setVisibility(View.VISIBLE);
            pendingListAdapter = new PendingListAdapter(this, arrayPendingAmount, null);
            rv_pending_amount.setAdapter(pendingListAdapter);
            txt_c_pending_amount.setText(getString(R.string.currency_symbol) + " " + arrayPendingAmount.get(0).total_amount + "");
            txt_c_service_amount.setText(getString(R.string.currency_symbol) + " " + requestModel.price + "");
            txt_c_grand_total.setText(getString(R.string.currency_symbol) + " " + grand_total_price + "");
            try {
                grand_total_price = grand_total_price + AppDelegate.getValidInt(arrayPendingAmount.get(0).total_amount);
                txt_c_grand_total.setText(getString(R.string.currency_symbol) + " " + grand_total_price + "");
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            findViewById(R.id.rl_pending_amount).setVisibility(View.GONE);
            txt_c_pending_amount.setText(getString(R.string.currency_symbol) + " 0");
            txt_c_service_amount.setText(getString(R.string.currency_symbol) + " " + grand_total_price + "");
            txt_c_grand_total.setText(getString(R.string.currency_symbol) + grand_total_price + "");
        }
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_pay_now).setOnClickListener(this);
        findViewById(R.id.img_c_info).setOnClickListener(this);

        txt_c_pending_amount = (TextView) findViewById(R.id.txt_c_pending_amount);
        txt_c_service_amount = (TextView) findViewById(R.id.txt_c_service_amount);
        txt_c_grand_total = (TextView) findViewById(R.id.txt_c_grand_total);

        rv_services = (RecyclerView) findViewById(R.id.rv_services);
        rv_services.setLayoutManager(new LinearLayoutManager(this));
        rv_services.setNestedScrollingEnabled(false);
        rv_services.setHasFixedSize(true);
        rv_services.setItemAnimator(new DefaultItemAnimator());
        serviceListAdapter = new CategoryServicePaymentListAdapter(this, serviceArray, null, false, "");
        rv_services.setAdapter(serviceListAdapter);

        rv_pending_amount = (RecyclerView) findViewById(R.id.rv_pending_amount);
        rv_pending_amount.setLayoutManager(new LinearLayoutManager(this));
        rv_pending_amount.setNestedScrollingEnabled(false);
        rv_pending_amount.setHasFixedSize(true);
        rv_pending_amount.setItemAnimator(new DefaultItemAnimator());

    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(PaymentActivity.this);
                        break;

                    case 11:
                        AppDelegate.hideProgressDialog(PaymentActivity.this);
                        break;

                    case 1:
                        setValues();
                        break;

                }
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_pay_now:
                onBuyPressed(grand_total_price + "", "HomeCall Order Reference id: " + requestModel.reference_id);
                break;
            case R.id.img_c_info:
                if (arrayPendingAmount != null && arrayPendingAmount.size() > 0) {
                    Intent intent = new Intent(PaymentActivity.this, PendingAmountActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList(Tags.amount, arrayPendingAmount);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    AppDelegate.showToast(PaymentActivity.this, "You've no previous due payment!");
                }
                break;
        }
    }

    private void checkDueAmount() {
        if (AppDelegate.haveNetworkConnection(PaymentActivity.this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().checkDueAmount(prefs.getUserdata().id + "", prefs.getUserdata().sessionid, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                    AppDelegate.showToast(PaymentActivity.this, "Please try again later!");
                    onBackPressed();
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(PaymentActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(PaymentActivity.this, getString(R.string.something_wrong_with_server_response));
                        onBackPressed();
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(PaymentActivity.this, obj_json.getString(Tags.message));
                            new Prefs(PaymentActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            if (MyOrderActivity.mActivity != null)
                                MyOrderActivity.mActivity.finish();
                            startActivity(new Intent(PaymentActivity.this, SignInActivity.class));
                            finish();
                        } else if (obj_json.getString(Tags.status).contains("1")) {
                            JSONArray jsonArray = obj_json.getJSONArray(Tags.order);
                            Type type = new TypeToken<ArrayList<PendingAmountModel>>() {
                            }.getType();
                            if (jsonArray.length() > 0) {
                                arrayPendingAmount = new Gson().fromJson(jsonArray.toString(), type);

                                for (int i = 0; i < arrayPendingAmount.size(); i++) {
                                    arrayPendingAmount.get(i).total_amount = obj_json.getString(Tags.amount);
                                }
                                setValues();
                            }
                        } else {
                            AppDelegate.showAlert(PaymentActivity.this, obj_json.getString("message"));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(PaymentActivity.this, getString(R.string.something_wrong_with_server_response));
                        onBackPressed();
                    }
                }
            });
        }
    }


    /*paypal integration code Start*/
    public void onBuyPressed(String str_amount, String str_item_name) {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(str_amount), "USD", str_item_name, PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */
        Intent intent = new Intent(PaymentActivity.this, com.paypal.android.sdk.payments.PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        intent.putExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, AppDelegate.REQUEST_CODE_PAYMENT);
    }

    private void startPayPalService() {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        startService(intent);
    }

    public String TAG = "paypal";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppDelegate.REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
                        callAsyncBuy(jsonObject.getJSONObject("response").getString("id"), requestModel.price);
//                        AppDelegate.showToast(PaymentActivity.this, "Payment info received from PayPal, Transaction ID is = " + jsonObject.getJSONObject("response").getString("id"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == AppDelegate.REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(PaymentActivity.this, "Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == AppDelegate.REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(PaymentActivity.this, "Profile Sharing code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void callAsyncBuy(String paypal_id, String amount) {
        if (AppDelegate.haveNetworkConnection(PaymentActivity.this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().completeOrder(prefs.getUserdata().id + "", prefs.getUserdata().sessionid, paypal_id, str_order_id, amount, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(PaymentActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(PaymentActivity.this, getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        AppDelegate.sendEventTracker(PaymentActivity.this, "Payment success");
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(PaymentActivity.this, obj_json.getString(Tags.message));
                            new Prefs(PaymentActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            if (MyOrderActivity.mActivity != null)
                                MyOrderActivity.mActivity.finish();
                            startActivity(new Intent(PaymentActivity.this, SignInActivity.class));
                            finish();
                        } else if (obj_json.getString(Tags.status).contains("1")) {
                            if (MyOrderDetailActivity.mActivity != null)
                                MyOrderDetailActivity.mActivity.finish();
                            startActivity(new Intent(PaymentActivity.this, PaymentConfirmationActivity.class));
                            finish();
                            AppDelegate.showToast(PaymentActivity.this, obj_json.getString("message"));
                        } else {
                            AppDelegate.showAlert(PaymentActivity.this, obj_json.getString("message"));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {
        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }
    /*paypal integration code Exit*/
}
