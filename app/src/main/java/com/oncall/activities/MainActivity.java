package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.oncall.AppDelegate;
import com.oncall.Async.LocationAddress;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.fragments.DashboardFragment;
import com.oncall.models.LocationModel;
import com.oncall.models.UserDataModel;
import com.oncall.utils.CircleImageView;
import com.oncall.utils.Prefs;

import SlidingPaneLayout.SlidingPaneLayout1;
import carbon.widget.TextView;

/**
 * Created by Bharat on 17-Jan-17.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    public static Activity mActivity;
    public static final int FROM_PUSH_NOTIFICATION = 0, FROM_CLASS_NOTIFICATION_LIST = 1, FROM_ORDER_LIST = 2;
    public static final int PANEL_HOME = 0, PANEL_MYPROFILE = 1, PANEL_MY_ORDER = 2, PANEL_ABOUT_US = 3, PANEL_CONTACT_US = 4, PANEL_HELP = 5, PANEL_NOTIFICATIONS = 6, PANEL_SETTING = 7, PANEL_LOGOUT = 8, PANEL_PRIVACY_POLICY = 9, PANEL_TERMS_CONDITION = 10, PANEL_SHARE = 11, PANEL_RATE_US = 12;
    public static final int BOOKING_FROM_CATEGORIES = 0, BOOKING_FROM_VENDOR_SEARCH = 1, BOOKING_FROM_RESCHEDULE = 2;
    public Prefs prefs;
    public CircleImageView cimg_user;
    public TextView txt_c_name;
    public TextView txt_c_home, txt_c_my_order, txt_c_about_us, txt_c_contact_us, txt_c_help, txt_c_notifications, txt_c_settings, txt_c_logout;

    public android.widget.LinearLayout side_panel;
    public SlidingPaneLayout1 mSlidingPaneLayout;
    public SlideMenuClickListener menuClickListener = new SlideMenuClickListener();
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;
    public Handler mHandler;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    public boolean apiShouldCall = false;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    private void initView() {
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.mSlidingPaneLayout);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());
        side_panel = (android.widget.LinearLayout) findViewById(R.id.side_panel);
        findViewById(R.id.img_c_menu).setOnClickListener(this);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);

        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_home = (TextView) findViewById(R.id.txt_c_home);
        txt_c_my_order = (TextView) findViewById(R.id.txt_c_my_order);
        txt_c_about_us = (TextView) findViewById(R.id.txt_c_about_us);
        txt_c_contact_us = (TextView) findViewById(R.id.txt_c_contact_us);
        txt_c_help = (TextView) findViewById(R.id.txt_c_help);
        txt_c_notifications = (TextView) findViewById(R.id.txt_c_notifications);
        txt_c_settings = (TextView) findViewById(R.id.txt_c_settings);
        txt_c_logout = (TextView) findViewById(R.id.txt_c_logout);
        findViewById(R.id.img_c_menu).setOnClickListener(this);
        findViewById(R.id.ll_c_home).setOnClickListener(this);
        findViewById(R.id.ll_c_my_order).setOnClickListener(this);
        findViewById(R.id.ll_c_about_us).setOnClickListener(this);
        findViewById(R.id.ll_c_privacy_policy).setOnClickListener(this);
        findViewById(R.id.ll_c_tnc).setOnClickListener(this);
        findViewById(R.id.ll_c_contact_us).setOnClickListener(this);
        findViewById(R.id.ll_c_help).setOnClickListener(this);
        findViewById(R.id.ll_c_notifications).setOnClickListener(this);
        findViewById(R.id.ll_c_settings).setOnClickListener(this);
        findViewById(R.id.ll_c_share).setOnClickListener(this);
        findViewById(R.id.ll_c_rate_us).setOnClickListener(this);
        findViewById(R.id.ll_c_logout).setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        menuClickListener = new SlideMenuClickListener();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mActivity = this;
        prefs = new Prefs(this);
        if (prefs.getLocationdata() == null || !AppDelegate.isValidString(prefs.getLocationdata().name)) {
            apiShouldCall = true;
        }
        initView();
        setHandler();
        displayView(PANEL_HOME);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                    Log.d("Firbase id login", "Refreshed token: " + refreshedToken);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2000);
    }

    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MainActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MainActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MainActivity.this);
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 0:
                        toggleSlider();
                        break;
                    case 4:
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        LocationModel locationModel;
        if (new Prefs(this).getLocationdata() != null)
            locationModel = new Prefs(this).getLocationdata();
        else {
            locationModel = new LocationModel();

            locationModel.location_type = Tags.current_location;
            locationModel.name = data.getString(Tags.city_param) + (AppDelegate.isValidString(data.getString(Tags.STATE)) ? ", " + data.getString(Tags.STATE) : "");
            locationModel.lat = String.valueOf(data.getDouble(Tags.LAT)) + "";
            locationModel.lng = String.valueOf(data.getDouble(Tags.LNG)) + "";
            new Prefs(this).setLocationData(locationModel);
        }
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mGoogleApiClient.connect();
        setData();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);

        new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        try {
            if (apiShouldCall) {
                mHandler.sendEmptyMessage(10);
                LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, this, mHandler);
//                execute_getDashboard();
            }
            /*else if (nearMyLocationClicked) {
                nearMyLocationClicked = false;
                execute_Search();
            }*/
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MainActivity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void setData() {
        UserDataModel userDataModel = new Prefs(this).getUserdata();
        if (userDataModel != null) {
            txt_c_name.setText((AppDelegate.isValidString(userDataModel.first_name) ? userDataModel.first_name : "") + " " + (AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name : ""));
            AppDelegate.LogT("Pic==>" + userDataModel.image);
            if (userDataModel != null && AppDelegate.isValidString(userDataModel.image)) {
                imageLoader.loadImage(new Prefs(this).getUserdata().image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        cimg_user.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });
            }
        } else {
            txt_c_name.setText("Guest");
            cimg_user.setImageResource(R.drawable.user);
        }
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.id + "")) {
            findViewById(R.id.ll_c_my_order).setVisibility(View.VISIBLE);
//            findViewById(R.id.ll_c_notifications).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_c_notifications).setVisibility(View.GONE);
            findViewById(R.id.ll_c_settings).setVisibility(View.VISIBLE);
            txt_c_logout.setText("LOGOUT");
            cimg_user.setOnClickListener(this);
            findViewById(R.id.rl_c_name).setOnClickListener(this);
        } else {
            findViewById(R.id.ll_c_my_order).setVisibility(View.GONE);
            findViewById(R.id.ll_c_notifications).setVisibility(View.GONE);
            findViewById(R.id.ll_c_settings).setVisibility(View.GONE);
            txt_c_logout.setText("LOGIN");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_menu:
                mHandler.sendEmptyMessage(0);
                break;
            case R.id.rl_c_name:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_MYPROFILE, PANEL_MYPROFILE);
                break;
            case R.id.cimg_user:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_MYPROFILE, PANEL_MYPROFILE);
                break;
            case R.id.ll_c_settings:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_SETTING, PANEL_SETTING);
                break;
            case R.id.ll_c_notifications:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_NOTIFICATIONS, PANEL_NOTIFICATIONS);
                break;
            case R.id.ll_c_my_order:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_MY_ORDER, PANEL_MY_ORDER);
                break;
            case R.id.ll_c_home:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_HOME, PANEL_HOME);
                break;
            case R.id.ll_c_about_us:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_ABOUT_US, PANEL_ABOUT_US);
                break;
            case R.id.ll_c_tnc:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_TERMS_CONDITION, PANEL_TERMS_CONDITION);
                break;
            case R.id.ll_c_share:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_SHARE, PANEL_SHARE);
                break;
            case R.id.ll_c_rate_us:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_RATE_US, PANEL_RATE_US);
                break;

            case R.id.ll_c_privacy_policy:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_PRIVACY_POLICY, PANEL_PRIVACY_POLICY);
                break;
            case R.id.ll_c_contact_us:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_CONTACT_US, PANEL_CONTACT_US);
                break;
            case R.id.ll_c_help:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_HELP, PANEL_HELP);
                break;
            case R.id.ll_c_logout:
                mHandler.sendEmptyMessage(0);
                if (new Prefs(MainActivity.this).getUserdata() != null)
                    AppDelegate.sendEventTracker(MainActivity.this, "User Logged Out");
//                execute_logoutAsync();
                mHandler.sendEmptyMessage(11);
                new Prefs(MainActivity.this).setUserData(null);
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                break;
        }
    }

//    private void execute_logoutAsync() {
//        if (AppDelegate.haveNetworkConnection(this) && new Prefs(MainActivity.this).getUserdata() != null) {
//            mHandler.sendEmptyMessage(10);
//            SingletonRestClient.get().logout(new Prefs(MainActivity.this).getUserdata().id + "", new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    new Prefs(MainActivity.this).setUserData(null);
//                    startActivity(new Intent(MainActivity.this, SignInActivity.class));
//                }
//            });
//        } else {
//            mHandler.sendEmptyMessage(11);
//            new Prefs(MainActivity.this).setUserData(null);
//            startActivity(new Intent(MainActivity.this, SignInActivity.class));
//        }
//    }


    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener {
        @Override
        public void onPanelOpened(View panel) {
            if (!isSlideOpen) {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(MainActivity.this);
        }

        @Override
        public void onPanelClosed(View panel) {
            if (isSlideOpen) {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset) {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }

    public void toggleSlider() {
        if (mSlidingPaneLayout != null)
            if (!mSlidingPaneLayout.isOpen()) {
                mSlidingPaneLayout.openPane();
            } else {
                mSlidingPaneLayout.closePane();
            }
    }

    public void whileSlide(int view) {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(position);
                }
            }, 600);
        }
    }

    public void setInitailSideBar(int value) {
        txt_c_home.setSelected(false);
        txt_c_my_order.setSelected(false);
        txt_c_about_us.setSelected(false);
        txt_c_contact_us.setSelected(false);
        txt_c_help.setSelected(false);
        txt_c_notifications.setSelected(false);
        txt_c_settings.setSelected(false);
        txt_c_logout.setSelected(false);
        switch (value) {
            case PANEL_HOME:
                txt_c_home.setSelected(true);
                break;
            case PANEL_MYPROFILE:
                break;
            case PANEL_MY_ORDER:
                txt_c_my_order.setSelected(true);
                break;
            case PANEL_SETTING:
                txt_c_settings.setSelected(true);
                break;
            case PANEL_LOGOUT:
                txt_c_logout.setSelected(true);
                break;
            case PANEL_ABOUT_US:
//                txt_c_about_us.setSelected(true);
                break;
            case PANEL_TERMS_CONDITION:
//                txt_c_ter.setSelected(true);
                break;
            case PANEL_PRIVACY_POLICY:
//                txt_c_about_us.setSelected(true);
                break;

            case PANEL_CONTACT_US:
                txt_c_contact_us.setSelected(true);
                break;
            case PANEL_HELP:
                txt_c_help.setSelected(true);
                break;
            case PANEL_NOTIFICATIONS:
                txt_c_notifications.setSelected(true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mSlidingPaneLayout.isOpen())
            mSlidingPaneLayout.closePane();
        else if (getSupportFragmentManager().findFragmentById(R.id.main_containt) instanceof DashboardFragment)
            finish();
        else
            super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        try {
//            if (mGoogleApiClient.isConnected()) {
//                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MainActivity.this);
//                mGoogleApiClient.disconnect();
//            }
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, MainActivity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            setloc(currentLatitude, currentLongitude);
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
//            if (apiShouldCall) {
//                execute_getDashboard();
//            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(MainActivity.this);
        setInitailSideBar(position);
        switch (position) {
            case PANEL_HOME:
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new DashboardFragment(), R.id.main_containt);
                return;
            case PANEL_MYPROFILE:
                if (new Prefs(MainActivity.this).getUserdata() != null && new Prefs(MainActivity.this).getUserdata().id > 0)
                    startActivity(new Intent(this, MyProfileActivity.class));
                break;
            case PANEL_MY_ORDER:
                startActivity(new Intent(this, MyOrderActivity.class));
                break;
            case PANEL_ABOUT_US: {
                Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
                intent.putExtra("type", 0);
                startActivity(intent);
                break;
            }
            case PANEL_PRIVACY_POLICY: {
                Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
                intent.putExtra("type", 2);
                startActivity(intent);
                break;
            }
            case PANEL_TERMS_CONDITION: {
                Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
                intent.putExtra("type", 3);
                startActivity(intent);
                break;
            }
            case PANEL_SHARE: {
                AppDelegate.shareApplicationUrl(MainActivity.this, "Please download our application using url.");
                break;
            }
            case PANEL_RATE_US: {
                AppDelegate.openInPlayStore(MainActivity.this);
                break;
            }
            case PANEL_CONTACT_US:
                startActivity(new Intent(this, ContactUsActivity.class));
                break;
            case PANEL_HELP: {
                Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
                intent.putExtra("type", 1);
                startActivity(intent);
//                openUrl(ServerRequestConstants.HELP_URL);
                break;
            }
            case PANEL_NOTIFICATIONS:
                startActivity(new Intent(MainActivity.this, NotificationUserActivity.class));
                break;
            case PANEL_SETTING:
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
                break;
            case PANEL_LOGOUT:
                new Prefs(MainActivity.this).setUserData(null);
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
//                finish();
                break;
        }
    }

    public void openUrl(String str_url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(str_url));
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

}
