package com.oncall.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.oncall.AppDelegate;
import com.oncall.Async.LocationAddress;
import com.oncall.Async.PlacesService;
import com.oncall.R;
import com.oncall.adapters.CategoryAdapterSearch;
import com.oncall.adapters.LocationMapAdapter;
import com.oncall.adapters.ServicesAdapterSearch;
import com.oncall.constants.Tags;
import com.oncall.expandablelayout.ExpandableRelativeLayout;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.CategoryModel;
import com.oncall.models.Place;
import com.oncall.models.UserDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.parser.JSONParser;
import com.oncall.utils.CircleImageView;
import com.oncall.utils.Prefs;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.yalantis.ucrop.UCrop;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ImageView;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;

/**
 * Created by Heena on 12-Jan-17.
 */
public class EditVendorProfileActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    private CircleImageView cimg_user;
    private UserDataModel userDataModel;
    ImageView img_c_loading1;
    private EditText et_contact, et_password, et_confirm_password, et_first_name, et_last_name, et_address, et_search, et_city, et_state, et_address_2, et_city_2;
    private Handler mHandler;
    private Dialog dialogue;
    CategoryAdapterSearch categoryAdapter;

    ServicesAdapterSearch serviceAdpter;
    private ListView list_view;
    public int category_id = 0;
    String service_id = "", service = "";
    TextView txt_c_search_dialog_title, txt_c_category, txt_c_services, txt_c_done, txt_c_username, txt_c_currency;
    ArrayList<CategoryModel> categoryArray = new ArrayList<>();
    ArrayList<CategoryModel> serviceArray = new ArrayList<>();

    RelativeLayout rl_search;
    private RecyclerView recycler_deals;

    private boolean[] is_checked;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private FileBody imageFile;
    private double currentLatitude = 0, currentLongitude = 0;


    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;

    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    public static final int COLLAPSE = 0, EXPAND = 1;
    Prefs prefs;

    private boolean canSearch = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.edit_vendor_profile);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        initView();
        prefs = new Prefs(this);
        setHandler();
        setData();
    }

    private void setData() {
        userDataModel = prefs.getUserdata();
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.image)) {
            img_c_loading1.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
            frameAnimation.setCallback(img_c_loading1);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            imageLoader.loadImage(new Prefs(this).getUserdata().image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                    img_c_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    cimg_user.setImageBitmap(bitmap);
                    img_c_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    img_c_loading1.setVisibility(View.GONE);
                }
            });
        }
        if (userDataModel != null) {
            txt_c_username.setText(userDataModel.email);
            et_contact.setText(userDataModel.contact_no);
            et_first_name.setText(userDataModel.first_name);
            et_last_name.setText(userDataModel.last_name);
            try {
                if (AppDelegate.isValidString(userDataModel.address)) {
                    et_address.setText(AppDelegate.isValidString(userDataModel.address) ? userDataModel.address + "" : "");
                    et_city.setText(AppDelegate.isValidString(userDataModel.city) ? userDataModel.city + "" : "");
                    et_state.setText(AppDelegate.isValidString(userDataModel.state) ? userDataModel.state + "" : "");
                    if (AppDelegate.isValidString(userDataModel.city2) || AppDelegate.isValidString(userDataModel.address2)) {
                        et_city.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                        findViewById(R.id.rl_c_additional_address).setVisibility(View.GONE);
                        findViewById(R.id.rl_address_2).setVisibility(View.VISIBLE);
                        findViewById(R.id.rl_city_2).setVisibility(View.VISIBLE);
                        et_address_2.setText(AppDelegate.isValidString(userDataModel.address2) ? userDataModel.address2 + "" : "");
                        et_city_2.setText(AppDelegate.isValidString(userDataModel.city2) ? userDataModel.city2 + "" : "");
                    } else {
                        et_city.setImeOptions(EditorInfo.IME_ACTION_DONE);
                        findViewById(R.id.rl_c_additional_address).setVisibility(View.VISIBLE);
                        findViewById(R.id.rl_address_2).setVisibility(View.GONE);
                        findViewById(R.id.rl_city_2).setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(EditVendorProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(EditVendorProfileActivity.this);
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
//                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        mHandler.sendEmptyMessage(10);
                        AppDelegate.LogT("mHandler.sendEmptyMessage(3) called choose location Activity;");
                        LocationAddress.getLocationFromReverseGeocoder(et_address.getText().toString(), EditVendorProfileActivity.this, mHandler);
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null) {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0) {
                            if (et_address.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        if (canSearch) {
                            if (et_address.length() != 0) {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            if (et_address.length() != 0) {
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 9:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_address.length() != 0) {
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;

                }
            }
        };
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude = " + currentLatitude + ", longitude = " + currentLongitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            try {
                currentLatitude = data.getDouble(Tags.latitude);
                currentLongitude = data.getDouble(Tags.longitude);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
        }

    }

    private void addtextWatcher() {
        et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                AppDelegate.LogT("et_search afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private void initView() {
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        et_contact = (EditText) findViewById(R.id.et_contact);
        txt_c_username = (TextView) findViewById(R.id.txt_c_username);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        et_contact.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        txt_c_username.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_confirm_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.rl_c_additional_address).setOnClickListener(this);
        et_confirm_password.setVisibility(View.GONE);
        et_password.setVisibility(View.GONE);

        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_address = (EditText) findViewById(R.id.et_address);
        et_address.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_address_2 = (EditText) findViewById(R.id.et_address_2);
        et_address_2.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_city_2 = (EditText) findViewById(R.id.et_city_2);
        et_city_2.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_city = (EditText) findViewById(R.id.et_city);
        et_city.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_state = (EditText) findViewById(R.id.et_state);
        et_state.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        txt_c_category = (TextView) findViewById(R.id.txt_c_category);
        txt_c_services = (TextView) findViewById(R.id.txt_c_services);
        txt_c_currency = (TextView) findViewById(R.id.txt_c_currency);

        findViewById(R.id.rl_c_currency).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(this, mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);
    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(EditVendorProfileActivity.this, false)) {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(EditVendorProfileActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_address.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(et_address.getText().toString()));
                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };

    private void setloc(double latitude, double longitude) {
        mHandler.sendEmptyMessage(10);
        LocationAddress.getAddressFromLocation(latitude, longitude, EditVendorProfileActivity.this, mHandler);
    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                executeEditProfileAsync();
                break;
            case R.id.rl_c_additional_address:
                findViewById(R.id.rl_c_additional_address).setVisibility(View.GONE);
                et_city.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                findViewById(R.id.rl_address_2).setVisibility(View.VISIBLE);
                findViewById(R.id.rl_city_2).setVisibility(View.VISIBLE);
                break;
            case R.id.cimg_user:
                showImageSelectorList();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.rl_c_services:
                AppDelegate.LogT("rl_c_services called");
                if (category_id == 0) {
                    AppDelegate.showToast(this, getString(R.string.please_select_category));
                } else {
                    if (serviceArray.size() == 0) {
                        executeServiceAsync();
                    } else {
                        serviceDialog(serviceArray);
                    }
                }
                break;

            case R.id.rl_c_category:
                AppDelegate.LogT("rl_c_category called");
                if (categoryArray.size() == 0) {
                    executeCategoryAsync();
                } else {
                    categoryDialog(categoryArray);
                }
                break;
        }
    }


    private void executeCategoryAsync() {
       /* if (capturedFile == null) {
            AppDelegate.showToast(this, "Please select an image.");
        } else*/
        if (AppDelegate.haveNetworkConnection(this)) {

            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getCategory(new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(EditVendorProfileActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(EditVendorProfileActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            JSONArray jsonArray = obj_json.getJSONArray(Tags.response);
                            categoryArray.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.id = object.getInt(Tags.id);
                                categoryModel.title = object.getString(Tags.category);
                                categoryModel.image = object.getString(Tags.image);
                                categoryArray.add(categoryModel);
                            }
                            categoryDialog(categoryArray);
                        } else {
                            AppDelegate.showToast(EditVendorProfileActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executeServiceAsync() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getServices(category_id, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(EditVendorProfileActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(EditVendorProfileActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            JSONArray jsonArray = obj_json.getJSONArray(Tags.response);
                            serviceArray.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.id = object.getInt(Tags.id);
                                categoryModel.title = object.getString(Tags.sub_category);
//                                categoryModel.image = object.getString(Tags.image);
                                serviceArray.add(categoryModel);
                            }
                            serviceDialog(serviceArray);
                        } else {
                            AppDelegate.showToast(EditVendorProfileActivity.this, obj_json.getString(Tags.message));

                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    public void dialogsearch() {
        dialogue = new Dialog(this);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog_search);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        et_search = (EditText) dialogue.findViewById(R.id.et_search);
        txt_c_search_dialog_title = (TextView) dialogue.findViewById(R.id.txt_c_search_dialog_title);
        rl_search = (RelativeLayout) dialogue.findViewById(R.id.rl_search);
        txt_c_done = (TextView) dialogue.findViewById(R.id.txt_c_done);
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        list_view = (ListView) dialogue.findViewById(R.id.dialog_list);
    }

    private void categoryDialog(final ArrayList<CategoryModel> categoryArray) {
        dialogsearch();
        list_view.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        list_view.setItemsCanFocus(false);
        txt_c_done.setVisibility(View.GONE);
        rl_search.setVisibility(View.VISIBLE);
        txt_c_search_dialog_title.setText(getString(R.string.select_category));
        categoryAdapter = new CategoryAdapterSearch(this, categoryArray);
        list_view.setAdapter(categoryAdapter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_c_category.setText(categoryArray.get(position).title);
                AppDelegate.LogT("onItemClick = " + categoryAdapter.getItem(position).id + "");
                for (CategoryModel cityItems : categoryArray) {
                    if (cityItems.title.equalsIgnoreCase(categoryAdapter.getItem(position).title + "")) {
                        txt_c_category.setText(cityItems.title);
                        category_id = cityItems.id;
                        txt_c_category.setError(null);
                        txt_c_category.clearFocus();
                        txt_c_services.setText("");
                        service_id = "";
                        serviceArray.clear();
                        if (EditVendorProfileActivity.this != null && dialogue != null && dialogue.isShowing()) {
                            dialogue.dismiss();
                        }
                        break;
                    }
                }
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                categoryAdapter.filter(et_search.getText().toString());
            }
        });
        if (dialogue != null && dialogue.isShowing()) {
            dialogue.dismiss();
            dialogue.show();
        } else {
            if (dialogue != null)
                dialogue.show();
        }
    }


    public void serviceDialog(final ArrayList<CategoryModel> serviceArray) {
        if (serviceArray != null) {
            if (is_checked == null || is_checked.length != serviceArray.size()) {
                is_checked = new boolean[serviceArray.size()];
                for (int i = 0; i < serviceArray.size(); i++) {
                    if (service_id.equalsIgnoreCase(serviceArray.get(i).id + "")) {
                        is_checked[i] = true;
                    } else
                        is_checked[i] = false;
                }
            } else {
                for (int i = 0; i < is_checked.length; i++) {
                    serviceArray.get(i).checked = is_checked[i] ? 1 : 0;
                }
            }

            final Dialog categoryDialog = new Dialog(this, android.R.style.Theme_Light);
            categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            categoryDialog.setContentView(R.layout.dialog_services);
            recycler_deals = (RecyclerView) categoryDialog.findViewById(R.id.recycler_deals);
            recycler_deals.setNestedScrollingEnabled(false);
            recycler_deals.setLayoutManager(new LinearLayoutManager(this));
            recycler_deals.setItemAnimator(new DefaultItemAnimator());

            serviceAdpter = new ServicesAdapterSearch(this, serviceArray, new OnListItemClickListener() {
                @Override
                public void setOnListItemClickListener(String name, int position) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, int PICK_UP) {

                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                    AppDelegate.LogT("interface value => " + like_dislike);
                    serviceArray.get(position).checked = like_dislike ? 0 : 1;
                    serviceAdpter.notifyDataSetChanged();
                    recycler_deals.invalidate();
                }
            });
            recycler_deals.setAdapter(serviceAdpter);

            categoryDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuilder stringBuilder = null;
                    StringBuilder service_name = null;
                    for (int i = 0; i < serviceArray.size(); i++) {
                        if (serviceArray.get(i).checked == 1) {
                            is_checked[i] = true;
                            if (stringBuilder == null) {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("" + serviceArray.get(i).id);
                            } else {
                                stringBuilder.append("," + serviceArray.get(i).id);
                            }
                            if (service_name == null) {
                                service_name = new StringBuilder();
                                service_name.append("" + serviceArray.get(i).title);
                            } else {
                                service_name.append(", " + serviceArray.get(i).title);
                            }
                        } else {
                            is_checked[i] = false;
                        }
                    }
                    if (stringBuilder != null) {
                        service_id = stringBuilder.toString();
                        service = service_name.toString();
                        txt_c_services.setText(service + "");
                        categoryDialog.dismiss();
                    } else {
                        AppDelegate.showToast(EditVendorProfileActivity.this, "Please select service.");
                    }
                }
            });
            categoryDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDialog.dismiss();
                }
            });
            categoryDialog.show();
        }
    }

    /* image selection*/
    public static File capturedFile;
    public static Uri imageURI = null;

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(EditVendorProfileActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditVendorProfileActivity.this);
        ListView modeList = new ListView(EditVendorProfileActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(EditVendorProfileActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private Bitmap OriginalPhoto;

    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                OriginalPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 512, 512, true);
                cimg_user.setImageBitmap(OriginalPhoto);
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(EditVendorProfileActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(EditVendorProfileActivity.this)) {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position) {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                } else if (AppDelegate.isValidString(placeDetail_pickup.name)) {
                    et_address.setText(placeDetail_pickup.name);
                } else if (AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.vicinity);
                }
                et_address.setSelection(et_address.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(3);
            }
            canSearch = true;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile(EditVendorProfileActivity.this);
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(EditVendorProfileActivity.this, getString(R.string.file_not_created));
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public static String getNewFile(Context mContext) {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getString(R.string.app_name));
        } else {
            directoryFile = mContext.getDir(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppDelegate.SELECT_PICTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(EditVendorProfileActivity.this, data.getData());
                } else {
                    Toast.makeText(EditVendorProfileActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (imageURI != null) {
                    startCropActivity(EditVendorProfileActivity.this, imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        capturedFile = new File(mActivity.getCacheDir(), destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(capturedFile));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(100);

//        if (SellItemFragment.onPictureResult != null) {
//            options.setHideBottomControls(false);
//            options.setFreeStyleCropEnabled(false);
//        } else {
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
//        }


        return uCrop.withOptions(options);
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void writeImageFile(Bitmap bitmap, String filePath) {
        if (AppDelegate.isValidString(filePath) && bitmap != null) {
            capturedFile = new File(filePath);
        } else {
            return;
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    private void executeEditProfileAsync() {
        if (OriginalPhoto != null) {
            writeImageFile(OriginalPhoto, getNewFile(EditVendorProfileActivity.this));
        }
        if (et_first_name.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_first_name));
        } else if (et_last_name.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_last_name));
        } else if (et_contact.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_address_enter));
//        } else if (et_contact.length() < 10) {
//            AppDelegate.showToast(this, getString(R.string.validation_phone_number_valid));
        } else if (et_address.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_address_enter));
//        } else if (currentLatitude == 0 && currentLongitude == 0) {
//            AppDelegate.showToast(this, getString(R.string.validation_address_select));
        } else if (et_city.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_city_enter));
//        } else if (et_state.length() == 0) {
//            AppDelegate.showToast(this, getString(R.string.validation_state_enter));
        } else if (AppDelegate.haveNetworkConnection(this)) {

            String device_token = new Prefs(EditVendorProfileActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            mHandler.sendEmptyMessage(10);
            TypedFile typedFileCurrentPhotoPath = null;
            if (capturedFile != null) {
                AppDelegate.LogT("while image not selected.");
                typedFileCurrentPhotoPath = new TypedFile("multipart/form-data", new File(String.valueOf(capturedFile)));
                SingletonRestClient.get().editProfile(userDataModel.id + "", userDataModel.sessionid + "", typedFileCurrentPhotoPath, et_first_name.getText().toString(),
                        et_last_name.getText().toString(),
                        et_address.getText().toString(),
                        et_address_2.getText().toString(),
                        et_contact.getText().toString(),
                        AppDelegate.DEVICE_TYPE,
                        device_token,
                        et_city.getText().toString(),
                        et_city_2.getText().toString(),
                        et_state.getText().toString(), new Callback<Response>() {
                            @Override
                            public void failure(RestError restError) {
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                super.failure(error);
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                    AppDelegate.checkJsonMessage(EditVendorProfileActivity.this, obj_json);
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                    AppDelegate.showToast(EditVendorProfileActivity.this, "");
                                }
                            }

                            @Override
                            public void success(Response response, Response response2) {
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                    if (obj_json.getInt(Tags.session_status) == 0) {
                                        AppDelegate.showToast(EditVendorProfileActivity.this, obj_json.getString(Tags.message));
                                        new Prefs(EditVendorProfileActivity.this).setUserData(null);
                                        if (MainActivity.mActivity != null)
                                            MainActivity.mActivity.finish();
                                        if (MyProfileActivity.mActivity != null)
                                            MyProfileActivity.mActivity.finish();
                                        startActivity(new Intent(EditVendorProfileActivity.this, SignInActivity.class));
                                        finish();
                                    } else if (obj_json.getInt(Tags.status) == 1) {
                                        AppDelegate.showToast(EditVendorProfileActivity.this, obj_json.getString(Tags.message));
                                        JSONObject object = obj_json.getJSONObject(Tags.response);
                                        UserDataModel userDataModel = new Prefs(EditVendorProfileActivity.this).getUserdata();
                                        userDataModel.id = JSONParser.getInt(object, Tags.id);
                                        userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                                        userDataModel.email = JSONParser.getString(object, Tags.email);
                                        userDataModel.first_name = JSONParser.getString(object, Tags.name);
                                        userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                                        userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                                        userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                                        userDataModel.address = JSONParser.getString(object, Tags.address1);
                                        userDataModel.address2 = JSONParser.getString(object, Tags.address2);
                                        userDataModel.city = JSONParser.getString(object, Tags.city);
                                        userDataModel.city2 = JSONParser.getString(object, Tags.city_2);
                                        userDataModel.state = JSONParser.getString(object, Tags.state);
                                        userDataModel.country = JSONParser.getString(object, Tags.country);
                                        userDataModel.zip = JSONParser.getString(object, Tags.zip);
                                        userDataModel.token = JSONParser.getString(object, Tags.token);
                                        userDataModel.device_id = JSONParser.getString(object, Tags.device_id);

                                        new Prefs(EditVendorProfileActivity.this).setUserData(userDataModel);
                                        finish();
                                    } else {
                                        AppDelegate.checkJsonMessage(EditVendorProfileActivity.this, obj_json);

                                    }
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                        });
            } else {
                SingletonRestClient.get().editProfile(userDataModel.id + "", userDataModel.sessionid + "", et_first_name.getText().toString(),
                        et_last_name.getText().toString(),
                        et_address.getText().toString(),
                        et_address_2.getText().toString(),
                        et_contact.getText().toString(),
                        AppDelegate.DEVICE_TYPE,
                        device_token,
                        et_city.getText().toString(),
                        et_city_2.getText().toString(),
                        et_state.getText().toString() + "", new Callback<Response>() {
                            @Override
                            public void failure(RestError restError) {
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                super.failure(error);
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                    AppDelegate.checkJsonMessage(EditVendorProfileActivity.this, obj_json);
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                    AppDelegate.showToast(EditVendorProfileActivity.this, "");
                                }
                            }

                            @Override
                            public void success(Response response, Response response2) {
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                    if (obj_json.getInt(Tags.session_status) == 0) {
                                        AppDelegate.showToast(EditVendorProfileActivity.this, obj_json.getString(Tags.message));
                                        new Prefs(EditVendorProfileActivity.this).setUserData(null);
                                        if (MainActivity.mActivity != null)
                                            MainActivity.mActivity.finish();
                                        if (MyProfileActivity.mActivity != null)
                                            MyProfileActivity.mActivity.finish();
                                        startActivity(new Intent(EditVendorProfileActivity.this, SignInActivity.class));
                                        finish();
                                    } else if (obj_json.getInt(Tags.status) == 1) {
                                        AppDelegate.showToast(EditVendorProfileActivity.this, obj_json.getString(Tags.message));
                                        JSONObject object = obj_json.getJSONObject(Tags.response);
                                        UserDataModel userDataModel = new Prefs(EditVendorProfileActivity.this).getUserdata();
                                        userDataModel.id = JSONParser.getInt(object, Tags.id);
                                        userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                                        userDataModel.email = JSONParser.getString(object, Tags.email);
                                        userDataModel.name = JSONParser.getString(object, Tags.name);
                                        userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                                        userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                                        userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                                        userDataModel.address = JSONParser.getString(object, Tags.address1);
                                        userDataModel.address2 = JSONParser.getString(object, Tags.address2);
                                        userDataModel.city = JSONParser.getString(object, Tags.city);
                                        userDataModel.city2 = JSONParser.getString(object, Tags.city_2);
                                        userDataModel.state = JSONParser.getString(object, Tags.state);
                                        userDataModel.country = JSONParser.getString(object, Tags.country);
                                        userDataModel.zip = JSONParser.getString(object, Tags.zip);
                                        userDataModel.token = JSONParser.getString(object, Tags.token);
                                        userDataModel.device_id = JSONParser.getString(object, Tags.device_id);

                                        new Prefs(EditVendorProfileActivity.this).setUserData(userDataModel);
                                        finish();
                                    } else {
                                        AppDelegate.checkJsonMessage(EditVendorProfileActivity.this, obj_json);

                                    }
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                        });
            }
        }
    }
}
