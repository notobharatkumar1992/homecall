package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.models.UserDataModel;
import com.oncall.utils.Prefs;
import com.oncall.utils.TransitionHelper;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    public static Activity mActivity;
    public Handler mHandler;
    public TextView txt_c_range;
    public UserDataModel userDataModel;
    private RelativeLayout rl_c_change_password, rl_c_upgrade_membership;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);
        userDataModel = new Prefs(this).getUserdata();
        mActivity = this;
        initView();
        setValues();
        setHandler();
    }

    public void initView() {
        txt_c_range = (TextView) findViewById(R.id.txt_c_range);
        rl_c_change_password = (RelativeLayout) findViewById(R.id.rl_c_change_password);
        rl_c_upgrade_membership = (RelativeLayout) findViewById(R.id.rl_c_upgrade_membership);
        txt_c_range.setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_change_password).setOnClickListener(this);
        findViewById(R.id.txt_c_phone).setOnClickListener(this);
    }

    public void setValues() {
//        if (AppDelegate.isValidString(new Prefs(SettingActivity.this).getStringValuefromTemp(Tags.social_id, ""))) {
//            rl_c_change_password.setVisibility(View.GONE);
//        } else {
//            rl_c_change_password.setVisibility(View.VISIBLE);
//        }
//        if (AppDelegate.isValidString(new Prefs(SettingActivity.this).getUserdata().social_id)) {
//            rl_c_change_password.setVisibility(View.GONE);
//        } else {
//            rl_c_change_password.setVisibility(View.VISIBLE);
//        }
       /* if (new Prefs(this).getUserdata() != null) {
            if (new Prefs(this).getUserdata().membership_id == 2) {
                rl_c_upgrade_membership.setVisibility(View.GONE);
            } else {
                rl_c_upgrade_membership.setVisibility(View.VISIBLE);
            }
        } else {
            rl_c_upgrade_membership.setVisibility(View.GONE);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        setValues();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SettingActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SettingActivity.this);
                        break;
                }
            }
        };
    }

    public Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_range:
//                showRangeDialog();
                break;
            case R.id.txt_c_change_password: {
                AppDelegate.showAlert(SettingActivity.this, "Change Password", "Are you sure you want to change password?", "Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intent = new Intent(SettingActivity.this, ChangePasswordActivity.class);
                        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SettingActivity.this, false, new Pair<>(findViewById(R.id.txt_c_change_password_ph), "change_password"));
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SettingActivity.this, pairs);
                        startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                    }
                }, "No", null);
                break;
            }
            case R.id.txt_c_phone: {
                AppDelegate.showAlert(SettingActivity.this, "Change Phone Number", "Are you sure you want to change phone number?", "Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intent = new Intent(SettingActivity.this, ChangePhoneNumberActivity.class);
                        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SettingActivity.this, false, new Pair<>(findViewById(R.id.txt_c_change_phone), "change_password"));
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SettingActivity.this, pairs);
                        startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                    }
                }, "No", null);

                break;
            }

//            case R.id.txt_c_upgrade: {
//                intent = new Intent(SettingActivity.this, UpgradeMembershipActivity.class);
//                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SettingActivity.this, false, new Pair<>(findViewById(R.id.txt_c_upgrade_ph), "upgrade_membership"));
//                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SettingActivity.this, pairs);
//                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
//                break;
//            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    //    private void parseUpdateRange(String result) {
//        try {
//            JSONObject jsonObject = new JSONObject(result);
//            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//                UserDataModel userDataModel = new Prefs(this).getUserdata();
//                userDataModel.user_range = int_seek_range;
//                new Prefs(this).setUserData(userDataModel);
//                // finish();
//            } else {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            }
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
//
//    }
//    public int int_seek_range = 10;
}
