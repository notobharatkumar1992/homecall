package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.VendorSearchDetailAdapter;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListCategoryItemClick;
import com.oncall.models.CategoryModel;
import com.oncall.models.VendorModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.parser.JSONParser;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class VendorSearchListActivity extends AppCompatActivity implements View.OnClickListener, OnListCategoryItemClick {

    public static Activity mActivity;
    public Handler mHandler;
    public TextView txt_c_range;
    public CategoryModel categoryModel;
    private ArrayList<VendorModel> vendorModelArrayList;
    EditText et_search;
    VendorSearchDetailAdapter vendorSearchDetailAdapter;
    RecyclerView rv_category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.green);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.vendor_search_list_activity);
        mActivity = this;
        categoryModel = getIntent().getParcelableExtra(Tags.category);
        initView();
        setValues();
        setHandler();
        executeVendorsAsync();
    }

    public void initView() {
        rv_category = (RecyclerView) findViewById(R.id.rv_category);
        rv_category.setLayoutManager(new LinearLayoutManager(this));
        rv_category.setHasFixedSize(true);
        rv_category.setItemAnimator(new DefaultItemAnimator());
        findViewById(R.id.img_c_back).setOnClickListener(this);
        ((TextView) findViewById(R.id.txt_c_titlename)).setText(categoryModel.title);
        et_search = (EditText) findViewById(R.id.et_search);
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mHandler.sendEmptyMessage(1);
            }
        });
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
    }

    private void executeVendorsAsync() {
        if (AppDelegate.haveNetworkConnection(this)) {
            String latitude = "0", longitude = "0";
            if (new Prefs(VendorSearchListActivity.this).getLocationdata() != null) {
                AppDelegate.LogT("executeVendorsAsync => location data available.");
                latitude = new Prefs(VendorSearchListActivity.this).getLocationdata().lat;
                longitude = new Prefs(VendorSearchListActivity.this).getLocationdata().lng;
            } else {
                AppDelegate.LogT("executeVendorsAsync => location data not available.");
                latitude = new Prefs(VendorSearchListActivity.this).getUserdata().latitude;
                longitude = new Prefs(VendorSearchListActivity.this).getUserdata().longitude;
            }
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getVendors(categoryModel.id + "", latitude, longitude, new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray categJsonArray = obj_json.getJSONArray(Tags.category);
                            vendorModelArrayList = new ArrayList<>();
                            for (int i = 0; i < categJsonArray.length(); i++) {
                                JSONObject object = categJsonArray.getJSONObject(i);
                                VendorModel vendorModel = new VendorModel();
                                vendorModel.id = JSONParser.getString(object, Tags.id);
                                vendorModel.email = JSONParser.getString(object, Tags.email);
                                vendorModel.name = JSONParser.getString(object, Tags.name);
                                vendorModel.last_name = JSONParser.getString(object, Tags.last_name);
                                vendorModel.profile_pic = JSONParser.getString(object, Tags.profile_pic);
                                vendorModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                                vendorModel.address1 = JSONParser.getString(object, Tags.address1);
                                vendorModel.address2 = JSONParser.getString(object, Tags.address_2);
                                vendorModel.city = JSONParser.getString(object, Tags.city);
                                vendorModel.state = JSONParser.getString(object, Tags.state);
                                vendorModel.country = JSONParser.getString(object, Tags.country);
                                vendorModel.zip = JSONParser.getString(object, Tags.zip);
                                vendorModel.currency_id = JSONParser.getString(object, Tags.currency_id);

                                JSONObject vendorobj = object.getJSONObject(Tags.vendor);
                                vendorModel.vendor_id = JSONParser.getString(vendorobj, Tags.id);
                                vendorModel.member_since = JSONParser.getString(vendorobj, Tags.member_since);
                                vendorModel.membership_id = JSONParser.getString(vendorobj, Tags.membership_id);
                                vendorModel.business_name = JSONParser.getString(vendorobj, Tags.business_name);
                                vendorModel.business_subcategory_id = JSONParser.getString(vendorobj, Tags.business_subcategory_id);
                                vendorModel.business_category_id = JSONParser.getString(vendorobj, Tags.business_category_id);
                                vendorModel.latitude = JSONParser.getString(vendorobj, Tags.latitude);
                                vendorModel.longitude = JSONParser.getString(vendorobj, Tags.longitude);
                                vendorModel.payment_status = JSONParser.getString(vendorobj, Tags.payment_status);
                                vendorModel.paid_amt = JSONParser.getString(vendorobj, Tags.paid_amt);
                                vendorModel.category = JSONParser.getString(object.getJSONObject(Tags.business_category_id), Tags.category);
                                vendorModel.image = JSONParser.getString(object.getJSONObject(Tags.business_category_id), Tags.image);
                                vendorModel.icon = JSONParser.getString(object.getJSONObject(Tags.business_category_id), Tags.icon);
                                vendorModel.full_name = JSONParser.getString(object, Tags.full_name);
                                vendorModel.name_email = JSONParser.getString(object, Tags.name_email);
                                vendorModelArrayList.add(vendorModel);
                            }
                            setValues();
                        } else {
                            AppDelegate.showToast(VendorSearchListActivity.this, obj_json.getString(Tags.message));
                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(VendorSearchListActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }


    public void setValues() {
        if (vendorModelArrayList != null && vendorModelArrayList.size() > 0) {
            vendorSearchDetailAdapter = new VendorSearchDetailAdapter(VendorSearchListActivity.this, vendorModelArrayList, VendorSearchListActivity.this);
            rv_category.setAdapter(vendorSearchDetailAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(VendorSearchListActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(VendorSearchListActivity.this);
                        break;
                    case 1:
                        if (vendorModelArrayList != null && vendorModelArrayList.size() > 0) {
                            AppDelegate.LogT("cityModels=");
                            vendorSearchDetailAdapter = new VendorSearchDetailAdapter(VendorSearchListActivity.this, vendorModelArrayList, VendorSearchListActivity.this);
                            rv_category.setAdapter(vendorSearchDetailAdapter);
                            vendorSearchDetailAdapter.notifyDataSetChanged();
                            rv_category.invalidate();
                            vendorSearchDetailAdapter.filterSting(et_search.getText().toString());
                        } else {
                            rv_category.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        };
    }

    public Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, VendorModel categoryModel, View view) {
        if (name.equalsIgnoreCase(Tags.city_name)) {
            AppDelegate.LogT("cityModel==" + categoryModel);
            Intent intent = new Intent(this, VendorSearchProfileActivity.class);
            intent.putExtra(Tags.vendor, categoryModel);
            final Pair<View, String>[] pairs = com.oncall.TransitionHelper.createSafeTransitionParticipants(this, false, new Pair<>(view, "square_blue_name"));
            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
            startActivity(intent/*, transitionActivityOptions.toBundle()*/);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }
}



