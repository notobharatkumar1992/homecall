package com.oncall.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.NotificationListAdapter;
import com.oncall.constants.Tags;
import com.oncall.models.NotificationModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 03-Apr-17.
 */
public class NotificationUserActivity extends AppCompatActivity implements View.OnClickListener {

    public Handler mHandler;
    public ImageView img_c_menu;
    public TextView txt_c_titlename;

    public RecyclerView rv_category;
    public TextView txt_c_address;
    public Prefs prefs;
    public ArrayList<NotificationModel> arrayNotification = new ArrayList<>();
    public NotificationListAdapter notificationListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        initView();
        setHandler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        executeUserNotificationAsync();
    }

    private void executeUserNotificationAsync() {
        if (new Prefs(NotificationUserActivity.this).getUserdata() == null || !AppDelegate.isValidString(new Prefs(NotificationUserActivity.this).getUserdata().id + "")) {
            finish();
            return;
        }
        if (AppDelegate.haveNetworkConnection(NotificationUserActivity.this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().notificationList(new Prefs(NotificationUserActivity.this).getUserdata().id + "", new Prefs(NotificationUserActivity.this).getUserdata().sessionid + "", new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        arrayNotification.clear();
                        if (obj_json.has(Tags.session_status) && obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(NotificationUserActivity.this, obj_json.getString(Tags.message));
                            new Prefs(NotificationUserActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            startActivity(new Intent(NotificationUserActivity.this, SignInActivity.class));
                            finish();
                        } else if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray jsonArray = obj_json.getJSONArray(Tags.data);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                NotificationModel notificationModel = new NotificationModel();
                                notificationModel.id = jsonObject.getString(Tags.id);
                                if (AppDelegate.isValidString(jsonObject.optString(Tags.message) + "")) {
                                    jsonObject = jsonObject.getJSONObject(Tags.message);
                                    notificationModel.message = jsonObject.getString(Tags.message);
                                    notificationModel.order_id = jsonObject.getString(Tags.order_id);
                                    notificationModel.subtitle = jsonObject.getString(Tags.subtitle);
                                    notificationModel.title = jsonObject.getString(Tags.title);
                                    notificationModel.tickerText = jsonObject.getString(Tags.tickerText);
                                    notificationModel.user_type = jsonObject.getString(Tags.user_type);
                                    arrayNotification.add(notificationModel);
                                }
                            }
                        } else {
                            AppDelegate.showToast(NotificationUserActivity.this, obj_json.getString(Tags.message));
                        }
                        mHandler.sendEmptyMessage(1);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(NotificationUserActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void initView() {
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        txt_c_titlename.setText("Notifications");
        img_c_menu = (ImageView) findViewById(R.id.img_c_menu);
        img_c_menu.setOnClickListener(this);

        rv_category = (RecyclerView) findViewById(R.id.rv_category);
        rv_category.setLayoutManager(new LinearLayoutManager(NotificationUserActivity.this));
        rv_category.setHasFixedSize(true);
        rv_category.setItemAnimator(new DefaultItemAnimator());
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(NotificationUserActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(NotificationUserActivity.this);
                        break;
                    case 1:
                        if (notificationListAdapter != null && notificationListAdapter != null) {
                            notificationListAdapter.notifyDataSetChanged();
                            rv_category.invalidate();
                        } else {
                            notificationListAdapter = new NotificationListAdapter(NotificationUserActivity.this, arrayNotification);
                            rv_category.setAdapter(notificationListAdapter);
                        }
                        break;
                    case 5:
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_menu:
                onBackPressed();
                break;
        }
    }
}
