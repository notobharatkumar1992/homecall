package com.oncall.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.URLUtil;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.models.NotificationModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONObject;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 11-May-17.
 */
public class NotificationDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public TextView txt_c_titlename, txt_c_description;
    public ImageView img_c_banner;
    public NotificationModel notificationModel;

    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageView img_c_loading1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_detail_activity);
        notificationModel = getIntent().getExtras().getParcelable(Tags.notification);
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration imgconfig = ImageLoaderConfiguration.createDefault(this);
        imageLoader.init(imgconfig);
        options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).resetViewBeforeLoading(true)/*.showImageOnFail(context.getResources().getDrawable(R.drawable.user_noimage))*/.
                build();
        initView();
        setValue();
        if (notificationModel != null && AppDelegate.isValidString(notificationModel.id))
            execute_markNotifyFromNotificationAsync(notificationModel.id);
    }

    private void setValue() {
        if (notificationModel != null) {
            txt_c_titlename.setText(notificationModel.title + "");
            txt_c_description.setText(notificationModel.message + "");
            if (AppDelegate.isValidString(notificationModel.tickerText) && URLUtil.isValidUrl(notificationModel.tickerText)) {
                img_c_loading1.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
                frameAnimation.setCallback(img_c_loading1);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();

                imageLoader.loadImage(notificationModel.tickerText, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                        img_c_loading1.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        img_c_banner.setImageBitmap(bitmap);
                        img_c_loading1.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        img_c_loading1.setVisibility(View.GONE);
                    }
                });
            } else findViewById(R.id.rl_image).setVisibility(View.GONE);
        }
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        txt_c_description = (TextView) findViewById(R.id.txt_c_description);
        img_c_banner = (ImageView) findViewById(R.id.img_c_banner);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void execute_markNotifyFromNotificationAsync(String notification_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
            SingletonRestClient.get().markView(new Prefs(NotificationDetailActivity.this).getUserdata().id + "", new Prefs(NotificationDetailActivity.this).getUserdata().sessionid + "", notification_id, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                }

                @Override
                public void success(Response response, Response response2) {
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(NotificationDetailActivity.this, obj_json.getString(Tags.message));
                            new Prefs(NotificationDetailActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            startActivity(new Intent(NotificationDetailActivity.this, SignInActivity.class));
                            finish();
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }
}
