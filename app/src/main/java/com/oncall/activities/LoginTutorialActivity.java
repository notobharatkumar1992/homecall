package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.adapters.MyPagerLoopAdapter;
import com.oncall.constants.Constants;
import com.oncall.utils.MyViewPager;
import com.oncall.utils.Prefs;

/**
 * Created by Heena on 02/15/2017.
 */
public class LoginTutorialActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_01, img_02, img_03, img_04;
    private MyViewPager view_pager;
    private MyPagerLoopAdapter mPagerAdapter;
    private Handler mHandler;
    public static Activity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login_tutorial_activity);
        new Prefs(LoginTutorialActivity.this).setSKIP(1);
        new Prefs(this).setTempUserData(null);
        Constants.isMapScreen = true;
        mActivity = this;
        setHandler();
        initView();
    }


    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(LoginTutorialActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(LoginTutorialActivity.this);
                }
            }
        };
    }


    public int pageCount = 4;

    private void initView() {

        img_01 = (ImageView) findViewById(R.id.img_01);
        img_01.setOnClickListener(this);
        img_02 = (ImageView) findViewById(R.id.img_02);
        img_02.setOnClickListener(this);
        img_03 = (ImageView) findViewById(R.id.img_03);
        img_03.setOnClickListener(this);
        img_04 = (ImageView) findViewById(R.id.img_04);
        img_04.setOnClickListener(this);
        findViewById(R.id.txt_c_login).setOnClickListener(this);
        findViewById(R.id.txt_c_sign_up).setOnClickListener(this);
        findViewById(R.id.txt_c_skip).setOnClickListener(this);

        switchPage(0);
        view_pager = (MyViewPager) findViewById(R.id.view_pager);
        mPagerAdapter = new MyPagerLoopAdapter(pageCount);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (pageCount > 0)
                    switchPage(position % pageCount);
                timeOutLoop();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        timeOutLoop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        int value = view_pager.getCurrentItem();
                        if (value == 4 * 1000 - 1) {
                            value = 0;
                        } else {
                            value++;
                        }
                        if (value < mPagerAdapter.getCount())
                            view_pager.setCurrentItem(value, true);
                        if (mActivity != null) {
                            mHandler.postDelayed(this, 3000);
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void switchPage(int position) {
        img_01.setImageResource(R.drawable.white_radius_square);
        img_02.setImageResource(R.drawable.white_radius_square);
        img_03.setImageResource(R.drawable.white_radius_square);
        img_04.setImageResource(R.drawable.white_radius_square);
        switch (position) {
            case 0:
//                txt_c_tut_1.setTextColor(getResources().getColor(R.color.blue_color));
                img_01.setImageResource(R.drawable.yellow_radius_square);
                break;

            case 1:
                img_02.setImageResource(R.drawable.yellow_radius_square);
                break;

            case 2:
                img_03.setImageResource(R.drawable.yellow_radius_square);
                break;

            case 3:
                img_04.setImageResource(R.drawable.yellow_radius_square);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_01:
                view_pager.setCurrentItem(0);
                break;

            case R.id.img_02:
                view_pager.setCurrentItem(1);
                break;

            case R.id.img_03:
                view_pager.setCurrentItem(2);
                break;

            case R.id.img_04:
                view_pager.setCurrentItem(3);
                break;

            case R.id.txt_c_login:
                Intent intent = new Intent(LoginTutorialActivity.this, SignInActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LoginTutorialActivity.this, false);
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginTutorialActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.txt_c_sign_up:
                intent = new Intent(LoginTutorialActivity.this, SignUpVendorActivity.class);
                pairs = TransitionHelper.createSafeTransitionParticipants(LoginTutorialActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginTutorialActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.txt_c_skip:
                startActivity(new Intent(LoginTutorialActivity.this, MainActivity.class));
                finish();
                break;
        }
    }
}
