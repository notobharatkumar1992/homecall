package com.oncall.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.models.OrderDataModel;

import carbon.widget.TextView;

/**
 * Created by Bharat on 22-Mar-17.
 */
public class OrderConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    public TextView txt_c_reference_no;
    public OrderDataModel orderDataModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_confirmation);
        orderDataModel = getIntent().getExtras().getParcelable(Tags.OrderData);
        initView();
    }

    private void initView() {
        txt_c_reference_no = (TextView) findViewById(R.id.txt_c_reference_no);
        txt_c_reference_no.setText(orderDataModel.reference_id + "");
        findViewById(R.id.txt_c_book_another).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        actionBookAnotherService();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_c_book_another:
                actionBookAnotherService();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void actionBookAnotherService() {
        if (MainActivity.mActivity != null)
            MainActivity.mActivity.finish();
        startActivity(new Intent(OrderConfirmationActivity.this, MainActivity.class));
        finish();
    }
}
