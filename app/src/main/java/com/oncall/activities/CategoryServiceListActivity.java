package com.oncall.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryServiceListAdapter;
import com.oncall.adapters.PagerAdapter;
import com.oncall.constants.Tags;
import com.oncall.fragments.BannerFragment;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.ServiceModel;
import com.oncall.models.UserDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 24-Nov-16.
 */
public class CategoryServiceListActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    public static Activity mActivity;
    private Prefs prefs;
    public Handler mHandler;
    ImageView img_c_category;
    EditText et_instructions;
    TextView txt_c_titlename, txt_c_currency, txt_c_continue, txt_c_done, txt_c_name, txt_c_address, txt_c_email, txt_c_phone, txt_c_business_name, txt_c_id, txt_c_category;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private UserDataModel userDataModel;
    String currency_id = "";
    private Dialog dialogue;
    EditText et_search;
    ArrayList<ServiceModel> serviceArray;
    RecyclerView rv_services;
    CategoryServiceListAdapter serviceListAdapter;
    int cat_id = 0;
    ViewPager view_pager;
    ArrayList<String> imageArrayList;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();
    private boolean[] is_checked;
    public String service_id = "", service = "", title_name = "", image_url = "";
    public ScrollView scrollView;
    public boolean needComment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_service_list);
        userDataModel = new Prefs(this).getUserdata();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        cat_id = getIntent().getIntExtra(Tags.cat_id, 0);
        title_name = getIntent().getStringExtra(Tags.name);
        image_url = getIntent().getStringExtra(Tags.image_url);
        prefs = new Prefs(this);
        mActivity = this;
        initView();
        setHandler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                execute_getCategoryDetail();
            }
        }, 500);
        new Prefs(CategoryServiceListActivity.this).setAddressData(null);
    }

    void setBanner(ArrayList<String> imageArray) {
        for (int i = 0; i < imageArray.size(); i++) {
            Fragment fragment = new BannerFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Tags.DATA, imageArray.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
        bannerPagerAdapter = new PagerAdapter(getSupportFragmentManager(), bannerFragment);
        view_pager.setAdapter(bannerPagerAdapter);
    }

    private void execute_getCategoryDetail() {
        if (AppDelegate.haveNetworkConnection(this)) {
            String device_token = new Prefs(CategoryServiceListActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getCategoryDetail(cat_id + "", new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONObject imgarray = obj_json.getJSONObject(Tags.category);
                            imageArrayList = new ArrayList<String>();
                            for (int i = 0; i < imgarray.length(); i++) {
                                imageArrayList.add(imgarray.getString(Tags.image));
                            }
                            setBanner(imageArrayList);
                            if (obj_json.has(Tags.service)) {
                                if (AppDelegate.isValidString(obj_json.optString(Tags.service))) {
                                    JSONArray array = obj_json.getJSONArray(Tags.service);
                                    serviceArray = new ArrayList<ServiceModel>();
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < array.length(); i++) {
                                        stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                        ServiceModel serviceModel = new ServiceModel();
                                        serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
                                        serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
                                        serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
                                        serviceModel.description = array.getJSONObject(i).optString(Tags.description);
                                        serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                        serviceModel.minimum_time = obj_json.optString(Tags.minimum_time);
                                        serviceModel.image = array.getJSONObject(i).optString(Tags.image);
                                        serviceModel.how_works = array.getJSONObject(i).optString(Tags.how_works);
                                        serviceModel.terms = array.getJSONObject(i).optString(Tags.terms);
                                        serviceModel.cancellation_policy = array.getJSONObject(i).optString(Tags.cancellation_policy);
                                        serviceArray.add(serviceModel);
                                    }
                                }
                            }
                            if (serviceArray != null && serviceArray.size() == 0) {
                                findViewById(R.id.txt_c_noservice).setVisibility(View.VISIBLE);
                            } else {
                                findViewById(R.id.txt_c_noservice).setVisibility(View.GONE);
                            }
                            setServiceAdapter();
                        } else {
                            AppDelegate.showToast(CategoryServiceListActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(CategoryServiceListActivity.this, obj_json);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(CategoryServiceListActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(CategoryServiceListActivity.this);
                        break;
                    case 5:
                        execute_getCategoryDetail();
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    private void setServiceAdapter() {
        if (serviceArray != null) {
            if (is_checked == null || is_checked.length != serviceArray.size()) {
                is_checked = new boolean[serviceArray.size()];
                for (int i = 0; i < serviceArray.size(); i++) {
                    if (service_id.equalsIgnoreCase(serviceArray.get(i).id + "")) {
                        is_checked[i] = true;
                    } else
                        is_checked[i] = false;
                }
            } else {
                for (int i = 0; i < is_checked.length; i++) {
                    serviceArray.get(i).checked = is_checked[i] ? 1 : 0;
                }
            }
            et_search = (EditText) findViewById(R.id.et_search);
            et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
            rv_services.setLayoutManager(new LinearLayoutManager(this));
            rv_services.setNestedScrollingEnabled(false);
            rv_services.setHasFixedSize(true);
            rv_services.setItemAnimator(new DefaultItemAnimator());
            serviceListAdapter = new CategoryServiceListAdapter(this, serviceArray, new OnListItemClickListener() {
                @Override
                public void setOnListItemClickListener(String name, int position) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, int PICK_UP) {

                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                    AppDelegate.LogT("interface value => " + like_dislike);
                    serviceArray.get(position).checked = like_dislike ? 0 : 1;
                    serviceListAdapter.notifyDataSetChanged();
                    rv_services.invalidate();
                    StringBuilder stringBuilder = null;
                    StringBuilder service_name = null;
                    for (int i = 0; i < serviceArray.size(); i++) {
                        service_id = "";
                        service = "";
                        if (serviceArray.get(i).checked == 1) {
                            is_checked[i] = true;
                            if (stringBuilder == null) {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("" + serviceArray.get(i).sub_category_id);
                            } else {
                                stringBuilder.append("," + serviceArray.get(i).sub_category_id);
                            }
                            if (service_name == null) {
                                service_name = new StringBuilder();
                                service_name.append("" + serviceArray.get(i).sub_category);
                            } else {
                                service_name.append(", " + serviceArray.get(i).sub_category);
                            }
                        } else {
                            is_checked[i] = false;
                        }
                    }
                    if (stringBuilder != null) {
                        service_id = stringBuilder.toString();

                        service = service_name.toString();
                        serviceListAdapter.notifyDataSetChanged();
                        AppDelegate.LogT("service selected =" + service);
                        AppDelegate.LogT("service selected id=" + service_id);
                    } else {
                    }
                }
            }, false, image_url);
            rv_services.setAdapter(serviceListAdapter);

            et_search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.toString().length() == 1) {
                        scrollView.smoothScrollTo(0, 200);
                    }
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() == 1) {
                        scrollView.smoothScrollTo(0, 200);
                    }
                    serviceListAdapter.filter(et_search.getText().toString());
                }
            });
        }
//
//        scrollView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (mActivity != null)
//                    scrollView.fullScroll(View.FOCUS_UP);
//            }
//        }, 500);
//        scrollView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (mActivity != null)
//                    scrollView.fullScroll(View.FOCUS_UP);
//            }
//        }, 700);
    }

    @Override
    protected void onResume() {
        super.onResume();
        userDataModel = new Prefs(this).getUserdata();
        setValues();
    }

    private void setValues() {
        if (userDataModel != null && userDataModel.id != -1) {
            txt_c_continue.setText("Continue");
        } else {
            txt_c_continue.setText("LOGIN");
        }
    }

    private void initView() {
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        img_c_category = (ImageView) findViewById(R.id.img_c_category);
        et_instructions = (EditText) findViewById(R.id.et_instructions);
        et_instructions.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        imageLoader.displayImage(image_url, img_c_category, options);
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        txt_c_titlename.setText(title_name + "");
        txt_c_currency = (TextView) findViewById(R.id.txt_c_currency);
        txt_c_currency.setOnClickListener(this);
        txt_c_currency.setVisibility(View.GONE);
        txt_c_continue = (TextView) findViewById(R.id.txt_c_continue);
        txt_c_continue.setOnClickListener(this);

        view_pager = (ViewPager) findViewById(R.id.view_pager);
        rv_services = (RecyclerView) findViewById(R.id.rv_services);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.rl_c_previous).setOnClickListener(this);
        findViewById(R.id.rl_c_next).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;

            case R.id.txt_c_continue:
                if (userDataModel == null || userDataModel.id == -1) {
                    Intent intent = new Intent(CategoryServiceListActivity.this, SignInActivity.class);
                    intent.putExtra(Tags.FROM, "Service");
                    startActivity(intent);
                } else if (!AppDelegate.isValidString(service_id)) {
                    AppDelegate.showToast(CategoryServiceListActivity.this, getString(R.string.validation_select_services));
                } else if (needComment || et_instructions.length() > 0) {

                    ArrayList<ServiceModel> listArray = new ArrayList<>();
                    for (int i = 0; i < serviceArray.size(); i++) {
                        if (serviceArray.get(i).checked == 1)
                            listArray.add(serviceArray.get(i));
                    }
                    Intent intent = new Intent(CategoryServiceListActivity.this, ScheduleJobActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Tags.from, MainActivity.BOOKING_FROM_CATEGORIES);
                    bundle.putString(Tags.service, service);
                    bundle.putString(Tags.service_id, service_id);
                    bundle.putString(Tags.description, et_instructions.getText().toString());
                    bundle.putParcelableArrayList(Tags.ARRAY_SERVICES, listArray);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    AppDelegate.showAlert(CategoryServiceListActivity.this, "Additional Notes", "Would you like to add additional notes for your job?", "YES", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                            et_instructions.setFocusable(true);
                            et_instructions.setFocusableInTouchMode(true);
                            AppDelegate.showKeyboard(CategoryServiceListActivity.this, et_instructions);
                            needComment = true;
                        }
                    }, "No", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ArrayList<ServiceModel> listArray = new ArrayList<>();
                            for (int i = 0; i < serviceArray.size(); i++) {
                                if (serviceArray.get(i).checked == 1)
                                    listArray.add(serviceArray.get(i));
                            }
                            Intent intent = new Intent(CategoryServiceListActivity.this, ScheduleJobActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt(Tags.from, MainActivity.BOOKING_FROM_CATEGORIES);
                            bundle.putString(Tags.service, service);
                            bundle.putString(Tags.service_id, service_id);
                            bundle.putString(Tags.description, et_instructions.getText().toString());
                            bundle.putParcelableArrayList(Tags.ARRAY_SERVICES, listArray);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            needComment = true;
                        }
                    });
                }
                break;
//            case R.id.txt_c_currency:
//                if (currencyArray.size() == 0) {
//                    executeCurrencyAsync();
//                } else {
//                    currencyDialog(currencyArray);
//                }
//                break;

            case R.id.rl_c_previous:
                if (view_pager.getAdapter().getCount() > 0)
                    if (view_pager.getCurrentItem() == 0) {
                        view_pager.setCurrentItem(view_pager.getAdapter().getCount() - 1);
                    } else {
                        view_pager.setCurrentItem(view_pager.getCurrentItem() - 1);
                    }
                break;
            case R.id.rl_c_next:
                if (view_pager.getAdapter().getCount() > 0)
                    if (view_pager.getCurrentItem() == view_pager.getAdapter().getCount() - 1) {
                        view_pager.setCurrentItem(0);
                    } else {
                        view_pager.setCurrentItem(view_pager.getCurrentItem() + 1);
                    }
                break;
        }
    }


//    private RecyclerView recycler_deals;
//    private String currency_str = "", currencysymbbol_str = "", currencyslug_str = "";
//
//    public void currencyDialog(final ArrayList<CategoryModel> currencyArray) {
//        if (currencyArray != null) {
//            final Dialog categoryDialog = new Dialog(this, android.R.style.Theme_Light);
//            categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
//            categoryDialog.setContentView(R.layout.dialog_services);
//            recycler_deals = (RecyclerView) categoryDialog.findViewById(R.id.recycler_deals);
//            recycler_deals.setNestedScrollingEnabled(false);
//            recycler_deals.setHasFixedSize(true);
//            recycler_deals.setLayoutManager(new LinearLayoutManager(this));
//            recycler_deals.setItemAnimator(new DefaultItemAnimator());
//
//            if (currencyArray.size() > 0) {
//                for (int i = 0; i < currencyArray.size(); i++) {
//                    if (currencyArray.get(i).title.equalsIgnoreCase(txt_c_currency.getText().toString())) {
//                        currencyArray.get(i).checked = 1;
//                        break;
//                    }
//                }
//            }
//
//            currencyAdapter = new CurrencyAdapterSearch(this, currencyArray, new OnListItemClickListener() {
//                @Override
//                public void setOnListItemClickListener(String name, int position) {
//                }
//
//                @Override
//                public void setOnListItemClickListener(String name, int position, int PICK_UP) {
//
//                }
//
//                @Override
//                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
//                    for (CategoryModel currency : currencyArray) {
//                        AppDelegate.LogT("interface value => " + like_dislike);
//                        currency.checked = 0;
//                    }
//                    currencyArray.get(position).checked = 1;
//                    currency_id = currencyArray.get(position).id + "";
//                    currency_str = currencyArray.get(position).title + "";
//                    currencysymbbol_str = currencyArray.get(position).symbol + "";
//                    currencyslug_str = currencyArray.get(position).slug + "";
//                    currencyAdapter.notifyDataSetChanged();
//                    recycler_deals.invalidate();
//                    AppDelegate.LogT("currencyArray==>" + currencyArray);
//                }
//            });
//            recycler_deals.setAdapter(currencyAdapter);
//
//            categoryDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    AppDelegate.LogT("currency id => " + currency_id + "");
//                    categoryDialog.dismiss();
//                    txt_c_currency.setText(currencyslug_str + "");
//                    for (int i = 0; i < currencyArray.size(); i++) {
//                        currencyArray.get(i).checked = 0;
//                    }
//                    String str_currency_slug = "";
//                    for (int i = 0; i < currencyArray.size(); i++) {
//                        if (currencyslug_str.equalsIgnoreCase(currencyArray.get(i).title)) {
//                            currencyArray.get(i).checked = 1;
//                            str_currency_slug = currencyArray.get(i).slug;
//                        }
//                    }
//                    new Prefs(CategoryServiceListActivity.this).putStringValue(Tags.CURRENCY, str_currency_slug);
//                    prefs.setCurrencyArrayList(currencyArray);
//                    serviceListAdapter.notifyDataSetChanged();
//                    rv_services.invalidate();
//                }
//            });
//            categoryDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    categoryDialog.dismiss();
//                }
//            });
//            categoryDialog.show();
//        }
//    }
//
//    private void executeChangeCurrencyAsync(final String currency_id) {
//        if (AppDelegate.haveNetworkConnection(this)) {
//            mHandler.sendEmptyMessage(10);
//            SingletonRestClient.get().changeCurrency(new Prefs(this).getUserdata().id + "", currency_id, new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.checkJsonMessage(CategoryServiceListActivity.this, obj_json);
//
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                        AppDelegate.showToast(CategoryServiceListActivity.this, "");
//                    }
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                        if (obj_json.getString(Tags.status).contains("1")) {
//                            userDataModel = new Prefs(CategoryServiceListActivity.this).getUserdata();
//                            userDataModel.currency_symbol = currencysymbbol_str;
//                            userDataModel.currency = currency_str;
//                            userDataModel.currency_id = currency_id;
//                            userDataModel.currency_slug = currencyslug_str;
//                            prefs.setUserData(userDataModel);
//                            serviceListAdapter.notifyDataSetChanged();
//                        } else {
//                            AppDelegate.showToast(CategoryServiceListActivity.this, obj_json.getString(Tags.message));
//                        }
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//            });
//        }
//
//    }

    @Override
    public void setOnListItemClickListener(String name, int position) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }
}
