package com.oncall.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryServiceSummaryListAdapter;
import com.oncall.constants.Tags;
import com.oncall.interfaces.PageReload;
import com.oncall.models.CategoryModel;
import com.oncall.models.CustomerModel;
import com.oncall.models.RequestModel;
import com.oncall.models.ReviewModel;
import com.oncall.models.ServiceModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.DateUtils;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import carbon.widget.EditText;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 27-Mar-17.
 */
public class MyOrderDetailActivity extends AppCompatActivity implements View.OnClickListener, PageReload {

    public static Activity mActivity;
    public static PageReload pageReload;
    public Handler mHandler;
    public Prefs prefs;

    public TextView txt_c_placed_on, txt_c_reference_no, txt_c_service, txt_c_status, txt_c_total, txt_c_special_instruction, txt_c_instruction_ph, txt_c_name, txt_c_phone, txt_c_address, txt_c_address_2, txt_c_city, txt_c_city_2, txt_c_cancel, txt_c_reschedule, txt_c_rate_us, txt_c_complete;
    public TextView txt_c_vendor_name, txt_c_vendor_business_name, txt_c_vendor_contact_no, txt_c_vendor_address, txt_c_rated_on, txt_c_feedback;
    public LinearLayout ll_vendor_detail, ll_review;
    public RecyclerView rv_services;
    public ArrayList<ServiceModel> serviceArray = new ArrayList<>();
    public CategoryServiceSummaryListAdapter serviceListAdapter;

    public RequestModel requestModel;
    public String str_order_id = "", notification_id = "", type = "";
    public int from = MainActivity.FROM_ORDER_LIST;
    public RatingBar rb_rating;
    private String str_global_dateTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_order_detail_activity);
        pageReload = this;
        mActivity = this;
        prefs = new Prefs(this);
        requestModel = getIntent().getExtras().getParcelable(Tags.request_model);
        str_order_id = getIntent().getExtras().getString(Tags.order_id);
        from = getIntent().getExtras().getInt(Tags.from);
        notification_id = getIntent().getExtras().getString(Tags.notification_id);
        type = getIntent().getExtras().getString(Tags.type);
        initView();
        setValues();
        setHandler();
        if (new Prefs(MyOrderDetailActivity.this).getUserdata() != null && AppDelegate.isValidString(new Prefs(MyOrderDetailActivity.this).getUserdata().id + ""))
            executeMyOrderAsync(str_order_id);
        else
            finish();
    }

    Calendar bookingCalender = Calendar.getInstance();

    private void setValues() {
        if (requestModel != null) {
            AppDelegate.LogT("setValues called requestModel.status => " + requestModel.status);
            bookingCalender = Calendar.getInstance();
            String date_time = "";
            try {
                date_time = requestModel.booking_date + " " + requestModel.booking_time;
                Date date = new SimpleDateFormat("dd-MM-yyyy hh:mm aa").parse(date_time);
                AppDelegate.LogT("Parsed date => " + date + " == " + date_time);
                bookingCalender.setTime(date);
                bookingCalender.add(Calendar.HOUR_OF_DAY, -1);
                AppDelegate.LogT("bookingCalender => " + bookingCalender.getTime());
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            txt_c_reference_no.setText(requestModel.reference_id);
            try {
                txt_c_placed_on.setText(requestModel.booking_date + "       " + requestModel.booking_time);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            txt_c_service.setText(requestModel.categoryModel.title);

            ll_review.setVisibility(View.GONE);
            txt_c_cancel.setVisibility(View.GONE);
            txt_c_reschedule.setVisibility(View.GONE);
            txt_c_rate_us.setVisibility(View.GONE);
            txt_c_complete.setVisibility(View.GONE);
            if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "")) {
                txt_c_status.setText("Pending");
//                isBookingTimeExceeded();
                txt_c_cancel.setVisibility(View.VISIBLE);
                txt_c_reschedule.setVisibility(View.VISIBLE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_ACCEPTED + "")) {
                txt_c_status.setText("Accepted");
//                isBookingTimeExceeded();
                txt_c_cancel.setVisibility(View.VISIBLE);
                txt_c_reschedule.setVisibility(View.VISIBLE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_REJECTED + "")) {
                txt_c_status.setText("Rejected");
                txt_c_reschedule.setVisibility(View.VISIBLE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_STARTED + "")) {
                txt_c_status.setText("Started");
                txt_c_complete.setVisibility(View.VISIBLE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_COMPLETED + "")) {
                txt_c_status.setText("Completed");
                txt_c_rate_us.setVisibility(View.VISIBLE);
                if (requestModel.reviewModel == null) {
                    ll_review.setVisibility(View.GONE);
                    txt_c_rate_us.setVisibility(View.VISIBLE);
                } else {
                    ll_review.setVisibility(View.VISIBLE);
                    txt_c_rate_us.setVisibility(View.GONE);
                    txt_c_feedback.setText(requestModel.reviewModel.review);
                    String str_placed_on = requestModel.reviewModel.created.substring(0, requestModel.reviewModel.created.indexOf("T"));
                    txt_c_rated_on.setText(str_placed_on);
                    try {
                        rb_rating.setRating(Float.parseFloat(requestModel.reviewModel.rating));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    rb_rating.setEnabled(false);
                }
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_CANCELLED + "")) {
                txt_c_status.setText("Cancelled");
            }

            txt_c_phone.setText(requestModel.phone);
            try {
                String s = txt_c_phone.getText().toString();
                txt_c_phone.setText(String.format("%s %s %s", s.subSequence(0, 3), s.subSequence(3, 6), s.subSequence(6, s.length())));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            txt_c_address.setText(requestModel.address + ", " + requestModel.city);
            txt_c_city.setText(requestModel.city);
            txt_c_city.setVisibility(View.GONE);

            if (AppDelegate.isValidString(requestModel.address2) && AppDelegate.isValidString(requestModel.city_2)) {
                findViewById(R.id.ll_additional_address).setVisibility(View.VISIBLE);
                txt_c_address_2.setText(requestModel.address2 + ", " + requestModel.city_2);
                txt_c_city_2.setText(requestModel.city_2);
                txt_c_city_2.setVisibility(View.GONE);
            } else {
                findViewById(R.id.ll_additional_address).setVisibility(View.GONE);
            }

            try {
                txt_c_total.setText("AED " + requestModel.price);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }

            if (AppDelegate.isValidString(requestModel.comment)) {
                txt_c_special_instruction.setText(Html.fromHtml(requestModel.comment));
                txt_c_instruction_ph.setVisibility(View.VISIBLE);
            } else {
                txt_c_special_instruction.setVisibility(View.GONE);
                txt_c_instruction_ph.setVisibility(View.GONE);
            }

            if (AppDelegate.isValidString(requestModel.vendor_id) && requestModel.customerModel != null) {
                ll_vendor_detail.setVisibility(View.VISIBLE);
                if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "") || requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_REJECTED + "")) {
                    if (AppDelegate.isValidString(requestModel.customerModel.business_name))
                        txt_c_vendor_business_name.setText(requestModel.customerModel.business_name + "");
                    else txt_c_vendor_business_name.setVisibility(View.GONE);
                    txt_c_vendor_address.setText(requestModel.customerModel.address2 + ", " + requestModel.customerModel.city + ", " + requestModel.customerModel.state + ", " + requestModel.customerModel.country);
                    txt_c_vendor_contact_no.setText(requestModel.customerModel.contact_no + "");
                    try {
                        String s = txt_c_vendor_contact_no.getText().toString();
                        txt_c_vendor_contact_no.setText(String.format("%s %s %s", s.subSequence(0, 3), s.subSequence(3, 6), s.subSequence(6, s.length())));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else {
                    if (AppDelegate.isValidString(requestModel.customerModel.business_name))
                        txt_c_vendor_business_name.setText(requestModel.customerModel.business_name + "");
                    else txt_c_vendor_business_name.setVisibility(View.GONE);
                    txt_c_vendor_address.setText(requestModel.customerModel.address2 + ", " + requestModel.customerModel.city + ", " + requestModel.customerModel.state + ", " + requestModel.customerModel.country);
                    txt_c_vendor_contact_no.setText(requestModel.customerModel.contact_no + "");
                    try {
                        String s = txt_c_vendor_contact_no.getText().toString();
                        txt_c_vendor_contact_no.setText(String.format("%s %s %s", s.subSequence(0, 3), s.subSequence(3, 6), s.subSequence(6, s.length())));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            } else {
                AppDelegate.LogT("Vendor model null");
                ll_vendor_detail.setVisibility(View.GONE);
            }

            txt_c_name.setText(requestModel.name);
            serviceArray = requestModel.arrayServices;
            if (serviceArray != null) {
                AppDelegate.LogT("serviceArray => " + serviceArray.size());
                serviceListAdapter = new CategoryServiceSummaryListAdapter(this, serviceArray, null, true, requestModel.categoryModel.image);
                rv_services.setAdapter(serviceListAdapter);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setValues();
    }

    public boolean isBookingTimeExceeded() {
        if (DateUtils.isToday(bookingCalender.getTime())) {
            Date date = Calendar.getInstance().getTime();
            Date dateCompareTwo = Calendar.getInstance().getTime();
            try {
                date = new SimpleDateFormat("HH:mm").parse(Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + ":" + Calendar.getInstance().get(Calendar.MINUTE));
                dateCompareTwo = new SimpleDateFormat("HH:mm").parse(bookingCalender.get(Calendar.HOUR_OF_DAY) + ":" + bookingCalender.get(Calendar.MINUTE));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.LogT("When today ==> date => " + date + " == " + dateCompareTwo + " => " + date.before(dateCompareTwo));
            if (date.before(dateCompareTwo)) {
                AppDelegate.LogT("When today ==> date => " + date + " =true= " + dateCompareTwo + " => " + date.before(dateCompareTwo));
                txt_c_cancel.setVisibility(View.VISIBLE);
                txt_c_reschedule.setVisibility(View.VISIBLE);
                return false;
            } else {
                AppDelegate.LogT("When today ==> date => " + date + " =false= " + dateCompareTwo + " => " + date.before(dateCompareTwo));
                txt_c_cancel.setVisibility(View.GONE);
                txt_c_reschedule.setVisibility(View.GONE);
                return true;
            }
        } else if (DateUtils.isBeforeDay(Calendar.getInstance().getTime(), bookingCalender.getTime())) {
            AppDelegate.LogT("When isBeforeDay ==> date => " + Calendar.getInstance().getTime() + " == " + bookingCalender.getTime());
            txt_c_cancel.setVisibility(View.VISIBLE);
            txt_c_reschedule.setVisibility(View.VISIBLE);
            return false;
        } else {
            AppDelegate.LogT("When isAfterDay ==> date => " + Calendar.getInstance().getTime() + " == " + bookingCalender.getTime());
            txt_c_cancel.setVisibility(View.GONE);
            txt_c_reschedule.setVisibility(View.GONE);
            return true;
        }
    }


    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyOrderDetailActivity.this);
                        break;

                    case 11:
                        AppDelegate.hideProgressDialog(MyOrderDetailActivity.this);
                        break;

                    case 1:
                        maintainGlobalTime();
                        setValues();
                        AppDelegate.LogT("MyOrderDetails from => " + from);
                        if (from == MainActivity.FROM_CLASS_NOTIFICATION_LIST || from == MainActivity.FROM_PUSH_NOTIFICATION) {
                            execute_markNotifyFromNotificationAsync(notification_id);
                        }
                        break;

                }
            }
        };
    }

    public Calendar minimumTimeCalender = Calendar.getInstance();

    private void maintainGlobalTime() {
        try {
            minimumTimeCalender.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str_global_dateTime));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        AppDelegate.LogT("minimumTimeCalender Before => " + minimumTimeCalender.getTime());
        minimumTimeCalender.add(Calendar.HOUR_OF_DAY, 12);
        timeHandler.removeCallbacks(timeRunnable);
        timeHandler.postDelayed(timeRunnable, 1000);
    }

    public Handler timeHandler = new Handler();
    public Runnable timeRunnable = new Runnable() {
        @Override
        public void run() {
            minimumTimeCalender.add(Calendar.SECOND, 1);
            try {
                prefs.setServerTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(minimumTimeCalender.getTime()));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            timeHandler.postDelayed(this, 1000);
        }
    };

    public boolean checkValidTime() {
        String str_minimumTime = requestModel.arrayServices.get(0).minimum_time;
        AppDelegate.LogT("str_minimumTime => " + str_minimumTime);
        Calendar minimumTimeCalender = Calendar.getInstance();
        try {
            minimumTimeCalender.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(new Prefs(MyOrderDetailActivity.this).getServerTime()));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        Date minimumTimeDate = minimumTimeCalender.getTime();
        Date selectedDate = Calendar.getInstance().getTime();
        try {
            selectedDate = new SimpleDateFormat(ScheduleJobActivity.DATE_FORMAT + " " + ScheduleJobActivity.TIME_FORMAT_12_HOUR).parse(requestModel.booking_date + " " + requestModel.booking_time);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        AppDelegate.LogT("minimumTimeCalender => " + minimumTimeDate + ", selectedDate => " + selectedDate + ", BEFORE => " + minimumTimeDate.before(selectedDate) + ", Equal => " + minimumTimeDate.equals(selectedDate));
        if (minimumTimeDate.before(selectedDate)) {
            return true;
        } else {
//            AppDelegate.showToast(MyOrderDetailActivity.this, "Your order time has been expire booking time should be greater than " + str_minimumTime + " from current time.");
            return false;
        }
    }

    private void execute_markNotifyFromNotificationAsync(String notification_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
            SingletonRestClient.get().markView(new Prefs(MyOrderDetailActivity.this).getUserdata().id + "", new Prefs(MyOrderDetailActivity.this).getUserdata().sessionid + "", notification_id, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
                            new Prefs(MyOrderDetailActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            if (MyOrderActivity.mActivity != null)
                                MyOrderActivity.mActivity.finish();
                            startActivity(new Intent(MyOrderDetailActivity.this, SignInActivity.class));
                            finish();
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void initView() {
        txt_c_placed_on = (TextView) findViewById(R.id.txt_c_placed_on);
        txt_c_reference_no = (TextView) findViewById(R.id.txt_c_reference_no);
        txt_c_service = (TextView) findViewById(R.id.txt_c_service);
        txt_c_status = (TextView) findViewById(R.id.txt_c_status);
        txt_c_total = (TextView) findViewById(R.id.txt_c_total);
        txt_c_special_instruction = (TextView) findViewById(R.id.txt_c_special_instruction);
        txt_c_instruction_ph = (TextView) findViewById(R.id.txt_c_instruction_ph);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_phone = (TextView) findViewById(R.id.txt_c_phone);
        txt_c_address_2 = (TextView) findViewById(R.id.txt_c_address_2);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        txt_c_city = (TextView) findViewById(R.id.txt_c_city);
        txt_c_city_2 = (TextView) findViewById(R.id.txt_c_city_2);

        txt_c_vendor_name = (TextView) findViewById(R.id.txt_c_vendor_name);
        txt_c_vendor_business_name = (TextView) findViewById(R.id.txt_c_vendor_business_name);
        txt_c_vendor_address = (TextView) findViewById(R.id.txt_c_vendor_address);
        txt_c_vendor_contact_no = (TextView) findViewById(R.id.txt_c_vendor_contact_no);

        ll_vendor_detail = (LinearLayout) findViewById(R.id.ll_vendor_detail);

        txt_c_cancel = (TextView) findViewById(R.id.txt_c_cancel);
        txt_c_cancel.setOnClickListener(this);
        txt_c_reschedule = (TextView) findViewById(R.id.txt_c_reschedule);
        txt_c_reschedule.setOnClickListener(this);
        txt_c_rate_us = (TextView) findViewById(R.id.txt_c_rate_us);
        txt_c_rate_us.setOnClickListener(this);
        txt_c_complete = (TextView) findViewById(R.id.txt_c_complete);
        txt_c_complete.setOnClickListener(this);

        findViewById(R.id.img_c_back).setOnClickListener(this);

        rv_services = (RecyclerView) findViewById(R.id.rv_services);
        rv_services.setLayoutManager(new LinearLayoutManager(this));
        rv_services.setNestedScrollingEnabled(false);
        rv_services.setHasFixedSize(true);
        rv_services.setItemAnimator(new DefaultItemAnimator());
        serviceListAdapter = new CategoryServiceSummaryListAdapter(this, serviceArray, null, false, "");
        rv_services.setAdapter(serviceListAdapter);

        ll_review = (LinearLayout) findViewById(R.id.ll_review);
        txt_c_rated_on = (TextView) findViewById(R.id.txt_c_rated_on);
        txt_c_feedback = (TextView) findViewById(R.id.txt_c_feedback);
        rb_rating = (RatingBar) findViewById(R.id.rb_rating);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_cancel:
                if (checkValidTime()) {
                    AppDelegate.showAlert(MyOrderDetailActivity.this, "Cancel Order", "Are you sure you want to cancel this job?", "Yes", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showCancelOrderDialog();
                        }
                    }, "No", null);
                } else {
                    AppDelegate.showAlert(MyOrderDetailActivity.this, "Cancel Order", "You will be charged for cancellation of job due to cancelling in 12 hour to booking time. Are you sure you want to cancel this job?", "Yes", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showCancelOrderDialog();
                        }
                    }, "No", null);
                }
                break;

            case R.id.txt_c_reschedule:
                AppDelegate.showAlert(MyOrderDetailActivity.this, "Reschedule Order", "Are you sure you want to reschedule this job?", "Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rescheduleJob();
                    }
                }, "No", null);
                break;

            case R.id.txt_c_rate_us:
                showRatingDialog();
                break;

            case R.id.txt_c_complete:
                Intent intent = new Intent(MyOrderDetailActivity.this, PaymentActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.request_model, requestModel);
                bundle.putString(Tags.order_id, requestModel.id);
                bundle.putInt(Tags.from, MainActivity.FROM_ORDER_LIST);
                intent.putExtras(bundle);
                startActivity(intent);
//                onBuyPressed(requestModel.price, "HomeCall Order Reference id: " + requestModel.reference_id);
                break;
        }
    }

    public float selectedRating = 0;
    public String str_rateDialogFeedback = "";

    private void showRatingDialog() {
        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .session(150)
                .threshold(5)
                .ratingBarColor(R.color.colorAccent)
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {
                        if (feedback.length() == 0) {
                            AppDelegate.showAlert(MyOrderDetailActivity.this, getString(R.string.rating_dialog_suggestions));
                        } else {
                            str_rateDialogFeedback = feedback;
                            executeAddRatingAsync();
                        }
                    }
                }).onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared) {
                        selectedRating = rating;
                    }
                })
                .build();
        ratingDialog.show();
    }

    private void executeAddRatingAsync() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().addRating(new Prefs(MyOrderDetailActivity.this).getUserdata().id + "",
                    new Prefs(MyOrderDetailActivity.this).getUserdata().sessionid + "",
                    requestModel.id + "",
                    selectedRating + "",
                    str_rateDialogFeedback + "",
                    new Callback<Response>() {

                        @Override
                        public void success(Response response, Response response2) {
                            try {
                                AppDelegate.sendEventTracker(MyOrderDetailActivity.this, "Rate Success");
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                if (obj_json.getInt(Tags.session_status) == 0) {
                                    AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
                                    new Prefs(MyOrderDetailActivity.this).setUserData(null);
                                    if (MainActivity.mActivity != null)
                                        MainActivity.mActivity.finish();
                                    if (MyOrderActivity.mActivity != null)
                                        MyOrderActivity.mActivity.finish();
                                    startActivity(new Intent(MyOrderDetailActivity.this, SignInActivity.class));
                                    finish();
                                } else if (obj_json.getInt(Tags.status) == 1) {
                                    executeMyOrderAsync(str_order_id);
                                    AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
                                } else {
                                    AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
                                }
                                setValues();
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                                mHandler.sendEmptyMessage(11);
                            }
                        }

                        @Override
                        public void failure(RestError restError) {
                            mHandler.sendEmptyMessage(11);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(MyOrderDetailActivity.this, obj_json);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
        }
    }


    private void rescheduleJob() {
        StringBuilder stringBuilder = null;
        for (int i = 0; i < requestModel.arrayServices.size(); i++) {
            if (stringBuilder == null) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("" + serviceArray.get(i).sub_category_id);
            } else {
                stringBuilder.append("," + serviceArray.get(i).sub_category_id);
            }
        }
        Intent intent = new Intent(MyOrderDetailActivity.this, ScheduleJobActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Tags.from, MainActivity.BOOKING_FROM_RESCHEDULE);
        bundle.putParcelable(Tags.customer, requestModel.customerModel);
        bundle.putString(Tags.order_id, requestModel.id);
        bundle.putString(Tags.service_id, stringBuilder.toString());
        bundle.putString(Tags.currency_id, requestModel.currency_id);
        bundle.putString(Tags.description, requestModel.comment);
        bundle.putParcelableArrayList(Tags.ARRAY_SERVICES, requestModel.arrayServices);
        bundle.putParcelable(Tags.request_model, requestModel);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public AlertDialog alertDialog;
    public String str_cancelDialogComment = "";

    public void showCancelOrderDialog() {
        /* Alert Dialog Code Start*/
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Cancel Order"); //Set Alert dialog title here
        alert.setCancelable(false);

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        input.setLayoutParams(params);
        input.setPadding(AppDelegate.dpToPix(this, 25), AppDelegate.dpToPix(this, 15), AppDelegate.dpToPix(this, 25), 0);
        input.setHint("Enter your comment");
        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        input.setFocusable(true);
        input.setSelection(input.length());
        input.setFocusableInTouchMode(true);
        input.setEnabled(true);
        alert.setView(input);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                str_cancelDialogComment = input.getText().toString();
//                if (!isBookingTimeExceeded())
                execute_cancelOrderAsync(requestModel.id);
                dialog.cancel();

            } // End of onClick(DialogInterface dialog, int whichButton)
        }); //End of alert.setPositiveButton
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
//                isBookingTimeExceeded();
                dialog.cancel();
            }
        }); //End of alert.setNegativeButton
        alertDialog = alert.create();
        alertDialog.show();
    }

    private void execute_cancelOrderAsync(String order_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().cancelJob(new Prefs(this).getUserdata().id + "", new Prefs(MyOrderDetailActivity.this).getUserdata().sessionid + "", order_id, str_cancelDialogComment, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(MyOrderDetailActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(MyOrderDetailActivity.this, getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
                            new Prefs(MyOrderDetailActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            if (MyOrderActivity.mActivity != null)
                                MyOrderActivity.mActivity.finish();
                            startActivity(new Intent(MyOrderDetailActivity.this, SignInActivity.class));
                            finish();
                        } else if (obj_json.getString(Tags.status).contains("1")) {
                            if (alertDialog != null && alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                            executeMyOrderAsync(str_order_id);
                            AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString("message"));
                        } else {
                            AppDelegate.showAlert(MyOrderDetailActivity.this, obj_json.getString("message"));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executeMyOrderAsync(String order_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().orderDetail(new Prefs(MyOrderDetailActivity.this).getUserdata().id + "", new Prefs(MyOrderDetailActivity.this).getUserdata().sessionid + "", order_id,
                    new Callback<Response>() {

                        @Override
                        public void success(Response response, Response response2) {
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                if (obj_json.getInt(Tags.session_status) == 0) {
                                    AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
                                    new Prefs(MyOrderDetailActivity.this).setUserData(null);
                                    if (MainActivity.mActivity != null)
                                        MainActivity.mActivity.finish();
                                    if (MyOrderActivity.mActivity != null)
                                        MyOrderActivity.mActivity.finish();
                                    startActivity(new Intent(MyOrderDetailActivity.this, SignInActivity.class));
                                    finish();
                                } else if (obj_json.getInt(Tags.status) == 1) {
                                    str_global_dateTime = obj_json.getString(Tags.date);
                                    ArrayList<RequestModel> arrayRequestModel = new ArrayList<RequestModel>();
//                                    JSONArray jsonArray = obj_json.getJSONArray(Tags.order);
//                                    for (int j = 0; j < jsonArray.length(); j++) {
                                    JSONObject object = obj_json.getJSONObject(Tags.order);
                                    RequestModel requestModel = new RequestModel();
                                    requestModel.minimum_booking_time = object.getString(Tags.minimum_time);
                                    requestModel.id = object.getString(Tags.id);
                                    requestModel.category_id = object.getString(Tags.category_id);
                                    requestModel.subcategory_id = object.getString(Tags.subcategory_id);
                                    requestModel.price = object.getString(Tags.price);
                                    requestModel.currency_id = object.getString(Tags.currency_id);
                                    requestModel.user_id = object.getString(Tags.user_id);
                                    requestModel.vendor_id = object.getString(Tags.vendor_id);

                                    requestModel.reference_id = object.getString(Tags.reference_id);
                                    requestModel.booking_date = object.getString(Tags.booking_date);
                                    requestModel.booking_time = object.getString(Tags.booking_time);
                                    requestModel.comment = object.getString(Tags.comment);
                                    requestModel.name = object.getString(Tags.name);
                                    requestModel.phone = object.getString(Tags.phone);
                                    requestModel.email = object.getString(Tags.email);
                                    requestModel.city = object.getString(Tags.city);
                                    requestModel.city_2 = object.getString(Tags.city_2);
                                    requestModel.state = object.getString(Tags.state);
                                    requestModel.address = object.getString(Tags.address);
                                    requestModel.address2 = object.getString(Tags.address_2);
                                    requestModel.zip_code = object.getString(Tags.zip);
                                    requestModel.status = object.getString(Tags.status);
                                    requestModel.created = object.getString(Tags.created);

                                    if (AppDelegate.isValidString(object.optString(Tags.review))) {
                                        JSONObject reviewJSONObject = object.getJSONObject(Tags.review);
                                        requestModel.reviewModel = new ReviewModel();
                                        requestModel.reviewModel.id = reviewJSONObject.getString(Tags.id);
                                        requestModel.reviewModel.review = reviewJSONObject.getString(Tags.review);
                                        requestModel.reviewModel.rating = reviewJSONObject.getString(Tags.rating);
                                        requestModel.reviewModel.created = reviewJSONObject.getString(Tags.created);
                                    }

                                    if (AppDelegate.isValidString(object.optString(Tags.category))) {
                                        JSONObject categoryJSONObject = object.getJSONObject(Tags.category);
                                        requestModel.categoryModel = new CategoryModel();
                                        requestModel.categoryModel.id = categoryJSONObject.getInt(Tags.id);
                                        requestModel.categoryModel.title = categoryJSONObject.getString(Tags.category);
                                        requestModel.categoryModel.image = categoryJSONObject.getString(Tags.image);
                                        requestModel.categoryModel.icon = categoryJSONObject.getString(Tags.icon);
                                    }

                                    if (AppDelegate.isValidString(object.optString(Tags.vendor))) {
                                        JSONObject customerJSONObject = object.getJSONObject(Tags.vendor);
                                        requestModel.customerModel = new CustomerModel();
                                        requestModel.customerModel.id = customerJSONObject.getString(Tags.id);
                                        requestModel.customerModel.role_id = customerJSONObject.getString(Tags.role_id);
                                        requestModel.customerModel.name = customerJSONObject.getString(Tags.name);
                                        requestModel.customerModel.email = customerJSONObject.getString(Tags.email);
                                        requestModel.customerModel.last_name = customerJSONObject.getString(Tags.last_name);
                                        requestModel.customerModel.profile_pic = customerJSONObject.getString(Tags.profile_pic);
                                        requestModel.customerModel.contact_no = customerJSONObject.getString(Tags.contact_no);
                                        requestModel.customerModel.address1 = customerJSONObject.getString(Tags.address1);
                                        requestModel.customerModel.address2 = customerJSONObject.getString(Tags.address2);
                                        requestModel.customerModel.city = customerJSONObject.getString(Tags.city);
                                        requestModel.customerModel.state = customerJSONObject.getString(Tags.state);
                                        requestModel.customerModel.country = customerJSONObject.getString(Tags.country);
                                        requestModel.customerModel.currency_id = customerJSONObject.getString(Tags.currency_id);
                                        requestModel.customerModel.latitude = customerJSONObject.getString(Tags.latitude);
                                        requestModel.customerModel.longitude = customerJSONObject.getString(Tags.longitude);
                                    }

                                    JSONArray array = object.getJSONArray(Tags.service);
                                    ArrayList<ServiceModel> serviceArray = new ArrayList<ServiceModel>();
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < array.length(); i++) {
                                        if (AppDelegate.isValidString(array.getString(i) + "")) {
                                            stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                            ServiceModel serviceModel = new ServiceModel();
                                            serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
                                            serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
                                            serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
                                            serviceModel.description = array.getJSONObject(i).optString(Tags.description);
                                            serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                            serviceModel.image = array.getJSONObject(i).optString(Tags.image);
                                            serviceModel.how_works = array.getJSONObject(i).optString(Tags.how_works);
                                            serviceModel.terms = array.getJSONObject(i).optString(Tags.terms);
                                            serviceModel.cancellation_policy = array.getJSONObject(i).optString(Tags.cancellation_policy);
                                            AppDelegate.LogT("Filling time price => " + serviceModel.price);
                                            serviceArray.add(serviceModel);
                                        }
                                    }
                                    requestModel.arrayServices = serviceArray;
//                                        arrayRequestModel.add(requestModel);
                                    MyOrderDetailActivity.this.requestModel = requestModel;
                                    setValues();
//                                    }
//                                    for (int i = 0; i < arrayRequestModel.size(); i++) {
//                                        if (arrayRequestModel.get(i).id.equalsIgnoreCase(str_order_id)) {
//                                            requestModel = arrayRequestModel.get(i);
//                                            AppDelegate.LogT("setValues called arrayRequestModel requestModel.status => " + requestModel.status);
//                                            setValues();
//                                            break;
//                                        }
//                                    }
                                } else {
                                    AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
                                }
                                mHandler.sendEmptyMessage(1);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }

                        @Override
                        public void failure(RestError restError) {
                            mHandler.sendEmptyMessage(11);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(MyOrderDetailActivity.this, obj_json);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pageReload = null;
        if (timeHandler != null && timeRunnable != null) {
            timeHandler.removeCallbacks(timeRunnable);
        }
        mActivity=null;
    }

    @Override
    public void needToReloadPage(String name, String status) {
        if (name.equalsIgnoreCase(Tags.RELOAD)) {
            executeMyOrderAsync(str_order_id);
        }
    }


//    /*paypal integration code Start*/
//    public void onBuyPressed(String str_amount, String str_item_name) {
//        /*
//         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
//         * Change PAYMENT_INTENT_SALE to
//         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
//         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
//         *     later via calls from your server.
//         *
//         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
//         */
//        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(str_amount), "USD", str_item_name, PayPalPayment.PAYMENT_INTENT_SALE);
//
//        /*
//         * See getStuffToBuy(..) for examples of some available payment options.
//         */
//        Intent intent = new Intent(MyOrderDetailActivity.this, PaymentActivity.class);
//
//        // send the same configuration for restart resiliency
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
//        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
//        startActivityForResult(intent, AppDelegate.REQUEST_CODE_PAYMENT);
//    }
//
//    private void startPayPalService() {
//        Intent intent = new Intent(this, PayPalService.class);
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
//        startService(intent);
//    }
//
//    public String TAG = "paypal";
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == AppDelegate.REQUEST_CODE_PAYMENT) {
//            if (resultCode == Activity.RESULT_OK) {
//                PaymentConfirmation confirm =
//                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
//                if (confirm != null) {
//                    try {
//                        Log.i(TAG, confirm.toJSONObject().toString(4));
//                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
//                        /**
//                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
//                         * or consent completion.
//                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
//                         * for more details.
//                         *
//                         * For sample mobile backend interactions, see
//                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
//                         */
//                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
//                        callAsyncBuy(jsonObject.getJSONObject("response").getString("id"), requestModel.price);
//                        AppDelegate.showToast(MyOrderDetailActivity.this, "Payment info received from PayPal, Transaction ID is = " + jsonObject.getJSONObject("response").getString("id"));
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
//                    }
//                }
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                Log.i(TAG, "The user canceled.");
//            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
//                Log.i(
//                        TAG,
//                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
//            }
//        } else if (requestCode == AppDelegate.REQUEST_CODE_FUTURE_PAYMENT) {
//            if (resultCode == Activity.RESULT_OK) {
//                PayPalAuthorization auth =
//                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
//                if (auth != null) {
//                    try {
//                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));
//
//                        String authorization_code = auth.getAuthorizationCode();
//                        Log.i("FuturePaymentExample", authorization_code);
//
//                        sendAuthorizationToServer(auth);
//                        AppDelegate.showToast(MyOrderDetailActivity.this, "Future Payment code received from PayPal");
//
//                    } catch (JSONException e) {
//                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
//                    }
//                }
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                Log.i("FuturePaymentExample", "The user canceled.");
//            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
//                Log.i(
//                        "FuturePaymentExample",
//                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
//            }
//        } else if (requestCode == AppDelegate.REQUEST_CODE_PROFILE_SHARING) {
//            if (resultCode == Activity.RESULT_OK) {
//                PayPalAuthorization auth =
//                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
//                if (auth != null) {
//                    try {
//                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));
//
//                        String authorization_code = auth.getAuthorizationCode();
//                        Log.i("ProfileSharingExample", authorization_code);
//
//                        sendAuthorizationToServer(auth);
//                        AppDelegate.showToast(MyOrderDetailActivity.this, "Profile Sharing code received from PayPal");
//
//                    } catch (JSONException e) {
//                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
//                    }
//                }
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                Log.i("ProfileSharingExample", "The user canceled.");
//            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
//                Log.i(
//                        "ProfileSharingExample",
//                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
//            }
//        }
//    }
//
//    private void callAsyncBuy(String paypal_id, String amount) {
//        if (AppDelegate.haveNetworkConnection(MyOrderDetailActivity.this)) {
//            SingletonRestClient.get().completeOrder(prefs.getUserdata().id + "", prefs.getUserdata().sessionid, paypal_id, str_order_id, amount, new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.checkJsonMessage(MyOrderDetailActivity.this, obj_json);
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                        AppDelegate.showToast(MyOrderDetailActivity.this, getString(R.string.something_wrong_with_server_response));
//                    }
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    JSONObject obj_json = null;
//                    try {
//                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                        if (obj_json.getInt(Tags.session_status) == 0) {
//                            AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString(Tags.message));
//                            new Prefs(MyOrderDetailActivity.this).setUserData(null);
//                            if (MainActivity.mActivity != null)
//                                MainActivity.mActivity.finish();
//                            if (MyOrderActivity.mActivity != null)
//                                MyOrderActivity.mActivity.finish();
//                            startActivity(new Intent(MyOrderDetailActivity.this, SignInActivity.class));
//                            finish();
//                        } else if (obj_json.getString(Tags.status).contains("1")) {
//                            if (alertDialog != null && alertDialog.isShowing()) {
//                                alertDialog.dismiss();
//                            }
//                            executeMyOrderAsync(str_order_id);
//                            AppDelegate.showToast(MyOrderDetailActivity.this, obj_json.getString("message"));
//                        } else {
//                            AppDelegate.showAlert(MyOrderDetailActivity.this, obj_json.getString("message"));
//                        }
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//            });
//        }
//    }
//
//    private void sendAuthorizationToServer(PayPalAuthorization authorization) {
//        /**
//         * TODO: Send the authorization response to your server, where it can
//         * exchange the authorization code for OAuth access and refresh tokens.
//         *
//         * Your server must then store these tokens, so that your server code
//         * can execute payments for this user in the future.
//         *
//         * A more complete example that includes the required app-server to
//         * PayPal-server integration is available from
//         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
//         */
//
//    }
//    /*paypal integration code Exit*/
}
