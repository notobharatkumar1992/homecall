package com.oncall.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 06-Dec-16.
 */
public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    public Handler mHandler;
    public EditText et_old_password, et_new_password, et_confirm_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_activity);
        initView();
        setHandler();
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        et_old_password = (EditText) findViewById(R.id.et_old_password);
        et_old_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_new_password = (EditText) findViewById(R.id.et_new_password);
        et_new_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        et_confirm_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ChangePasswordActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ChangePasswordActivity.this);
                        break;
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                check_validation();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void check_validation() {
        if (!AppDelegate.isValidString(et_old_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.enter_old_password));
        } else if (!AppDelegate.isLegalPassword(et_old_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        } else if (!AppDelegate.isValidString(et_new_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.enter_new_password));
        } else if (!AppDelegate.isLegalPassword(et_new_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        } else if (!AppDelegate.isValidString(et_confirm_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.enter_confim_password));
        } else if (et_new_password.getText().toString().equalsIgnoreCase(et_old_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forchange_pass));
        } else if (!et_confirm_password.getText().toString().equalsIgnoreCase(et_new_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.formatch));
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            execute_changePasswrodAsync();
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    private void execute_changePasswrodAsync() {
        mHandler.sendEmptyMessage(10);
        SingletonRestClient.get().changePassword(new Prefs(this).getUserdata().id + "", new Prefs(this).getUserdata().sessionid + "", et_new_password.getText().toString(), et_old_password.getText().toString(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void failure(RetrofitError error) {
                super.failure(error);
                mHandler.sendEmptyMessage(11);
                try {
                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                    AppDelegate.checkJsonMessage(ChangePasswordActivity.this, obj_json);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(ChangePasswordActivity.this, "");
                }
            }

            @Override
            public void success(Response response, Response response2) {
                mHandler.sendEmptyMessage(11);
                try {
                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    /*if (obj_json.getInt(Tags.session_status) == 0) {
                        AppDelegate.showToast(ChangePasswordActivity.this, obj_json.getString(Tags.message));
                        new Prefs(ChangePasswordActivity.this).setUserData(null);
                        if (MainActivity.mActivity != null)
                            MainActivity.mActivity.finish();
                        startActivity(new Intent(ChangePasswordActivity.this, SignInActivity.class));
                        finish();
                    } else*/
                    if (obj_json.getString(Tags.status).contains("1")) {
                        AppDelegate.showToast(ChangePasswordActivity.this, obj_json.getString(Tags.message));
                        onBackPressed();
                    } else {
                        AppDelegate.showAlert(ChangePasswordActivity.this, obj_json.getString(Tags.message));
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        });
    }
}
