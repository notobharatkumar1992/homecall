package com.oncall.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CurrencyAdapterSearch;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.CategoryModel;
import com.oncall.models.UserDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.parser.JSONParser;
import com.oncall.utils.CircleImageView;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 24-Nov-16.
 */
public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    public static Activity mActivity;
    private Prefs prefs;
    public Handler mHandler;
    ImageView img_user_bg, img_c_loading1;
    CircleImageView cimg_user;
    TextView txt_c_name, txt_c_address, txt_c_email, txt_c_phone;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private UserDataModel userDataModel;
    CurrencyAdapterSearch currencyAdapter;
    ArrayList<CategoryModel> currencyArray = new ArrayList<>();
    String currency_id = "";
    private Dialog dialogue;
    EditText et_search;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.green);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.profile_activity);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(this);
        mActivity = this;
        initView();
        setHandler();
        mHandler.sendEmptyMessage(5);
    }

    private void execute_getMyProfile() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().myProfile(prefs.getUserdata().id + "", prefs.getUserdata().sessionid + "", new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(MyProfileActivity.this, obj_json.getString(Tags.message));
                            new Prefs(MyProfileActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            startActivity(new Intent(MyProfileActivity.this, SignInActivity.class));
                            finish();
                        } else if (obj_json.getInt(Tags.status) == 1) {
                            JSONObject object = obj_json.getJSONObject(Tags.response);
                            UserDataModel userDataModel = new Prefs(MyProfileActivity.this).getUserdata();
                            if (userDataModel == null)
                                userDataModel = new UserDataModel();
                            userDataModel.id = JSONParser.getInt(object, Tags.id);
                            userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                            userDataModel.email = JSONParser.getString(object, Tags.email);
                            userDataModel.first_name = JSONParser.getString(object, Tags.name);
                            userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                            userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                            userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                            userDataModel.address = JSONParser.getString(object, Tags.address1);
                            userDataModel.address2 = JSONParser.getString(object, Tags.address2);
                            userDataModel.city = JSONParser.getString(object, Tags.city);
                            userDataModel.city2 = JSONParser.getString(object, Tags.city_2);
                            userDataModel.state = JSONParser.getString(object, Tags.state);
                            userDataModel.country = JSONParser.getString(object, Tags.country);
                            userDataModel.zip = JSONParser.getString(object, Tags.zip);
                            userDataModel.token = JSONParser.getString(object, Tags.token);
                            userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                            userDataModel.name_email = JSONParser.getString(object, Tags.name_email);
                            userDataModel.latitude = JSONParser.getString(object, Tags.latitude);
                            userDataModel.longitude = JSONParser.getString(object, Tags.longitude);
                            userDataModel.sessionid = JSONParser.getString(object, Tags.sessionid);

                            new Prefs(MyProfileActivity.this).setUserData(userDataModel);
                            setValues();
                        } else {
                            AppDelegate.showToast(MyProfileActivity.this, obj_json.getString(Tags.message));
                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(MyProfileActivity.this, obj_json);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MyProfileActivity.this);
                        break;
                    case 4:
                        setValues();
                        break;
                    case 5:
                        execute_getMyProfile();
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    void setValues() {
        AppDelegate.LogT("setValues called");
        userDataModel = new Prefs(MyProfileActivity.this).getUserdata();
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.image)) {
            img_c_loading1.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
            frameAnimation.setCallback(img_c_loading1);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            imageLoader.loadImage(new Prefs(this).getUserdata().image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    img_c_loading1.setVisibility(View.GONE);
//                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    cimg_user.setImageBitmap(bitmap);
                    img_c_loading1.setVisibility(View.GONE);
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            img_user_bg.setImageBitmap(AppDelegate.blurRenderScript(MyProfileActivity.this, bitmap));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    img_c_loading1.setVisibility(View.GONE);
                }
            });
        }
        if (userDataModel != null) {
            txt_c_name.setText((AppDelegate.isValidString(userDataModel.first_name) ? userDataModel.first_name : "") + " " + (AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name : ""));
            txt_c_address.setText(AppDelegate.isValidString(userDataModel.address) ? userDataModel.address + "" : "" + (AppDelegate.isValidString(userDataModel.city + "") ? ", " + userDataModel.address + "" : ""));
            txt_c_email.setText(userDataModel.email + "");
            txt_c_phone.setText(userDataModel.contact_no + "");

//            oneNumberUtil phoneUtil = PhoneNumberUtils.getInstance();
//            try {
//                PhoneNumber numberProto = phoneUtil.parse(numberStr, "US");
//                //Since you know the country you can format it as follows:
//                System.out.println(phoneUtil.format(numberProto, PhoneNumberFormat.NATIONAL));
//            } catch (NumberParseException e) {
//                System.err.println("NumberParseException was thrown: " + e.toString());
//            }
//
            try {
                String s = txt_c_phone.getText().toString();
                txt_c_phone.setText(String.format("%s %s %s", s.subSequence(0, 3), s.subSequence(3, 6), s.subSequence(6, s.length())));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userDataModel = new Prefs(this).getUserdata();
        setValues();

    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.img_c_edit).setOnClickListener(this);
        img_user_bg = (ImageView) findViewById(R.id.img_user_bg);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        txt_c_email = (TextView) findViewById(R.id.txt_c_email);
        txt_c_phone = (TextView) findViewById(R.id.txt_c_phone);
        userDataModel = prefs.getUserdata();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                finish();
                break;
            case R.id.img_c_edit:
                if (userDataModel != null)
                    startActivity(new Intent(this, EditVendorProfileActivity.class));
                break;
//            case R.id.img_c_edit_currency:
//                if (currencyArray.size() == 0) {
//                    executeCurrencyAsync();
////                } else {
////                    currencyDialog(currencyArray);
//                }
//                break;
        }
    }

//    private void executeCurrencyAsync() {
//       /* if (capturedFile == null) {
//            AppDelegate.showToast(this, "Please select an image.");
//        } else*/
//        if (AppDelegate.haveNetworkConnection(this)) {
//
//            mHandler.sendEmptyMessage(10);
//            SingletonRestClient.get().getCurrency(new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.checkJsonMessage(MyProfileActivity.this, obj_json);
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                        AppDelegate.showToast(MyProfileActivity.this, "");
//                    }
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                        if (obj_json.getString(Tags.status).contains("1")) {
//                            JSONArray jsonArray = obj_json.getJSONArray(Tags.category);
//                            currencyArray.clear();
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject object = jsonArray.getJSONObject(i);
//                                CategoryModel categoryModel = new CategoryModel();
//                                categoryModel.id = object.getInt(Tags.id);
//                                categoryModel.title = object.getString(Tags.title);
//                                categoryModel.symbol = object.getString(Tags.symbol);
//                                categoryModel.slug = object.getString(Tags.slug);
//                                currencyArray.add(categoryModel);
//                            }
////                            currencyDialog(currencyArray);
//                        } else {
//                            AppDelegate.showToast(MyProfileActivity.this, obj_json.getString(Tags.message));
//                        }
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//            });
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }
}
