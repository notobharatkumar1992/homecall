package com.oncall.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryDialogAdapter;
import com.oncall.adapters.MyOrderListAdapter;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.interfaces.PageReload;
import com.oncall.models.CategoryModel;
import com.oncall.models.CustomerModel;
import com.oncall.models.RequestModel;
import com.oncall.models.ReviewModel;
import com.oncall.models.ServiceModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import carbon.widget.EditText;
import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 27-Mar-17.
 */
public class MyOrderActivity extends AppCompatActivity implements OnListItemClickListener, View.OnClickListener, PageReload {

    public static PageReload pageReload;
    public static Activity mActivity;
    public Prefs prefs;
    public Handler mHandler;

    public ArrayList<RequestModel> mainArrayRequestModel = new ArrayList<>();
    public ArrayList<RequestModel> arrayRequestModel = new ArrayList<>();
    public RecyclerView recycler_view;
    public MyOrderListAdapter myOrderListAdapter;
    public LinearLayout ll_c_all, ll_c_pending, ll_c_completed, ll_c_cancelled;
    //    0=>Pendinng, 1=>Accepted, 2=>Rejected, 3=>Canceled, 4=>Started, 5=>Completed
    public static final int ORDER_ALL = -1, ORDER_PENDING = 0, ORDER_ACCEPTED = 1, ORDER_REJECTED = 2, ORDER_CANCELLED = 3, ORDER_STARTED = 4, ORDER_COMPLETED = 5;
    public int selected_page = ORDER_ALL;
    public boolean showDialogClicked = false;
    private String str_global_dateTime = "2017-06-14 17:47:48";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_order_activity);
        mActivity = this;
        pageReload = this;
        prefs = new Prefs(MyOrderActivity.this);
        categoryArray = prefs.getCategoryArrayList();
        if (categoryArray == null || categoryArray.size() == 0) {
            showDialogClicked = false;
            executedashBoardAsync();
        }
        initView();
        setHandler();
        switchPage(ORDER_ALL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        executeMyOrderAsync();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyOrderActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MyOrderActivity.this);
                        break;

                    case 1:
                        break;
                }
            }
        };
    }

    private void executeMyOrderAsync() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().myOrder(new Prefs(MyOrderActivity.this).getUserdata().id + "", new Prefs(MyOrderActivity.this).getUserdata().sessionid + "", category_id,
                    new Callback<Response>() {

                        @Override
                        public void success(Response response, Response response2) {
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                if (obj_json.getInt(Tags.status) == 1) {
                                    str_global_dateTime = obj_json.getString(Tags.date);
                                    JSONArray jsonArray = obj_json.getJSONArray(Tags.order);
                                    arrayRequestModel.clear();
                                    mainArrayRequestModel.clear();
                                    for (int j = 0; j < jsonArray.length(); j++) {
                                        JSONObject object = jsonArray.getJSONObject(j);
                                        RequestModel requestModel = new RequestModel();
                                        requestModel.minimum_booking_time = object.getString(Tags.minimum_time);
                                        requestModel.id = object.getString(Tags.id);
                                        requestModel.category_id = object.getString(Tags.category_id);
                                        requestModel.subcategory_id = object.getString(Tags.subcategory_id);
                                        requestModel.price = object.getString(Tags.price);
                                        requestModel.currency_id = object.getString(Tags.currency_id);
                                        requestModel.user_id = object.getString(Tags.user_id);
                                        requestModel.vendor_id = object.getString(Tags.vendor_id);

                                        requestModel.reference_id = object.getString(Tags.reference_id);
                                        requestModel.booking_date = object.getString(Tags.booking_date);
                                        requestModel.booking_time = object.getString(Tags.booking_time);
                                        requestModel.comment = object.getString(Tags.comment);
                                        requestModel.name = object.getString(Tags.name);
                                        requestModel.phone = object.getString(Tags.phone);
                                        requestModel.email = object.getString(Tags.email);
                                        requestModel.city = object.getString(Tags.city);
                                        requestModel.city_2 = object.getString(Tags.city_2);
                                        requestModel.state = object.getString(Tags.state);
                                        requestModel.address = object.getString(Tags.address);
                                        requestModel.address2 = object.getString(Tags.address_2);
                                        requestModel.zip_code = object.getString(Tags.zip);
                                        requestModel.status = object.getString(Tags.status);
                                        requestModel.created = object.getString(Tags.created);

                                        if (AppDelegate.isValidString(object.optString(Tags.review))) {
                                            JSONObject reviewJSONObject = object.getJSONObject(Tags.review);
                                            AppDelegate.LogT("inside review model => " + j + " => " + reviewJSONObject);
                                            requestModel.reviewModel = new ReviewModel();
                                            requestModel.reviewModel.id = reviewJSONObject.getString(Tags.id);
                                            requestModel.reviewModel.review = reviewJSONObject.getString(Tags.review);
                                            requestModel.reviewModel.rating = reviewJSONObject.getString(Tags.rating);
                                            requestModel.reviewModel.created = reviewJSONObject.getString(Tags.created);
                                        }

                                        if (AppDelegate.isValidString(object.optString(Tags.category))) {
                                            JSONObject categoryJSONObject = object.getJSONObject(Tags.category);
                                            requestModel.categoryModel = new CategoryModel();
                                            requestModel.categoryModel.id = categoryJSONObject.getInt(Tags.id);
                                            requestModel.categoryModel.title = categoryJSONObject.getString(Tags.category);
                                            requestModel.categoryModel.image = categoryJSONObject.getString(Tags.image);
                                            requestModel.categoryModel.icon = categoryJSONObject.getString(Tags.icon);
                                        }

                                        if (AppDelegate.isValidString(object.optString(Tags.vendor))) {
                                            JSONObject customerJSONObject = object.getJSONObject(Tags.vendor);
                                            requestModel.customerModel = new CustomerModel();
                                            requestModel.customerModel.id = customerJSONObject.getString(Tags.id);
                                            requestModel.customerModel.role_id = customerJSONObject.getString(Tags.role_id);
                                            requestModel.customerModel.name = customerJSONObject.getString(Tags.name);
                                            requestModel.customerModel.email = customerJSONObject.getString(Tags.email);
                                            requestModel.customerModel.last_name = customerJSONObject.getString(Tags.last_name);
                                            requestModel.customerModel.profile_pic = customerJSONObject.getString(Tags.profile_pic);
                                            requestModel.customerModel.contact_no = customerJSONObject.getString(Tags.contact_no);
                                            requestModel.customerModel.address1 = customerJSONObject.getString(Tags.address1);
                                            requestModel.customerModel.address2 = customerJSONObject.getString(Tags.address2);
                                            requestModel.customerModel.city = customerJSONObject.getString(Tags.city);
                                            requestModel.customerModel.state = customerJSONObject.getString(Tags.state);
                                            requestModel.customerModel.country = customerJSONObject.getString(Tags.country);
                                            requestModel.customerModel.currency_id = customerJSONObject.getString(Tags.currency_id);
                                            requestModel.customerModel.latitude = customerJSONObject.getString(Tags.latitude);
                                            requestModel.customerModel.longitude = customerJSONObject.getString(Tags.longitude);
                                            try {
                                                if (AppDelegate.isValidString(customerJSONObject.getJSONObject(Tags.vendor).optString(Tags.business_name)))
                                                    requestModel.customerModel.business_name = customerJSONObject.getJSONObject(Tags.vendor).optString(Tags.business_name);
                                                else requestModel.customerModel.business_name = "";
                                            } catch (Exception e) {
                                                AppDelegate.LogE(e);
                                            }
                                        }

                                        JSONArray array = object.getJSONArray(Tags.service);
                                        ArrayList<ServiceModel> serviceArray = new ArrayList<ServiceModel>();
                                        StringBuilder stringBuilder = new StringBuilder();
                                        for (int i = 0; i < array.length(); i++) {
                                            if (AppDelegate.isValidString(array.getString(i) + "")) {
                                                stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                                ServiceModel serviceModel = new ServiceModel();
                                                serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
                                                serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
                                                serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
                                                serviceModel.description = array.getJSONObject(i).optString(Tags.description);
                                                serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                                serviceModel.image = array.getJSONObject(i).optString(Tags.image);
                                                serviceModel.how_works = array.getJSONObject(i).optString(Tags.how_works);
                                                serviceModel.terms = array.getJSONObject(i).optString(Tags.terms);
                                                serviceModel.cancellation_policy = array.getJSONObject(i).optString(Tags.cancellation_policy);
                                                AppDelegate.LogT("Filling time price => " + serviceModel.price);
                                                serviceArray.add(serviceModel);
                                            }
                                        }
                                        requestModel.arrayServices = serviceArray;
                                        arrayRequestModel.add(requestModel);
                                        mainArrayRequestModel.add(requestModel);

                                    }

                                    myOrderListAdapter = new MyOrderListAdapter(MyOrderActivity.this, arrayRequestModel, MyOrderActivity.this, ORDER_ALL);
                                    recycler_view.setAdapter(myOrderListAdapter);
                                    myOrderListAdapter.filter(selected_page);

                                } else {
                                    AppDelegate.showToast(MyOrderActivity.this, obj_json.getString(Tags.message));
                                }
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }

                        @Override
                        public void failure(RestError restError) {
                            mHandler.sendEmptyMessage(11);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(MyOrderActivity.this, obj_json);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    }

            );
        }
    }

    public ArrayList<RequestModel> testArrayRequestModel = new ArrayList<>();
    public String[] arrayCategory;

    private void refreshArrayAccordingCategory() {
        AppDelegate.LogT("refreshArrayAccordingCategory called, category_id => " + category_id);
        testArrayRequestModel = new ArrayList<>();
        testArrayRequestModel.addAll(mainArrayRequestModel);
        if (AppDelegate.isValidString(category_id)) {
            arrayRequestModel.clear();
            arrayCategory = category_id.split(",");
            AppDelegate.LogT("refreshArrayAccordingCategory called, arrayCategory => " + arrayCategory.length);
            for (int i = 0; i < arrayCategory.length; i++) {
                for (int j = 0; j < testArrayRequestModel.size(); j++) {
                    AppDelegate.LogT("arrayCategory[i] => " + arrayCategory[i] + " == " + testArrayRequestModel.get(j).category_id);
                    if (arrayCategory[i].equalsIgnoreCase(testArrayRequestModel.get(j).category_id)) {
                        if (arrayRequestModel.size() == 0) {
                            arrayRequestModel.add(testArrayRequestModel.get(j));
                        } else {
                            boolean shouldNotAdd = false;
                            for (int k = 0; k < arrayRequestModel.size(); k++) {
                                if (arrayRequestModel.get(k).id.equalsIgnoreCase(testArrayRequestModel.get(j).id)) {
                                    shouldNotAdd = true;
                                }
                            }
                            if (!shouldNotAdd) {
                                arrayRequestModel.add(testArrayRequestModel.get(j));
                            }
                        }
                    }
                }
            }
            AppDelegate.LogT("arrayRequestModel => " + arrayRequestModel.size());
            myOrderListAdapter = new MyOrderListAdapter(MyOrderActivity.this, arrayRequestModel, MyOrderActivity.this, ORDER_ALL);
            recycler_view.setAdapter(myOrderListAdapter);
            myOrderListAdapter.filter(selected_page);
        } else {
            myOrderListAdapter = new MyOrderListAdapter(MyOrderActivity.this, arrayRequestModel, MyOrderActivity.this, ORDER_ALL);
            recycler_view.setAdapter(myOrderListAdapter);
            myOrderListAdapter.filter(selected_page);
        }


    }

    private void initView() {
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        recycler_view.setNestedScrollingEnabled(false);
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        myOrderListAdapter = new MyOrderListAdapter(this, arrayRequestModel, this, ORDER_ALL);
        recycler_view.setAdapter(myOrderListAdapter);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        ((ImageView) findViewById(R.id.img_c_filter)).setImageResource(R.drawable.filter);
        findViewById(R.id.img_c_filter).setOnClickListener(this);

        ll_c_all = (LinearLayout) findViewById(R.id.ll_c_all);
        ll_c_all.setOnClickListener(this);
        ll_c_pending = (LinearLayout) findViewById(R.id.ll_c_pending);
        ll_c_pending.setOnClickListener(this);
        ll_c_completed = (LinearLayout) findViewById(R.id.ll_c_completed);
        ll_c_completed.setOnClickListener(this);
        ll_c_cancelled = (LinearLayout) findViewById(R.id.ll_c_cancelled);
        ll_c_cancelled.setOnClickListener(this);

    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.name)) {
            arrayRequestModel.get(position);
        }
    }

    private void execute_cancelOrderAsync(String order_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().cancelJob(new Prefs(this).getUserdata().id + "", new Prefs(MyOrderActivity.this).getUserdata().sessionid + "", order_id, str_cancelDialogComment, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(MyOrderActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(MyOrderActivity.this, getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(MyOrderActivity.this, obj_json.getString(Tags.message));
                            new Prefs(MyOrderActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            if (MyOrderActivity.mActivity != null)
                                MyOrderActivity.mActivity.finish();
                            startActivity(new Intent(MyOrderActivity.this, SignInActivity.class));
                            finish();
                        } else if (obj_json.getString(Tags.status).contains("1")) {
                            if (alertDialog != null && alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                            executeMyOrderAsync();
                            AppDelegate.showToast(MyOrderActivity.this, obj_json.getString("message"));
                        } else {
                            AppDelegate.showAlert(MyOrderActivity.this, obj_json.getString("message"));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    public AlertDialog alertDialog;
    public String str_cancelDialogComment = "";

    public void showCancelOrderDialog(final String id) {
        /* Alert Dialog Code Start*/
        final AlertDialog.Builder alert = new AlertDialog.Builder(MyOrderActivity.this);
        alert.setTitle("Cancel Order"); //Set Alert dialog title here
        alert.setCancelable(false);

        // Set an EditText view to get user input
        final EditText input = new EditText(MyOrderActivity.this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        input.setLayoutParams(params);
        input.setPadding(AppDelegate.dpToPix(MyOrderActivity.this, 25), AppDelegate.dpToPix(MyOrderActivity.this, 15), AppDelegate.dpToPix(MyOrderActivity.this, 25), 0);
        input.setHint("Enter your comment");
        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        input.setFocusable(true);
        input.setSelection(input.length());
        input.setFocusableInTouchMode(true);
        input.setEnabled(true);
        alert.setView(input);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                str_cancelDialogComment = input.getText().toString();
                execute_cancelOrderAsync(id);
                dialog.cancel();

            } // End of onClick(DialogInterface dialog, int whichButton)
        }); //End of alert.setPositiveButton
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                dialog.cancel();
            }
        }); //End of alert.setNegativeButton
        alertDialog = alert.create();
        alertDialog.show();
    }

    int selectedPosition = 0;

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {
        if (name.equalsIgnoreCase(Tags.cancel)) {
            for (int i = 0; i < mainArrayRequestModel.size(); i++) {
                if (Integer.parseInt(mainArrayRequestModel.get(i).id) == PICK_UP) {
                    selectedPosition = i;
//                    AppDelegate.showAlert(MyOrderActivity.this, "Cancel Order", "Are you sure you want to cancel this job?", "Yes", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                        }
//                    }, "No", null);
//
                    executeMyOrderAsync(mainArrayRequestModel.get(i).id + "");
//                    showCancelOrderDialog(mainArrayRequestModel.get(i).id);
                }
            }
        } else if (name.equalsIgnoreCase(Tags.rating)) {
            for (int i = 0; i < mainArrayRequestModel.size(); i++) {
                if (Integer.parseInt(mainArrayRequestModel.get(i).id) == PICK_UP) {
                    showRatingDialog(mainArrayRequestModel.get(i).id);
                }
            }
        }
    }

    public Calendar minimumTimeCalender = Calendar.getInstance();

    private void executeMyOrderAsync(String order_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().orderDetail(new Prefs(MyOrderActivity.this).getUserdata().id + "", new Prefs(MyOrderActivity.this).getUserdata().sessionid + "", order_id,
                    new Callback<Response>() {

                        @Override
                        public void success(Response response, Response response2) {
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                if (obj_json.getInt(Tags.status) == 1) {
                                    str_global_dateTime = obj_json.getString(Tags.date);
                                    try {
                                        minimumTimeCalender.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str_global_dateTime));
                                    } catch (Exception e) {
                                        AppDelegate.LogE(e);
                                    }
                                    AppDelegate.LogT("minimumTimeCalender Before => " + minimumTimeCalender.getTime());
                                    minimumTimeCalender.add(Calendar.HOUR_OF_DAY, 12);
                                    Date minimumTimeDate = minimumTimeCalender.getTime();
                                    Date selectedDate = Calendar.getInstance().getTime();
                                    try {
                                        selectedDate = new SimpleDateFormat(ScheduleJobActivity.DATE_FORMAT + " " + ScheduleJobActivity.TIME_FORMAT_12_HOUR).parse(mainArrayRequestModel.get(selectedPosition).booking_date + " " + mainArrayRequestModel.get(selectedPosition).booking_time);
                                    } catch (Exception e) {
                                        AppDelegate.LogE(e);
                                    }
                                    AppDelegate.LogT("minimumTimeCalender => " + minimumTimeDate + ", selectedDate => " + selectedDate + ", BEFORE => " + minimumTimeDate.before(selectedDate) + ", Equal => " + minimumTimeDate.equals(selectedDate));
                                    if (minimumTimeDate.before(selectedDate)) {
                                        AppDelegate.showAlert(MyOrderActivity.this, "Cancel Order", "Are you sure you want to cancel this job?", "Yes", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                showCancelOrderDialog(mainArrayRequestModel.get(selectedPosition).id);
                                            }
                                        }, "No", null);
                                    } else {
                                        AppDelegate.showAlert(MyOrderActivity.this, "Cancel Order", "You will be charged for cancellation of job due to cancelling in 12 hour to booking time. Are you sure you want to cancel this job?", "Yes", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                showCancelOrderDialog(mainArrayRequestModel.get(selectedPosition).id);
                                            }
                                        }, "No", null);
                                    }

                                } else {
                                    AppDelegate.showToast(MyOrderActivity.this, obj_json.getString(Tags.message));
                                }
                                mHandler.sendEmptyMessage(1);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }

                        @Override
                        public void failure(RestError restError) {
                            mHandler.sendEmptyMessage(11);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(MyOrderActivity.this, obj_json);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
        }
    }

    public float selectedRating = 0;
    public String str_rateDialogFeedback = "";

    private void showRatingDialog(final String order_id) {
        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .session(150)
                .threshold(5)
                .ratingBarColor(R.color.colorAccent)
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {
                        if (feedback.length() == 0) {
                            AppDelegate.showAlert(MyOrderActivity.this, getString(R.string.rating_dialog_suggestions));
                        } else {
                            str_rateDialogFeedback = feedback;
                            executeAddRatingAsync(order_id);
                        }
                    }
                }).onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared) {
                        selectedRating = rating;
                    }
                })
                .build();
        ratingDialog.show();
    }

    private void executeAddRatingAsync(String order_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().addRating(new Prefs(MyOrderActivity.this).getUserdata().id + "",
                    new Prefs(MyOrderActivity.this).getUserdata().sessionid + "",
                    order_id + "",
                    selectedRating + "",
                    str_rateDialogFeedback + "",
                    new Callback<Response>() {

                        @Override
                        public void success(Response response, Response response2) {
                            try {
                                AppDelegate.sendEventTracker(MyOrderActivity.this, "Rate Success");
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                if (obj_json.getInt(Tags.session_status) == 0) {
                                    AppDelegate.showToast(MyOrderActivity.this, obj_json.getString(Tags.message));
                                    new Prefs(MyOrderActivity.this).setUserData(null);
                                    if (MainActivity.mActivity != null)
                                        MainActivity.mActivity.finish();
                                    if (MyOrderActivity.mActivity != null)
                                        MyOrderActivity.mActivity.finish();
                                    startActivity(new Intent(MyOrderActivity.this, SignInActivity.class));
                                    finish();
                                } else if (obj_json.getInt(Tags.status) == 1) {
                                    executeMyOrderAsync();
                                    AppDelegate.showToast(MyOrderActivity.this, obj_json.getString(Tags.message));
                                } else {
                                    AppDelegate.showToast(MyOrderActivity.this, obj_json.getString(Tags.message));
                                }
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                                mHandler.sendEmptyMessage(11);
                            }
                        }

                        @Override
                        public void failure(RestError restError) {
                            mHandler.sendEmptyMessage(11);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(MyOrderActivity.this, obj_json);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
        }
    }


    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;

            case R.id.img_c_filter:
                showDialogClicked = true;
                if (categoryArray != null && categoryArray.size() > 0) {
                    categoryDialog();
                } else {
                    executedashBoardAsync();
                }
                break;

            case R.id.ll_c_all:
                switchPage(ORDER_ALL);
                break;

            case R.id.ll_c_pending:
                switchPage(ORDER_PENDING);
                break;

            case R.id.ll_c_completed:
                switchPage(ORDER_COMPLETED);
                break;

            case R.id.ll_c_cancelled:
                switchPage(ORDER_CANCELLED);
                break;
        }
    }

    private RecyclerView recycler_deals;
    private boolean[] is_checked;
    public String category_id;
    private String currency_str = "", currencysymbbol_str = "", currencyslug_str = "";
    public ArrayList<CategoryModel> categoryArray;
    private CategoryDialogAdapter mAdapter;

    public void categoryDialog() {
        if (categoryArray != null) {
            final Dialog categoryDialog = new Dialog(this, android.R.style.Theme_Light);
            categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            categoryDialog.setContentView(R.layout.dialog_services);
            recycler_deals = (RecyclerView) categoryDialog.findViewById(R.id.recycler_deals);
            recycler_deals.setNestedScrollingEnabled(false);
            recycler_deals.setHasFixedSize(true);
            recycler_deals.setLayoutManager(new LinearLayoutManager(this));
            recycler_deals.setItemAnimator(new DefaultItemAnimator());
            TextView txt2 = (TextView) categoryDialog.findViewById(R.id.txt2);
            txt2.setText("Categories");

            if (is_checked == null || is_checked.length != categoryArray.size()) {
                is_checked = new boolean[categoryArray.size()];
                for (int i = 0; i < categoryArray.size(); i++) {
                    is_checked[i] = true;
                    categoryArray.get(i).checked = 1;
                }
            } else {
                for (int i = 0; i < is_checked.length; i++) {
                    categoryArray.get(i).checked = is_checked[i] ? 1 : 0;
                }
            }

            mAdapter = new CategoryDialogAdapter(this, categoryArray, new OnListItemClickListener() {
                @Override
                public void setOnListItemClickListener(String name, int position) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, int PICK_UP) {

                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                    categoryArray.get(position).checked = like_dislike ? 1 : 0;
                    mAdapter.notifyDataSetChanged();
                    recycler_deals.invalidate();
                }
            });
            recycler_deals.setAdapter(mAdapter);

            categoryDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuilder stringBuilder = null;
                    for (int i = 0; i < categoryArray.size(); i++) {
                        if (categoryArray.get(i).checked == 1) {
                            is_checked[i] = true;
                            if (stringBuilder == null) {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("" + categoryArray.get(i).id);
                            } else {
                                stringBuilder.append("," + categoryArray.get(i).id);
                            }
                        } else {
                            is_checked[i] = false;
                        }
                    }
                    if (stringBuilder != null) {
                        category_id = stringBuilder.toString();
                        refreshArrayAccordingCategory();
                        categoryDialog.dismiss();
                    } else {
                        AppDelegate.showToast(MyOrderActivity.this, "Please select category.");
                    }
                }
            });
            categoryDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDialog.dismiss();
                }
            });
            categoryDialog.show();
        }
    }

    private void executedashBoardAsync() {
        if (AppDelegate.haveNetworkConnection(MyOrderActivity.this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getDashboard(new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONArray categJsonArray = obj_json.getJSONArray(Tags.category);
                            categoryArray = new ArrayList<>();
                            for (int i = 0; i < categJsonArray.length(); i++) {
                                CategoryModel serviceModel = new CategoryModel();
                                serviceModel.id = categJsonArray.getJSONObject(i).getInt(Tags.id);
                                serviceModel.title = categJsonArray.getJSONObject(i).getString(Tags.category);
                                serviceModel.image = categJsonArray.getJSONObject(i).getString(Tags.image);
                                serviceModel.icon = categJsonArray.getJSONObject(i).getString(Tags.icon);
//                                serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                categoryArray.add(serviceModel);
                            }
                            if (showDialogClicked) {
                                categoryDialog();
                                showDialogClicked = false;
                            }
                        } else {
                            AppDelegate.showToast(MyOrderActivity.this, obj_json.getString(Tags.message));
                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    try {
                        mHandler.sendEmptyMessage(11);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(MyOrderActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }


    private void switchPage(int value) {
        selected_page = value;

        ll_c_all.setSelected(false);
        ll_c_pending.setSelected(false);
        ll_c_completed.setSelected(false);
        ll_c_cancelled.setSelected(false);
        switch (value) {
            case ORDER_ALL:
                ll_c_all.setSelected(true);
                myOrderListAdapter.filter(ORDER_ALL);
                break;
            case ORDER_PENDING:
                ll_c_pending.setSelected(true);
                myOrderListAdapter.filter(ORDER_PENDING);
                break;
            case ORDER_COMPLETED:
                ll_c_completed.setSelected(true);
                myOrderListAdapter.filter(ORDER_COMPLETED);
                break;
            case ORDER_CANCELLED:
                ll_c_cancelled.setSelected(true);
                myOrderListAdapter.filter(ORDER_CANCELLED);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
        pageReload = null;
    }

    @Override
    public void needToReloadPage(String name, String status) {
        if (name.equalsIgnoreCase(Tags.RELOAD)) {
            executeMyOrderAsync();
        }
    }

}
