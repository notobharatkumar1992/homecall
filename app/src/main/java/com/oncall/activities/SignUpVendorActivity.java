package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.oncall.AppDelegate;
import com.oncall.Async.LocationAddress;
import com.oncall.Async.PlacesService;
import com.oncall.R;
import com.oncall.adapters.LocationMapAdapter;
import com.oncall.constants.Tags;
import com.oncall.expandablelayout.ExpandableRelativeLayout;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.Place;
import com.oncall.models.UserDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.PhoneNumberTextWatcherDefault;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ProgressBar;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class SignUpVendorActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    private EditText et_username, et_contact, et_password, et_confirm_password, et_first_name, et_last_name, et_address, et_city, et_state;
    private Handler mHandler;
    RelativeLayout rl_search;
    public static Activity mActivity;
    private double currentLatitude = 0, currentLongitude = 0;

    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;

    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    public static final int COLLAPSE = 0, EXPAND = 1;
    private Prefs prefs;

    private boolean canSearch = false;
    public UserDataModel userDataModel;
    public String login_from = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sign_up_vendor);
        login_from = getIntent().getStringExtra(Tags.FROM);
        if (getIntent().getExtras() != null)
            userDataModel = getIntent().getExtras().getParcelable(Tags.user);
        mActivity = this;
        initView();
        setValues();
        setHandler();
    }

    private void setValues() {
        if (userDataModel != null) {
            if (AppDelegate.isValidString(userDataModel.first_name))
                et_first_name.setText(userDataModel.first_name + "");
            else if (AppDelegate.isValidString(userDataModel.name))
                et_first_name.setText(userDataModel.name + "");
            et_last_name.setText(userDataModel.last_name + "");
            if (AppDelegate.isValidString(userDataModel.email)) {
                et_username.setText(userDataModel.email + "");
                et_username.setEnabled(false);
            }
        }
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude = " + currentLatitude + ", longitude = " + currentLongitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            placeDetail_pickup.city = data.getString(Tags.city_param);
            placeDetail_pickup.state = data.getString(Tags.STATE);
            placeDetail_pickup.country = data.getString(Tags.country_param);
            placeDetail_pickup.postal_code = data.getString(Tags.postalCode);

            AppDelegate.LogT("placeDetail_pickup => " + placeDetail_pickup.toString());
            try {
                currentLatitude = data.getDouble(Tags.latitude);
                currentLongitude = data.getDouble(Tags.longitude);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = 0;
            currentLongitude = 0;
        }
    }

    private void addtextWatcher() {
        et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.toString().length() == 1) {
                    ((ScrollView) (findViewById(R.id.scrollView))).smoothScrollTo(0, ((ScrollView) (findViewById(R.id.scrollView))).getBottom());
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1) {
                    ((ScrollView) (findViewById(R.id.scrollView))).smoothScrollTo(0, ((ScrollView) (findViewById(R.id.scrollView))).getBottom());
                }
                AppDelegate.LogT("et_search afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SignUpVendorActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SignUpVendorActivity.this);
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        mHandler.sendEmptyMessage(10);
                        AppDelegate.LogT("mHandler.sendEmptyMessage(3) called choose location Activity;");
                        LocationAddress.getLocationFromReverseGeocoder(et_address.getText().toString(), SignUpVendorActivity.this, mHandler);
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null) {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0) {
                            if (et_address.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        if (canSearch) {
                            currentLatitude = 0;
                            currentLongitude = 0;
                            if (et_address.length() != 0) {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            if (et_address.length() != 0) {
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 9:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_address.length() != 0) {
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;

                }
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(SignUpVendorActivity.this, false)) {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(SignUpVendorActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_address.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(et_address.getText().toString()));
                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void initView() {
        et_contact = (EditText) findViewById(R.id.et_contact);
        et_contact.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_contact.addTextChangedListener(new PhoneNumberTextWatcherDefault(et_contact));
        et_username = (EditText) findViewById(R.id.et_username);
        et_username.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_password = (EditText) findViewById(R.id.et_password);
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        et_confirm_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_address = (EditText) findViewById(R.id.et_address);
        et_address.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_city = (EditText) findViewById(R.id.et_city);
        et_city.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_state = (EditText) findViewById(R.id.et_state);
        et_state.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
//        addtextWatcher();
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);

        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(this, mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(SignUpVendorActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(SignUpVendorActivity.this)) {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position) {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                } else if (AppDelegate.isValidString(placeDetail_pickup.name)) {
                    et_address.setText(placeDetail_pickup.name);
                } else if (AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.vicinity);
                }
                et_address.setSelection(et_address.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(3);
            }
            canSearch = true;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                executeSignInAsync();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void executeSignInAsync() {
        if (et_first_name.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_first_name));
        } else if (et_last_name.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_last_name));
        } else if (et_username.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_email_enter));
        } else if (!AppDelegate.isValidEmail(et_username.getText().toString())) {
            AppDelegate.showToast(this, getString(R.string.validation_email_valid_enter));
        } else if (et_contact.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_phone_number));
        } else if (et_contact.length() < 10) {
            AppDelegate.showToast(this, getString(R.string.validation_phone_number_valid));
//        } else if (et_password.length() == 0) {
//            AppDelegate.showToast(this, getString(R.string.validation_password_enter));
//        } else if (!AppDelegate.isLegalPassword(et_password.getText().toString())) {
//            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        } else if (et_address.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_address_enter));
        } else if (et_city.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_city_enter));
//        } else if (et_state.length() == 0) {
//            AppDelegate.showToast(this, getString(R.string.validation_state_enter));
        } else if (et_password.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_password_enter));
        } else if (!AppDelegate.isLegalPassword(et_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        } else if (et_confirm_password.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_confirm_password_enter));
        } else if (!et_password.getText().toString().equals(et_confirm_password.getText().toString())) {
            AppDelegate.showToast(this, getString(R.string.validation_confirm_password_valid));
        } else if (AppDelegate.haveNetworkConnection(this)) {
            String device_token = new Prefs(SignUpVendorActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            mHandler.sendEmptyMessage(10);
            String str_phone = et_contact.getText().toString().trim();
            str_phone = str_phone.replaceAll(" ", "");
            if (userDataModel != null) {
                SingletonRestClient.get().signUp(
                        et_first_name.getText().toString(),
                        et_last_name.getText().toString(),
                        et_username.getText().toString(),
                        et_password.getText().toString(),
                        et_address.getText().toString(),
                        "05" + str_phone, AppDelegate.DEVICE_TYPE, device_token,
                        "", "",
                        et_city.getText().toString() + "",
                        et_state.getText().toString() + "",
                        "",
                        "",
                        userDataModel.social_id + "",
                        userDataModel.image + "", userDataModel.login_type + "", AppDelegate.getUUID(SignUpVendorActivity.this),
                        new Callback<Response>() {
                            @Override
                            public void failure(RestError restError) {
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                super.failure(error);
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                    AppDelegate.checkJsonMessage(SignUpVendorActivity.this, obj_json);
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                    AppDelegate.showToast(SignUpVendorActivity.this, "");
                                }
                            }

                            @Override
                            public void success(Response response, Response response2) {
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                    if (obj_json.getString(Tags.status).contains("1")) {
                                        AppDelegate.showToast(SignUpVendorActivity.this, obj_json.getString(Tags.message));
                                        if (LoginTutorialActivity.mActivity != null)
                                            LoginTutorialActivity.mActivity.finish();
                                        if (SignInActivity.mActivity != null)
                                            SignInActivity.mActivity.finish();

                                        AppDelegate.sendEventTracker(SignUpVendorActivity.this, "User Signed up success");

                                        Intent intent = new Intent(SignUpVendorActivity.this, VerificationActivity.class);
                                        intent.putExtra(Tags.FROM, login_from);
                                        intent.putExtra(Tags.email, et_username.getText().toString());
                                        intent.putExtra(Tags.phone, et_contact.getText().toString());
                                        intent.putExtra(Tags.mobileotp, obj_json.getString(Tags.mobileotp));
                                        intent.putExtra(Tags.emailotp, obj_json.getString(Tags.emailotp));
                                        startActivity(intent);

                                    } else {
                                        AppDelegate.showToast(SignUpVendorActivity.this, obj_json.getString(Tags.message));
                                    }
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                        });
            } else {
                SingletonRestClient.get().signUp(
                        et_first_name.getText().toString(),
                        et_last_name.getText().toString(),
                        et_username.getText().toString(),
                        et_password.getText().toString(),
                        et_address.getText().toString(),
                        "05" + str_phone, AppDelegate.DEVICE_TYPE, device_token,
                        "", "",
                        et_city.getText().toString() + "",
                        et_state.getText().toString() + "",
                        "",
                        "", AppDelegate.getUUID(SignUpVendorActivity.this), new Callback<Response>() {
                            @Override
                            public void failure(RestError restError) {
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                super.failure(error);
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                    AppDelegate.checkJsonMessage(SignUpVendorActivity.this, obj_json);
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                    AppDelegate.showToast(SignUpVendorActivity.this, "");
                                }
                            }

                            @Override
                            public void success(Response response, Response response2) {
                                mHandler.sendEmptyMessage(11);
                                try {
                                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                    if (obj_json.getString(Tags.status).contains("1")) {
                                        AppDelegate.showToast(SignUpVendorActivity.this, obj_json.getString(Tags.message));
//
                                        if (LoginTutorialActivity.mActivity != null)
                                            LoginTutorialActivity.mActivity.finish();
                                        if (SignInActivity.mActivity != null)
                                            SignInActivity.mActivity.finish();

                                        Intent intent = new Intent(SignUpVendorActivity.this, VerificationActivity.class);
                                        intent.putExtra(Tags.FROM, login_from);
                                        intent.putExtra(Tags.email, et_username.getText().toString());
                                        intent.putExtra(Tags.phone, et_contact.getText().toString());
                                        intent.putExtra(Tags.mobileotp, obj_json.getString(Tags.mobileotp));
                                        intent.putExtra(Tags.emailotp, obj_json.getString(Tags.emailotp));
                                        startActivity(intent);

                                    } else {
                                        AppDelegate.showToast(SignUpVendorActivity.this, obj_json.getString(Tags.message));
                                    }
                                } catch (Exception e) {
                                    AppDelegate.LogE(e);
                                }
                            }
                        });
            }
        }
    }
}
