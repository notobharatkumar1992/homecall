package com.oncall.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.CategoryServiceListAdapter;
import com.oncall.constants.Tags;
import com.oncall.fragments.PendingRequestFragment;
import com.oncall.interfaces.PageReload;
import com.oncall.models.CategoryModel;
import com.oncall.models.RequestModel;
import com.oncall.models.ServiceModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 27-Mar-17.
 */
public class ManageJobDetailActivity extends AppCompatActivity implements View.OnClickListener, PageReload {

    public static PageReload pageReload;
    public Handler mHandler;
    public Prefs prefs;

    public TextView txt_c_titlename, txt_c_status, txt_c_action, txt_c_placed_on, txt_c_reference_no, txt_c_service, txt_c_total, txt_c_special_instruction, txt_c_name, txt_c_phone, txt_c_street, txt_c_landmark, txt_c_address;
    public RecyclerView rv_services;
    public ArrayList<ServiceModel> serviceArray = new ArrayList<>();
    public CategoryServiceListAdapter serviceListAdapter;

    public RequestModel requestModel;
    public String str_order_id = "", notification_id = "", type = "";
    public int from = MainActivity.FROM_ORDER_LIST;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_job_detail_activity);
        pageReload = this;
        prefs = new Prefs(this);
        getValues();
        initView();
        setValues();
        setHandler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                execute_orderDetailAsync(str_order_id);
            }
        }, 500);
    }

    private void getValues() {
        if (getIntent().getExtras() != null) {
            requestModel = getIntent().getExtras().getParcelable(Tags.request_model);
            str_order_id = getIntent().getExtras().getString(Tags.order_id);
            from = getIntent().getExtras().getInt(Tags.from);
            notification_id = getIntent().getExtras().getString(Tags.notification_id);
            type = getIntent().getExtras().getString(Tags.type);
        }
    }

    public String currency, currency_symbol;

    private void setValues() {
        AppDelegate.LogT("setValues called");
        if (requestModel != null) {
            if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "")) {
                txt_c_status.setText("PENDING");
                txt_c_action.setText("Start");
                txt_c_action.setVisibility(View.GONE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_ACCEPTED + "")) {
                txt_c_status.setText("ACCEPTED");
                txt_c_action.setText("Start");
                txt_c_action.setVisibility(View.VISIBLE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_STARTED + "")) {
                txt_c_status.setText("STARTED");
                txt_c_action.setText("Complete");
                txt_c_action.setVisibility(View.VISIBLE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_COMPLETED + "")) {
                txt_c_status.setText("COMPLETED");
                txt_c_action.setText("Complete");
                txt_c_action.setVisibility(View.GONE);
            } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_CANCELLED + "")) {
                txt_c_status.setText("CANCELLED");
                txt_c_action.setText("Complete");
                txt_c_action.setVisibility(View.GONE);
            }
            txt_c_reference_no.setText(requestModel.reference_id);
            txt_c_placed_on.setText(requestModel.booking_date.substring(0, requestModel.booking_date.indexOf("T")) + "   |   " + requestModel.booking_time);
            if (requestModel.categoryModel != null)
                txt_c_service.setText(requestModel.categoryModel.title);
            try {
                String currency = "";
                ArrayList<CategoryModel> currencyArrayList = new Prefs(ManageJobDetailActivity.this).getCurrencyArrayList();
                for (int i = 0; i < currencyArrayList.size(); i++) {
                    if (currencyArrayList.get(i).id == Integer.parseInt(requestModel.currency_id)) {
                        currency = currencyArrayList.get(i).symbol;
                    }
                }
                txt_c_total.setText(requestModel.price + " " + currency);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }


            txt_c_special_instruction.setText(Html.fromHtml("<b>Special Instruction: </b>" + requestModel.comment));

            txt_c_name.setText(requestModel.name);
//            if (requestModel.orderCheck == 0) {
//                txt_c_phone.setText("**************");
//                txt_c_street.setText("**************");
//                txt_c_landmark.setText("**************");
//                txt_c_address.setText("**************");
//            } else {
            txt_c_phone.setText(requestModel.phone);
            txt_c_street.setText(requestModel.city);
            txt_c_landmark.setText(requestModel.state);
            txt_c_address.setText(requestModel.address);
//            }

            serviceArray = requestModel.arrayServices;
            if (serviceArray != null) {
                AppDelegate.LogT("serviceArray => " + serviceArray.size());
                serviceListAdapter = new CategoryServiceListAdapter(this, serviceArray, null, true, requestModel.categoryModel.image);
                rv_services.setAdapter(serviceListAdapter);
            }

//            if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_PENDING + "")) {
//                ll_c_bottom.setVisibility(View.VISIBLE);
//            } else {
//                ll_c_bottom.setVisibility(View.GONE);
//            }
        }
    }

    public void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ManageJobDetailActivity.this);
                        break;

                    case 11:
                        AppDelegate.hideProgressDialog(ManageJobDetailActivity.this);
                        break;

                    case 1:
                        setValues();
                        AppDelegate.LogT("ManageJobsDetail from => " + from);
                        if (from == MainActivity.FROM_CLASS_NOTIFICATION_LIST) {
                            execute_markNotifyFromListAsync(notification_id, type);
                        } else if (from == MainActivity.FROM_PUSH_NOTIFICATION) {
                            execute_markNotifyFromNotificationAsync(str_order_id, type);
                        }
                        break;

                    case 2:
                        execute_orderDetailAsync(str_order_id);
                        break;

                }
            }
        };
    }

    private void execute_markNotifyFromNotificationAsync(String order_id, String action) {
        if (AppDelegate.haveNetworkConnection(this)) {
//            SingletonRestClient.get().marknotifyFromNotification(order_id, new Prefs(ManageJobDetailActivity.this).getUserdata().sessionid + "", action, new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    JSONObject obj_json = null;
//                    try {
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//            });
        }
    }

    private void execute_markNotifyFromListAsync(String notification_id, String action) {
        if (AppDelegate.haveNetworkConnection(this)) {
//            SingletonRestClient.get().marknotifyFromNotificationList(notification_id, new Prefs(ManageJobDetailActivity.this).getUserdata().sessionid + "", action, new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    JSONObject obj_json = null;
//                    try {
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//            });
        }
    }

    private void initView() {
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        txt_c_titlename.setText("Job Detail");
        txt_c_status = (TextView) findViewById(R.id.txt_c_status);
        txt_c_action = (TextView) findViewById(R.id.txt_c_action);
        txt_c_action.setOnClickListener(this);
        txt_c_placed_on = (TextView) findViewById(R.id.txt_c_placed_on);
        txt_c_reference_no = (TextView) findViewById(R.id.txt_c_reference_no);
        txt_c_service = (TextView) findViewById(R.id.txt_c_service);
        txt_c_total = (TextView) findViewById(R.id.txt_c_total);
        txt_c_special_instruction = (TextView) findViewById(R.id.txt_c_special_instruction);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_phone = (TextView) findViewById(R.id.txt_c_phone);
        txt_c_street = (TextView) findViewById(R.id.txt_c_street);
        txt_c_landmark = (TextView) findViewById(R.id.txt_c_landmark);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);

        findViewById(R.id.img_c_back).setOnClickListener(this);

        rv_services = (RecyclerView) findViewById(R.id.rv_services);
        rv_services.setLayoutManager(new LinearLayoutManager(this));
        rv_services.setNestedScrollingEnabled(false);
        rv_services.setHasFixedSize(true);
        rv_services.setItemAnimator(new DefaultItemAnimator());
        serviceListAdapter = new CategoryServiceListAdapter(this, serviceArray, null, true, "");
        rv_services.setAdapter(serviceListAdapter);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;

            case R.id.txt_c_action:
                if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_ACCEPTED + "")) {
                    AppDelegate.showAlert(ManageJobDetailActivity.this, "Start Job", "Are you sure you want to start this job?", "Yes", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            execute_actionRequestAsync(str_order_id, MyOrderActivity.ORDER_STARTED + "");
                        }
                    }, "No", null);
                } else if (requestModel.status.equalsIgnoreCase(MyOrderActivity.ORDER_STARTED + "")) {
                    AppDelegate.showAlert(ManageJobDetailActivity.this, "Complete Job", "Are you sure you want to complete this job?", "Yes", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            execute_actionRequestAsync(str_order_id, MyOrderActivity.ORDER_COMPLETED + "");
                        }
                    }, "No", null);
                }
                break;
        }
    }

    public void execute_actionRequestAsync(String order_id, final String action) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().actionRequest(order_id, action, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
//                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ManageJobDetailActivity.this, obj_json);
                    } catch (Exception e) {
                        mHandler.sendEmptyMessage(11);
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(ManageJobDetailActivity.this, getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            AppDelegate.showAlert(ManageJobDetailActivity.this, obj_json.getString("message"));
//                            requestModel.orderCheck = 1;
                            requestModel.status = action;
                            PendingRequestFragment.needToUpdate = true;
                            setValues();
                        } else {
                            AppDelegate.showAlert(ManageJobDetailActivity.this, obj_json.getString("message"));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void execute_orderDetailAsync(String order_id) {
        if (AppDelegate.haveNetworkConnection(this)) {
//            mHandler.sendEmptyMessage(10);
//            SingletonRestClient.get().orderDetail(prefs.getUserdata().id + "", order_id, new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.checkJsonMessage(ManageJobDetailActivity.this, obj_json);
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                        AppDelegate.showToast(ManageJobDetailActivity.this, getString(R.string.something_wrong_with_server_response));
//                    }
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    JSONObject obj_json = null;
//                    try {
//                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                        if (obj_json.getString(Tags.status).contains("1")) {
//                            JSONObject object = obj_json.getJSONObject(Tags.order);
//                            requestModel = new RequestModel();
//                            requestModel.id = object.getString(Tags.id);
//                            requestModel.category_id = object.getString(Tags.category_id);
//                            requestModel.subcategory_id = object.getString(Tags.subcategory_id);
//                            requestModel.price = object.getString(Tags.price);
//                            requestModel.currency_id = object.getString(Tags.currency_id);
//                            requestModel.user_id = object.getString(Tags.user_id);
//                            requestModel.vendor_id = object.getString(Tags.vendor_id);
//
//                            requestModel.reference_id = object.getString(Tags.reference_id);
//                            requestModel.booking_date = object.getString(Tags.booking_date);
//                            requestModel.booking_time = object.getString(Tags.booking_time);
//                            requestModel.comment = object.getString(Tags.comment);
//                            requestModel.name = object.getString(Tags.name);
//                            requestModel.phone = object.getString(Tags.phone);
//                            requestModel.email = object.getString(Tags.email);
//                            requestModel.city = object.getString(Tags.city);
//                            requestModel.state = object.getString(Tags.state);
//                            requestModel.address2 = object.getString(Tags.address_2);
//                            requestModel.address = object.getString(Tags.address);
//                            requestModel.status = object.getString(Tags.status);
//                            requestModel.created = object.getString(Tags.created);
//
//                            if (AppDelegate.isValidString(object.optString(Tags.category))) {
//                                JSONObject categoryJSONObject = object.getJSONObject(Tags.category);
//                                requestModel.categoryModel = new CategoryModel();
//                                requestModel.categoryModel.id = categoryJSONObject.getInt(Tags.id);
//                                requestModel.categoryModel.title = categoryJSONObject.getString(Tags.category);
//                                requestModel.categoryModel.image = categoryJSONObject.getString(Tags.image);
//                                requestModel.categoryModel.icon = categoryJSONObject.getString(Tags.icon);
//                            }
//
//                            if (AppDelegate.isValidString(object.optString(Tags.user))) {
//                                JSONObject customerJSONObject = object.getJSONObject(Tags.user);
//                                requestModel.customerModel = new CustomerModel();
//                                requestModel.customerModel.id = customerJSONObject.getString(Tags.id);
//                                requestModel.customerModel.role_id = customerJSONObject.getString(Tags.role_id);
//                                requestModel.customerModel.name = customerJSONObject.getString(Tags.name);
//                                requestModel.customerModel.email = customerJSONObject.getString(Tags.email);
//                                requestModel.customerModel.last_name = customerJSONObject.getString(Tags.last_name);
//                                requestModel.customerModel.profile_pic = customerJSONObject.getString(Tags.profile_pic);
//                                requestModel.customerModel.contact_no = customerJSONObject.getString(Tags.contact_no);
//                                requestModel.customerModel.address1 = customerJSONObject.getString(Tags.address1);
//                                requestModel.customerModel.currency_id = customerJSONObject.getString(Tags.currency_id);
//                                requestModel.customerModel.latitude = customerJSONObject.getString(Tags.latitude);
//                                requestModel.customerModel.longitude = customerJSONObject.getString(Tags.longitude);
//                                requestModel.customerModel.full_name = customerJSONObject.getString(Tags.full_name);
//                            }
//
//                            JSONArray array = object.getJSONArray(Tags.service);
//                            ArrayList<ServiceModel> serviceArray = new ArrayList<ServiceModel>();
//                            StringBuilder stringBuilder = new StringBuilder();
//                            for (int i = 0; i < array.length(); i++) {
//                                stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
//                                ServiceModel serviceModel = new ServiceModel();
//                                serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
//                                serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
//                                serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
//                                serviceModel.description = array.getJSONObject(i).optString(Tags.description);
//                                serviceModel.price = array.getJSONObject(i).optString(Tags.price);
//                                AppDelegate.LogT("Filling time price => " + serviceModel.price);
//                                serviceArray.add(serviceModel);
//                            }
//                            requestModel.arrayServices = serviceArray;
//
//                            mHandler.sendEmptyMessage(1);
//                        } else {
//                            AppDelegate.showAlert(ManageJobDetailActivity.this, getString(R.string.service_error_title), getString(R.string.service_error_message), "OK");
//                        }
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//            });
        } else {
            mHandler.sendEmptyMessage(11);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pageReload = null;
    }

    @Override
    public void needToReloadPage(String name, String status) {
        if (name.equalsIgnoreCase(Tags.RELOAD)) {
            execute_orderDetailAsync(str_order_id);
        }
    }
}
