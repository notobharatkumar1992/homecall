package com.oncall.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 12-Jan-17.
 */
public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_username;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.forgot_password_activity);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ForgotPasswordActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ForgotPasswordActivity.this);
                        break;

                }
            }
        };
    }

    private void initView() {
        et_username = (EditText) findViewById(R.id.et_username);
        et_username.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Medium)));
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                executeForgotPasswordAsync();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void executeForgotPasswordAsync() {
        if (et_username.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_email_enter));
        } else if (!AppDelegate.isValidEmail(et_username.getText().toString())) {
            AppDelegate.showToast(this, getString(R.string.validation_email_valid_enter));
        } else if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().forgotPassword(et_username.getText().toString(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ForgotPasswordActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(ForgotPasswordActivity.this, getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            AppDelegate.showToast(ForgotPasswordActivity.this, obj_json.getString(Tags.message));
                            onBackPressed();
                        } else {
                            AppDelegate.showToast(ForgotPasswordActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
