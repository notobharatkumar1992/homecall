package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.view.Window;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.utils.Prefs;

public class SplashActivity extends Activity {

    // Global variable declaration
    protected int mSplashTime = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (new Prefs(SplashActivity.this).getSKIP() == 0) {
                    Intent intent = new Intent(SplashActivity.this, LoginTutorialActivity.class);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SplashActivity.this, false);
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashActivity.this, pairs);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 1000);
        AppDelegate.LogGC(new Prefs(this).getGCMtoken());
        AppDelegate.getHashKey(SplashActivity.this);
        AppDelegate.sendEventTracker(this, "Application start");
    }

    // when user press the back_black button then only current thread would be interrupted
    @Override
    public void onBackPressed() {
    }

}
