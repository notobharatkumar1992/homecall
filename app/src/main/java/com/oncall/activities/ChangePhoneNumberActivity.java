package com.oncall.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.PhoneNumberTextWatcherDefault;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 06-Dec-16.
 */
public class ChangePhoneNumberActivity extends AppCompatActivity implements View.OnClickListener {

    public Handler mHandler;
    public RelativeLayout rl_c_phone_number;
    public LinearLayout ll_phone_otp;
    public TextView txt_c_send_otp;
    public EditText et_phone_otp, et_new_phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_phone_number);
        initView();
        setHandler();
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_resend_otp).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);

        txt_c_send_otp = (TextView) findViewById(R.id.txt_c_send_otp);
        txt_c_send_otp.setOnClickListener(this);

        et_phone_otp = (EditText) findViewById(R.id.et_phone_otp);
        et_phone_otp.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_new_phone = (EditText) findViewById(R.id.et_new_phone);
        et_new_phone.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_new_phone.addTextChangedListener(new PhoneNumberTextWatcherDefault(et_new_phone));

        rl_c_phone_number = (RelativeLayout) findViewById(R.id.rl_c_phone_number);
        rl_c_phone_number.setVisibility(View.VISIBLE);
        ll_phone_otp = (LinearLayout) findViewById(R.id.ll_phone_otp);
        ll_phone_otp.setVisibility(View.GONE);

        et_new_phone.setImeOptions(EditorInfo.IME_ACTION_DONE);

    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ChangePhoneNumberActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ChangePhoneNumberActivity.this);
                        break;
                }
            }
        };
    }

    public static final int SEND_OTP = 0, RESEND_OTP = 1;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                check_validation();
                break;
            case R.id.txt_c_send_otp:
                if (et_new_phone.length() == 0) {
                    AppDelegate.showToast(this, getString(R.string.validation_phone_number));
                } else
                    execute_sendOTP(SEND_OTP);
                break;
            case R.id.txt_c_resend_otp:
                if (et_new_phone.length() == 0) {
                    AppDelegate.showToast(this, getString(R.string.validation_phone_number));
                } else
                    execute_sendOTP(RESEND_OTP);
                break;

            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void check_validation() {
        if (!AppDelegate.isValidString(et_phone_otp.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.validation_enter_OTP));
        } else if (et_new_phone.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_phone_number));
        } else if (et_new_phone.length() < 10) {
            AppDelegate.showToast(this, getString(R.string.validation_phone_number_valid));
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            execute_changePhoneNumber();
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    private void execute_changePhoneNumber() {
        mHandler.sendEmptyMessage(10);
        SingletonRestClient.get().changePhoneNumber(new Prefs(this).getUserdata().id + "", new Prefs(this).getUserdata().sessionid + "", "05" + et_new_phone.getText().toString().replaceAll(" ", ""), et_phone_otp.getText().toString(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void failure(RetrofitError error) {
                super.failure(error);
                mHandler.sendEmptyMessage(11);
                try {
                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                    AppDelegate.checkJsonMessage(ChangePhoneNumberActivity.this, obj_json);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(ChangePhoneNumberActivity.this, "");
                }
            }

            @Override
            public void success(Response response, Response response2) {
                mHandler.sendEmptyMessage(11);
                try {
                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                   /* if (obj_json.optInt(Tags.session_status, 1) == 0) {
                        AppDelegate.showToast(ChangePhoneNumberActivity.this, obj_json.getString(Tags.message));
                        new Prefs(ChangePhoneNumberActivity.this).setUserData(null);
                        if (MainActivity.mActivity != null)
                            MainActivity.mActivity.finish();
                        if (SettingActivity.mActivity != null)
                            SettingActivity.mActivity.finish();
                        startActivity(new Intent(ChangePhoneNumberActivity.this, SignInActivity.class));
                        finish();
                    } else*/
                    if (obj_json.getString(Tags.status).contains("1")) {
                        AppDelegate.showToast(ChangePhoneNumberActivity.this, obj_json.getString(Tags.message));
                        onBackPressed();
                    } else {
                        AppDelegate.showAlert(ChangePhoneNumberActivity.this, obj_json.getString(Tags.message));
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        });
    }

    private void execute_sendOTP(final int fromOTP) {
        if (AppDelegate.haveNetworkConnection(this, false)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().otpForPhoneNumber(new Prefs(this).getUserdata().id + "", new Prefs(this).getUserdata().sessionid, "05" + et_new_phone.getText().toString().replaceAll(" ", ""), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ChangePhoneNumberActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(ChangePhoneNumberActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.session_status) == 0) {
                            AppDelegate.showToast(ChangePhoneNumberActivity.this, obj_json.getString(Tags.message));
                            new Prefs(ChangePhoneNumberActivity.this).setUserData(null);
                            if (MainActivity.mActivity != null)
                                MainActivity.mActivity.finish();
                            if (SettingActivity.mActivity != null)
                                SettingActivity.mActivity.finish();
                            startActivity(new Intent(ChangePhoneNumberActivity.this, SignInActivity.class));
                            finish();
                        } else if (obj_json.getString(Tags.status).contains("1")) {
                            AppDelegate.showToast(ChangePhoneNumberActivity.this, obj_json.getString(Tags.message));
                            et_phone_otp.setText(obj_json.getString(Tags.mobileotp));

                            if (fromOTP == SEND_OTP) {
                                rl_c_phone_number.setBackground(null);
                                et_new_phone.setEnabled(false);
                                et_new_phone.setFocusable(false);
                                et_new_phone.setFocusableInTouchMode(false);
                                txt_c_send_otp.setVisibility(View.GONE);
                                ll_phone_otp.setVisibility(View.VISIBLE);
                            }
                        } else {
                            AppDelegate.showAlert(ChangePhoneNumberActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }
}
