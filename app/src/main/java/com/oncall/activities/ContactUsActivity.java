package com.oncall.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import java.security.SecureRandom;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 21-Apr-17.
 */
public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener/*, ReCaptcha.OnShowChallengeListener, ReCaptcha.OnVerifyAnswerListener*/ {

    public TextView txt_c_contact, txt_c_address, txt_c_captcha;
    public EditText et_email, et_subject, et_comment;
    public Handler mHandler;

    private static final String PUBLIC_KEY = "6LcPWugSAAAAAC-MP5sg6wp_CQiyxHvPvkQvVlVf";
    private static final String PRIVATE_KEY = "6LcPWugSAAAAALWMp-gg9QkykQQyO6ePBSUk-Hjg";

    //    private ReCaptcha reCaptcha;
//    private ProgressBar progress;
    private EditText answer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us_activity);
        initView();
        setHandler();
        executeContactDetails();
        generateRandom(6);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ContactUsActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ContactUsActivity.this);
                        break;
                }
            }
        };
    }

    private void initView() {
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_contact = (TextView) findViewById(R.id.txt_c_contact);
        txt_c_contact.setOnClickListener(this);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        txt_c_captcha = (TextView) findViewById(R.id.txt_c_captcha);
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_subject = (EditText) findViewById(R.id.et_subject);
        et_subject.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_comment = (EditText) findViewById(R.id.et_comment);
        et_comment.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        if (new Prefs(this).getUserdata() != null && AppDelegate.isValidString(new Prefs(this).getUserdata().email)) {
            et_email.setText(new Prefs(this).getUserdata().email);
        }
//        this.reCaptcha = (ReCaptcha) this.findViewById(R.id.recaptcha);
//        this.progress = (ProgressBar) this.findViewById(R.id.progress);
        this.answer = (EditText) this.findViewById(R.id.answer);
        this.answer.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        this.findViewById(R.id.verify).setOnClickListener(this);
        this.findViewById(R.id.reload).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_c_submit:
                AppDelegate.sendEventTracker(this, "Contact us executed");
                executeContactUs();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_contact:
                AppDelegate.callNumber(ContactUsActivity.this, txt_c_contact.getText().toString());
                break;

            case R.id.verify:
                this.verifyAnswer();
                break;

            case R.id.reload:
                verified = false;
                generateRandom(6);
                break;
        }
    }

    private void executeContactUs() {
        if (et_email.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_email_enter));
        } else if (!AppDelegate.isValidEmail(et_email.getText().toString())) {
            AppDelegate.showToast(this, getString(R.string.validation_email_valid_enter));
        } else if (et_subject.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_subject));
        } else if (et_comment.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_comment));
        } else if (!verifyAnswer()) {
            AppDelegate.showToast(this, getString(R.string.validation_CAPTCHA));
        } else if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().contactUs(et_email.getText().toString(), et_subject.getText().toString(), et_comment.getText().toString(), new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            AppDelegate.showToast(ContactUsActivity.this, obj_json.getString(Tags.message));
                            onBackPressed();
                        } else {
                            AppDelegate.showToast(ContactUsActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ContactUsActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executeContactDetails() {
        if (AppDelegate.haveNetworkConnection(this)) {
            AppDelegate.showProgressDialog(ContactUsActivity.this);
            SingletonRestClient.get().contactDetail(new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    AppDelegate.hideProgressDialog(ContactUsActivity.this);
                    try {
                        if (error == null)
                            return;
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ContactUsActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(ContactUsActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        AppDelegate.hideProgressDialog(ContactUsActivity.this);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        txt_c_contact.setText(Html.fromHtml(obj_json.getString(Tags.phone)));
                        try {
                            String s = txt_c_contact.getText().toString();
                            txt_c_contact.setText(String.format("%s %s %s", s.subSequence(0, 3), s.subSequence(3, 6), s.subSequence(6, s.length())));
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                        txt_c_address.setText(Html.fromHtml(obj_json.getString(Tags.address)));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

//
//    @Override
//    public void onChallengeShown(final boolean shown) {
//        this.progress.setVisibility(View.GONE);
//        if (shown) {
//            // If a CAPTCHA is shown successfully, displays it for the user to enter the words
//            this.reCaptcha.setVisibility(View.VISIBLE);
//        } else {
//            Toast.makeText(this, R.string.show_error, Toast.LENGTH_SHORT).show();
//        }
//    }

    boolean verified = false;

//    @Override
//    public void onAnswerVerified(final boolean success) {
//        if (success) {
//            Toast.makeText(this, R.string.verification_success, Toast.LENGTH_SHORT).show();
//            verified = true;
//            this.answer.setEnabled(false);
//        } else {
//            verified = false;
//            Toast.makeText(this, R.string.verification_failed, Toast.LENGTH_SHORT).show();
//            // (Optional) Shows the next CAPTCHA
////            this.showChallenge();
//            generateRandom(6);
//        }
//    }


    private boolean verifyAnswer() {
        if (TextUtils.isEmpty(this.answer.getText())) {
            Toast.makeText(this, R.string.instruction, Toast.LENGTH_SHORT).show();
        } else {
            // Displays a progress bar while submitting the answer for verification
//            this.progress.setVisibility(View.VISIBLE);
//            this.reCaptcha.verifyAnswerAsync(PRIVATE_KEY, this.answer.getText().toString(), this);
            if (answer.getText().toString().equals(txt_c_captcha.getText().toString())) {
                verified = true;
                Toast.makeText(this, R.string.verification_success, Toast.LENGTH_SHORT).show();
                this.answer.setEnabled(false);
            } else {
                verified = false;
                Toast.makeText(this, R.string.verification_failed, Toast.LENGTH_SHORT).show();
                generateRandom(6);
            }
        }
        return verified;
    }

    public String generatedCode = "C123DS";

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public void generateRandom(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        txt_c_captcha.setText(sb.toString());
        generatedCode = sb.toString();
        answer.setEnabled(true);
        answer.setText("");
        AppDelegate.LogE("generateRandom => " + sb.toString());
    }

}
