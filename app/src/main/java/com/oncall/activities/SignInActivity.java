package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.constants.Tags;
import com.oncall.models.FbDetails;
import com.oncall.models.Fb_detail_GetSet;
import com.oncall.models.UserDataModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.parser.JSONParser;
import com.oncall.utils.Prefs;

import org.json.JSONObject;

import java.util.Arrays;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 12-Jan-17.
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private Handler mHandler;
    public static Activity mActivity;
    public EditText et_username, et_password;

    public CallbackManager callbackManager;
    private boolean isCalledOnce = false;
    private Fb_detail_GetSet fbUserData;
    private String fb_LoginToken;
    private Bundle bundle;
    String login_from = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.signin_activity);
        callbackManager = CallbackManager.Factory.create();
        login_from = getIntent().getStringExtra(Tags.FROM);
        AppDelegate.session = "";
        mActivity = this;
        initView();
        setHandler();
        googleClient();
    }

    public GoogleApiClient mGoogleApiClient;
    public static final int RC_SIGN_IN = 500;

    public void googleClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SignInActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SignInActivity.this);
                        break;
                }
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
        try {
            signOut();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView() {
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        et_username.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Medium)));
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Medium)));
        findViewById(R.id.txt_c_forgot_password).setOnClickListener(this);
        findViewById(R.id.txt_c_sign_up).setOnClickListener(this);
        findViewById(R.id.txt_c_sign_in).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);

        findViewById(R.id.img_c_fb).setOnClickListener(this);
        findViewById(R.id.img_c_google).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;

            case R.id.txt_c_forgot_password:
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SignInActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_forgot_password), getString(R.string.title_name)),
                        new Pair<>(findViewById(R.id.txt_c_sign_in), getString(R.string.apply_button)),
                        new Pair<>(et_username, getString(R.string.met_username)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SignInActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.txt_c_sign_up:
                intent = new Intent(SignInActivity.this, SignUpVendorActivity.class);
                intent.putExtra(Tags.FROM, login_from);
                pairs = TransitionHelper.createSafeTransitionParticipants(SignInActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SignInActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.txt_c_sign_in:
                executeSignInAsync();
                break;

            case R.id.img_c_fb:
                openFacebook();
                break;

            case R.id.img_c_google:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }

    private void executeSignInAsync() {
        if (et_username.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_email_enter));
        } else if (!AppDelegate.isValidEmail(et_username.getText().toString())) {
            AppDelegate.showToast(this, getString(R.string.validation_email_valid_enter));
        } else if (et_password.length() == 0) {
            AppDelegate.showToast(this, getString(R.string.validation_password_enter));
        } else if (!AppDelegate.isLegalPassword(et_password.getText().toString())) {
            AppDelegate.showToast(this, getString(R.string.validation_password_valid_enter));
        } else if (AppDelegate.haveNetworkConnection(this)) {
            String device_token = new Prefs(SignInActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().userLogin(et_username.getText().toString(), et_password.getText().toString(), AppDelegate.DEVICE_TYPE + "", device_token, AppDelegate.getUUID(SignInActivity.this), new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        AppDelegate.sendEventTracker(SignInActivity.this, "User Signed In");
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        int value = -1;
                        try {
                            value = obj_json.getInt(Tags.is_verify);
                        } catch (Exception e) {
                            value = -1;
                        }

                        if (obj_json.getInt(Tags.status) == 1) {
                            AppDelegate.showToast(SignInActivity.this, obj_json.getString(Tags.message));
                            JSONObject object = obj_json.getJSONObject(Tags.user);
                            UserDataModel userDataModel = new UserDataModel();
                            userDataModel.id = JSONParser.getInt(object, Tags.id);
                            userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                            userDataModel.email = JSONParser.getString(object, Tags.email);
                            userDataModel.first_name = JSONParser.getString(object, Tags.name);
                            userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                            userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                            userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                            userDataModel.address = JSONParser.getString(object, Tags.address1);
                            userDataModel.latitude = JSONParser.getString(object, Tags.latitude);
                            userDataModel.longitude = JSONParser.getString(object, Tags.longitude);
                            userDataModel.address2 = JSONParser.getString(object, Tags.address2);
                            userDataModel.city = JSONParser.getString(object, Tags.city);
                            userDataModel.city2 = JSONParser.getString(object, Tags.city_2);
                            userDataModel.state = JSONParser.getString(object, Tags.state);
                            userDataModel.country = JSONParser.getString(object, Tags.country);
                            userDataModel.zip = JSONParser.getString(object, Tags.zip);
                            userDataModel.token = JSONParser.getString(object, Tags.token);
                            userDataModel.sessionid = JSONParser.getString(object, Tags.sessionid);

                            new Prefs(SignInActivity.this).setUserData(userDataModel);
                            if (!AppDelegate.isValidString(login_from))
                                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                            finishAllInitialActivities();
                            finish();
                        } else if (value == 0) {
                            Intent intent = new Intent(SignInActivity.this, VerificationActivity.class);
                            intent.putExtra(Tags.FROM, login_from);
                            intent.putExtra(Tags.email, et_username.getText().toString());
                            intent.putExtra(Tags.mobileotp, obj_json.optString(Tags.mobileotp));
                            intent.putExtra(Tags.emailotp, obj_json.optString(Tags.emailotp));
                            startActivity(intent);
                        } else {
                            AppDelegate.showToast(SignInActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
//                    mHandler.sendEmptyMessage(11);
//                    AppDelegate.LogT("restError = " + restError.getcode() + ", " + restError.getstrMessage());
//                    AppDelegate.showToast(SignInActivity.this, restError.getstrMessage());
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.LogT("restError = " + new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())));
                        AppDelegate.checkJsonMessage(SignInActivity.this, obj_json);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    public static void finishAllInitialActivities() {
        if (LoginTutorialActivity.mActivity != null)
            LoginTutorialActivity.mActivity.finish();
        if (SignUpVendorActivity.mActivity != null)
            SignUpVendorActivity.mActivity.finish();
    }

    public void openFacebook() {
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("login success" + loginResult.getAccessToken() + "");
                        AppDelegate.LogT("onSuccess = " + loginResult.getAccessToken() + "");
                        AppDelegate.showProgressDialog(SignInActivity.this);
                        fb_LoginToken = loginResult.getAccessToken().toString();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        if (response != null) {
                                            fbUserData = new Fb_detail_GetSet();
                                            fbUserData = new FbDetails().getFacebookDetail(response.getJSONObject().toString());
                                            AppDelegate.LogT("Facebook details==" + fbUserData + "");
                                            AppDelegate.hideProgressDialog(SignInActivity.this);
                                            new Prefs(SignInActivity.this).putStringValueinTemp(Tags.social_id, fbUserData.getSocial_id());
                                            executeFbLogin(fbUserData);
//                                            callAsyncFacebookVerify(fbUserData);
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,id,name,gender,birthday,picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");

                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(SignInActivity.this);
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook();
                                }
                            }
                        }
                    }
                });
    }

    private void executeFbLogin(final Fb_detail_GetSet mFbDetails) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            String device_token = new Prefs(SignInActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            SingletonRestClient.get().checkSocialLogin(AppDelegate.isValidString(mFbDetails.emailid) ? mFbDetails.emailid : "", mFbDetails.getSocial_id(), AppDelegate.FROM_FACEBOOK + "", AppDelegate.DEVICE_TYPE + "", device_token, AppDelegate.getUUID(SignInActivity.this), new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            AppDelegate.sendEventTracker(SignInActivity.this, "User Signed In via - Facebook");

                            AppDelegate.showToast(SignInActivity.this, obj_json.getString(Tags.message));
                            JSONObject object = obj_json.getJSONObject(Tags.response);
                            UserDataModel userDataModel = new UserDataModel();
                            userDataModel.id = JSONParser.getInt(object, Tags.id);
                            userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                            userDataModel.email = JSONParser.getString(object, Tags.email);
                            userDataModel.first_name = JSONParser.getString(object, Tags.name);
                            userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                            userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                            userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                            userDataModel.address = JSONParser.getString(object, Tags.address1);
                            userDataModel.latitude = JSONParser.getString(object, Tags.latitude);
                            userDataModel.longitude = JSONParser.getString(object, Tags.longitude);
                            userDataModel.address2 = JSONParser.getString(object, Tags.address2);
                            userDataModel.city = JSONParser.getString(object, Tags.city);
                            userDataModel.city2 = JSONParser.getString(object, Tags.city_2);
                            userDataModel.state = JSONParser.getString(object, Tags.state);
                            userDataModel.country = JSONParser.getString(object, Tags.country);
                            userDataModel.zip = JSONParser.getString(object, Tags.zip);
                            userDataModel.token = JSONParser.getString(object, Tags.token);
                            userDataModel.sessionid = JSONParser.getString(object, Tags.sessionid);

                            new Prefs(SignInActivity.this).setUserData(userDataModel);
                            if (!AppDelegate.isValidString(login_from))
                                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                            finishAllInitialActivities();
                            finish();
                        } else {
                            UserDataModel userDataModel = new UserDataModel();
                            userDataModel.social_id = mFbDetails.social_id;
                            userDataModel.first_name = mFbDetails.firstname;
                            userDataModel.last_name = mFbDetails.last_name;
                            userDataModel.name = mFbDetails.name;
                            userDataModel.email = mFbDetails.emailid;
                            userDataModel.gender = mFbDetails.gender;
                            userDataModel.birthday = mFbDetails.bitrhday;
                            userDataModel.image = mFbDetails.profile_pic;
                            userDataModel.login_type = AppDelegate.LOGIN_FACEBOOK;

                            Bundle bundle = new Bundle();
                            bundle.putParcelable(Tags.user, userDataModel);
                            AppDelegate.showToast(SignInActivity.this, obj_json.getString(Tags.message));
                            Intent intent = new Intent(SignInActivity.this, SignUpVendorActivity.class);
                            intent.putExtra(Tags.FROM, login_from);
                            intent.putExtras(bundle);
                            Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SignInActivity.this, false,
                                    new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name)));
                            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SignInActivity.this, pairs);
                            startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(SignInActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executeGoogleLogin(final GoogleSignInAccount mFbDetails) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            String device_token = new Prefs(SignInActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            SingletonRestClient.get().checkSocialLogin(AppDelegate.isValidString(mFbDetails.getEmail()) ? mFbDetails.getEmail() : "",
                    mFbDetails.getId(), AppDelegate.FROM_GOOGLE + "", AppDelegate.DEVICE_TYPE + "", device_token, AppDelegate.getUUID(SignInActivity.this), new Callback<Response>() {

                        @Override
                        public void success(Response response, Response response2) {
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                                if (obj_json.getInt(Tags.status) == 1) {
                                    AppDelegate.sendEventTracker(SignInActivity.this, "User Signed In via - Google");

                                    AppDelegate.showToast(SignInActivity.this, obj_json.getString(Tags.message));
                                    JSONObject object = obj_json.getJSONObject(Tags.response);
                                    UserDataModel userDataModel = new UserDataModel();
                                    userDataModel.id = JSONParser.getInt(object, Tags.id);
                                    userDataModel.role_id = JSONParser.getInt(object, Tags.role_id);
                                    userDataModel.email = JSONParser.getString(object, Tags.email);
                                    userDataModel.first_name = JSONParser.getString(object, Tags.name);
                                    userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
                                    userDataModel.image = JSONParser.getString(object, Tags.profile_pic);
                                    userDataModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                                    userDataModel.address = JSONParser.getString(object, Tags.address1);
                                    userDataModel.latitude = JSONParser.getString(object, Tags.latitude);
                                    userDataModel.longitude = JSONParser.getString(object, Tags.longitude);
                                    userDataModel.address2 = JSONParser.getString(object, Tags.address2);
                                    userDataModel.city = JSONParser.getString(object, Tags.city);
                                    userDataModel.city2 = JSONParser.getString(object, Tags.city_2);
                                    userDataModel.state = JSONParser.getString(object, Tags.state);
                                    userDataModel.country = JSONParser.getString(object, Tags.country);
                                    userDataModel.zip = JSONParser.getString(object, Tags.zip);
                                    userDataModel.token = JSONParser.getString(object, Tags.token);
                                    userDataModel.sessionid = JSONParser.getString(object, Tags.sessionid);

                                    new Prefs(SignInActivity.this).setUserData(userDataModel);
                                    if (!AppDelegate.isValidString(login_from))
                                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                    finishAllInitialActivities();
                                    finish();
                                } else {
                                    UserDataModel userDataModel = new UserDataModel();
                                    userDataModel.social_id = mFbDetails.getId();
                                    userDataModel.first_name = mFbDetails.getDisplayName();
                                    userDataModel.last_name = "";
                                    userDataModel.name = mFbDetails.getDisplayName();
                                    userDataModel.email = mFbDetails.getEmail();
                                    userDataModel.gender = "";
                                    userDataModel.birthday = "";
                                    userDataModel.image = mFbDetails.getPhotoUrl() + "";
                                    userDataModel.login_type = AppDelegate.LOGIN_GOOGLE;

                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable(Tags.user, userDataModel);
                                    AppDelegate.showToast(SignInActivity.this, obj_json.getString(Tags.message));
                                    Intent intent = new Intent(SignInActivity.this, SignUpVendorActivity.class);
                                    intent.putExtra(Tags.FROM, login_from);
                                    intent.putExtras(bundle);
                                    Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SignInActivity.this, false,
                                            new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name)));
                                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SignInActivity.this, pairs);
                                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                                }
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }

                        @Override
                        public void failure(RestError restError) {
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(SignInActivity.this, obj_json);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("LoginActivity onActivityResult called");
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();
                Log.d("Google", "handleSignInResult:" + acct.getPhotoUrl());
                executeGoogleLogin(acct);
            } else {
                GoogleSignInAccount acct = result.getSignInAccount();
                Log.d("Google", "else handleSignInResult:" + acct);
            }
        } else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    // [START signOut]
    private void signOut() {
        try {
            if (mGoogleApiClient != null)
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                AppDelegate.LogT("signOut onResult => " + status);
                            }
                        });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
//                        updateUI(false);
                    }
                });
    }
    // [END revokeAccess]

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("Google", "onConnectionFailed:" + connectionResult);
    }
}
