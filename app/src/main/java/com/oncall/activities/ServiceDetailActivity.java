package com.oncall.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.models.ServiceModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 25-Apr-17.
 */
public class ServiceDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public LinearLayout ll_terms, ll_how_work, ll_cancellation_policy;
    public TextView txt_c_titlename, txt_c_currency, txt_c_service, txt_c_description, txt_c_terms, txt_c_how_work, txt_c_cancellation_policy;
    public ImageView img_c_category;

    public String image_url;
    public ServiceModel serviceModel;

    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_detail);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        initView();
        setValues();
    }

    private void setValues() {
        if (getIntent().getExtras() != null && getIntent().getExtras().getParcelable(Tags.service) != null) {
            serviceModel = getIntent().getExtras().getParcelable(Tags.service);
            AppDelegate.LogT("serviceModel => " + serviceModel.toString());
            image_url = serviceModel.image;
            txt_c_titlename.setText(serviceModel.sub_category);
            txt_c_currency.setText(" AED " + serviceModel.price);
            txt_c_service.setText(serviceModel.sub_category);
            txt_c_description.setText(Html.fromHtml(serviceModel.description));
            if (AppDelegate.isValidString(image_url))
                imageLoader.displayImage(image_url, img_c_category, options);
            if (AppDelegate.isValidString(serviceModel.terms))
                txt_c_terms.setText(Html.fromHtml(serviceModel.terms));
            else ll_terms.setVisibility(View.GONE);
            if (AppDelegate.isValidString(serviceModel.how_works))
                txt_c_how_work.setText(Html.fromHtml(serviceModel.how_works));
            else ll_how_work.setVisibility(View.GONE);
            if (AppDelegate.isValidString(serviceModel.cancellation_policy))
                txt_c_cancellation_policy.setText(Html.fromHtml(serviceModel.cancellation_policy));
            else ll_cancellation_policy.setVisibility(View.GONE);
        }
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        txt_c_currency = (TextView) findViewById(R.id.txt_c_currency);
        txt_c_service = (TextView) findViewById(R.id.txt_c_service);
        txt_c_description = (TextView) findViewById(R.id.txt_c_description);
        txt_c_terms = (TextView) findViewById(R.id.txt_c_terms);
        txt_c_how_work = (TextView) findViewById(R.id.txt_c_how_work);
        txt_c_cancellation_policy = (TextView) findViewById(R.id.txt_c_cancellation_policy);

        img_c_category = (ImageView) findViewById(R.id.img_c_category);

        ll_terms = (LinearLayout) findViewById(R.id.ll_terms);
        ll_how_work = (LinearLayout) findViewById(R.id.ll_how_work);
        ll_cancellation_policy = (LinearLayout) findViewById(R.id.ll_cancellation_policy);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }


}
