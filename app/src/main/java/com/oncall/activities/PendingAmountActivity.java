package com.oncall.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.adapters.PendingListAdapter;
import com.oncall.constants.Tags;
import com.oncall.models.PendingAmountModel;
import com.oncall.utils.Prefs;

import java.util.ArrayList;

/**
 * Created by Bharat on 11-May-17.
 */
public class PendingAmountActivity extends AppCompatActivity implements View.OnClickListener {

    public Handler mHandler;
    public Prefs prefs;

    public RecyclerView rv_pending_amount;
    public ArrayList<PendingAmountModel> arrayPendingAmount = new ArrayList<>();
    public PendingListAdapter pendingListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_amount_acitivity);
        prefs = new Prefs(this);
        arrayPendingAmount = getIntent().getExtras().getParcelableArrayList(Tags.amount);
        initView();
        setHandler();
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        rv_pending_amount = (RecyclerView) findViewById(R.id.rv_pending_amount);
        rv_pending_amount.setLayoutManager(new LinearLayoutManager(this));
        rv_pending_amount.setNestedScrollingEnabled(false);
        rv_pending_amount.setHasFixedSize(true);
        rv_pending_amount.setItemAnimator(new DefaultItemAnimator());

        if (arrayPendingAmount != null && arrayPendingAmount.size() > 0) {
            pendingListAdapter = new PendingListAdapter(this, arrayPendingAmount, null);
            rv_pending_amount.setAdapter(pendingListAdapter);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(PendingAmountActivity.this);
                        break;

                    case 11:
                        AppDelegate.hideProgressDialog(PendingAmountActivity.this);
                        break;

                }
            }
        };
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

}
