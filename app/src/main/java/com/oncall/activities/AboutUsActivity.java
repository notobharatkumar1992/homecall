package com.oncall.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.constants.Tags;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;

import org.json.JSONObject;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 04-May-17.
 */

public class AboutUsActivity extends AppCompatActivity {

    private TextView txt_c_description;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us_detail);
        initView();
        if (getIntent().getIntExtra("type", 0) == 0) {
            ((TextView) findViewById(R.id.txt_c_titlename)).setText("About US");
            executeAboutUs();
        } else if (getIntent().getIntExtra("type", 0) == 1) {
            ((TextView) findViewById(R.id.txt_c_titlename)).setText("Help");
            executeHelp();
        } else if (getIntent().getIntExtra("type", 0) == 2) {
            ((TextView) findViewById(R.id.txt_c_titlename)).setText("Privacy Policy");
            executePrivacyPolicy();
        } else if (getIntent().getIntExtra("type", 0) == 3) {
            ((TextView) findViewById(R.id.txt_c_titlename)).setText("Terms & Conditions");
            executeTermsConditions();
        }
    }


    private void initView() {
        txt_c_description = (TextView) findViewById(R.id.txt_c_description);
        findViewById(R.id.img_c_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void executeAboutUs() {
        if (AppDelegate.haveNetworkConnection(this)) {
            AppDelegate.showProgressDialog(AboutUsActivity.this);
            SingletonRestClient.get().aboutUs(new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    AppDelegate.hideProgressDialog(AboutUsActivity.this);
                    try {
                        if (error == null)
                            return;
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(AboutUsActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(AboutUsActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        AppDelegate.hideProgressDialog(AboutUsActivity.this);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            JSONObject jsonObject = obj_json.getJSONObject(Tags.response);
                            txt_c_description.setText(Html.fromHtml(jsonObject.getString(Tags.content)));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executeHelp() {
        if (AppDelegate.haveNetworkConnection(this)) {
            AppDelegate.showProgressDialog(AboutUsActivity.this);
            SingletonRestClient.get().help(new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    AppDelegate.hideProgressDialog(AboutUsActivity.this);
                    try {
                        if (error == null)
                            return;
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(AboutUsActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(AboutUsActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        AppDelegate.hideProgressDialog(AboutUsActivity.this);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            JSONObject jsonObject = obj_json.getJSONObject(Tags.response);
                            txt_c_description.setText(Html.fromHtml(jsonObject.getString(Tags.content)));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executePrivacyPolicy() {
        if (AppDelegate.haveNetworkConnection(this)) {
            AppDelegate.showProgressDialog(AboutUsActivity.this);
            SingletonRestClient.get().privacyPolicy(new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    AppDelegate.hideProgressDialog(AboutUsActivity.this);
                    try {
                        if (error == null)
                            return;
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(AboutUsActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(AboutUsActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        AppDelegate.hideProgressDialog(AboutUsActivity.this);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            JSONObject jsonObject = obj_json.getJSONObject(Tags.response);
                            txt_c_description.setText(Html.fromHtml(jsonObject.getString(Tags.content)));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void executeTermsConditions() {
        if (AppDelegate.haveNetworkConnection(this)) {
            AppDelegate.showProgressDialog(AboutUsActivity.this);
            SingletonRestClient.get().termsCondition(new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    AppDelegate.hideProgressDialog(AboutUsActivity.this);
                    try {
                        if (error == null)
                            return;
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(AboutUsActivity.this, obj_json);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(AboutUsActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        AppDelegate.hideProgressDialog(AboutUsActivity.this);
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            JSONObject jsonObject = obj_json.getJSONObject(Tags.response);
                            txt_c_description.setText(Html.fromHtml(jsonObject.getString(Tags.content)));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }
}
