package com.oncall.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.oncall.AppDelegate;
import com.oncall.Async.LocationAddress;
import com.oncall.Async.PlacesService;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.adapters.LocationMapAdapter;
import com.oncall.constants.Tags;
import com.oncall.expandablelayout.ExpandableRelativeLayout;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.AddressModel;
import com.oncall.models.BookOrderModel;
import com.oncall.models.Place;
import com.oncall.models.UserDataModel;
import com.oncall.utils.PhoneNumberTextWatcherDefault;
import com.oncall.utils.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ProgressBar;
import carbon.widget.TextView;

/**
 * Created by HEENA on 12-Jan-17.
 */
public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    public static Activity mActivity;
    private RelativeLayout rl_c_additional_address;
    private EditText et_contact, et_name, et_address, et_address_2, et_city, et_city_2, et_state, et_zip_code;
    private Handler mHandler;
    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;

    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    public static final int COLLAPSE = 0, EXPAND = 1;
    Prefs prefs;
    private double currentLatitude = 0, currentLongitude = 0;
    private boolean canSearch = false;
    public UserDataModel userDataModel;
    public int booking_from;
    public String category_id = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_address);
        prefs = new Prefs(AddAddressActivity.this);
        mActivity = this;
        userDataModel = prefs.getUserdata();
        booking_from = getIntent().getIntExtra(Tags.FROM, MainActivity.BOOKING_FROM_CATEGORIES);
        category_id = getIntent().getStringExtra(Tags.category_id);
        initView();
        setHandler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setValues();
            }
        }, 300);

    }

    private void setValues() {
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.id + "")) {
            try {
                String value = userDataModel.contact_no.replaceAll(" ", "");
                value = String.format("%s %s %s", value.subSequence(0, 3), value.subSequence(3, 6), value.subSequence(6, value.length()));
                value = value.substring(2, value.length());
                et_contact.setText(AppDelegate.isValidString(value) ? value : "");
                et_contact.setEnabled(false);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }

            et_name.setText(userDataModel.first_name + " " + userDataModel.last_name);
            et_address.setText(AppDelegate.isValidString(userDataModel.address) ? userDataModel.address : "");
            et_city.setText(AppDelegate.isValidString(userDataModel.city) ? userDataModel.city : "");
            et_state.setText(AppDelegate.isValidString(userDataModel.state) ? userDataModel.state : "");
            et_zip_code.setText(AppDelegate.isValidString(userDataModel.zip) ? userDataModel.zip : "");

            if (AppDelegate.isValidString(userDataModel.address2) && AppDelegate.isValidString(userDataModel.city2)) {
                findViewById(R.id.ll_additional_address).setVisibility(View.VISIBLE);
                et_address_2.setText(AppDelegate.isValidString(userDataModel.address2) ? userDataModel.address2 : "");
                et_city_2.setText(AppDelegate.isValidString(userDataModel.city2) ? userDataModel.city2 : "");
            } else {
                findViewById(R.id.ll_additional_address).setVisibility(View.GONE);
            }

        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(AddAddressActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(AddAddressActivity.this);
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        mHandler.sendEmptyMessage(10);
                        AppDelegate.LogT("mHandler.sendEmptyMessage(3) called choose location Activity;");
                        LocationAddress.getLocationFromReverseGeocoder(et_address.getText().toString(), AddAddressActivity.this, mHandler);
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null) {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0) {
                            if (et_address.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        if (canSearch) {
                            if (et_address.length() != 0) {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            if (et_address.length() != 0) {
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 9:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_address.length() != 0) {
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;
                }
            }
        };
    }

    private void initView() {
        rl_c_additional_address = (RelativeLayout) findViewById(R.id.rl_c_additional_address);
        rl_c_additional_address.setOnClickListener(this);

        et_contact = (EditText) findViewById(R.id.et_contact);
        et_name = (EditText) findViewById(R.id.et_name);
        et_address = (EditText) findViewById(R.id.et_address);
        et_address_2 = (EditText) findViewById(R.id.et_address_2);
        et_city = (EditText) findViewById(R.id.et_city);
        et_city_2 = (EditText) findViewById(R.id.et_city_2);
        et_state = (EditText) findViewById(R.id.et_state);
        et_zip_code = (EditText) findViewById(R.id.et_zip_code);

        et_contact.addTextChangedListener(new PhoneNumberTextWatcherDefault(et_contact));

        et_contact.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_name.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_address.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_address_2.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_city.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_city_2.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_state.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_zip_code.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));

        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);

//        addtextWatcher();

        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(this, mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude = " + currentLatitude + ", longitude = " + currentLongitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            try {
                currentLatitude = data.getDouble(Tags.latitude);
                currentLongitude = data.getDouble(Tags.longitude);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
        }

    }

    private void executeSearchServiceAsync() {
        if (!AppDelegate.isValidString(et_name.getText().toString())) {
            AppDelegate.showToast(this, "Please enter name");
        } else if (!AppDelegate.isValidString(et_contact.getText().toString())) {
            AppDelegate.showToast(this, "Please enter contact number");
        } else if (et_contact.length() != 10) {
            AppDelegate.showToast(this, getString(R.string.validation_phone_number_valid));
        } else if (!AppDelegate.isValidString(et_address.getText().toString())) {
            AppDelegate.showToast(this, "Please enter address line 1.");
        } else if (!AppDelegate.isValidString(et_city.getText().toString())) {
            AppDelegate.showToast(this, "Please enter city.");
        } else if (rl_c_additional_address.getVisibility() == View.GONE && !AppDelegate.isValidString(et_address_2.getText().toString())) {
            AppDelegate.showToast(this, "Please enter additional address.");
        } else if (rl_c_additional_address.getVisibility() == View.GONE && !AppDelegate.isValidString(et_city_2.getText().toString())) {
            AppDelegate.showToast(this, "Please enter additional address city.");
        } else if (AppDelegate.haveNetworkConnection(this)) {
            AddressModel addressModel = new AddressModel();
            addressModel.name = et_name.getText().toString();
            addressModel.contact_no = "05" + et_contact.getText().toString().trim().replaceAll(" ", "");
            addressModel.address = et_address.getText().toString();
            addressModel.address_2 = et_address_2.getText().toString();
            addressModel.city = et_city.getText().toString();
            addressModel.city2 = et_city_2.getText().toString();
            addressModel.state = et_state.getText().toString();
            addressModel.zip_code = et_zip_code.getText().toString();

            Bundle bundle = getIntent().getExtras();
            BookOrderModel bookOrderModel = bundle.getParcelable(Tags.book_order);
            bookOrderModel.addressModel = addressModel;

            Intent intent = new Intent(this, OrderSummaryActivity.class);
            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(AddAddressActivity.this, false,
                    new Pair<>(findViewById(R.id.txt_c_titlename), "title"));
            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(AddAddressActivity.this, pairs);
            bundle.putParcelable(Tags.book_order, bookOrderModel);
            intent.putExtras(bundle);
            startActivity(intent/*, transitionActivityOptions.toBundle()*/);

//            } else {
//                mHandler.sendEmptyMessage(10);
//                SingletonRestClient.get().checkLocation(new Prefs(this).getUserdata().id + "", currentLatitude + "", currentLongitude + "", category_id, new Callback<Response>() {
//                    @Override
//                    public void failure(RestError restError) {
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        super.failure(error);
//                        mHandler.sendEmptyMessage(11);
//                        try {
//                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                            AppDelegate.checkJsonMessage(AddAddressActivity.this, obj_json);
//                        } catch (Exception e) {
//                            AppDelegate.LogE(e);
//                            AppDelegate.showToast(AddAddressActivity.this, getString(R.string.something_wrong_with_server_response));
//                        }
//                    }
//
//                    @Override
//                    public void success(Response response, Response response2) {
//                        mHandler.sendEmptyMessage(11);
//                        JSONObject obj_json = null;
//                        try {
//                            obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                            if (obj_json.getString(Tags.status).contains("1")) {
//                                AddressModel addressModel = new AddressModel();
//                                addressModel.name = et_name.getText().toString();
//                                addressModel.contact_no = et_contact.getText().toString();
//                                addressModel.address = et_address.getText().toString();
//                                addressModel.address_2 = et_address_2.getText().toString();
//                                addressModel.city = et_city.getText().toString();
//                                addressModel.state = et_state.getText().toString();
//                                addressModel.zip_code = et_zip_code.getText().toString();
//                                new Prefs(AddAddressActivity.this).setAddressData(addressModel);
//                                onBackPressed();
//                            } else {
//                                AppDelegate.showAlert(AddAddressActivity.this, getString(R.string.service_error_title), getString(R.string.service_error_message), "OK");
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_submit:
                executeSearchServiceAsync();
                break;
            case R.id.rl_c_additional_address:
                rl_c_additional_address.setVisibility(View.GONE);
                findViewById(R.id.ll_additional_address).setVisibility(View.VISIBLE);
                break;
        }
    }

    private void addtextWatcher() {
        et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AppDelegate.LogT("et_search afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(AddAddressActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(AddAddressActivity.this)) {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position) {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                } else if (AppDelegate.isValidString(placeDetail_pickup.name)) {
                    et_address.setText(placeDetail_pickup.name);
                } else if (AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_address.setText(placeDetail_pickup.vicinity);
                }
                et_address.setSelection(et_address.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(3);
            }
//            canSearch = true;
        }
    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(AddAddressActivity.this, false)) {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(AddAddressActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_address.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(et_address.getText().toString()));
                mHandler.sendEmptyMessage(4);
//                canSearch = true;
            }
        }
    };

    private void setloc(double latitude, double longitude) {
        mHandler.sendEmptyMessage(10);
        LocationAddress.getAddressFromLocation(latitude, longitude, AddAddressActivity.this, mHandler);
    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }
}
