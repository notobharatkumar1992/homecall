package com.oncall.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ScrollView;

import com.oncall.AppDelegate;
import com.oncall.R;
import com.oncall.TransitionHelper;
import com.oncall.adapters.CategoryServiceListAdapter;
import com.oncall.adapters.CurrencyAdapterSearch;
import com.oncall.constants.Tags;
import com.oncall.interfaces.OnListItemClickListener;
import com.oncall.models.CategoryModel;
import com.oncall.models.ServiceModel;
import com.oncall.models.UserDataModel;
import com.oncall.models.VendorModel;
import com.oncall.net.Callback;
import com.oncall.net.RestError;
import com.oncall.net.SingletonRestClient;
import com.oncall.parser.JSONParser;
import com.oncall.utils.Prefs;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Heena on 24-Nov-16.
 */

public class VendorSearchProfileActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    public static Activity mActivity;
    private Prefs prefs;
    public Handler mHandler;
    public EditText et_instructions;
    TextView txt_c_titlename, txt_c_currency, txt_c_vendor_name, txt_c_vendor_business_name, txt_c_vendor_contact_no, txt_c_vendor_address, txt_c_phone, txt_c_business_name, txt_c_id, txt_c_category;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private UserDataModel userDataModel;
    CurrencyAdapterSearch currencyAdapter;
    ArrayList<CategoryModel> currencyArray = new ArrayList<>();
    String currency_id = "";
    ArrayList<ServiceModel> serviceArray;
    RecyclerView rv_services;
    CategoryServiceListAdapter serviceListAdapter;
    private boolean[] is_checked;
    public String service_id = "", service = "", title_name = "";
    VendorModel vendorModel;
    public EditText et_search;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vendor_search_profile_activity);
        userDataModel = new Prefs(this).getUserdata();
        currencyArray = new Prefs(this).getCurrencyArrayList();
        vendorModel = getIntent().getParcelableExtra(Tags.vendor);
        prefs = new Prefs(this);
        mActivity = this;
        initView();
        setHandler();
        mHandler.sendEmptyMessage(5);
    }


    private void execute_VendorProfile() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getVendorServices(userDataModel.id + "", vendorModel.id + "", new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getInt(Tags.status) == 1) {
                            JSONObject object = obj_json.getJSONObject(Tags.vendorservice);
                            vendorModel = new VendorModel();
                            vendorModel.id = JSONParser.getString(object, Tags.id);
                            vendorModel.email = JSONParser.getString(object, Tags.email);
                            vendorModel.name = JSONParser.getString(object, Tags.name);
                            vendorModel.last_name = JSONParser.getString(object, Tags.last_name);
                            vendorModel.profile_pic = JSONParser.getString(object, Tags.profile_pic);
                            vendorModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                            vendorModel.address1 = JSONParser.getString(object, Tags.address1);
                            vendorModel.address2 = JSONParser.getString(object, Tags.address_2);
                            vendorModel.city = JSONParser.getString(object, Tags.city);
                            vendorModel.state = JSONParser.getString(object, Tags.state);
                            vendorModel.country = JSONParser.getString(object, Tags.country);
                            vendorModel.zip = JSONParser.getString(object, Tags.zip);
                            vendorModel.currency_id = JSONParser.getString(object, Tags.currency_id);
                            JSONObject vendorobj = object.getJSONObject(Tags.vendor);
                            vendorModel.vendor_id = JSONParser.getString(vendorobj, Tags.user_id);
                            vendorModel.member_since = JSONParser.getString(vendorobj, Tags.member_since);
                            vendorModel.membership_id = JSONParser.getString(vendorobj, Tags.membership_id);
                            vendorModel.business_name = JSONParser.getString(vendorobj, Tags.business_name);
                            vendorModel.business_subcategory_id = JSONParser.getString(vendorobj, Tags.business_subcategory_id);
                            vendorModel.business_category_id = JSONParser.getString(vendorobj, Tags.business_category_id);
                            vendorModel.latitude = JSONParser.getString(vendorobj, Tags.latitude);
                            vendorModel.longitude = JSONParser.getString(vendorobj, Tags.longitude);
                            vendorModel.payment_status = JSONParser.getString(vendorobj, Tags.payment_status);
                            vendorModel.paid_amt = JSONParser.getString(vendorobj, Tags.paid_amt);
                            vendorModel.full_name = JSONParser.getString(object, Tags.full_name);
                            vendorModel.name_email = JSONParser.getString(object, Tags.name_email);
                            vendorModel.orderCheck = JSONParser.getInt(object, Tags.orderCheck);

                            if (object.has(Tags.service)) {
                                if (AppDelegate.isValidString(object.optString(Tags.service))) {
                                    JSONArray array = object.getJSONArray(Tags.service);
                                    serviceArray = new ArrayList<ServiceModel>();
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < array.length(); i++) {
                                        stringBuilder.append(array.getJSONObject(i).getString(Tags.sub_category)).append(", ");
                                        ServiceModel serviceModel = new ServiceModel();
                                        serviceModel.category_id = array.getJSONObject(i).getString(Tags.category_id);
                                        serviceModel.sub_category_id = array.getJSONObject(i).getString(Tags.id);
                                        serviceModel.sub_category = array.getJSONObject(i).getString(Tags.sub_category);
                                        serviceModel.description = array.getJSONObject(i).optString(Tags.description);
                                        serviceModel.price = array.getJSONObject(i).optString(Tags.price);
                                        serviceArray.add(serviceModel);
                                    }
                                }
                            }
                            if (serviceArray != null && serviceArray.size() == 0) {
                                findViewById(R.id.txt_c_noservice).setVisibility(View.VISIBLE);
                            } else {
                                findViewById(R.id.txt_c_noservice).setVisibility(View.GONE);
                            }
                            if (vendorModel.orderCheck == 0) {
                                txt_c_vendor_address.setText("**************");
                                txt_c_vendor_contact_no.setText("**************");
                            } else {
                                txt_c_vendor_address.setText(vendorModel.address1);
                                txt_c_vendor_contact_no.setText(vendorModel.contact_no);
                            }
                            setValues();
                            setServieAdapter();
                        } else {
                            AppDelegate.showToast(VendorSearchProfileActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(VendorSearchProfileActivity.this, obj_json);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(VendorSearchProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(VendorSearchProfileActivity.this);
                        break;
                    case 4:
//                        setValues();
                        break;
                    case 5:
                        execute_VendorProfile();
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    private void setValues() {
        if (serviceArray != null && serviceArray.size() > 0) {
            JSONObject price = null;
            try {
                price = new JSONObject(serviceArray.get(0).price);
                AppDelegate.LogT("setValues called => " + price.toString());
                ArrayList<CategoryModel> arrayList = prefs.getCurrencyArrayList();
                String tag = "", currency_slug = "";
                for (int i = 0; i < arrayList.size(); i++) {
                    if (arrayList.get(i).checked == 1) {
                        tag = arrayList.get(i).title;
                        currency_slug = arrayList.get(i).slug;
                    }
                }
                txt_c_currency.setText(tag + "");
                new Prefs(this).putStringValue(Tags.CURRENCY, currency_slug);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void setServieAdapter() {
        if (serviceArray != null) {
            if (is_checked == null || is_checked.length != serviceArray.size()) {
                is_checked = new boolean[serviceArray.size()];
                for (int i = 0; i < serviceArray.size(); i++) {
                    if (service_id.equalsIgnoreCase(serviceArray.get(i).id + "")) {
                        is_checked[i] = true;
                    } else
                        is_checked[i] = false;
                }
            } else {
                for (int i = 0; i < is_checked.length; i++) {
                    serviceArray.get(i).checked = is_checked[i] ? 1 : 0;
                }
            }
            rv_services.setLayoutManager(new LinearLayoutManager(this));
            rv_services.setHasFixedSize(true);
            rv_services.setNestedScrollingEnabled(false);
            rv_services.setItemAnimator(new DefaultItemAnimator());
            serviceListAdapter = new CategoryServiceListAdapter(this, serviceArray, new OnListItemClickListener() {
                @Override
                public void setOnListItemClickListener(String name, int position) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, int PICK_UP) {

                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                    AppDelegate.LogT("interface value => " + like_dislike);
                    serviceArray.get(position).checked = like_dislike ? 0 : 1;
                    serviceListAdapter.notifyDataSetChanged();
                    rv_services.invalidate();
                    StringBuilder stringBuilder = null;
                    StringBuilder service_name = null;
                    for (int i = 0; i < serviceArray.size(); i++) {
                        service_id = "";
                        service = "";
                        if (serviceArray.get(i).checked == 1) {
                            is_checked[i] = true;
                            if (stringBuilder == null) {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("" + serviceArray.get(i).sub_category_id);
                            } else {
                                stringBuilder.append("," + serviceArray.get(i).sub_category_id);
                            }
                            if (service_name == null) {
                                service_name = new StringBuilder();
                                service_name.append("" + serviceArray.get(i).sub_category);
                            } else {
                                service_name.append(", " + serviceArray.get(i).sub_category);
                            }
                        } else {
                            is_checked[i] = false;
                        }
                    }
                    if (stringBuilder != null) {
                        service_id = stringBuilder.toString();

                        service = service_name.toString();
                        serviceListAdapter.notifyDataSetChanged();
                        AppDelegate.LogT("service selected =" + service);
                        AppDelegate.LogT("service selected id=" + service_id);
                    } else {
                    }
                }
            }, false, "");
            rv_services.setAdapter(serviceListAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userDataModel = new Prefs(this).getUserdata();
        mHandler.sendEmptyMessage(4);

    }

    private void initView() {
        et_instructions = (EditText) findViewById(R.id.et_instructions);
        et_instructions.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        txt_c_titlename.setText(vendorModel.full_name + "");
        txt_c_currency = (TextView) findViewById(R.id.txt_c_currency);
        txt_c_currency.setOnClickListener(this);
        txt_c_vendor_name = (TextView) findViewById(R.id.txt_c_vendor_name);
        txt_c_vendor_business_name = (TextView) findViewById(R.id.txt_c_vendor_business_name);
        txt_c_vendor_address = (TextView) findViewById(R.id.txt_c_vendor_address);
        txt_c_vendor_contact_no = (TextView) findViewById(R.id.txt_c_vendor_contact_no);
        rv_services = (RecyclerView) findViewById(R.id.rv_services);
        AppDelegate.LogT("vendorModel==>+" + vendorModel);
        txt_c_vendor_name.setText(vendorModel.full_name + "");
        if (AppDelegate.isValidString(vendorModel.business_name)) {
            txt_c_vendor_business_name.setVisibility(View.VISIBLE);
            txt_c_vendor_business_name.setText(vendorModel.business_name + "");
        } else {
            txt_c_vendor_business_name.setVisibility(View.VISIBLE);
        }
//      txt_c_vendor_address.setText(vendorModel.address1 + "");
//      txt_c_vendor_contact_no.setText(vendorModel.contact_no + "");
        findViewById(R.id.txt_c_review).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_continue).setOnClickListener(this);
        et_search = (EditText) findViewById(R.id.et_search);
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 1) {
                    ((ScrollView) (findViewById(R.id.scrollView))).smoothScrollTo(0, 200);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1) {
                    ((ScrollView) (findViewById(R.id.scrollView))).smoothScrollTo(0, 200);
                }
                serviceListAdapter.filter(et_search.getText().toString());
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                finish();
                break;
            case R.id.txt_c_review: {
                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(VendorSearchProfileActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_review), "square_blue_name"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(VendorSearchProfileActivity.this, pairs);
                Intent intent = new Intent(VendorSearchProfileActivity.this, VendorReviewsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.vendor, vendorModel);
                intent.putExtra(Tags.id, vendorModel.id);
                intent.putExtras(bundle);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
            }
            case R.id.txt_c_continue:
                if (!AppDelegate.isValidString(service_id)) {
                    AppDelegate.showToast(VendorSearchProfileActivity.this, "Please select services for booking.");
                } else {
                    ArrayList<ServiceModel> listArray = new ArrayList<>();
                    for (int i = 0; i < serviceArray.size(); i++) {
                        if (serviceArray.get(i).checked == 1)
                            listArray.add(serviceArray.get(i));
                    }
                    String str_currency_id = "";
                    for (int i = 0; i < currencyArray.size(); i++) {
                        if (currencyArray.get(i).checked == 1)
                            str_currency_id = currencyArray.get(i).id + "";
                    }

                    Intent intent = new Intent(VendorSearchProfileActivity.this, ScheduleJobActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Tags.from, MainActivity.BOOKING_FROM_VENDOR_SEARCH);
                    bundle.putParcelable(Tags.vendor_detail, vendorModel);
                    bundle.putString(Tags.service, service);
                    bundle.putString(Tags.service_id, service_id);
                    bundle.putString(Tags.currency_id, str_currency_id);
                    bundle.putString(Tags.description, et_instructions.getText().toString());
                    bundle.putParcelableArrayList(Tags.ARRAY_SERVICES, listArray);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

                break;
            case R.id.txt_c_currency:
//                if (currencyArray.size() == 0) {
//                    executeCurrencyAsync();
//                } else {
//                    currencyDialog(currencyArray);
//                }
                break;
        }
    }

//    private void executeCurrencyAsync() {
//       /* if (capturedFile == null) {
//            AppDelegate.showToast(this, "Please select an image.");
//        } else*/
//        if (AppDelegate.haveNetworkConnection(this)) {
//
//            mHandler.sendEmptyMessage(10);
//            SingletonRestClient.get().getCurrency(new Callback<Response>() {
//                @Override
//                public void failure(RestError restError) {
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    super.failure(error);
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.checkJsonMessage(VendorSearchProfileActivity.this, obj_json);
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                        AppDelegate.showToast(VendorSearchProfileActivity.this, "");
//                    }
//                }
//
//                @Override
//                public void success(Response response, Response response2) {
//                    mHandler.sendEmptyMessage(11);
//                    try {
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
//                        if (obj_json.getString(Tags.status).contains("1")) {
//                            JSONArray jsonArray = obj_json.getJSONArray(Tags.category);
//                            currencyArray.clear();
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject object = jsonArray.getJSONObject(i);
//                                CategoryModel categoryModel = new CategoryModel();
//                                categoryModel.id = object.getInt(Tags.id);
//                                categoryModel.title = object.getString(Tags.slug);
//                                categoryModel.symbol = object.getString(Tags.symbol);
//                                categoryModel.slug = object.getString(Tags.slug);
//                                currencyArray.add(categoryModel);
//                            }
//                            currencyDialog(currencyArray);
//                        } else {
//                            AppDelegate.showToast(VendorSearchProfileActivity.this, obj_json.getString(Tags.message));
//                        }
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//            });
//        }
//    }

    private RecyclerView recycler_deals;
    String currency_str = "", currencysymbbol_str = "", currencyslug_str = "";

    public void currencyDialog(final ArrayList<CategoryModel> currencyArray) {
        if (currencyArray != null) {
            final Dialog categoryDialog = new Dialog(this, android.R.style.Theme_Light);
            categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            categoryDialog.setContentView(R.layout.dialog_services);
            recycler_deals = (RecyclerView) categoryDialog.findViewById(R.id.recycler_deals);
            recycler_deals.setNestedScrollingEnabled(false);
            recycler_deals.setLayoutManager(new LinearLayoutManager(this));
            recycler_deals.setItemAnimator(new DefaultItemAnimator());

            if (currencyArray.size() > 0) {
                for (int i = 0; i < currencyArray.size(); i++) {
                    if (currencyArray.get(i).title.equalsIgnoreCase(txt_c_currency.getText().toString())) {
                        currencyArray.get(i).checked = 1;
                        break;
                    }
                }
            }

            currencyAdapter = new CurrencyAdapterSearch(this, currencyArray, new OnListItemClickListener() {
                @Override
                public void setOnListItemClickListener(String name, int position) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, int PICK_UP) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                    for (CategoryModel currency : currencyArray) {
                        AppDelegate.LogT("interface value => " + like_dislike);
                        currency.checked = 0;
                    }
                    currencyArray.get(position).checked = 1;
                    currency_id = currencyArray.get(position).id + "";
                    currency_str = currencyArray.get(position).title + "";
                    currencysymbbol_str = currencyArray.get(position).symbol + "";
                    currencyslug_str = currencyArray.get(position).slug + "";
                    currencyAdapter.notifyDataSetChanged();
                    recycler_deals.invalidate();
                    AppDelegate.LogT("currencyArray==>" + currencyArray);
                }
            });
            recycler_deals.setAdapter(currencyAdapter);

            categoryDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppDelegate.LogT("currency id => " + currency_id + "");
                    categoryDialog.dismiss();
                    txt_c_currency.setText(currencyslug_str + "");
                    for (int i = 0; i < currencyArray.size(); i++) {
                        currencyArray.get(i).checked = 0;
                    }
                    String str_currencySlug = "";
                    for (int i = 0; i < currencyArray.size(); i++) {
                        if (currencyslug_str.equalsIgnoreCase(currencyArray.get(i).slug)) {
                            currencyArray.get(i).checked = 1;
                            str_currencySlug = currencyArray.get(i).slug;
                        }
                    }
                    new Prefs(VendorSearchProfileActivity.this).putStringValue(Tags.CURRENCY, str_currencySlug);
                    prefs.setCurrencyArrayList(currencyArray);
                    serviceListAdapter.notifyDataSetChanged();
                    rv_services.invalidate();
                }
            });
            categoryDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDialog.dismiss();
                }
            });
            categoryDialog.show();
        }
    }

    private void executeChangeCurrencyAsync(final String currency_id, final String currency_str, final String currencysymbbol_str, final String currencyslug_str) {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().changeCurrency(new Prefs(this).getUserdata().id + "", currency_id, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(VendorSearchProfileActivity.this, obj_json);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(VendorSearchProfileActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.status).contains("1")) {
                            userDataModel = new Prefs(VendorSearchProfileActivity.this).getUserdata();
                            userDataModel.currency_symbol = currencysymbbol_str;
                            userDataModel.currency = currency_str;
                            userDataModel.currency_id = currency_id;
                            userDataModel.currency_slug = currencyslug_str;
                            prefs.setUserData(userDataModel);
                            serviceListAdapter.notifyDataSetChanged();
                        } else {
                            AppDelegate.showToast(VendorSearchProfileActivity.this, obj_json.getString(Tags.message));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }

    }

    @Override
    public void setOnListItemClickListener(String name, int position) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }
}
