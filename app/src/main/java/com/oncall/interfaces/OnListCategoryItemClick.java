package com.oncall.interfaces;

import android.view.View;

import com.oncall.models.VendorModel;

/**
 * Created by Heena on 25-Nov-16.
 */

public interface OnListCategoryItemClick {

    public void setOnListItemClickListener(String name, int position, VendorModel categoryModel, View view);
}
