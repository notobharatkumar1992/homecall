package com.oncall.interfaces;

public interface PageReload {

    public abstract void needToReloadPage(String name, String status);
}