package com.oncall.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.oncall.AppDelegate;

/**
 * Created by Bharat on 04-May-17.
 */
public class PhoneNumberTextWatcherDefault implements TextWatcher {

    private static final String TAG = "PhoneNumberTextWatcher";
    private EditText editText;

    public PhoneNumberTextWatcherDefault(EditText edTxtPhone) {
        this.editText = edTxtPhone;
    }

    public void onTextChanged(CharSequence s, int cursorPosition, int before,
                              int count) {

        if (before == 0 && count == 1) {  //Entering values
            String val = s.toString();
            String a = "";
            String b = "";
            String c = "";
            if (val != null && val.length() > 0) {
                val = val.replace(" ", "");
                if (val.length() >= 1) {
                    a = val.substring(0, 1);
                } else if (val.length() < 4) {
                    a = val.substring(0, val.length());
                }
                if (val.length() < 4) {
                    b = val.substring(1, val.length());
                } else if (val.length() >= 4) {
                    b = val.substring(1, 4);
                    c = val.substring(4, val.length());
                } else if (val.length() > 1 && val.length() < 4) {
                    b = val.substring(1, val.length());
                }
                AppDelegate.LogT("before == 0 && count == 1 || a => " + a + ", b => " + b + ", c => " + c);
                StringBuffer stringBuffer = new StringBuffer();
                if (a != null && a.length() > 0) {
                    stringBuffer.append(a);

                }
                if (b != null && b.length() > 0) {
                    stringBuffer.append(" ");
                    stringBuffer.append(b);

                }
                if (c != null && c.length() > 0) {
                    stringBuffer.append(" ");
                    stringBuffer.append(c);
                }
                editText.removeTextChangedListener(this);
                editText.setText(stringBuffer.toString());
                if (cursorPosition == 1 || cursorPosition == 5) {
                    cursorPosition = cursorPosition + 2;
                } else {
                    cursorPosition = cursorPosition + 1;
                }
                if (cursorPosition <= editText.getText().toString().length()) {
                    editText.setSelection(cursorPosition);
                } else {
                    editText.setSelection(editText.getText().toString().length());
                }
                editText.addTextChangedListener(this);
            } else {
                editText.removeTextChangedListener(this);
                editText.setText("");
                editText.addTextChangedListener(this);
            }

        }

        if (before == 1 && count == 0) {  //Deleting values

            String val = s.toString();
            String a = "";
            String b = "";
            String c = "";

            if (val != null && val.length() > 0) {
                val = val.replace(" ", "");
                if (cursorPosition == 1) {
                    val = removeCharAt(val, cursorPosition - 1, s.toString().length() - 1);
                } else if (cursorPosition == 5) {
                    val = removeCharAt(val, cursorPosition - 2, s.toString().length() - 2);
                }
                if (val.length() >= 1) {
                    a = val.substring(0, 1);
                } else if (val.length() < 1) {
                    a = val.substring(0, val.length());
                }
                if (val.length() >= 4) {
                    b = val.substring(1, 4);
                    c = val.substring(4, val.length());
                } else if (val.length() > 1 && val.length() < 4) {
                    b = val.substring(1, val.length());
                }
                StringBuffer stringBuffer = new StringBuffer();
                if (a != null && a.length() > 0) {
                    stringBuffer.append(a);

                }
                if (b != null && b.length() > 0) {
                    stringBuffer.append(" ");
                    stringBuffer.append(b);
                }
                if (c != null && c.length() > 0) {
                    stringBuffer.append(" ");
                    stringBuffer.append(c);
                }
                AppDelegate.LogT("before == 1 && count == 0 || a => " + a + ", b => " + b + ", c => " + c);
                editText.removeTextChangedListener(this);
                editText.setText(stringBuffer.toString());
                if (cursorPosition == 1 || cursorPosition == 5) {
                    cursorPosition = cursorPosition - 1;
                }
                if (cursorPosition <= editText.getText().toString().length()) {
                    editText.setSelection(cursorPosition);
                } else {
                    editText.setSelection(editText.getText().toString().length());
                }
                editText.addTextChangedListener(this);
            } else {
                editText.removeTextChangedListener(this);
                editText.setText("");
                editText.addTextChangedListener(this);
            }

        }


    }

    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
    }

    public void afterTextChanged(Editable s) {


    }

    public static String removeCharAt(String s, int pos, int length) {

        String value = "";
        if (length > pos) {
            value = s.substring(pos + 1);
        }
        return s.substring(0, pos) + value;
    }
}